function initAutocomplete() {
    var input = document.getElementById('geolocation_search');
    var searchBox = new google.maps.places.SearchBox(input);

    var geocoder;
    var map;

    var defaultLat = document.getElementById('latitude').value;
    var defaultLon = document.getElementById('longitude').value;

    if (!defaultLat || defaultLat == null || defaultLat == 0) {
        defaultLat = -25.274398;
    }
    if (!defaultLon || defaultLon == null || defaultLon == 0) {
        defaultLon = 133.77513599999997;
    }


    geocoder = new google.maps.Geocoder();

    var latlng = new google.maps.LatLng(defaultLat, defaultLon);

    var typeId = google.maps.MapTypeId.ROADMAP;

    //       var typeId = google.maps.MapTypeId.SATELLITE;

    var mapOptions = {
        zoom: 10,
        center: latlng,
        mapTypeId: typeId,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        //mapTypeId: google.maps.MapTypeId.SATELLITE
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: latlng
    });

    google.maps.event.addListener(marker, 'dragend', function (event) {

        latitude = event.latLng.lat();
        longitude = event.latLng.lng();

        jQuery('#latitude').val(latitude);
        jQuery('#longitude').val(longitude);

        var draglatlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
        geocoder.geocode({'latLng': draglatlng}, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var add = data[1].formatted_address; //this is the full address
                jQuery('#geolocation_search').val(add);
            } else {
                alert('Geocode was not successful for finding address for following reason: ' + status);
            }
        });
    });

    searchBox.addListener('places_changed', function(event) {
        var places = searchBox.getPlaces();
        console.log(places);
        longitude = places[0].geometry.location.lng();
        latitude = places[0].geometry.location.lat();

        map.setCenter(places[0].geometry.location);
        marker.setPosition(places[0].geometry.location);

        $(".search_longitude").val(longitude);
        $(".search_latitude").val(latitude);
    });
}
