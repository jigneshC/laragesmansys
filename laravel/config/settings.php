<?php

return [

    'ticket_editable_time_limit' => 60, // minuts
    'DEFAULT_LANG' => env('DEFAULT_LANG', 'en'),
    'min_balance_cut_up_to' => -100, // Website balance gose to minus minimum to 
    'notification_at_low_min_balance' => 100,
    'notification_at_balance_about_to_expire_in_day' => 5,
    'MASTER_ADMIN' => env('MASTER_ADMIN','jignesh.citrusbug@gmail.com'),
	'GOOGLE_MAP_API' => env('GOOGLE_MAP_API', 'AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U'),
	'WEB_URL' => env('WEB_URL', 'https://admin.gesmansys.com'),
	'PER_PAGE_DATA' => 10,
    
    'dnsmadeeasy' =>[
        'API_KEY'=>'5562f5b0-78a6-4cb2-b781-4c61f1f3306b',
        'SECREAT_KEY'=>'a6f77dc5-c0de-4810-a403-0de261e7cbed',
        'ROOT_DOMAIN'=>[
            'name'=>env('ROOT_DOMAIN_NAME', 'gesmansys.com'),
            'ttl'=>env('ROOT_DOMAIN_TTL', '1800'),
            'id'=>'5885470'
        ],
		'ALLOW_DOMAIN_API'=>env('ALLOW_DOMAIN_API',1)
    ],
    'package_class'=>[
        'STARTER'=>'blue-background',
        'BASIC'=>'orange-background',
        'PRO'=>'red-background',
        'ULTIMATE'=>'purple-background'
    ],
    'currency'=>[
        'USD'=>[
            'symbole'=>'$',
            'display_name'=>"USD"
        ],
        'EUR'=>[
            'symbole'=>'€',
            'display_name'=>"EURO"
        ],
    ],
    'unit_charge_route' =>[
        
        'users'=>["users.create","users.store"],
        //'websites'=>["websites.create","websites.store"],
        'companies'=>["companies.create","companies.store"],
        'sites'=>["sites.create","sites.store"],
        'services'=>["services.create","services.store"],
        'audits'=>["audits.create","audits.store"],
        'buildings'=>["buildings.create","buildings.store"],
        'subjects'=>["subjects.create","subjects.store"],
        'tickets'=>["tickets.create","tickets.store"],
     //   'duty'=>["duty.store"],
     //   'planned-activity'=>["planned-activity.create","planned-activity.store"],
        
        
        'users_module'=>["users.index"],
        'websites_module'=>["websites.index"],
        'companies_module'=>["companies.index"],
        'sites_module'=>["sites.index"],
        'services_module'=>["services.index"],
        'audits_module'=>["subjects.index"],
        'buildings_module'=>["buildings.index"],
        'subjects_module'=>["subjects.index"],
        'tickets_module'=>["tickets.index"],
        'duty_module'=>["duty.index"],
        'plannedactivity_module'=>["planned-activity.index"],
		'boatingharbour_module'=>["boating-harbour.index"],
        
    ],
    'free_license' =>[
        
        'users'=>2,
        'websites'=>0,
        'companies'=>1,
        'sites'=>1,
        'services'=>1,
        'audits'=>1,
        'buildings'=>1,
        'subjects'=>10,
        'tickets'=>1,
        'duty'=>1,
        'planned-activity'=>1,
        
    ],
    'qrcode'=>[
        'domain_URL'=>"https://qrcodelibrary.info",
        'WEB_URL'=>"https://qrcodelibrary.info/api/v1.0",
        "module"=>["users","companies","sites","services","buildings","subjects","_websites"]
    ],
	
	'twilio' => [
        "SID" => "ACaa7c16d7441cf1c84aa8497cc110a01a",
		"TOKEN"=> "a4af18bfd778be286e10fe5efe4c5e61",
		"FROM" => "+61488853123"
    ]
];
