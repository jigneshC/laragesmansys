<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'v1.0/ticket', 'middleware' => 'api_pass', 'namespace' => 'Api'], function () {
    //
    Route::get('/get-subjects', 'TicketController@getSubjects');
    Route::get('/get-geolocation', 'TicketController@getGeolocation');
	Route::get('/get-sites', 'TicketController@getSites');
    Route::get('/get-subject/{subject_id}', 'TicketController@getSubject');
    //
    Route::get('/get-tickets', 'TicketController@getTickets');
    Route::get('/get-ticket/{ticket_id}', 'TicketController@getTicket');
//    //
    Route::post('/create-ticket', 'TicketController@postTicket');
    Route::put('/edit-ticket/{ticket_id}', 'TicketController@putTicket');
    //
    Route::delete('/delete-ticket/{ticket_id}', 'TicketController@deleteTicket');


});

Route::group(['prefix' => 'v1.0', 'namespace' => 'Api'], function () {
    Route::post('login', 'v1\UserController@UserLogin');
    Route::get('login-with-qrcode', 'v1\UserController@qrcodeLogin');
    Route::post('login-with-qrcode', 'v1\UserController@qrcodeLoginSubmit');
	
    Route::post('forgot-password-request', 'v1\UserController@forgotpassword');
    Route::post('forgot-password-submit', 'v1\UserController@forgotpasswordSubmit');
    
	
});

Route::group(['prefix' => 'v1.0', 'namespace' => 'Api', 'middleware' => 'api_pass'], function () {
    Route::post('get-profile', 'v1\UserController@getProfile');
	
    Route::post('logout', 'v1\UserController@logout');
	Route::post('change-password', 'v1\UserController@changePassword');
	
	Route::post('scan-qrcode', 'v1\QrcodeController@scanQrcode');
	Route::get('get-sub-domains', 'v1\DomainController@getDomain');
	Route::get('get-sites', 'v1\SiteController@getSites');
	Route::get('get-subjects', 'v1\SubjectsController@getSubjects');
	
	Route::get('get-tickets', 'v1\TicketController@getTickets');
	Route::get('get-ticket', 'v1\TicketController@getTicket');
	Route::post('create-ticket', 'v1\TicketController@createTicket');
	Route::post('create-ticket-action', 'v1\TicketController@createTicketAction');
	
    
});


