<?php

Route::get('tickets-acknowledged/{id}', 'HomeController@ticketack');

Route::get('php-info', 'HomeController@phpinfo');

Route::post('sign-in', 'Auth\LoginController@loginA');
Route::get('login/{qrcode}', 'Auth\LoginController@loginQrcodeCheck');
Route::Post('login-with-otp', 'Auth\LoginController@sendOtp');
Route::Post('do-login-otp', 'Auth\LoginController@doLoginOtp');

Auth::routes();

//its worked use
//Route::group(array('domain' => '{account}.gesmansys.com'), function () {
//
//    Route::get('/', 'HomeController@subDomain');
//
//});


Route::post('language/', 'LanguageController@changeByForm')->name('lang.post');
Route::get('language/{lang}', 'LanguageController@change')->name('lang.change');


Route::group(['middleware' => ['auth', 'admin','is_enough_credit','session_notification']], function () {

    Route::get('/', 'HomeController@redirect');


//    Route::group(['prefix' => 'admin', 'middleware' => 'roles', 'roles' => 'SU'], function () {
    Route::group(['prefix' => 'admin','middleware' => ['web_role_check']], function () {
		
		Route::get('/reset-folder-structure', 'Admin\AdminController@resetFolder')->name('tickets.resetfile');
		Route::get('/servece-to-manysite', 'Admin\AdminController@servecetoManysite')->name('tickets.stom');
		
		Route::post('recover-item', 'Admin\AdminController@recoverItem')->name('recover.item');

        Route::get('/', 'Admin\TicketsController@index')->name('tickets.index');
		

        Route::get('/create', 'WizardController@index');
        Route::post('/create', 'WizardController@storeCompany');
        //
        Route::get('/create/site', 'WizardController@site');
        Route::post('/create/site', 'WizardController@storeSite');
        //
        Route::get('/create/building', 'WizardController@building');
        Route::post('/create/building', 'WizardController@storeBuilding');
        Route::get('/create/success', 'WizardController@success');

		
		
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');

        Route::get('roles/datatable', 'Admin\RolesController@datatable');
        Route::resource('/roles', 'Admin\RolesController');
        
        Route::get('activityoptionfilterbywebsite', 'Admin\MasterTicketController@filter_by_website');
        Route::get('planned-activity/datatable', 'Admin\MasterTicketController@ticketDatatable');
        Route::resource('/planned-activity', 'Admin\MasterTicketController');

        
        Route::get('useroptionfilterbywebsite', 'Admin\UsersController@filter_by_website');
        Route::get('/users/search', 'Admin\UsersController@search');
        Route::get('/users/datatable', 'Admin\UsersController@userDatatable');
        Route::resource('/users', 'Admin\UsersController');


        Route::get('/companies/search', 'Admin\\CompaniesController@search');
        Route::get('companies/datatable', 'Admin\CompaniesController@datatable');
        Route::resource('/companies', 'Admin\\CompaniesController');
        
        Route::get('company-modules/{id}', 'Admin\CompaniesController@chargableModule');
        Route::post('company-modules/{id}', 'Admin\CompaniesController@assignChargableModule');
		
		Route::get('company-users/{id}', 'Admin\CompaniesController@changeCompanyUserDomain');
		Route::post('company-users/{id}', 'Admin\CompaniesController@storeCompanyUserDomain');


        Route::get('/sites/search', 'Admin\\SitesController@search');
        Route::get('sites/datatable', 'Admin\SitesController@datatable');
        Route::get('siteoptionfilterbywebsite', 'Admin\SitesController@filter_by_website');
        Route::resource('/sites', 'Admin\\SitesController');

        Route::get('/services/search', 'Admin\\ServicesController@search');
        Route::get('servicesoptionfilterbywebsite', 'Admin\ServicesController@filter_by_website');
        Route::get('services/datatable', 'Admin\ServicesController@datatable');

        Route::resource('/services', 'Admin\\ServicesController');


        Route::get('buildings/datatable', 'Admin\BuildingsController@datatable');
        Route::get('buildingoptionfilterbywebsite', 'Admin\BuildingsController@filter_by_website');
        Route::resource('/buildings', 'Admin\\BuildingsController');

        //--profile

        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        //
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');


        //-- company
        Route::get('/company', 'Admin\CompanyController@index')->name('company.index');
        Route::get('/company/edit', 'Admin\CompanyController@edit')->name('company.edit');




        Route::get('subjects-type', 'Admin\SubjectsController@getSubjectType');
        Route::get('subjects/get-caller/{subject_id}', 'Admin\SubjectsController@getSubjectCaller');
        Route::get('/subjects/search', 'Admin\\SubjectsController@search');
        Route::get('subjects/datatable', 'Admin\SubjectsController@datatable');
        Route::get('subjectoptionfilterbywebsite', 'Admin\SubjectsController@filter_by_website');
        Route::resource('/subjects', 'Admin\\SubjectsController');

        Route::get('subjects/get-view/{subject_id}', 'Admin\SubjectsController@getFileView');
        Route::post('subjects/get-caller', 'Admin\SubjectsController@getCaller');

        Route::post('/tickets/assignuser', 'Admin\TicketsController@assignUser');
        Route::post('/tickets/comment', 'Admin\\TicketsController@postComment');
        Route::post('/tickets/action', 'Admin\\TicketsController@postAction');
        Route::get('tickets/datatable', 'Admin\TicketsController@ticketDatatable');
		Route::get('tickets/datatable/{id}', 'Admin\TicketsController@commentDatatable');
        Route::get('tickets/pdf-generate', 'Admin\TicketsController@generatePdf');

        Route::get('ticketsoptionfilterbywebsite', 'Admin\TicketsController@filter_by_website');

        Route::resource('/tickets', 'Admin\\TicketsController');

        Route::post('duty/closeticket', 'Admin\DutyController@closeticket');
        Route::post('duty/addsubject', 'Admin\DutyController@storeSubject');
        Route::get('duty/datatable', 'Admin\DutyController@dutyDatatable');
        Route::resource('duty', 'Admin\DutyController');

        Route::get('api-access', 'Admin\ApiAccessController@index');
        Route::post('api-access', 'Admin\ApiAccessController@postPass');
        Route::post('api-access/regen', 'Admin\ApiAccessController@regenerateToken');


        Route::group(['middleware' => ['is_master_website']], function () {

            Route::resource('/permissions', 'Admin\PermissionsController');

            Route::get('equipments/datatable', 'Admin\EquipmentsController@datatable');
            Route::resource('equipments', 'Admin\\EquipmentsController');

            Route::get('languages/datatable', 'Admin\LanguagesController@datatable');
            Route::resource('/languages', 'Admin\\LanguagesController');

            Route::get('settings/datatable', 'Admin\SettingsController@datatable');
            Route::resource('/settings', 'Admin\\SettingsController');

            Route::get('audits/datatable', 'Admin\AuditController@datatable');
            Route::resource('/audits', 'Admin\\AuditController');

            Route::get('dropdowns-types/datatable', 'Admin\DropdownsTypesController@datatable');
            Route::resource('/dropdowns-types', 'Admin\\DropdownsTypesController');

            Route::get('dropdown-values/datatable', 'Admin\DropdownValuesController@datatable');
            Route::resource('/dropdown-values', 'Admin\\DropdownValuesController');

            Route::get('/dropdown-values/{id}/add-languages', 'Admin\DropdownValuesController@languageForm');
            Route::post('/dropdown-values/{id}/add-languages', 'Admin\DropdownValuesController@languageStore');

            Route::get('/translate/{to}', 'Admin\AdminController@getLangTranslation');

            Route::post('websites/credit-units', 'Admin\WebsitesController@creditUnits');
            Route::get('website/datatable', 'Admin\WebsitesController@datatable');
            Route::resource('/websites', 'Admin\\WebsitesController');

            Route::get('module-units/datatable', 'Admin\UnitsModuleController@datatable');
            Route::resource('/module-units', 'Admin\\UnitsModuleController');

            Route::get('units-packages/datatable', 'Admin\UnitsPackageController@datatable');
            Route::resource('/units-packages', 'Admin\\UnitsPackageController');

            
            Route::resource('/qrcode', 'Admin\\QrcodeController');

            

            Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
            Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

        });

            Route::get('domain-unit', 'Admin\UnitsOrderController@pricing');
            Route::get('order-payment/{order_id}', 'Admin\UnitsOrderController@payment');
            Route::get('order-payment-success', 'Admin\UnitsOrderController@paymentSuccess');
            Route::get('order-payment-cancel', 'Admin\UnitsOrderController@paymentCancel');
            Route::get('order-confirm/{package_id}', 'Admin\UnitsOrderController@confirmOrder');
            Route::resource('order', 'Admin\\UnitsOrderController');

            Route::get('unit-history/datatable', 'Admin\UnitsTransactionController@datatable');
            Route::resource('unit-history', 'Admin\\UnitsTransactionController');
            
            Route::get('statistics', 'Admin\AdminController@statistics');
            Route::get('export-data', 'Admin\AdminController@exportData');

    });

});

Route::get('/insert-db', function()
{
	
   Artisan::call('Insertdatabase:insert_db');

});



Route::get('/generate-planned-tickets', function()
{
			/*	\Mail::raw("this is me", function ($message) {
  $message->to("jignesh.citrusbug@gmail.com")
    ->subject("Call test");
}); */

    Artisan::queue('generate:plannedtickets');
    Artisan::queue('dailyunit:charge');
	Artisan::queue('cron:run');
    
});

Route::get('/cron/run', function()
{
    //Artisan::queue('cron:run');
	Artisan::call('cron:run');
});

Route::get('/daily-unit-charge', function()
{
    Artisan::queue('dailyunit:charge');
});
