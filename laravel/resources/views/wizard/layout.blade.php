@extends('layouts.apex')

@section('title','Create Site')


@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> Create Site </h4>
       {{-- @include('partials.page_tooltip',['model' => 'companies','page'=>'form']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
       
        <div class="card-body">
          <div class="card-block">
          
				<div class="site-tab-container clearfix">
               <ul class="nav nav-tabs">
                <li class="nav-item ">
                    @if($m->c_ac && $m->c_ac=='active')
                     <a href="{!! url('admin/create') !!}" class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" title="Company" href="#company" aria-expanded="true" >
                         <i class="fa fa-building"></i> <span>@lang('company.label.company')</span>
                     </a>
                    @else
                    <a href="{!! url('admin/create') !!}" class="nav-link" id="baseIcon-tab1" >
                         <i class="fa fa-building"></i> <span>@lang('company.label.company')</span>
                     </a>
                    @endif
                </li>
                <li class="nav-item">
                    @if($m->s_ac && $m->s_ac=='active') 
                    <a href="{!! url('admin/create/site') !!}" class="nav-link active" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" title="Sites" href="#sites" aria-expanded="false">
                        <i class="fa fa-globe "></i> <span>@lang('site.label.sites')</span>
                    </a>
                    @else
                    <a href="{!! url('admin/create/site') !!}" class="nav-link " id="baseIcon-tab2"  >
                        <i class="fa fa-globe "></i> <span>@lang('site.label.sites')</span>
                    </a>
                    @endif
                </li>
                <li class="nav-item">
                    @if($m->b_ac && $m->b_ac=='active')
                    <a href="{!! url('admin/create/building') !!}" class="nav-link active" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" title="Building" href="#building" aria-expanded="false">
                        <i class="fa fa-box"></i> <span>Building</span>
                    </a>
                    @else
                    <a href="{!! url('admin/create/building') !!}" class="nav-link" id="baseIcon-tab2" >
                        <i class="fa fa-box"></i> <span>Building</span>
                    </a>
                    @endif
                    
                </li>
              </ul>
			  </div>
             
            
            
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="company" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @include('partials.form_notification')
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                        @yield('wizard_content')
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 

               
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection


