@include('admin.for_master.site_input')


<?php
$btn = function ($field) use ($site) {
    if (!isset($site->{$field}) && $site->{$field} == '') {
        return '';
    }
    return '
<button type="button" class="cp btn btn-default btn-xs pull-right" data-val="' . $site->{$field} . ' ">Copy from Site
</button>';
}
?>


<div class="form-group row {{ $errors->has('site_id') ? 'has-error' : ''}}">
    {!! Form::label('site_id', trans('building.label.site'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('site_id',$sites, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('site_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('building.label.name'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

    </div>
</div>
<div class="form-group row {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', trans('building.label.address'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        {!! $btn('address') !!}

    </div>
</div>

<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('building.label.active'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rad1']) !!}<label for="rad1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false, ['id' => 'rad2']) !!} <label for="rad2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>


@push('js')

<script>
    $(document).ready(function () {
        $('.cp').click(function () {
            var val = $(this).data('val');
            console.log(val);

            var ctrl = $(this).prev('.form-control')

            if (ctrl.is('input')) {
                ctrl.val(val);
            }

            if (ctrl.is('select')) {
                ctrl.val('').val(val).change();
//
//                $(this).prev('select > option[value="' + val + '"]').prop("selected", "true");
////                $(this).prev(".form-control > [value=" + val + "]").attr("selected", "true");
            }
        })
    });
</script>

@endpush
