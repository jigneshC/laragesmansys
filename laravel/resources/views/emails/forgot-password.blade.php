@extends('emails.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

    <p>
        
        @lang('mail.slug.you_have_request_to_forgot_password',['app'=>\config('settings.APP_NAME')],'en')
		<br/><br/>
		
		@lang('mail.slug.your_otp_to_reset_password_is',[],'en') : <b> {{$otpuser->otp}} </b>
		
        <br/>
		@lang('mail.slug.otp_will_be_expire_within',['minuts'=>$otp_exp_minuts],'en')
		<br/>
		
        
		@lang('mail.slug.if_you_did_not_request',[],'en')
		
		<br/>
    </p>
    
    <hr>
    
@endsection