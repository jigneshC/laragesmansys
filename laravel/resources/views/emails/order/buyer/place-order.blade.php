<!DOCTYPE html>
<html>
	@include ('emails.include.header', ['lang' => $lang])
	<body style="margin:30px auto; ">
		<div style="background:#000; padding:20px; text-align:center;">
			<img class="brand_icon" src="{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}" alt="logo"/>
		</div>
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			@if($status =="completed")
				<h2>@lang('unitorder.mail_admin.order_place_success_headerline1',[],$lang)</h2>
			@elseif($order->order_status =="cancelled")
				<h2>@lang('unitorder.mail_admin.order_place_canceled_headerline1',[],$lang)</h2>
			@else
				<h2 >@lang('unitorder.mail_admin.order_place_failed_headerline1',[],$lang)</h2>
			@endif
			
			@include ('emails.include.order-table', ['lang' => $lang])
			
			<h5 style="text-align: center;margin-top: 55px;font-size: 15px;color: #555;">
			</h5>
		</div>
		@include ('emails.include.footer',['lang' => $lang])
	</body>
</html>
