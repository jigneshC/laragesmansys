@extends('emails.mailtemplate.cit')

@section('body')
    @if(isset($user))
        <h2 class="title">Hi {{$user->full_name}}</h2>
    @endif

    <p>
        
        @lang('auth.here_is_otp_to_login',["otp"=>$otpuser->otp],$lang)
		<br/>
    </p>
    
    <hr>
    
@endsection

