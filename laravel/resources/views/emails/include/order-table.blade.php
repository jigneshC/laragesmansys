			<table>
			  <tr>
				<th>Order Id</th>
				<th>@lang('unitpackage.label.name',[],$lang)</th>
				<th>@lang('unitpackage.label.desc',[],$lang)</th>
				<th>@lang('unitpackage.label.unit',[],$lang)</th>
				<th>@lang('unitpackage.label.price',[],$lang)</th>
				
				
				<th>@lang('unitorder.label.item_qty',[],$lang)</th>
				<th>@lang('unitorder.label.price_total',[],$lang)</th>
				
				<th>@lang('unitorder.label.discount',[],$lang)</th>
				<th>@lang('unitorder.label.net_payable_amount',[],$lang)</th>
				<th>@lang('unitorder.label.orderstatus',[],$lang)</th>
				
              </tr>
			  <tr>
				<td>#{{ $order->id}}</td>
				<td>{{  $package->name}}</td>
				<td>{{  $package->desc}}</td>
				<td>{{  $package->unit}}</td>
				<td>{{\config('settings.currency')[$order->price_currency]['symbole']}} {{  $order->item_price}}</td>
				
				<td>{{  $order->item_qty}}</td>
				<td>{{\config('settings.currency')[$order->price_currency]['symbole']}} {{  $order->item_price * $order->item_qty}}</td>
				
				<td>{{  $order->discount}} %</td>
				<td>{{\config('settings.currency')[$order->price_currency]['symbole']}} {{  $order->order_total}}</td>
				<td>{{  $order->payment_status}}</td>
				
			  </tr>
			</table>
			
			
			<h4 style="margin-top: 35px;">@lang('unitorder.label.buyer_detail',array(),$lang)</h4>
			<table class="buyer_detail">
			  <tr>
				<th>@lang('website.label.website',array(),$lang)</th>
				<td>{{$website->domain}}</td>
			  </tr>
			  <tr style="background-color:#f2f2f2">
				<th>@lang('unitorder.label.buyer_name',array(),$lang)</th>
				<td>{{$order->buyer_name}}</th>
			  </tr>
			  <tr>
				<th>@lang('unitorder.label.buyer_email',array(),$lang)</th>
				<td>{{$order->buyer_email}}</td>
			  </tr>
			  <tr>
				<th>@lang('unitorder.label.buyer_contact',array(),$lang)</th>
				<td>{{$order->buyer_contact}}</td>
			  </tr>
			  
			</table>
			
			<h4 style="margin-top: 35px;">@lang('unitorder.label.payment_detail',array(),$lang)</h4>
			<table class="buyer_detail">
			  <tr>
				<th>@lang('unitorder.label.payment_status',array(),$lang)</th>
				<td>{{$order->payment_status}}</td>
			  </tr>
			  <tr style="background-color:#f2f2f2">
				<th>@lang('unitorder.label.payment_type',array(),$lang)</th>
				<td>{{$order->payment_type}}</th>
			  </tr>
			  <tr>
				<th>@lang('unitorder.label.invoicedate',array(),$lang)</th>
				<td>{{$order->created_at->format('Y-m-d')}}</td>
			  </tr>
			  <tr>
				<th>@lang('unitorder.label.reference_id',array(),$lang)</th>
				<td>{{$order->payment_request_id}}</td>
			  </tr>
			  
			</table>
			
		