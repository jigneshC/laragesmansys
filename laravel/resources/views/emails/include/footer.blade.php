    	<div class="footer" style="background:#333333; color:#ffffff; padding:10px;">
			<?php $today = getdate(); ?>
			<p>Copyright &copy; {{$today['year']}} {{ config('app.name') }} All rights reserved</p>
			<a href="{{url('/')}}">{{ config('app.name') }}</a>
		</div>
	