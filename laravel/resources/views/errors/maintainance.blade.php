@extends('layouts.apex-auth')

@section('title',trans('comman.responce_msg.page_not_found') )

@section('content')

@push('css')
<style>
.main-panel{
	margin-top: 0px !important;
}
</style>
@endpush
<section id="error">
    <div class="container-fluid bg-grey bg-lighten-3">
        <div class="container">
            <div class="row full-height-vh">
                
                <div class="col-md-12 col-lg-12 d-flex align-items-center justify-content-center">
                    <div class="error-container">
                        <div class="no-border">
                           
                        </div>
                        <div class="error-body">
                           <div class="row text-center mb-3">
                        <div class="col-12">
                            <img src="{{ asset('assets/images/logo.png') }}" alt="User" width="300">
                        </div>
                        <div class="col-12">
                            <h3>Under Maintenance</h3>
                        </div>
                    </div>
                            <div class="row py-2 justify-content-center">
								<b>@lang('comman.responce_msg.sorry_maintainance') </b>
                                <div class="col-8">
                                   
                                </div>
                            </div>
                        </div>
                        <div class="error-footer bg-transparent">
                            <div class="row">
                                <p class="text-muted text-center col-12 py-1"> <?php $today = getdate(); ?>
        &copy; <a href="#" target="_blank"> {{ config('app.name') }} {{$today['year']}} </a>@lang('comman.label.all_rights')</p>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
