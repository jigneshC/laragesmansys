@if(1)
@php( $notifications_unread = auth()->user()->unreadNotifications )
@php( $notifications_read = auth()->user()->readNotifications )

@if($notifications_unread->count()>0 || $notifications_read->count()>0 )


                <li class="dropdown nav-item">
                    <a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle" aria-expanded="false">
                        <i class="ft-bell font-medium-3 blue-grey darken-4"></i>
                        @if($notifications_unread->count()>0) <span class="notification badge badge-pill badge-danger">{{ $notifications_unread->count() }}</span> @endif
                        <p class="d-none">Notifications</p>
                    </a>
                  <div class="notification-dropdown dropdown-menu dropdown-menu-right">
                    <div class="noti-list ps-container ps-theme-default" data-ps-id="74c1c601-9fc1-3475-ee80-f992ae07a6d8">
                    @php( $max_notification=10)
                    @php( $cntn = 0)    
                        
                        @foreach($notifications_unread as $notification)
                            @if($cntn<$max_notification )
                                @php($ticket = $notification->data['ticket'])
							<?php //echo "----------------<pre>"; print_r($ticket); ?>
                                @php($actioner = $notification->data['actioner'])
								@php( $nurl = "" )
                                @if($ticket['_website_id'] == _WEBSITE_ID)
									@php( $nurl = url('admin/tickets/'.$notification->data['ticket_id'])."?v_notification=".$notification->id )
								@else
									@php( $web = \App\WebSite::where("id",$ticket['_website_id'])->first() )
									@if($web)
									@php( $nurl = "//".$web->domain.'/admin/tickets/'.$notification->data['ticket_id']."?v_notification=".$notification->id )
									@else
									@php( $nurl = "" )
									@endif
								@endif
								
								 @if($nurl != "")
                                <a href="{{ $nurl }}" class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4">
                                    <i class="ft-bell info float-left d-block font-large-1 mt-1 mr-2"></i>
                                    <span class="noti-wrapper">
                                        <span class="noti-title line-height-1 d-block text-bold-400 info"> @lang('ticket.notification.actioner_has_assing_you_ticket',['actioner_name'=>$actioner['name']])</span>
                                        <span class="noti-text"> </span>
                                        <small class='text-muted' style="float: right">{{$notification->created_at->diffForHumans()}}</small>
                                    </span>
                                </a>
								@php( $cntn++)    
								 @endif  
                            @endif
                         @endforeach
                        
                         @foreach($notifications_read as $notification)
                            @if($cntn<$max_notification )
                                @php($ticket = $notification->data['ticket'])
							<?php //echo "<pre>"; print_r($ticket); ?>
                                @php($actioner = $notification->data['actioner'])
                                @php( $nurl = "" )
                                @if($ticket['_website_id'] == _WEBSITE_ID)
									@php( $nurl = url('admin/tickets/'.$notification->data['ticket_id'])."?v_notification=".$notification->id )
								@else
									@php( $web = \App\WebSite::where("id",$ticket['_website_id'])->first() )
									@if($web)
									@php( $nurl = "//".$web->domain.'/admin/tickets/'.$notification->data['ticket_id']."?v_notification=".$notification->id )
									@else
									@php( $nurl = "" )
									@endif
								@endif
								
								 @if($nurl != "")
                                <a href="{{ $nurl }}" class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4">
                                    <i class="ft-bell success  float-left d-block font-large-1 mt-1 mr-2"></i>
                                    <span class="noti-wrapper">
                                        <span class="noti-title line-height-1 d-block text-bold-400 success"> @lang('ticket.notification.actioner_has_assing_you_ticket',['actioner_name'=>$actioner['name']])</span>
                                        <span class="noti-text"> </span>
                                        <small class='text-muted' style="float: right">{{$notification->created_at->diffForHumans()}}</small>
                                    </span>
                                </a>
								@php( $cntn++)    
								@endif
                            @endif
                         @endforeach
                         
                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                
                        </div>
                        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;">
                                
                            </div>
                                
                        </div>
                            
                    </div>
                      <a href="{{url('admin/tickets')}}?v_notification=all" class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1">
                          Read All Notifications
                      </a>
                  </div>
                </li>

@endif
@endif


                