<nav class="navbar navbar-expand-lg navbar-light bg-faded">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span><span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
			
				@if(request()->is('admin/tickets') || request()->is('admin'))       
               
				
				
				<div class="form-group btn-list" role="group" aria-label="Basic example" style="padding-top:20px;">
							<input type="hidden" value="" id="fiter_by_type">
						
							<button type="button" data-val="ticket" class="type_filter btn btn-raised btn-secondary  btn-min-width mr-1 mb-1 ticket-color">
								<span id="type_ticket"> {{$type_count['ticket']}} </span> @lang('subject.subject_type.ticket')
							</button>
							<button type="button" data-val="alarm" class="type_filter btn btn-raised btn-secondary  btn-min-width mr-1 mb-1 alarm-color">
								<span id="type_ticket"> {{$type_count['alarm']}} </span> @lang('subject.subject_type.alarm')
							</button>
							<button type="button" data-val="event" class="type_filter btn btn-raised btn-secondary  btn-min-width mr-1 mb-1 event-color">
								<span id="type_ticket"> {{$type_count['event']}} </span> @lang('subject.subject_type.event')
							</button>
							<button type="button" data-val="duty" class="type_filter btn btn-raised btn-secondary  btn-min-width mr-1 mb-1 duty-color">
								<span id="type_ticket"> {{$type_count['duty']}} </span> @lang('subject.subject_type.duty')
							</button>
							<button type="button" data-val="planned_activity" class="type_filter btn btn-raised btn-secondary  btn-min-width mr-1 mb-1 planned-color">
								<span id="type_ticket"> {{$type_count['planned_activity']}} </span> @lang('subject.subject_type.planned_activity')
							</button>
							
							
							
							
							
							<button type="button" data-val="" class="type_filter btn btn-raised btn-success  btn-min-width mr-1 mb-1 btn-right ">
								<span id="type_ticket"> {{$type_count['all']}} </span> ALL
							</button>
							<button type="button" data-assgin="0" class="assign_tag  btn btn-success  btn-min-width mr-1 mb-1 btn-right ">
								<span id="type_ticket"> {{$type_count['unassign']}} </span> @lang('comman.label.unassigned_tickets')
							</button>
							
							<button type="button" data-assgin="1" class="assign_tag btn btn-success  btn-min-width mr-1 mb-1 ">
								<span id="type_ticket"> {{$type_count['my']}} </span> @lang('comman.label.my_works')
							</button>
							
							
							<button type="button" data-assgin="2" class="assign_tag btn btn-success  btn-min-width mr-1 mb-1 ">
							<span id="type_ticket"> {{$type_count['my_created']}} </span> @lang('comman.label.my_tickets')
						   </button>
							
							
                                       
                        </div>
				@endif		
              <ul class="navbar-nav">
				<li class="dropdown nav-item">
                    <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle">
                        <i class="ft-flag font-medium-3 blue-grey darken-4"></i>
                        <span class="selected-language d-none"></span>
                    </a>
                  <div class="dropdown-menu dropdown-menu-right">
                      <a href="{{request()->fullUrlWithQuery(['lang'=>'en'])}}" class="dropdown-item py-1">
                          <span> English</span>
                      </a>
                      <a href="{{request()->fullUrlWithQuery(['lang'=>'fr'])}}" class="dropdown-item">
                          <span> French</span>
                      </a>
                  </div>
                </li>
                @include('apex.include.notifications')
                <li class="dropdown nav-item">
                    <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle" aria-expanded="false">
                        @if(isset(Auth::user()->people->photo))
                        <img class="img-circle" width="23" height="23" alt="{!! Auth::user()->name !!}"
                             src="{!! asset('uploads/'.Auth::user()->people->photo) !!}" style="vertical-align: top;"/>
                        @else 
                        <i class="ft-user font-medium-3 blue-grey darken-4"></i>
                        @endif
                        <p class="d-none">User Settings</p>
                    </a>
                  <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                      <a href="{!! url('admin/profile') !!}" class="dropdown-item py-1">
                          <i class="ft-edit mr-2"></i>
                          <span>Profile</span>
                      </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="ft-power mr-2"></i>
                        <span>Logout</span>
                    </a>
                    
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>