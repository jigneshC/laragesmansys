<!-- Theme customizer Starts-->
    <div class="customizer border-left-blue-grey border-left-lighten-4 d-none d-sm-none d-md-block"><a class="customizer-close"><i class="ft-x font-medium-3"></i></a><a id="customizer-toggle-icon" class="customizer-toggle bg-danger"><i class="ft-filter font-medium-4  white align-middle"></i></a>
      <div data-ps-id="df6a5ce4-a175-9172-4402-dabd98fc9c0a" class="customizer-content p-3 ps-container ps-theme-dark">
       
        <!-- Sidebar Options Starts-->
        <h6 class="text-center text-bold-500 mb-3 text-uppercase">@lang('ticket.label.advance_search')</h6>
        <div class="cz-bg-color">
          <div class="row p-1">
            @if(_MASTER)
                                       
                <div class="form-group full-width">
                   {!! Form::select('_website_id',$_websites_pluck,(Session::has('filter_website'))? Session::get('filter_website') : null, ['class' => 'filter full-width','id'=>'filter_website_id']) !!}
                </div>
             @endif
                 <div class="form-group clearfix full-width">
				   <input type="text" name="filter_company" class="filter" id="filter_company" style="width: 100%;">
				</div>
				
				<div class="form-group full-width">
				   <input type="text" name="filter_site" class="filter" id="filter_site" style="width: 100%;">
				</div>
				
				<div class="form-group full-width">
                    <input type="text" name="filter_services" class="filter" multiple = true id="filter_services" style="width: 100%;">
                </div>
										  
				<div class="form-group full-width">
                      <input type="text" name="filter_subject" class="filter" id="filter_subject" style="width: 100%;">
                </div>	

				<div class="form-group full-width">
                    <select class="full-width filter" id="filter_status" name="filter">
                      <option value="">@lang('dashboard.label.all_tickets')</option>
                      @foreach(trans('dashboard.status_dropdown') as $k => $val)
                          <option value="{{$k}}" @if(Session::has('filter_status') && Session::get('filter_status') == $k) selected="selected" @endif>{{$val}}</option>
                      @endforeach
                  </select>
                </div>

				<div class="form-group full-width">
                    <div class="ticket-select-process">
                      <div style="width: 100%">
                          <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                              <span></span> <b class="caret"></b>
                          </div>

                      </div>
					</div>
                </div>	
				<div class="form-group full-width">
                       <a href="javascript:void(0);" class="btn btn-success btn-sm btn-filter full-width" id="reset_filter" >
                          <i class="fa fa-undo" ></i> @lang('comman.label.filter_reset')
                      </a>
                </div>
				<div class="form-group full-width">
                     <a href="#" class="btn btn-success btn-sm btn-filter full-width" id="genrate_pdf">
						<i class="fa fa-download" ></i> @lang('comman.label.download_pdf')
					</a>
                </div>
           
          </div>
          
        </div>
        <!-- Sidebar Options Ends-->
       
        <div class="cz-bg-image row">
           
        </div>
        <!-- Sidebar BG Image Ends-->
		
		@if(_MASTER)
			@include("admin.for_master.filter_deleted-right") 
		
			
		@endif
        
       
       
      </div>
    </div>
    <!-- Theme customizer Ends-->