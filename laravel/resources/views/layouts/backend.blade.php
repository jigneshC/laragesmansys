<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Gesmansys' name='description'>
    <link href='{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}' rel='shortcut icon' type='image/x-icon'>

    <link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">

    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all" rel="stylesheet"  type="text/css"/>

    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}" media="all" rel="stylesheet" type="text/css"/>


    <link href="{!! asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / theme file [required] -->
    <link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
    <!-- / coloring file [optional] (if you are going to use custom contrast color) -->
    <link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / demo file [not required!] -->
    <link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/design.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/style.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <link href="{!! asset('assets/stylesheets/ges-fonts.css?family=Montserrat') !!}" media="all" rel="stylesheet" type="text/css"/>    
    <link rel="stylesheet" href="{!! asset('assets/build/css/intlTelInput.css')!!}">

    <style>
        a:hover {
            text-decoration: none;
        }
    </style>
    <style>
        #main-nav .navigation > .nav > li .nav {
            background: #333244 !important;
        }
        #main-nav .navigation > .nav > li .nav > li > a {
            color: #fff;
            font-size: 16px;
            padding-left: 25px;
            border-top: none;
            padding: 0px 0px 0px 25px;
        }
        #main-nav .navigation > .nav > li .nav > li.active > a , #main-nav .navigation > .nav > li .nav > li > a:hover, #main-nav .navigation > .nav > li .nav > li > a:focus {
            background: transparent;
            color: #17A9A8;
        }

        #main-nav .navigation > .nav > li > .nav > li {
            padding: 10px;
        }
    </style>

    @yield('headExtra')

    @stack('css')

    {{-- Data Table--}}
    <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

    <!--[if lt IE 9]>
    <script src="{!! asset('assets/javascripts/ie/html5shiv.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('assets/javascripts/ie/respond.min.js') !!}" type="text/javascript"></script>


    <![endif]-->
</head>
<body class='contrast-red'>
<header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='{!! url('/') !!}'>
            @if(_WEBSITE_LOGO)
                <img height="31" src="{!! asset('uploads/website/'._WEBSITE_LOGO) !!}"/>
            @else
                <img height="31" src="{!! asset('assets/images/logo_lg.png') !!}"/>
            @endif
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>


            {{--@include('partials.language')--}}

            @include('partials.notifications')

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                    @if(isset(Auth::user()->people->photo))
                        <img width="23" height="23" alt="{!! Auth::user()->name !!}"
                             src="{!! asset('uploads/'.Auth::user()->people->photo) !!}"/>
                    @endif
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='{!! url('admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
            </li>
        </ul>
        {{--<form action='search_results.html' class='navbar-form navbar-right hidden-xs' method='get'>--}}
        {{--<button class='btn btn-link icon-search' name='button' type='submit'></button>--}}
        {{--<div class='form-group'>--}}
        {{--<input value="" class="form-control" placeholder="Search..." autocomplete="off" id="q_header" name="q"--}}
        {{--type="text"/>--}}
        {{--</div>--}}
        {{--</form>--}}
    </nav>
</header>
<div id='wrapper'>
    <div id='main-nav-bg'></div>

    @include('partials.sidebar')

    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12 '>
                    <div class="row displaynone_on_print">
                        <div class="col-sm-12">
                            <div class="page-header">
                                @if(View::hasSection('pageHeader'))
                                    @yield('pageHeader')
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    @if (Session::has('session_notification_header1') && Session::get('session_notification_header1') !="")
                        <div class="alert a alert-danger alert-important">
                             <a class="close" data-dismiss="alert" href="#">×</a>
                            {{ Session::get('session_notification_header1') }}
                        </div>
                    @endif
                    
                    @if (Session::has('session_notification_header2') && Session::get('session_notification_header2')!="")
                        <div class="alert  alert-warning alert-important">
                             <a class="close" data-dismiss="alert" href="#">×</a>
                            {{ Session::get('session_notification_header2') }}
                        </div>
                    @endif

                    
                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert"
                                    aria-hidden="true">&times;</button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif

                    @include('flash::message')

                    @yield('content')


                </div>
            </div>
            <footer id='footer'>
                <div class='footer-wrapper'>
                    <div class='row'>
                        <div class='col-sm-6 text'>
                            <?php $today = getdate(); ?>
                            Copyright &copy; {{$today['year']}} {{ config('app.name') }}
                        </div>
                        <div class='col-sm-6 buttons'>

                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>
<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="{!! asset('assets/javascripts/moment.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/moment/fr.js')!!}"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>


<script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"
        type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>




{{-- Data Table--}}
<script src="{!! asset('assets/javascripts/datatable/jquery.dataTables.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/datatable/dataTables.responsive.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/toastr.min.js')!!}"></script>

<script src="{!! asset('assets/javascripts/daterangepicker.js')!!}"></script>
<script src="{!! asset('assets/build/js/intlTelInput.js')!!} " type="text/javascript"></script>


<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>

<script>

    $(document).ready(function () {


        $('#flash-overlay-modal').modal();

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.selectTag').select2({
            tokenSeparators: [",", " "],
        });


        $('.time_pick').datetimepicker({
            format: 'LT'
        });
    });


</script>

@yield('footerExtra')
@stack('js')

<script>
    moment.locale('{{\App::getLocale()}}');
    function getLangFullDate(fulldate,monthtype="short"){
        return moment.utc(fulldate,'YYYY-MM-DD HH:mm:ss').format('MMM Do YYYY, h:mm a');
    }
    function getLangDate(date,monthtype="short"){
        return moment.utc(date,'YYYY-MM-DD').format('MMM Do YYYY');
    }

</script>

<!-- / START - page related files and scripts [optional] -->

<!-- / END - page related files and scripts [optional] -->
</body>
</html>