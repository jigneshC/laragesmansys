@extends('layouts.apex-auth')

@section('title','Register')

@section('content')


<section id="login">
    <div class="container-fluid">
        <div class="row full-height-vh">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="card gradient-indigo-purple text-center width-400">
                    <div class="card-img overlap">
                        <img alt="element 06" class="mb-1" src="{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}" width="190">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <h2 class="white">@lang('comman.label.register')</h2>
                             <form class='validate-form' role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input value="" placeholder="@lang('comman.label.name')" class="form-control" data-rule-required="true"
                                       name="name" type="text" value="{{ old('name') }}"  required autofocus/>
                                        @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input value="" placeholder="@lang('comman.label.email')" class="form-control" data-rule-required="true"
                                       name="email" type="email" value="{{ old('email') }}" required/>
                                        @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input value="" placeholder="@lang('comman.label.password')" class="form-control" data-rule-required="true"
                                       name="password" type="password"/>
                                         @if ($errors->has('password'))
                                         <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                         @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input value="" placeholder="@lang('comman.label.confirm_pasword')" class="form-control" data-rule-required="true"
                                       name="password_confirmation" type="password"/>
                                         @if ($errors->has('password_confirmation'))
                                         <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                         @endif
                                    </div>
                                </div>

                                

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-pink btn-block btn-raised">@lang('comman.label.register')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <a href='{{ route('password.request') }}' class="white txt-decoration">@lang('comman.label.forgot_password')</a>
                         </div>

                        <div class="float-right white">
                             @lang('comman.label.already_account')  
                            <a href="{!! route('login') !!}" class="white txt-decoration">
                                 @lang('comman.label.login_here') !
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection



