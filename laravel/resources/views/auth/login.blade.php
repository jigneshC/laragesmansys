@extends('layouts.apex-auth')

@section('title','Login')

@section('content')


	

<section id="login">
    <div class="container-fluid">
        <div class="row full-height-vh">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="card gradient-indigo-purple text-center width-400">
                    <div class="card-img overlap">
                        <img alt="element 06" class="mb-1" src="{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}" width="190">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <h2 class="white">@lang('comman.label.login')</h2>
                            <form class='validate-form' role="form" method="POST" action="{{ url('sign-in') }}">
                                {{ csrf_field() }}
								<div class="normal-login">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input name="email" type="text" value="{{ old('email') }}" class="form-control"  placeholder="@lang('comman.label.email')"  >
                                        @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input type="password" name="password" value="" placeholder="@lang('comman.label.password')" class="form-control" >
                                         @if ($errors->has('password'))
                                         <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                         @endif
                                    </div>
                                </div>

								{{--
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox check_remember">
                                                <input type="checkbox" name="remember_me" class="custom-control-input "  id="remember_me" value='1' {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label float-left white" for="remember_me">@lang('comman.label.remember_me')</label>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
								
								</div>
								
								<div class="qr-code">
									<div class="form-group">
										<div class="row">
											<div class="col-md-12">
												<video id="preview"></video>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
										<div class="row">
											<div class="col-md-12">
												<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox check_remember">
													<input type="checkbox" name="login_option" class="custom-control-input "  id="login_option" value='1' >
													<label class="custom-control-label float-left white" for="login_option">Login with Qr-code</label>
												</div>
											</div>
										</div>
									</div>
								<p class="help-block text-danger form_error_msg"></p>	
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-pink btn-block btn-raised">@lang('comman.label.signin')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            <a href='{{ route('password.request') }}' class="white txt-decoration">@lang('comman.label.forgot_password')</a>
                         </div>

                        <div class="float-right white">@lang('comman.label.have_account')  
                            <a href="{!! route('register') !!}" class="white txt-decoration">@lang('comman.label.registernow')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection

@push('js')


<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<script type="text/javascript">

	$('#login_option').change(function() {
		togleinput();
    });
	
	
	togleinput();
	
	function togleinput(){
		if($('#login_option').is(":checked")){
			$(".qr-code").show();
			$(".normal-login").hide();
			startScann();
		}else{
			$(".qr-code").hide();
			$(".normal-login").show();
		}
	}	
	
	function startScann(){
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      scanner.addListener('scan', function (content) {
        
		var tmp = content.split("/");
		var code = tmp.pop();
		if(code && code != ""){
			window.location = "{{url('login')}}/"+code;
		}
      });
      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 1) {
          scanner.start(cameras[1]);
        }else if (cameras.length > 0) {
          scanner.start(cameras[0]);
        } else {
          alert('No cameras found.');
        }
      }).catch(function (e) {
        alert(e);
      });
	}
    </script>

@endpush
