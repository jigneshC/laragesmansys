@extends('layouts.apex-auth')

@section('title','Qr-code Login')

@section('content')


	

<section id="login">
    <div class="container-fluid">
        <div class="row full-height-vh">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="card gradient-indigo-purple text-center width-400">
                    <div class="card-img overlap">
                        <img alt="element 06" class="mb-1" src="{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}" width="190">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <h2 class="white">@lang('comman.label.qrcode_login')</h2>
							<br/>
							@if(isset($otpuser) && \Carbon\Carbon::now() < $otpuser->exp_time)
								<form class='validate-form' role="form" method="POST" action="{{ url('do-login-otp') }}">
							@else
								<form class='validate-form' role="form" method="POST" action="{{ url('login-with-otp') }}">
							@endif
                            
                                {{ csrf_field() }}
								{!! Form::hidden('qrcode', $qrcode->qrcode, ['class' => 'form-control']) !!}

								@if(isset($otpuser) && \Carbon\Carbon::now() < $otpuser->exp_time)
									
								<div class="form-group {{ $errors->has('otp') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input type="text" name="otp" value="" placeholder="Enter otp" class="form-control" >
										{!! $errors->first('otp', '<p class="help-block text-danger">:message</p>') !!}	
                                         
                                    </div>
                                </div>

								<div class="form-group {{ $errors->has('login_to_option') ? ' has-error' : '' }}">
									<label for="table_row_id" class="label-control">@lang('comman.label.login_to') </label>
                                    <div class="col-md-12">
                                        {!! Form::select('login_to_option',trans('comman.login_to_option'),null, ['class' => 'form-control']) !!}
										{!! $errors->first('login_to_option', '<p class="help-block text-danger">:message</p>') !!}	
                                         
                                    </div>
                                </div>
								
								
								<p> <a href="{!! url('login') !!}/{{$qrcode->qrcode}}?resend=1" class=" txt-decoration">@lang('comman.label.resend_otp')</a> <p>
								
								@else
			@if($people && $people->phone_number_1 && $people->phone_number_1 != "" && $people->code_phone_number_1 && $people->code_phone_number_1 != "")
				<div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox check_remember">
                                <input type="checkbox" name="send_to_mobile" class="custom-control-input "  id="send_to_mobile" value='1' {{ old('send_to_mobile') ? 'checked' : '' }}>
                                <label class="custom-control-label float-left white" for="send_to_mobile">
								@lang('comman.label.send_otp_sms') ( +{{ \config('country.'.strtoupper($people->code_phone_number_1).'.code') }} .......{{substr($people->phone_number_1, -3)}} )
								</label>
                            </div>
                        </div>
                    </div>
                </div>		
			@endif
								
                                
								

								<div class="form-group">
									<div class="row">
										<div class="col-md-12">
											
											<div class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0 ml-3 checkbox check_remember">
												<input type="checkbox" name="send_to_email" class="custom-control-input "  id="send_to_email" value='1' >
												<label class="custom-control-label float-left white" for="send_to_email">
												@lang('comman.label.send_otp_email') ( {{substr($user->email, 0,3)}} ......{{ substr($user->email,-4) }} )
												</label>
											</div>
										</div>
									</div>
								</div>
								
								{!! $errors->first('email_phone', '<p class="help-block text-danger">:message</p>') !!}	
								
								@endif
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-pink btn-block btn-raised">
										@if(isset($otpuser) && \Carbon\Carbon::now() < $otpuser->exp_time)
											@lang('comman.label.signin')
									    @else
											@lang('comman.label.send_otp')	
										@endif
										</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        

                        <div class="float-right white"> 
                            <a href="{!! route('login') !!}" class="white txt-decoration">@lang('comman.label.login_with_username_pass')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
