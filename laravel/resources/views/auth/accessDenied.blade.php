@extends('layouts.backend')


@section('title','Access Denied')


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Oops !</div>

                <div class="panel-body">
                    @if(isset($reasion) && $reasion=="insufficient_unit")
                        @lang('moduleunit.responce_msg.you_have_not_enough_credit')
                        <br/>
                        <br/>
                        @if(Auth::user()->can('access.domainunits'))
                         <p> @lang('moduleunit.responce_msg.click_to_check_packages') : <a href="{{url('admin/domain-unit')}}"> @lang('comman.label.click_here') </a> </p>
                        @endif
                    @else
                    You don't have permission for access this page.
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

