@extends('layouts.apex-auth')

@section('title','Reset Password')

@section('content')


<section id="login">
    <div class="container-fluid">
        <div class="row full-height-vh">
            <div class="col-12 d-flex align-items-center justify-content-center">
                <div class="card gradient-indigo-purple text-center width-400">
                    <div class="card-img overlap">
                        <img alt="element 06" class="mb-1" src="{{ (_WEBSITE_LOGO)? asset('uploads/website/'._WEBSITE_LOGO): asset('assets/images/logo_lg.png') }}" width="190">
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <h2 class="white">@lang('comman.label.reset') @lang('comman.label.password')</h2>
                            
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                    
                            <form class='validate-form' role="form" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <input name="email" type="text" value="{{ old('email') }}" class="form-control"  placeholder="@lang('comman.label.email')"  >
                                        @if ($errors->has('email'))
                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-pink btn-block btn-raised">@lang('comman.label.password_reset')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                            
                         </div>

                        <div class="float-right">@lang('comman.label.have_account')  
                            <a href="{!! route('register') !!}" class="white"><strong>@lang('comman.label.registernow')</strong</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

