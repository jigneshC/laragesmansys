@extends('layouts.apex')

@section('title',trans('equipment.label.show_equipment'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('equipment.label.show_equipment') {{ $equipment->id }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'equipment','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                         <a href="{{ url('/admin/equipments') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                           
                            <a href="{{ url('/admin/equipments/' . $equipment->id . '/edit') }}" title="Edit Equipment">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    @lang('comman.label.edit')
                                </button>
                            </a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/equipments', $equipment->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Equipment',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                            {!! Form::close() !!}
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $equipment->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th>@lang('equipment.label.equipment_id')</th>
                                                    <td> {{ $equipment->equipment_id }} </td>
                                                </tr>
                                                <tr>
                                                    <th>@lang('equipment.label.website_id')</th>
                                                    <td> {{ $equipment->_website_id }} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection





