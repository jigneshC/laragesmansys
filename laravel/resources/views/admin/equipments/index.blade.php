@extends('layouts.apex')

@section('title',trans('equipment.label.equipments'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('equipment.label.equipments') </div>
        {{--   @include('partials.page_tooltip',['model' => 'equipment','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                            
                              <a href="{{ url('/admin/equipments/create') }}" class="btn btn-success btn-sm ticket-add-new"
                               title="Add New Equipment">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                            </a>
							
								@include("admin.for_master.export_pdf",["module"=>"equipments","fields"=>[]])
							
                          </div>
                         </div>
                        <div class="col-9">
                          
                             
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>@lang('comman.label.id')</th>
                                <th>@lang('equipment.label.equipment_id')</th>
                                <th>@lang('equipment.label.website_id')</th>
                                <th>@lang('comman.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection


@push('js')
<script>

    var url = "{{url('admin/equipments')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            { "data": "equipment_id","name":"equipment_id"},
            { "data": "_website_id","name":"_website_id"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.equipment.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                    @if(Auth::user()->can('access.equipment.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                    @endif

                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";


                    return v+d+e;

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/equipments/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                 // d.filter_website = $('#filter_website').val();
            }
        }
    });

    /*$('.filter').change(function() {
        datatable.fnDraw();
    });*/
    $("#filter_website").select2();
    

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Equipment'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

