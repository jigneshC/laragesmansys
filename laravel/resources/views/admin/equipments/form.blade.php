<div class="form-group row {{ $errors->has('equipment_id') ? 'has-error' : ''}}">
    <label for="equipment_id" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('equipment.label.equipment_id')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.equipment.form_field.equipment_id')])

    </label>
    <div class="col-md-6">
        {!! Form::text('equipment_id', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('equipment_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row ">
     <label class="col-md-4 label-control"></label>
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>
