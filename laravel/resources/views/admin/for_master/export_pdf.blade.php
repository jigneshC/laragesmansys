@if(_MASTER)
<a id="export_data_csv" href="javascript:void(0)" class="btn btn-success btn-sm " title="@lang('comman.label.export_csv')" >
	<i class="fa fa-download" ></i> @lang('comman.label.export_csv')
</a>


@push('js')


<script>
$(document).ready(function(){ 
    @if(isset($fields) && isset($module))
	$(document).on('click', '#export_data_csv', function (e) {
         var expurl="{{ url('admin/export-data') }}?module={{ $module }}";
		
		@foreach($fields as $field)
		
		@if($field == "is_deleted_record")
		var is_deleted_record = ($('#{{$field}}').is(":checked")) ? 1 : 0;
		expurl = expurl + "&{{$field}}="+is_deleted_record;
		@else
		expurl = expurl + "&{{$field}}="+$('#{{$field}}').val();	
		@endif
		@endforeach
		
        window.location = expurl;

    });
	@endif
	
	
});
    
</script>
@endpush


@endif


