@if(_MASTER)

    <?php
    $_id = isset($website_ids) ? $website_ids : [];

    ?>


    <div class="form-group row {{ $errors->has('_website_ids[]') ? 'has-error' : ''}}">
        {!! Form::label('_website_ids[]',trans('ticket.label.website'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            <select class="form-control selectTag" required="required" id="_website_id" name="_website_ids[]" multiple = true>
                @foreach($_websites as $website)
                    <option value="{{$website->id}}" @if(in_array($website->id,$_id)) selected="selected" @endif>{{$website->domain}}</option>
                @endforeach
            </select>
            {!! $errors->first('_website_ids[]', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

@endif