@if(_MASTER)
<hr>
<!-- Sidebar BG Image Toggle Starts-->
<div class="togglebutton">
  <div class="switch font-medium-2 text-bold-600 ml-1"><span>@lang('comman.label.deleted_decord')</span>
	<div class="float-right">
	  <div class="custom-control">
		<input id="is_deleted_record" type="checkbox"   class=" switchery ">
		<label for="is_deleted_record" class="font-medium-2 text-bold-600 ml-1"></label>
	  </div>
	</div>
  </div>
</div>
<!-- Sidebar BG Image Toggle Ends-->
<hr>
								
@endif