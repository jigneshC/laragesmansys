@if(_MASTER)

    <?php
    $_id = isset($website_id) ? $website_id : null;

    $_sel = null;
    if (is_null($_id)) {
        $_sel = _WEBSITE_ID;
    }else{
        $_sel = $_id;
    }

    $_requ = (isset($isrquired) && $isrquired==0) ? ['class' => 'form-control'] : ['class' => 'form-control','required'=>'required'] ;
    ?>


    <div class="form-group row {{ $errors->has('_website_id') ? 'has-error' : ''}}">
        {!! Form::label('_website_id',trans('ticket.label.website'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('_website_id',$_websites_pluck, $_sel,$_requ) !!}
            {!! $errors->first('_website_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

@endif