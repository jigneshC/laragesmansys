<div class="modal fade text-left" id="ucredit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('moduleunit.label.creadit_unit')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => '', 'class' => 'form-horizontal ucredit_form', 'files' => true]) !!}
            {!! Form::hidden('website_id',0, ['class' => 'form-control','id'=>'cwebsite_id']) !!}
                        
            <div class="modal-body">
                {!! Form::label('user_id', trans('moduleunit.label.unit'),['class' => '']) !!}
                <div class="form-group position-relative">
                     <input type="number" name="unit" class="filter form-control" min="0" id="unit" style="">
                </div>
                
                {!! Form::label('comment', trans('ticket.label.place_a_comment'), ['class' => '']) !!}
                <div class="form-group position-relative">
                    {!! Form::textarea('comment','' , ['class' => 'form-control', 'rows' => 3]) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-outline-primary btn-lg']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>

    var url = "{{url('admin/websites')}}";
/***************action model******************/
    var _model = "#ucredit_modal";
        $(document).on('click', '.credit_unit', function (e) {
            var id=$(this).attr('data-id');
            $("#cwebsite_id").val(id);
            $("#unit").val(0);
            $("#comment").val("");
            $(_model).modal('show');
            return false;
        });


    $('.ucredit_form').submit(function(event) {


        var formData = {
            'unit': $("#unit").val(),
            'website_id': $("#cwebsite_id").val(),
            'comment': $("#comment").val()
        };

        $.ajax({
            type: "POST",
            url: url + "/credit-units",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {

                $(_model).modal('hide');
                @if($isReload==0)
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                @else
                    window.location.reload();
                @endif
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush