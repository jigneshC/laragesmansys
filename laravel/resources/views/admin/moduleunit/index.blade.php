@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('moduleunit.label.moduleunit'))

@push('css')

@endpush

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">
            <label >@lang('moduleunit.label.moduleunit')</label>
        </div>
        {{--  @include('partials.page_tooltip',['model' => 'moduleunit','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                   
                        <div class="col-12">
                           @can('access.ticket.create')
                            <div class="row">
                                <div class="col-xl-4 col-sm-6">
                                    <div class="ticket-select-process">
                                        
                                        <select class="filter full-width" id="action" name="action">
                                                    @foreach(trans('moduleunit.unit_charge_lable') as $k => $val)
                                                        <option value="{{$k}}">{{$val}}</option>
                                                    @endforeach
                                                </select>
                                                <p class="add_moduleunit_error text-error"></p>
                                    </div>
                                   

                                </div>
                                <div class="col-xl-3 col-sm-6">
                                         <div class="ticket-select-process">
                                       <input type="number" name="unit" placeholder="@lang('moduleunit.label.unit_charge_per_month')" class="filter" min="0" id="unit">
                                                <p class="add_moduleunit_error_unit text-error"></p>
                                    </div>

                                </div>
                                <div class="col-xl-4 col-sm-12">
								
									<a href="#" class="btn btn-success btn-sm ticket-add-new" id="add_new"
                                                   title="@lang('comman.label.add_or_update_units')">
                                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_or_update_units')
                                                </a>
                                       


                                </div>

                            </div>
			@endcan
                       
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <table class="table table-borderless datatable responsive" width="100%">
                                <thead>
                                <tr>
                                    <th data-priority="1">@lang('moduleunit.label.action')</th>
                                    <th data-priority="2">@lang('moduleunit.label.units_day')</th>
                                    <th data-priority="4">@lang('comman.label.created')</th>
                                    <th data-priority="3">@lang('comman.label.action')</th>
                                </tr>
                                </thead>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection





@push('js')

<script>
    var unit_charge = <?php echo json_encode(trans('moduleunit.unit_charge_lable')); ?>;

    var url = "{{url('admin/module-units')}}";
    $("#action").select2();

    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            {
                "data": null,
                "name": 'action',
                "orderable": true,
                "searchable": true,
                "render": function (o) {
                    var res = (o.action in unit_charge) ? unit_charge[o.action] : o.action;
                    return res;
                }
            },
            { "data": "unit","name":"unit"},
            {
                "data": null,
                "name": 'created_at',
                "searchable": false,
                "render": function (o) {
                    return getLangFullDate(o.created_at,"short");
                }
            },
            { "data": null,
                "searchable": false,
                "orderable": false,
                "visible": <?php echo (Auth::user()->can('access.moduleunits.delete')) ? 'true' : 'false' ?>,
                "render": function (o) {
                    var d ="";
                    @if(Auth::user()->can('access.moduleunits.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')' ><i class='fa fa-trash action_icon '></i></a>";
                    @endif

                    return d;
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/module-units/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {

            }
        }
    });





    
    $(document).on('click', '#add_new', function (e) {
        var action = $('#action').val();
        var unit = $('#unit').val();
        var error_msg = "";
        if(action==""){ 
            error_msg = "*Please select An Action";
        }
        else if(unit==""){ 
            error_msg = "*Units Value Required";
            $(".add_moduleunit_error_unit").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }

        if(error_msg!=""){
            $(".add_moduleunit_error").html(error_msg).show().delay(5000).fadeOut();
        }else{
            var formData = {
                'action': action,
                'unit': unit,
            };

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message);
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }



        return false;
    });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Module Unit'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });




</script>


@endpush

