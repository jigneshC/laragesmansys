@extends('layouts.backend')

@section('title',trans('people.label.peoples'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('people.label.peoples')
                    </div>
                    <div class="actions">
                        @include('partials.page_tooltip',['model' => 'people','page'=>'index'])
                    </div>

                </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">

                            @if(Auth::user()->can('access.people.create'))
                                <a href="{{ url('/admin/peoples/create') }}" class="btn btn-success btn-sm ticket-add-new"
                                   title="Add New People">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                            @endif


                        </div>
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-3">
                            @if(_MASTER)
                                {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter full-width','required'=>'required','id'=>'filter_website']) !!}
                            @endif
                        </div>

                    </div>
</div>
<div class="box-content ">

                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>@lang('comman.label.id')</th>
                                <th>@lang('people.label.user')</th>
                                <th>@lang('people.label.title')</th>
                                <th>@lang('people.label.first_name')</th>
                                <th>@lang('people.label.email')</th>
                                @if(_MASTER)
                                <th>@lang('website.label.website')</th>
                                @endif
                                <th>@lang('comman.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>

</div>

                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
<script>

    var url = "{{url('admin/peoples')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            {
                "data": null,
                "name": "user_id",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    if(o.user){
                        return o.user.name;
                    }else{
                        return '';
                    }
                }
            },
            { "data": "title","name":"title"},
            { "data": "first_name","name":"first_name"},
            { "data": "email","name":"email"},
            @if(_MASTER)
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o._website){
                        return o._website.domain;
                    }else{
                        return "";
                    }
                }
            },
            @endif
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.people.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.people.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";


                    return v+d+e;

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/peoples/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                 d.filter_website = $('#filter_website').val();
            }
        }
    });

    
    $('.filter').change(function() {
        datatable.fnDraw();
    });
    $("#filter_website").select2();
    

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'people'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

