@push('css')
<style>
    .intl-tel-input{
        display: block;
    }
</style>
@endpush

<div class="box-double-padding">
<div class="row ">

    <div class="col-md-4 col-sm-offset-1">
        @include('admin.for_master.site_input2')
        
        

        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
            <label for="first_name" class="">
                <span class="field_compulsory">*</span>@lang('people.label.first_name')
            </label>
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
            <label for="last_name" class="">
                <span class="field_compulsory">*</span>@lang('people.label.last_name')
            </label>
                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('language') ? 'has-error' : ''}}">
            <label for="language" class="">
                <span class="field_compulsory">*</span>@lang('people.label.language')
            </label>
                {!! Form::select('language',$languages, null, ['class' => 'form-control']) !!}
                {!! $errors->first('language', '<p class="help-block">:message</p>') !!}

        </div>
        
        
        
        
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('user.label.role')
            </label>
            <div >
                {!! Form::select('roles[]',[],[], ['class' => 'full-width selectTag2', 'multiple' => true]) !!}
            </div>
        </div>

        @if(_MASTER)

            <?php
            $_id = isset($website_ids) ? $website_ids : [];

            ?>


            <div class="form-group {{ $errors->has('accessible_websites[]') ? 'has-error' : ''}}">
                {!! Form::label('accessible_websites[]',trans('user.label.accessible_website'), ['class' => '']) !!}
                <div class="">
                    <select class="form-control selectTag" required="required" id="accessible_websites" name="accessible_websites[]" multiple = true>
                        @foreach($accessible_websites as $website)
                            <option value="{{$website->id}}" @if(in_array($website->id,$user_accessible_website)) selected="selected" @endif>{{$website->domain}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('accessible_websites[]', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        @endif
        
        <div class="form-group {{ $errors->has('services') ? 'has-error' : ''}}">
            <label for="services" class="">@lang('people.label.services')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Services')])
            </label>
            {!! Form::select('services[]',$services, isset($user_services) ? $user_services : [], ['class' => 'form-control selectTag','multiple'=>true,'id'=>'services_selection']) !!}
            {!! $errors->first('services', '<p class="help-block">:message</p>') !!}

        </div>

        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            <label for="title" class="">
                @lang('people.label.title')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Title')])
            </label>
            {!! Form::text('title', null, ['class' => 'form-control','max'=>10]) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

        </div>
        
       
        <div class="form-group {{ $errors->has('quality') ? 'has-error' : ''}}">
            <label for="quality" class="">@lang('people.label.quality')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Quality')])
            </label>

                {!! Form::select('quality',$people_quality, null, ['class' => 'form-control ']) !!}
                {!! $errors->first('quality', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
            {!! Form::label('photo', trans('people.label.change_photo'), ['class' => '']) !!}


                @if(isset($people) && $people->photo != '')
                     <br>
                    <img src="{!! asset('uploads/'.$people->photo) !!}" alt="" width="150">
                    <br><br>
                @endif
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}

        </div>
    </div>
    <div class="col-md-4  col-sm-offset-1 ">
        
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="">
                <span class="field_compulsory">*</span>@lang('user.label.email')
            </label>
                 {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="">
                @if(!isset($user_roles))
                    <span class="field_compulsory">*</span>
                @endif
                @lang('user.label.password')
            </label>
                 {!! Form::password('password', ['class' => 'form-control']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
            {!! Form::label('phone_number_1',  trans('people.label.phone_number_1'), ['class' => ""]) !!}
            {{ Form::hidden('code_phone_number_1',null, array('id' => 'code_phone_number_1')) }}
            {!! Form::text('phone_number_1', null, ['class' => 'form-control','style'=>'padding-left: 84px !important']) !!}
            {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
            {!! Form::label('phone_type_1', trans('people.label.phone_type_1'), ['class' => '']) !!}
            {!! Form::select('phone_type_1',$phoneTypes, null, ['class' => 'form-control']) !!}
            {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('phone_number_2') ? 'has-error' : ''}}">
            {!! Form::label('phone_number_2',  trans('people.label.phone_number_2'), ['class' => '']) !!}
            {{ Form::hidden('code_phone_number_2',null, array('id' => 'code_phone_number_2')) }}
            {!! Form::text('phone_number_2', null, ['class' => 'form-control','style'=>'padding-left: 84px !important']) !!}
            {!! $errors->first('phone_number_2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('phone_type_2') ? 'has-error' : ''}}">
            {!! Form::label('phone_type_2', trans('people.label.phone_type_2'), ['class' => '']) !!}
            {!! Form::select('phone_type_2',$phoneTypes, null, ['class' => 'form-control']) !!}
            {!! $errors->first('phone_type_2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('emergency_password') ? 'has-error' : ''}}">
            <label for="emergancy_password" class="">@lang('people.label.emergancy_password')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Emergancy_Password')])
            </label>

            {!! Form::text('emergency_password',null, ['class' => 'form-control']) !!}
            {!! $errors->first('emergency_password', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('availability') ? 'has-error' : ''}}">
            <label for="availability" class="">@lang('people.label.availability')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Availability')])
            </label>
            {!! Form::select('availability',[''=>'select',true=>'True',false=>'False'], null, ['class' => 'form-control drop_down','id'=>'availability']) !!}
            {!! $errors->first('availability', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('status_text') ? 'has-error' : ''}}" id="dropbox">
            {!! Form::label('status_text',  'status-Text', ['class' => ''],['id'=>'statusvalue']) !!}
            {!! Form::text('status_text', null, ['class' => 'form-control']) !!}
            {!! $errors->first('status_text', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('hold_key') ? 'has-error' : ''}}">
            <label for="hold_key" class="">@lang('people.label.hold_key')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Hold_Key')])
            </label>
            {!! Form::select('hold_key',['yes'=>'Yes','no'=>"No"], null, ['class' => 'form-control ']) !!}
            {!! $errors->first('hold_key', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
            {!! Form::label('active', trans('people.label.active'), ['class' => '']) !!}
            {!! Form::select('active',[1=>'Active',0=>'Inactive'], null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row availability_head_row">
	<div class="form-group">
	<div class="col-md-9 col-md-offset-1">
		{!! Form::label('availability', trans('people.label.availability'), ['class' => 'control-label']) !!}
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Availability')])
    </div>
	</div>
</div>
<div class="row">
    <div class="form-group">

        
        <div class="col-md-9 col-md-offset-1">

            <p>
                <button type="button" class="btn btn-info btn-xs" id="cp_day_btn" style="display: none">Copy from monday
                </button>
            </p>

            <table class="tbl_avialability" width="80%" border="1" bordercolor="#cccccc" cellpadding="5" cellspacing="5">

                <thead>
                <tr>
                    <th>Day</th>
                    <th>From</th>
                    <th>To</th>
                </tr>
                </thead>

                <tbody>

                <tr>
                    <th>Monday</th>

                    <td> {!! Form::text('av[monday][from]', isset($av['monday']['from'])?$av['monday']['from']:null, ['class' => 'form-control time_pick mon_from']) !!}</td>

                    <td> {!! Form::text('av[monday][to]', isset($av['monday']['to'])?$av['monday']['to']:null, ['class' => 'form-control time_pick mon_to']) !!}</td>
                </tr>


                {{--foreach $ev as $k=>v--}}

                {{--day from--}}
                {{--k v[from] v[to]--}}

                <tr class="tuesday tr">
                    <th>
                        Tuesday
                    </th>

                    <td> {!! Form::text('av[Tuesday][from]', isset($av['tuesday']['from'])?$av['monday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Tuesday][to]', isset($av['tuesday']['to'])?$av['tuesday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="wednesday tr">

                    <th>
                        Wednesday
                    </th>

                    <td> {!! Form::text('av[Wednesday][from]', isset($av['wednesday']['from'])?$av['wednesday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Wednesday][to]', isset($av['wednesday']['to'])?$av['wednesday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="thursday tr">

                    <th>
                        Thursday
                    </th>

                    <td> {!! Form::text('av[Thursday][from]', isset($av['thursday']['from'])?$av['thursday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Thursday][to]', isset($av['thursday']['to'])?$av['thursday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="friday tr">

                    <th>
                        Friday
                    </th>

                    <td> {!! Form::text('av[Friday][from]', isset($av['friday']['from'])?$av['friday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Friday][to]', isset($av['friday']['to'])?$av['friday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>

                <tr class="saturday tr">

                    <th>
                        Saturday
                    </th>

                    <td> {!! Form::text('av[Saturday][from]', isset($av['saturday']['from'])?$av['saturday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Saturday][to]', isset($av['saturday']['to'])?$av['saturday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="sunday tr">

                    <th>
                        Sunday
                    </th>
                    <td> {!! Form::text('av[Sunday][from]', isset($av['sunday']['from'])?$av['sunday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Sunday][to]', isset($av['sunday']['to'])?$av['sunday']['to']:null, ['class' => 'form-control time_pick ']) !!}</td>

                </tr>

                </tbody>
            </table>

        </div>

    </div>


    <div class="form-group">
        <div class="col-md-offset-1 col-md-9">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-primary']) }}
        </div>
    </div>

</div>

</div>















