@extends('layouts.backend')

@section('title',trans('people.label.edit_peoples'))


@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                           @lang('people.label.edit_peoples') # {{$people->first_name }}  {{$people->last_name }}
                                       </div>
                       <div class="actions">
                           @include('partials.page_tooltip',['model' => 'people','page'=>'form'])
                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ url('/admin/peoples') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back') </button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($people, [
                            'method' => 'PATCH',
                            'url' => ['/admin/peoples', $people->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'autocomplete'=>'off'
                        ]) !!}

                        @include ('admin.peoples.form', ['submitButtonText' => trans('comman.label.update')])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
