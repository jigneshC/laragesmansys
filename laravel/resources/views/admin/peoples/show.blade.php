@extends('layouts.backend')

@section('title',trans('people.label.show_people'))


@section('content')

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#people">@lang('people.label.people')</a></li>
        <li><a data-toggle="tab" href="#services">@lang('people.label.services')</a></li>
    </ul>

    <div class="tab-content">
        <div id="people" class="tab-pane fade in active">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bordered-box blue-border">
                        
                        <div class="box-content ">
<div class="table-responsive">
                                <table class="table responsive table-borderless">
                                    <tbody>
                                  

                                    <tr>

                                        <th>@lang('people.label.services')</th>
                                        <td>
                                            @if($people->services)
                                                @foreach($people->services as $k=>$itm)
                                                    {{ $itm->name }} @if(count($people->services) != $k+1) ,  @endif
                                                @endforeach

                                            @endif
                                        </td>
                                    </tr>
                                   

                                 
                                    <tr>
                                        <th>  @lang('people.label.title')</th>
                                        <td> {{ $people->title }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('people.label.first_name') </th>
                                        <td> {{ $people->first_name }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.last_name') </th>
                                        <td> {{ $people->last_name }} </td>
                                    </tr>

                                   

                                    <tr>
                                        <th> @lang('people.label.quality') </th>
                                        <td> {{ $people->quality }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.phone_number_1')</th>
                                        <td> {{ $people->phone_number_1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('people.label.phone_number_2')</th>
                                        <td> {{ $people->phone_number_2 }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.phone_type_1')</th>
                                        <td> {{ $people->dropdownvalue->name OR '' }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.phone_type_2')</th>
                                        <td> {{ $people->dropdownvalueM->name OR '' }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.emergancy_password') </th>
                                        <td> {{ $people->emergency_password }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.availability')</th>
                                        <td> {{ $people->availability }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.hold_key') </th>
                                        <td> {{ $people->hold_key }} </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.photo') </th>
                                        <td>
                                            @if($people->photo)
                                                <img src="{!! asset('uploads/'.$people->photo) !!}" alt="" width="150">
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th> @lang('people.label.active')</th>
                                        <td> {{ $people->active == true?"True":'False' }} </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="services" class="tab-pane fade">
            <div class="row">
                <div class="col-md-12">
                    <div class="box bordered-box blue-border">
                        <div class="box-header blue-background">
                            <div class="title">
                                <i class="icon-circle-blank"></i>
                                Services
                            </div>

                        </div>
                        <div class="box-content ">


                            <div class="row">
                                <div class="col-md-6">
                                    @if(Auth::user()->can('access.service.create'))
                                        <a href="{{ url('/admin/services/create') }}" class="btn btn-success btn-sm"
                                           title="Add New Service">
                                            <i class="fa fa-plus" aria-hidden="true"></i> Add New

                                        </a>
                                    @endif
                                </div>

                                <div class="col-md-6">


                                    {!! Form::open(['method' => 'GET', 'url' => '/admin/services', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search..."
                                               value="{!! request()->get('search') !!}">

                                        <span class="input-group-btn">
                                <button class="btn btn-default no-radious" type="submit" style="
    padding: 7.5px 15px;
">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>


                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Site</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($people->services))
                                        @foreach($people->services as $item)
                                            <tr>
                                                <td>{{ $item->id }}</td>
                                                <td>{{ $item->site->name or null}}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/services/' . $item->id) }}" title="View Service">
                                                        <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                               aria-hidden="true"></i> View
                                                        </button>
                                                    </a>

                                                    @if(Auth::user()->can('access.service.edit'))

                                                        <a href="{{ url('/admin/services/' . $item->id . '/edit') }}"
                                                           title="Edit Service">
                                                            <button class="btn btn-primary btn-xs"><i
                                                                        class="fa fa-pencil-square-o"
                                                                        aria-hidden="true"></i> Edit
                                                            </button>
                                                        </a>

                                                    @endif

                                                    @if(Auth::user()->can('access.service.delete'))

                                                        {!! Form::open([
                                                            'method'=>'DELETE',
                                                            'url' => ['/admin/services', $item->id],
                                                            'style' => 'display:inline'
                                                        ]) !!}
                                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                                'type' => 'submit',
                                                                'class' => 'btn btn-danger btn-xs',
                                                                'title' => 'Delete Service',
                                                                'onclick'=>'return confirm("Confirm delete?")'
                                                        )) !!}
                                                        {!! Form::close() !!}
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{--<div class="pagination-wrapper"> {!! $services->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>





@endsection
