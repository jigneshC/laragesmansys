@extends('layouts.apex')

@section('body_class',' pace-done')
@section('title',trans('ticket.label.ticket'))

@push('css')

@endpush

@section('content')

@php( $is_expanded= 0)
@if(Session::has('filter_website') || Session::has('range_start') || Session::has('filter_status') || Session::has('filter_site') || Session::has('filter_services') || Session::has('filter_subject') || Session::has('filter_company')) 
@php( $is_expanded= 1)
@endif




    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-12">
					
                        <h4 class="card-title">@lang('ticket.label.ticket')
							@if(Auth::user()->can('access.ticket.create'))
                               <a href="{{ url('/admin/tickets/create') }}" class="btn btn-success btn-sm ticket-add-new"
                                  title="@lang('comman.label.add_new')">
                                   <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                               </a>
                           @endif
							
						</h4>
						
						 
                    </div>
                    {{--  @include('partials.page_tooltip',['model' => 'ticket','page'=>'index']) --}}
                    <div class="col-xl-9 col-lg-9 col-md-9 col-12">

                        <div class="actions pull-right">
                           
                          
                           @if(request()->has('v_notification') && request()->has('v_notification')=="all")
                                   <a href="{{ url('/admin/tickets') }}" class="btn btn-success btn-sm "
                                      title="Close Notification Ticket">
                                       <i class="fa fa-close" aria-hidden="true"></i> @lang('ticket.label.all_notification')
                                   </a>
                           @endif
						   
						   
						   
						   <a id="btn-show-all-children" href="javascript:void(0)" class="btn btn-success btn-sm "
                                  title="Expand All" cstatus="expand">
                                    Expand  <i class="fa fa-arrows-v" aria-hidden="true"></i>
                           </a>
						 
							

                       </div>
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
					
						
					
                       
                       <div class="table-responsive1"> 
                        <table class="table table-striped table-bordered base-style responsive nowrap datatable">
                            
                           <thead>
                            <tr>
                                <th data-priority="1" >@lang('subject.label.type')</th>
                                <th data-priority="2" title="@lang('tooltip.ticket.label.subject')">@lang('ticket.label.subject')</th>
                               {{-- <th data-priority="3" title="@lang('tooltip.ticket.label.title')">@lang('ticket.label.title')</th> --}}
                                <th data-priority="8" title="@lang('tooltip.ticket.label.site')">@lang('ticket.label.site')</th>
                                
                                <th data-priority="10" class="all" title="@lang('tooltip.ticket.label.content')">@lang('ticket.label.content')</th>
                                <th data-priority="12" class="none" title="@lang('tooltip.ticket.label.content')">@lang('ticket.label.content')</th>
                                <th data-priority="11" class="none">@lang('ticket.label.action_log')</th>
								<th data-priority="6">@lang('comman.label.status')</th>
								<th data-priority="7" title="@lang('tooltip.ticket.label.assign')">@lang('ticket.label.assign')</th>
                                
                                <th data-priority="2" class="all">@lang('comman.label.created')</th>

                                @if(Auth::user()->can('access.ticket.edit'))<th data-priority="5" class="noExport" title="@lang('tooltip.ticket.label.action')">@lang('comman.label.action')</th> @endif
                            </tr>
                            </thead>
                        </table>
						</div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


           

    @if(Auth::user()->can('access.ticket.action'))
        @include("admin.tickets.actionmodel",['isReload' => 0])
        @include("admin.tickets.assignmodel",['isReload' => 0])
        @include("admin.tickets.fileuploadmodel")
    @endif
    @include("admin.tickets.evidencemodel")
@endsection



@push('js')

<style>
    table.dataTable thead .sorting_asc , table.dataTable thead .sorting_desc ,table.dataTable thead .sorting{
        background-image: none;
    }
</style>
{{--<script src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>--}}
<!--
	<script>
							$(document).ready(function() {
								datatable = $('.datatable').dataTable({
									"scrollX": true
								});
								
							});
						</script> -->
<script>
    /***************************Select 2 ***********/

    $("#filter_website_id").select2();
    $("#filter_status").select2();

	$("#filter_company").select2({
        placeholder: "@lang('comman.label.select_company')",
        allowClear: true,
        @if(Session::has('filter_company'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_company')}}, text: "{{Session::get('filter_company_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/companies/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website_id").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	
    $("#filter_site").select2({
        placeholder: "@lang('comman.label.select_site')",
        allowClear: true,
        @if(Session::has('filter_site'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_site')}}, text: "{{Session::get('filter_site_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/sites/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website_id").val(),
                     company_id: $("#filter_company").val(),
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);

    $("#filter_services").select2({
        placeholder: "@lang('comman.label.select_services')",
        allowClear: true,
		multiple:true,
        @if(Session::has('filter_services'))
            initSelection: function (element, callback) {
				var selected_service = <?php echo json_encode($selected_service); ?>;
                callback(selected_service);
            },
        @else   
            initSelection: function (element, callback) {
              //  callback({id: [], text: "" });
            },
        @endif
        
        ajax: {
            url: "{{ url('admin/services/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_services: params, // search term
                    site_id: $("#filter_site").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);

    $("#filter_subject").select2({
        placeholder: "@lang('comman.label.select_subject')",
        allowClear: true,
		multiple:true,
        @if(Session::has('filter_subject'))
            initSelection: function (element, callback) {
                var selected_subject = <?php echo json_encode($selected_subject); ?>;
                callback(selected_subject);
            },
        @else   
            initSelection: function (element, callback) {
                //callback({id: [], text: "" });
            },
        @endif

        ajax: {
            url: "{{ url('admin/subjects/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_subject: params, // search term
                    service_id: $("#filter_services").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);



    var range_start ="";
    var range_end ="";
    var auth_id = "{{\Auth::user()->id}}";
    var status_dropdown = <?php echo json_encode(trans('dashboard.status_dropdown')); ?>;
    var status_tooltip = <?php echo json_encode(trans('tooltip.ticket.status')); ?>;
    var editable_time_limit = "{{\config('settings.ticket_editable_time_limit')}}";
    var now_time = "{{ \Carbon\Carbon::now()}}";
    var view_assign = "";


    var url = "{{url('admin/tickets')}}";
    var datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
		scrollX: true,
        order: [8, "DESC"],
        columns: [
            { "data": "subject_type","name":"subjects.subject_type","searchable": false,"orderable": false,"width":"5%"},
            {
                "data": null,
                "name": "subjects.name",
                "render": function (o) {
                    var res = o.subject_name +" ("+o.services_name+") ";
                    return res;
                },
				"width":"10%"
            },
            { 
                "data": null,
                "name":"sites.name",
                "render": function (o) {
                    var res = o.site_name +" ("+o.company_name+") ";
                    return res;
                },
                "width":"10%"
            },
            {
                "data": null,
                "name": "content",
                "render": function (o) {
                    var content = o.content;
                    if(content && content.length > 120){
                        content = content.substr(0, 120) + "...";
                    }
                    return "<span class='line-break'>"+content+"</span>";
                },
				 "width":"200px"
            },
            {
                "data": null,
                "name": "content",
                "render": function (o) {
                    var content = o.content;
                    var res = (content)? "<span class='ticket_content line-break'>"+content+"</span>" :"";
                    return res;
                }
            },
            {
                "data": null,
                "name": 'comments',
                "orderable": false,
                "searchable": false,
                "render": function (o) {
                    if(o.logs){
                        var table="<div class='table-responsive'><table class='table responsive table-borderless'> ";
                        for(var i=0;i<o.logs.length;i++){
                            var action =""; var des="";
                            if(o.logs[i].history_type=="action"){
                                action ="@lang('ticket.label.mark_as')";
                                des= o.logs[i].desc;
                            }else if(o.logs[i].history_type=="assign"){
                                action ="@lang('ticket.label.assign_ticket_to')";
                                des= o.logs[i].desc;
                            }else if(o.logs[i].history_type=="file"){
                                action ="@lang('ticket.label.added_a_file')";
                                var ext = o.logs[i].desc.split('.').pop();
								var iimg= "<img src = '{!! asset('img/image.png') !!}' height='25'>";
								
								if(ext != ""){ ext = ext.toLowerCase(); }
								
								if(ext=="pdf"){
									iimg= "<img src = '{!! asset('img/apdf.png') !!}' height='25'>";
								}else if(ext=="docx" || ext=="docm" || ext=="dotx" || ext=="dotm" || ext=="docb"){
									iimg= "<img src = '{!! asset('img/word.png') !!}' height='25'>";
								}else{
									
								}
								
								if(ext=="jpg" || ext=="jpeg" || ext=="png" || ext=="gif"){
									des= "<a class='example-image-link' href='{!! asset('uploads') !!}/"+o.logs[i].desc+"' data-lightbox='example-1' data-title=''>"+iimg+"</a>";
								}else{
									des= "<a href='{!! asset('uploads') !!}/"+o.logs[i].desc+"' file_type='"+ext+"' class='link_file view_in_popup' target='_blank'> "+iimg+" </a>";
								}
                                
								
								
                            }else{
                                action ="@lang('ticket.label.comments')";
                                des= o.logs[i].desc;
                            }
                            var actioner = (o.logs[i].user)? o.logs[i].user.name : "";
                            var tr = "<tr><td><span class='text-red'><i class='fa fa-user'></i> "+ actioner  +"</span></td>" +
                                "<td>"+ action + "</td>" +
                                "<td class='line-break'>"+des + "</td>" +
                                "<td>"+getLangFullDate(o.logs[i].created_tz,"short")+ "</td></tr>";
                            table = table+tr;
                        }
                        table= table+"</tr></table></div>";
                        return table;
                    }else{
                        return "";
                    }
                }
            },
            {
                "data": null,
                "name": 'status',
                "searchable": false,
                "render": function (o) {
                    var res = (o.status in status_dropdown) ? status_dropdown[o.status] : o.status;
                    var tooltip = (o.status in status_tooltip) ? status_tooltip[o.status] : o.status;
                    var c_class="";
					var indx= 1;
                    if(o.status=="new"){ indx= 1; c_class ="btn-success"; }
                    if(o.status=="open"){ indx= 2; c_class =" btn-info"; }
                    if(o.status=="closed"){ indx= 6; c_class ="btn-danger"; }
                    if(o.status=="solved"){ indx= 5; c_class ="btn-warning"; }
                    if(o.status=="pending"){ indx= 3; c_class ="btn-primary"; }
                    if(o.status=="on-hold"){ indx= 4; c_class ="btn-secondary"; }
                    
                    var temp=  "<a href='#' class='more-action' data-status="+o.status+" data-id="+o.id+" title='@lang('tooltip.ticket.icon.checked')' >"+indx+". "+res+"</a>";
					
					return temp;
                }
            },
			{
                "data": null,
                "name": "users.name",
                "render": function (o) {
                    var user_name = (o.user_name)? o.user_name:"";
                    return "<a href='#' class='assign_to' data-web-id="+o._website_id+" data-id="+o.id+" data-uid="+o.user_id+"><i class='fa fa-edit action_icon'></i></a>"+user_name;
                }
            },
            {
                "data": null,
                "name": 'created_at',
                "searchable": false,
                "render": function (o) {
                    var creatorname = "";
                    if(o.creater_name && o.creater_name!=""){
                        creatorname = "<br/>"+o.creater_name;
                    }
                    return getLangFullDate(o.created_tz,"short")+" "+creatorname;
                }
            },
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
					var evidence_class = "text-muted";
					if(o.total_files && o.total_files >0){
						evidence_class = "text-danger";
					}
                    var file= "<a href='#' data-id="+o.id+" class='file_upload' title='@lang('tooltip.ticket.icon.file')' ><i class='fa fa-file action_icon "+evidence_class+"'></i></a>";
                    var e="";

                    if(auth_id==o.created_by){
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    }
                    var d = "<a href='javascript:void(0);' deleted_at="+o.deleted_at+" class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                    var v =  " <a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";

                    var posted_min = moment(now_time).diff(moment(o.created_at), 'minutes');
                    if(posted_min > editable_time_limit){
                        e="";
                        d="";
                    }
					
					if(o.deleted_at && o.deleted_at!=""){
						var jio = "<a href='javascript:void(0);' class='recover-item' moduel='ticket' data-id="+o.id+" title='@lang('tooltip.common.icon.recover')'><i class='fa fa-repeat action_icon'></i></a>"+d;
						return jio;
					}else{
						return file+v+d+e;
					}

                }
            },
        ],
		 "drawCallback": function (settings) { 
			// Here the response
			var response = settings.json;
			/*if(response.types){
				$("#type_ticket").html(response.types.ticket);
				$("#type_alarm").html(response.types.alarm);
				$("#type_event").html(response.types.event);
				$("#type_planned_activity").html(response.types.planned_activity);
				$("#type_duty").html(response.types.duty);
				$("#type_all").html(response.types.all);
			}*/
		},
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
			 $(nRow).addClass(aData.subject_type);
        },
        ajax: {
            url: "{{ url('admin/tickets/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
				@if(_MASTER)
					d.filter_website = $('#filter_website_id').val();
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
				
                d.status = $('#filter_status').val();
                d.filter_site = $('#filter_site').val();
                d.filter_subject_type = $('#fiter_by_type').val();
                d.filter_subject = $('#filter_subject').val();
                d.filter_services = $('#filter_services').val();
               
                d.filter_company = $("#filter_company").val();

                d.filter_company_text = ($("#filter_company").select2("data"))? $("#filter_company").select2("data").text : "";
                d.filter_site_text = ($("#filter_site").select2("data"))? $("#filter_site").select2("data").text : "";
                d.filter_subject_text = ($("#filter_subject").select2("data"))? $("#filter_subject").select2("data").text : "";
                d.filter_services_text = ($("#filter_services").select2("data"))? $("#filter_services").select2("data").text : "";
                
                d.range_start = range_start;
                d.range_end = range_end;
                d.view_assign = view_assign;
                @if(request()->has('v_notification') && request()->has('v_notification')=="all")
                    d.v_notification = "{{request()->get('v_notification')}}";
                @endif
            }
        }
    });
    $('.filter').change(function() {
        var filter_id = $(this).attr('id');
        if(filter_id=="filter_website_id"){
			$("#filter_company").select2("val", "");
            $("#filter_site").select2("val", "");
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
		if(filter_id=="filter_company"){
			$("#filter_site").select2("val", "");
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
        if(filter_id=="filter_site"){
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
        if(filter_id=="filter_services"){
            $("#filter_subject").select2("val", "");
        }
        datatable.fnDraw();
    });
    
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });
	
    $(document).on('click', '.type_filter', function (e) {
		 var subject_type = $(this).attr('data-val');
		 $("#fiter_by_type").val(subject_type);
		 view_assign = "";
		 datatable.fnDraw();
		 
		 return true;
    });	 
		 
    $(document).on('click', '.del-item', function (e) {
		var id = $(this).attr('data-id');
        var deleted_at = $(this).attr('deleted_at');
		
		var r = false;
		if(deleted_at && deleted_at != "" && deleted_at != "undefined" ){
			r = confirm("@lang('comman.js_msg.confirm_for_delete_perminent',['item_name'=>'Ticket'])");	
		}else{
			r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Ticket'])");
		}
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
	
	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete_recover',['item_name'=>'Ticket'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });



    $(document).on('click', '#genrate_pdf', function (e) {
        var filter_website_id = $('#filter_website_id').val();
		@if(!_MASTER)
				filter_website_id = {{_WEBSITE_ID}};
        @endif
        var status = $('#filter_status').val();
        var filter_site = $('#filter_site').val();
        var filter_subject = $('#filter_subject').val();

        var pdfurl="{{ url('admin/tickets/pdf-generate') }}?status="+status+"&filter_site="+filter_site+"&filter_subject="+filter_subject+"&range_start="+range_start+"&range_end="+range_end+"&_website_id="+filter_website_id;
        window.location = pdfurl;

    });
	
	



    /*************************daterange selection*****************/



    var start = moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment();
    @if(Session::has('range_start') && Session::has('range_end'))
     start = moment.utc("{{Session::get('range_start')}}",'YYYY-MM-DD');
     end = moment.utc("{{Session::get('range_end')}}",'YYYY-MM-DD');
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');

            datatable.fnDraw();
        }


    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment()],
            "@lang('comman.daterange.today')": [moment(), moment()],
            "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            "@lang('comman.daterange.last7day')": [moment().subtract(6, 'days'), moment()],
            "@lang('comman.daterange.last30day')": [moment().subtract(29, 'days'), moment()],
            "@lang('comman.daterange.thismonth')": [moment().startOf('month'), moment().endOf('month')],
            "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            "@lang('comman.daterange.thisyear')": [moment().startOf('year'), moment().endOf('year')],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

     cb(start, end);

    $(".range_inputs").find(".applyBtn").html("@lang('comman.daterange.applyBtn')");
    $(".range_inputs").find(".cancelBtn").html("@lang('comman.daterange.cancelBtn')");
    $(".ranges").find('ul li:last-child').html("@lang('comman.daterange.customeRange')");

     $(document).on('click', '#reset_filter', function (e) {
        $("#filter_website_id").select2("val", "");
        $("#filter_company").select2("val", "");
        $("#filter_site").select2("val", "");
        $("#filter_services").select2("val", "");
        $("#filter_subject").select2("val", "");
        $("#filter_status").select2("val", "");
		$("#fiter_by_type").val("");
		view_assign = "";
        
        var start_r = moment.utc('2015-01-01','YYYY-MM-DD');
        var end_r = moment();

        cb(start_r, end_r);
        return false;
    });

    $(document).on('click', '.assign_tag', function (e) {
        view_assign =  $(this).attr("data-assgin");
        datatable.fnDraw();
    });
    
    $('#btn-show-all-children').on('click', function(){
		if($(this).attr("cstatus") == "expand"){
			datatable.api().rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
			$(this).attr("cstatus","colleps");
		}else{
			datatable.api().rows('.parent').nodes().to$().find('td:first-child').trigger('click');
			$(this).attr("cstatus","expand");
		}
        
    });
    



   


    //***************Auto refress********//
    window.setInterval(function(){
        datatable.fnDraw(false);
    }, 60*3000);
</script>


@endpush
