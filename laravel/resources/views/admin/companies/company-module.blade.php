@extends('layouts.apex')

@section('title',trans('company.label.Assign_Company_Modules'))
@push('css')
<link href="{!! asset('assets/plugins/duallistbox/bootstrap-duallistbox.css') !!}" rel="stylesheet" type="text/css">

@endpush


@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('company.label.Assign_Company_Modules')  </h4>
       {{-- @include('partials.page_tooltip',['model' => 'companies_module','page'=>'form']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title"># {{ $company->name }}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            @include('admin.companies.company-tab')  
            
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="company" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a href="{{ url('/admin/companies') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('comman.label.back')
                                        </button>
                                    </a>
                                    

                                    @include('partials.form_notification')
                                    
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                       {!! Form::open(['url' => '/admin/company-modules/'.$company->id, 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
                    

                                        @php( $lang_chargem = trans('moduleunit.unit_charge_lable'))
                                        <!-- Basic example -->
                                        <div class="form-group text-center bootstrap_duallistbox">

                                            <div class="panel-body">
                                                <select multiple="multiple" name="actions[]" class="form-control listbox">
                                                    @foreach($chargableModule as $k => $val)
													@if(isset($lang_chargem[$val->action]))
                                                            <option value="{{$val->action}}" @if(in_array($val->action, $selected_chargable_module)) selected="selected" @endif>{{$lang_chargem[$val->action]}}</option>
													@endif	
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group text-center">
                                            <div class="btn-submit-edit-s text-center">
                                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : "Assign Module", ['class' => 'btn btn-primary']) !!}
                                            </div>
                                        </div>

                                        {!! Form::close() !!}
                                        
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 

               
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection


@push('js')

<script type="text/javascript" src="{!! asset('assets/plugins/duallistbox/jquery.bootstrap-duallistbox.js') !!}"></script>


<script type="text/javascript" >
$(document).ready(function () {
    $('.listbox').bootstrapDualListbox({
        nonSelectedListLabel: "@lang('company.label.Non_Assigned_Modules')",
        selectedListLabel: "@lang('company.label.Assigned_Modules')",
    });
});
</script>

@endpush
