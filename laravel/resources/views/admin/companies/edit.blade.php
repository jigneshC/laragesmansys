@extends('layouts.apex')

@section('title',trans('company.label.edit_company'))


@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('company.label.edit_company') </h4>
       {{-- @include('partials.page_tooltip',['model' => 'companies','page'=>'form']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title"># {{ $company->name }}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            @include('admin.companies.company-tab')  
            
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="company" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a href="{{ url('/admin/companies') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('comman.label.back')
                                        </button>
                                    </a>
                                    

                                    @include('partials.form_notification')
                                    
                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                       @if ($errors->any())
                                            <ul class="alert alert-danger">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif

                                        {!! Form::model($company, [
                                            'method' => 'PATCH',
                                            'url' => ['/admin/companies', $company->id],
                                            'class' => 'form-horizontal',
                                            'files' => true,
                                            'autocomplete'=>'off'
                                        ]) !!}

                                        @include ('admin.companies.form', ['submitButtonText' => trans('comman.label.update')])

                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 

               
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection

