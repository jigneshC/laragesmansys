
<ul class="nav nav-tabs">
              <li class="nav-item">
                   @if(isset($company) && $company)
                            <a href="{!! url('admin/companies') !!}/{{$company->id}}/edit" class="nav-link {{ (request()->is('admin/companies/*/edit') || request()->is('admin/companies/create')) ? 'active' : '' }}"  aria-expanded="{{ (request()->is('admin/companies/*/edit') || request()->is('admin/companies/create')) ? 'true' : 'false' }}" >
                                <i class="fa fa-industry"></i> @lang('company.label.company')
                            </a>
                            
                            
                   @else
                            <a href="{!! url('admin/companies') !!}/create" class="nav-link {{ (request()->is('admin/companies/*/edit') || request()->is('admin/companies/create')) ? 'active' : '' }}"  aria-expanded="{{ (request()->is('admin/companies/*/edit') || request()->is('admin/companies/create')) ? 'true' : 'false' }}" >
                                <i class="fa fa-industry"></i> @lang('company.label.company')
                            </a>
                            
                   @endif
                   
              </li>
              <li class="nav-item">
                  
                  @if(isset($company) && $company)
                            <a href="{!! url('admin/company-modules') !!}/{{$company->id}}" class="nav-link {{ (request()->is('admin/company-modules/*')) ? 'active' : '' }}"  aria-expanded="{{ (request()->is('admin/company-modules/*')) ? 'true' : 'false' }}">
                                <i class="fa fa-sliders"></i>@lang('company.label.Assign_Company_Modules')
                            </a>
                  @else
                            <a href="javascript:void(0)" onclick="alert(' @lang('company.label.to_process_company_module_plaease_complete_company_form_first')')" class="nav-link {{ (request()->is('admin/company-modules/*')) ? 'active' : '' }}"  aria-expanded="{{ (request()->is('admin/company-modules/*')) ? 'true' : 'false' }}">
                                <i class="fa fa-sliders"></i>@lang('company.label.Assign_Company_Modules')
                            </a>
                  @endif
                            
                  
              </li>
            </ul>