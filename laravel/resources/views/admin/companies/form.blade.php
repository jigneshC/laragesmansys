
@include('admin.for_master.site_input3')

<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('company.label.name')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('user.label.email')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address',trans('company.label.address'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('address', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', trans('company.label.city'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('city', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('postcode') ? 'has-error' : ''}}">
    {!! Form::label('postcode', trans('company.label.postcode'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('postcode', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', trans('company.label.country'), ['class' => 'col-xl-4 col-sm-4 label-control','autocomplete'=>'off']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('country',$countries, null, ['class' => 'form-control']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_number_1', trans('company.label.phone_number_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {{ Form::hidden('code_phone_number_1',null, array('id' => 'code_phone_number_1')) }}
        {!! Form::text('phone_number_1', null, ['id'=>'phone_number_1','class' => 'form-control phone_number_1','autocomplete'=>'off','style'=>'padding-left: 84px !important','autocomplete'=>'off']) !!}
        {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_type_1', trans('company.label.phone_type_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('phone_type_1',$phoneTypes, null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row {{ $errors->has('business_type') ? 'has-error' : ''}}">
    <label for="business_type" class="col-xl-4 col-sm-4 label-control">@lang('company.label.business_type')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.companies.form_field.reference')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('business_type',$businessTypes, null, ['class' => 'form-control']) !!}
        {!! $errors->first('business_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@if(isset($company) && $company->logo != '')


    <div class="form-group row {{ $errors->has('logo') ? 'has-error' : ''}}">
        <label for="logo" class="col-xl-4 col-sm-4 label-control">@lang('company.label.logo')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.companies.form_field.reference')])
        </label>
        <div class="col-xl-8 col-sm-8">

            <img src="{!! asset('uploads/'.$company->logo) !!}" alt="" width="150">
            <br><br>
            {!! Form::file('logo', null, ['class' => 'form-control']) !!}
            {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@else
    <div class="form-group row {{ $errors->has('logo') ? 'has-error' : ''}}">
        <label for="logo" class="col-xl-4 col-sm-4 label-control">@lang('company.label.logo')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.companies.form_field.reference')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('logo', null, ['class' => 'form-control']) !!}
            {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif




<div class="form-group row {{ $errors->has('reference') ? 'has-error' : ''}}">
    <label for="reference" class="col-xl-4 col-sm-4 label-control">@lang('company.label.reference')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.companies.form_field.reference')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group row {{ $errors->has('geolocation') ? 'has-error' : ''}}">
    {!! Form::label('geolocation', "geolocation", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
		 {!! Form::hidden('longitude', null, ['class' => 'form-control search_longitude','id'=>"longitude"]) !!}
         {!! Form::hidden('latitude', null, ['class' => 'form-control search_latitude','id'=>"latitude"]) !!}
		 
         {!! Form::text('geolocation', null, ['class' => 'form-control','id'=>"geolocation_search"]) !!}
		  <div id="map-canvas" class="gmaps" style="height: 300px;width: 100%"></div>	
         {!! $errors->first('geolocation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('company.label.active'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rd1']) !!} <label for="rd1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false ,['id' => 'rd2']) !!} <label for="rd2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row">
    <lable class="col-xl-4 col-sm-4 label-control"></lable>
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-primary']) }}
    </div>
</div>



           


@push('js')

<script src="{!! asset('js/google-search-location-map.js')!!}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\config('settings.GOOGLE_MAP_API')}}&libraries=places&callback=initAutocomplete"
            async defer></script>
			
<script>

    var old_code1 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";
    var old_code2 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";;
    @if(isset($company) && $company->code_phone_number_1 != '')
        old_code1 ="{{$company->code_phone_number_1}}";
    @endif
    @if(isset($company) && $company->code_phone_number_2 != '')
        old_code2 ="{{$company->code_phone_number_2}}";
    @endif

    $("#phone_number_2").intlTelInput({
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });
    $("#phone_number_1").intlTelInput({
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });

    $("#phone_number_1").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_1").val(code);
    });
    $("#phone_number_2").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_2").val(code);
    });
    
    $("#phone_number_1").intlTelInput("setCountry", old_code1);
    $("#phone_number_2").intlTelInput("setCountry", old_code2);
    
    $('#country').change(function() {
        var code = $(this).val();
        $("#phone_number_1").intlTelInput("setCountry", code);
        $("#phone_number_2").intlTelInput("setCountry", code);
    });



</script>


@endpush
