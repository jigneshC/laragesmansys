@extends('layouts.apex')

@section('title',trans('company.label.show_company'))

@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('company.label.show_company') </h4>
       {{-- @include('partials.page_tooltip',['model' => 'companies','page'=>'index']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title">@lang('company.label.show_company') # {{ $company->name }}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                   <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#company" aria-expanded="true" ><i class="fa fa-user"></i> @lang('company.label.company') </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#sites" aria-expanded="false"><i class="fa fa-cubes"></i> @lang('site.label.sites') </a>
              </li>
            </ul>
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="company" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a href="{{ url('/admin/companies') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('comman.label.back')
                                        </button>
                                    </a>
                                    @if(Auth::user()->can('access.company.edit'))

                                    <a href="{{ url('/admin/companies/' . $company->id . '/edit') }}" title="Edit Company">
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                  aria-hidden="true"></i>
                                            @lang('comman.label.edit')
                                        </button>
                                    </a>
                                    @endif

                                    @if(Auth::user()->can('access.company.delete'))

                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/companies', $company->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Company',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Company'])."')"
                                    ))!!}
                                    {!! Form::close() !!}
                                    @endif
                                    
                                    <div class="next_previous">

                                       
                                    </div>  

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                       <div class="box-content ">
                                       
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                         <table class="table table-borderless">
                                        <tbody>
                                            @if(_MASTER)
                                            <tr>
                                                <th> @lang('website.label.website')</th>
                                                <td>{{ $company->_website->domain OR NULL }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <th>@lang('comman.label.id')</th>
                                                <td>#{{ $company->id }}</td>
                                            </tr>
                                            <tr>
                                                <th> @lang('comman.label.name') </th>
                                                <td> {{ $company->name }} </td>
                                            </tr>
											<tr>
                                                <th> @lang('user.label.email') </th>
                                                <td> {{ $company->email }} </td>
                                            </tr>
                                            @if(isset($company) && $company->logo != '')


                                            <tr>
                                                <th> @lang('company.label.logo') </th>
                                                <td> <img src="{!! asset('uploads/'.$company->logo) !!}" alt="" width="150"> </td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <th> @lang('site.label.address') </th>
                                                <td> {{ $company->address }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('site.label.city')</th>
                                                <td> {{ $company->city }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('company.label.postcode')</th>
                                                <td> {{ $company->postcode }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('company.label.country')</th>
                                                <td> {{ $company->country }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('company.label.phone_number_1')</th>
                                                <td> {{ $company->phone_number_1 }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('company.label.phone_type_1')</th>
                                                <td> {{ $company->phone_type_1 }} </td>
                                            </tr>
                                            
                                            <tr>
                                                <th>@lang('company.label.business_type')</th>
                                                <td> {{ $company->business_type }} </td>
                                            </tr>
                                            <tr>
                                                <th>@lang('company.label.reference')</th>
                                                <td> {{ $company->reference }} </td>
                                            </tr>

                                        </tbody>
                                    </table>          
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                     @if($company->qrcode)
                                                        @include('partials.qrcode',['qrcode' => $company->qrcode]) 
                                                    @endif
                                                </div>
                                            </div>



                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 
              <div class="tab-pane" id="sites" aria-labelledby="baseIcon-tab2" aria-expanded="false">
                
                  
                        <div class="row ">
                  <div class="col-md-12">
                      <div class="box bordered-box blue-border">
                          
                          <div class="box-content ">


                              <div class="row">
                                  <div class="col-md-6">

                                      @if(Auth::user()->can('access.site.create'))


                                        <a href="{{ url('/admin/sites/create') }}" class="btn btn-success btn-sm"
                                           title="Add New Company">
                                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                        </a>

                                        @endif


                                  </div>
                                  <div class="col-md-6">
                                      {!! Form::open(['method' => 'GET', 'url' => '/admin/sites', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                       {{--  <div class="input-group">
                                            <input type="text" class="form-control" name="search"
                                                   placeholder="@lang('comman.label.search')"
                                                   value="{!! request()->get('search') !!}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default no-radious" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div> --}}
                                        {!! Form::close() !!}
                                  </div>

                              </div>

                              <div class="table-responsive">
                                  <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>@lang('comman.label.id')</th>
                                        <th>@lang('comman.label.name')</th>
                                        <th>@lang('site.label.address')</th>
                                        <th>@lang('site.label.city')</th>
                                        <th>@lang('comman.label.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($company->sites as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->address }}</td>
                                        <td>{{ $item->city }}</td>
                                        <td>

                                            <a href="{{ url('/admin/sites/' . $item->id) }}" title="{{__('View')}}">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> @lang('comman.label.view')
                                                </button>
                                            </a>

                                            @if(Auth::user()->can('access.site.edit'))

                                            <a href="{{ url('/admin/sites/' . $item->id . '/edit') }}"
                                               title="Edit Site">
                                                <button class="btn btn-primary btn-xs"><i
                                                        class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> @lang('comman.label.edit')
                                                </button>
                                            </a>
                                            @endif

                                            @if(Auth::user()->can('access.site.delete'))

                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/sites', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => __('Delete Site'),
                                            'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Site'])."')"
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                              </div>

                          </div>
                      </div>
                  </div>
              </div>
                  
                  
              </div>
               
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection


