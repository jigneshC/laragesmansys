@extends('layouts.apex')

@section('title',trans('company.label.edit_company'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('company.label.move_user_to_domain', ['olddomain' => $oldweb->domain,'newdomain'=>$newweb->domain])  </div>
                {{--  @include('partials.page_tooltip',['model' => 'role','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/companies') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
                        
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           
							
							<div class="col-xl-12 col-sm-12">

                            {!! Form::open(['url' => '/admin/company-users/'.$company->id, 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}

                           <ul class="ul_list_style_n">
								<div class="checkbox">
									<input type="checkbox" name="used_list[]" class="parent" id="used_list"
														   data-parent="used_list"
														   value="used_list" checked >

									<label for="used_list"> <span class="text-danger">@lang('company.label.already_used_domain_user_as', ['olddomain' => $oldweb->domain]) </span></label>
								</div>
								<ul class="child">
								@foreach($webuser_used as $user)
									<li>
										<div class="checkbox">
										   <input type="checkbox" name="users[]" id="{{$user->id}}"
														  class="used_list"
														  checked
														  value="{{$user->id}}">
											<label for="{{$user->id}}">
												<span class="text-info "> {{$user->first_name}} {{$user->last_name}}</span> [ {{$user->email}} ]
											</label>
										</div>
									</li>
								
								@endforeach
								</ul>	
								
								<div class="checkbox">
									<input type="checkbox" name="un_used_list[]" class="parent" id="un_used_list"
														   data-parent="un_used_list"
														   value="un_used_list"  >

									<label for="un_used_list"> <span class="text-danger">@lang('company.label.other_domain_user', ['olddomain' => $oldweb->domain]) </span></label>
								</div>
								<ul class="child">
								@foreach($webuser as $user)
									<li>
										<div class="checkbox">
										   <input type="checkbox" name="users[]" id="{{$user->id}}"
														  class="un_used_list"
														  
														  value="{{$user->id}}">
											<label for="{{$user->id}}">
												<span class="text-info "> {{$user->first_name}} {{$user->last_name}}</span> [ {{$user->email}} ]
											</label>
										</div>
									</li>
								@endforeach
								</ul>	
						   </ul>

						   					
<div class="form-group row">
    <label class="col-xl-4 col-sm-4 label-control"></label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::submit(trans('company.label.move_selected_users_to_new_domain'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

                            {!! Form::close() !!}
							
							</div>
                            
	                </div>
					

	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection



@push('js')


    <script>

        $(document).ready(function () {

            $('.parent').change(function (e) {

                var $this = $(this),
                    parent = $this.data('parent'),
                    child = $('.' + parent);

                if ($this.is(':checked')) {
                    child.prop('checked', true);
                } else {
                    child.prop('checked', false);
                }
                e.preventDefault();
            });


        });

    </script>
@endpush