@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',"Statistics Report")

@push('css')
<link href="{!! asset('assets/charts/css/minified/components.min.css') !!}" rel="stylesheet" type="text/css">
<style>
 .display_in_print{
    display: none !important;
}
.content-wrapper{
    display: block !important;
}
.footer {
    position: inherit !important;
}
@media print {
    .hide-on-print{
        visibility: hidden;
    }
    .displaynone_on_print{
        display: none !important;
    }
    .display_in_print{
        display: block !important;
    }
    .app-sidebar{
        display: none !important;
    }
   
}


</style>
@endpush


@section('content')






<section class="taskboard-app-form bg-blue-grey bg-lighten-5 advance_search_section displaynone_on_print">
    <div class="card">
        <div class="card-block pt-3">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="card collapse-icon accordion-icon-rotate">
                      <div id="headingCollapse11" class="card-header">
                        <a data-toggle="collapse" href="#collapse11" aria-expanded="true" aria-controls="collapse11" class="card-title lead"> Statistics Report</a>
                      </div>
                      <div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse show" style="">
                        <div class="card-body">
                          <div class="card-block">
                              
                              
                              <div class="form-body">
                                  <div class="row">
                                      @if(_MASTER)
                                        <div class="col-xl-3 col-sm-4">
                                            <div class="form-group clearfix">
                                               {!! Form::select('_website_id',$_websites_pluck,(Session::has('filter_website'))? Session::get('filter_website') : null, ['class' => 'filter full-width','id'=>'filter_website_id']) !!}
                                            </div>
                                        </div>
                                            
                                        @endif
                                        
                                        <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                             <input type="text" name="filter_company" class="filter" id="filter_company" style="width: 100%;">
                                          </div>
                                      </div>
                                      <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                             <input type="text" name="filter_site" class="filter" id="filter_site" style="width: 100%;">
                                          </div>
                                      </div>
                                      <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                              <input type="text" name="filter_services" class="filter" id="filter_services" style="width: 100%;">
                                          </div>
                                      </div>
                                      
                                      <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                                 <div class="ticket-select-process">
                                                <div style="width: 100%">
                                                    <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;">
                                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                                        <span></span> <b class="caret"></b>
                                                    </div>

                                                </div>

                                            </div>
                                          </div>
                                      </div>
                                       <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                                {!! Form::select('duration',['weekly'=>'weekly','monthly'=>'monthly','yearly'=>'yearly'],'monthly', ['class' => 'filter full-width','id'=>'filter_duration']) !!}
                                          </div>
                                      </div>
                                        <div class="col-xl-3 col-sm-4">
                                          <div class="form-group clearfix">
                                                 <a href="javascript:void(0);" title="@lang('statastic.label.create_image')" class="btn btn-success btn-sm btn-filter" id="print_report_btn" >
                                                    <i class="fa fa-image" ></i> @lang('statastic.label.create_image')
                                                </a>
                                              
                                            </div>
                                      </div>
                                       
                                  </div>





                              </div>
                              
              
                          </div>
                        </div>
                      </div>






                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>

<section class="taskboard-app-form bg-blue-grey bg-lighten-5 advance_search_section">
    <div class="card">
        <div class="card-block pt-3">
            <div class="row">
                <div class="col-lg-12 col-xl-12">
                    <div class="card collapse-icon accordion-icon-rotate">
                      
                      <div id="collapse11" role="tabpanel" aria-labelledby="headingCollapse11" class="collapse show" style="">
                        <div class="card-body">
                          <div class="card-block">
                              
                             
                <div class="row">
                    <div class="col-lg-12">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="top_twenty"></div>
                        </div>
                    </div>
                </div>
            
           
                <div class="row">
                    <div class="col-sm-6">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_pie"></div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_column"></div>
                        </div>
                    </div>
                </div>
            
                          </div>
                        </div>
                      </div>

                       </div>
                  </div>
            </div>
        </div>
    </div>
    <div class="card display_in_print text-center">
                
                <div class="card-body">
                    <div class="card-block">
                       <div class="row">
                                        <div class="col-lg-12">
                                            <div style="width:100%" class="text-center">
                                                    <b>@lang('ticket.label.website')</b> <span class="website_selected"> </span>
                                                 | <b>@lang('company.label.company')</b> <span class="select_company"> </span>
                                                 | <b>@lang('site.label.site')</b> <span class="select_site"> </span>
                                                 | <b>@lang('service.label.service')</b> <span class="select_services"> </span>
                                                 | <b>@lang('statastic.label.duration')</b> <span class="select_duration"> </span>
                                            </div>
                                        </div>
                                     </div>
                        
                    </div>
                </div>
            </div>
</section>







<!-- /content area -->

@endsection

@push('js')

<script type="text/javascript" src="{!! asset('assets/charts/js/plugins/visualization/echarts/echarts.js') !!}"></script>


<script type="text/javascript" >

    var stat1 = <?php echo json_encode($graph_pie); ?>;
    var graph_x = <?php echo json_encode($graph_xy['graph_x']); ?>;
    var graph_y = <?php echo json_encode($graph_xy['graph_y']); ?>;
    var toptwnty_name = <?php echo json_encode($top_twenty['items']); ?>;
    var toptwnty_name_value = <?php echo json_encode($top_twenty['item_value']); ?>;
    var toptwenty_module_name = <?php echo json_encode($top_twenty['module_name']); ?>;
    var connect_pie = "";
    var connect_column = "";
    var basic_donut = "";
    var range_start ="";
    var range_end ="";
    $("#filter_website_id").select2();
    $("#filter_duration").select2();
    
    $("#filter_company").select2({
        placeholder: "@lang('comman.label.select_company')",
        allowClear: true,
        @if(Session::has('filter_company'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_company')}}, text: "{{Session::get('filter_company_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/companies/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website_id").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
    
    $("#filter_site").select2({
        placeholder: "@lang('comman.label.select_site')",
        allowClear: true,
        @if(Session::has('filter_site'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_site')}}, text: "{{Session::get('filter_site_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/sites/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website_id").val(),
                    company_id: $("#filter_company").val(),
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);

    $("#filter_services").select2({
        placeholder: "@lang('comman.label.select_services')",
        allowClear: true,
        @if(0 && Session::has('filter_services'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_services')}}, text: "{{Session::get('filter_services_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        
        ajax: {
            url: "{{ url('admin/services/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_services: params, // search term
                    site_id: $("#filter_site").val(),
                    _website_id: $("#filter_website_id").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
    
    graphDrow();
    
    $(function () {
        $('.filter').change(function() {
            var filter_id = $(this).attr('id');
            if(filter_id=="filter_website_id"){
                $("#filter_site").select2("val", "");
                $("#filter_services").select2("val", "");
                $("#filter_company").select2("val", "");
            }
            if(filter_id=="filter_company"){
                $("#filter_site").select2("val", "");
                $("#filter_services").select2("val", "");
            }
            if(filter_id=="filter_site"){
                $("#filter_services").select2("val", "");
            }
            getAndGraphRedrow();
        });
        
        function getAndGraphRedrow() {
             var formData = {range_start:range_start,range_end:range_end,"filter_companies":$("#filter_company").val(),"filter_website":$("#filter_website_id").val(),"filter_site":$("#filter_site").val(),"filter_services":$("#filter_services").val(),"duration":$("#filter_duration").val(),"x_limit":5};
            $.ajax({
                type: "get",
                url: "{{url('admin/statistics')}}",
                data: formData,
                success: function (data) {
                    stat1 = data.graph_pie;
                    graph_x = data.graph_xy.graph_x;
                    graph_y = data.graph_xy.graph_y;
                    toptwnty_name = (data.top_twenty['items'])? data.top_twenty['items'] : [];
                    toptwnty_name_value = (data.top_twenty['item_value'])? data.top_twenty['item_value']: [];
                    toptwenty_module_name = data.top_twenty['module_name'];
                    graphDrow();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
        
        /*************************daterange selection*****************/



            var start = moment.utc('2015-01-01','YYYY-MM-DD');
            var end = moment();
            
            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                if(range_start==""){
                    range_start = start.format('YYYY-MM-DD');
                    range_end = end.format('YYYY-MM-DD');
                }else{
                    range_start = start.format('YYYY-MM-DD');
                    range_end = end.format('YYYY-MM-DD');

                    getAndGraphRedrow();
                }


            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment()],
                    "@lang('comman.daterange.today')": [moment(), moment()],
                    "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    "@lang('comman.daterange.last7day')": [moment().subtract(6, 'days'), moment()],
                    "@lang('comman.daterange.last30day')": [moment().subtract(29, 'days'), moment()],
                    "@lang('comman.daterange.thismonth')": [moment().startOf('month'), moment().endOf('month')],
                    "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    "@lang('comman.daterange.thisyear')": [moment().startOf('year'), moment().endOf('year')],
                    "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
                }
            }, cb);

             cb(start, end);

            $(".range_inputs").find(".applyBtn").html("@lang('comman.daterange.applyBtn')");
            $(".range_inputs").find(".cancelBtn").html("@lang('comman.daterange.cancelBtn')");
            $(".ranges").find('ul li:last-child').html("@lang('comman.daterange.customeRange')");

            

    });
    
    
    function graphDrow() {
        require.config({
            paths: {
                echarts: "{!! asset('/assets/charts/js/plugins/visualization/echarts') !!}"
            }
        });
        require(['echarts', 'echarts/theme/limitless', 'echarts/chart/line', 'echarts/chart/bar', 'echarts/chart/pie', 'echarts/chart/scatter', 'echarts/chart/k', 'echarts/chart/radar', 'echarts/chart/gauge'],
                // Charts setup
                        function (ec, limitless) {
                            // Initialize charts
                            // ------------------------------

                            connect_pie = ec.init(document.getElementById('connect_pie'), limitless);
                            connect_column = ec.init(document.getElementById('connect_column'), limitless);
                            basic_donut = ec.init(document.getElementById('top_twenty'), limitless);


                            // Pie options
                            connect_pie_options = {
                                // Add title
                                title: {
                                    text: 'Overall Ticket Report',
                                    subtext: "@lang('statastic.label.Tickets')",
                                    x: 'center'
                                },
                                // Add tooltip
                                tooltip: {
                                    trigger: 'item',
                                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                                },
                                // Add legend
                                legend: {
                                    orient: 'vertical',
                                    x: 'left',
                                    data: ["@lang('statastic.label.Tickets')","@lang('statastic.label.planned_activity')", "@lang('statastic.label.alarm')","@lang('statastic.label.event')"]
                                },
                                // Enable drag recalculate
                                calculable: true,
                                // Add series
                                series: [{
                                        name: "@lang('statastic.label.Tickets')",
                                        type: 'pie',
                                        radius: '75%',
                                        center: ['50%', '57.5%'],
                                        data: stat1
                                    }],
									"color": ["#336699", "#A9E023","#FD1312", "#F8A12D"]
                            };

                            // Column options
                            connect_column_options = {
                                // Setup grid
                                grid: {
                                    x: 40,
                                    x2: 47,
                                    y: 35,
                                    y2: 25
                                },
                                // Add tooltip
                                tooltip: {
                                    trigger: 'axis',
                                    axisPointer: {
                                        type: 'shadow'
                                    }
                                },
                                // Add legend
                                legend: {
                                    data: ["@lang('statastic.label.Tickets')","@lang('statastic.label.planned_activity')", "@lang('statastic.label.alarm')","@lang('statastic.label.event')"]
                                },
                                // Add toolbox
                                toolbox: {
                                    show: true,
                                    orient: 'vertical',
                                    x: 'right',
                                    y: 35,
                                    feature: {
                                      /*  mark: {
                                            show: true,
                                            title: {
                                                mark: 'Markline switch',
                                                markUndo: 'Undo markline',
                                                markClear: 'Clear markline'
                                            }
                                        },*/
                                        magicType: {
                                            show: true,
                                            title: {
                                                line: 'Switch to line chart',
                                                bar: 'Switch to bar chart',
                                                stack: 'Switch to stack',
                                                tiled: 'Switch to tiled'
                                            },
                                            type: ['line', 'bar', 'stack', 'tiled']
                                        },
                                        restore: {
                                            show: true,
                                            title: 'Restore'
                                        },
                                        /*next: {
                                            show: true,
                                            title: 'Next',
                                            icon: "{!! asset('/assets/images/plugins/lightbox/next.png') !!}",
                                            onclick: function (){
                                                    // do something
                                                }
                                            
                                        },
                                        prev: {
                                            show: true,
                                            title: 'Prev',
                                            icon: "{!! asset('/assets/images/plugins/lightbox/prev.png') !!}",
                                            onclick: function (){
                                                    // do something
                                                }
                                            
                                        },*/
                                       
                                    }
                                },
                                // Enable drag recalculate
                                calculable: true,
                                // Horizontal axis
                                xAxis: [{
                                        type: 'category',
                                        data: graph_x
                                    }],
                                // Vertical axis
                                yAxis: [{
                                        type: 'value',
                                        splitArea: {show: true}
                                    }],
                                // Add series
                                series: graph_y,
								"color": ["#336699", "#A9E023","#FD1312", "#F8A12D"]
                            };


                            basic_donut_options = {

                                    // Add title
                                    title: {
                                        text: 'Top Twenty',
                                        subtext: toptwenty_module_name,
                                        x: 'center'
                                    },

                                    // Add legend
                                    legend: {
                                        orient: 'vertical',
                                        x: 'left',
                                        data: toptwnty_name
                                    },

                                    // Display toolbox
                                    toolbox: {
                                        show: true,
                                        orient: 'vertical',
                                        feature: {
                                        }
                                    },

                                    // Enable drag recalculate
                                    calculable: true,

                                    // Add series
                                    series: [
                                        {
                                            name: 'Browsers',
                                            type: 'pie',
                                            radius: ['50%', '70%'],
                                            center: ['50%', '57.5%'],
                                            itemStyle: {
                                                normal: {
                                                    label: {
                                                        show: true
                                                    },
                                                    labelLine: {
                                                        show: true
                                                    }
                                                },
                                                emphasis: {
                                                    label: {
                                                        show: true,
                                                        formatter: '{b}' + '\n\n' + '{c} ({d}%)',
                                                        position: 'center',
                                                        textStyle: {
                                                            fontSize: '17',
                                                            fontWeight: '500'
                                                        }
                                                    }
                                                }
                                            },

                                            data: toptwnty_name_value
                                        }
                                    ]
                                };
            
                            // Connect charts
                            connect_pie.connect(connect_column);
                            connect_column.connect(connect_pie);

                            connect_pie.setOption(connect_pie_options);
                            connect_column.setOption(connect_column_options);
                            basic_donut.setOption(basic_donut_options);

                            window.onresize = function () {
                                setTimeout(function () {
                                    connect_pie.resize();
                                    connect_column.resize();
                                    basic_donut.resize();
                                }, 200);
                            }


                        }
                );
            }
            
            $("#print_report_btn").click(function () {
                var website_selected = ($("#filter_website_id").val())? $("#filter_website_id").select2('data').text : "-" ;
                $(".website_selected").html(website_selected);
                
                var select_company = ($("#filter_company").val())? $("#filter_company").select2('data').text : "-" ;
                $(".select_company").html(select_company);
                
                var select_site = ($("#filter_site").val())? $("#filter_site").select2('data').text : "-" ;
                $(".select_site").html(select_site);
                
                var select_services = ($("#filter_services").val())? $("#filter_services").select2('data').text : "-" ;
                $(".select_services").html(select_services);
                
                $(".select_duration").html(range_start+" to "+range_end);
                
               
                window.print();
            });

</script>

@endpush
 