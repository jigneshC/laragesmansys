


<div class="form-group row {{ $errors->has('domain') ? 'has-error' : ''}}">
    <label for="domain" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('website.label.domain')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.website.form_field.domain')])
       
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('domain', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
		<p>i.e test.gesmansys.com </p>
        {!! $errors->first('domain', '<p class="help-block">:message</p>') !!}
		{!! $errors->first('domains', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('logo_image') ? 'has-error' : ''}}">
    <label for="logo_image" class="col-xl-4 col-sm-4 label-control">@lang('comman.label.logo_image')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.website.form_field.logo_image')])
    </label>
    <div class="col-xl-8 col-sm-8">

        @if(isset($website) && $website->refimages != '')
            <img src="{!! asset('uploads/website/'.$website->refimages->image) !!}" alt="" width="150">
            <br><br>
        @endif

        {!! Form::file('logo_image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('logo_image', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description',trans('comman.label.description'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('description', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.name')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 @if(!isset($website))
     @include ('admin.websites.user')
 @endif
<div class="form-group row {{ $errors->has('master') ? 'has-error' : ''}}">
    <label for="master" class="col-xl-4 col-sm-4 label-control">@lang('website.label.master')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.website.form_field.master')])
    </label>
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('master', '1',false ,['id' => 'rd2']) !!} <label for="rd2">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('master', '0', true, ['id' => 'rd1']) !!} <label for="rd1">No</label>
        </div>
        {!! $errors->first('master', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 
 <div class="form-group row {{ $errors->has('enable_charge') ? 'has-error' : ''}}">
    <label for="enable_charge" class="col-xl-4 col-sm-4 label-control">@lang('website.label.enable_charge')
    </label>
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('enable_charge', 1,null ,['id' => 'ec2']) !!} <label for="ec2">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('enable_charge', 0, null, ['id' => 'ec1']) !!} <label for="ec1">No</label>
        </div>
        {!! $errors->first('enable_charge', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 
 <div class="form-group row {{ $errors->has('enable_qrcode') ? 'has-error' : ''}}">
    <label for="enable_qrcode" class="col-xl-4 col-sm-4 label-control">@lang('website.label.enable_qrcode')
    </label>
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('enable_qrcode', 1,null ,['id' => 'eq2']) !!} <label for="eq2">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('enable_qrcode', 0, null, ['id' => 'eq1']) !!} <label for="eq1">No</label>
        </div>
        {!! $errors->first('enable_qrcode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
 
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.status')
    </label>

    <div class="col-xl-8 col-sm-8">
        {!! Form::select('status', ['active'=>'Active','inactive'=>'Inactive'],null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row">
    <label class="col-xl-4 col-sm-4 label-control"></label>
    <div class="col-md-offset-4 col-xl-8 col-sm-8 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>
