@extends('layouts.apex')

@section('title',trans('website.label.show_website'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('website.label.show_website') # {{ $website->name }} </div>
                {{--  @include('partials.page_tooltip',['model' => 'website','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/websites') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')</button></a>
                        
                        <a href="{{ url('/admin/websites/' . $website->id . '/edit') }}" title="Edit Website"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('comman.label.edit')</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/websites', $website->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Website',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Website'])."')"
                            ))!!}
                        {!! Form::close() !!}
                        
	                 <div class="next_previous pull-right">
                            @if(!empty($website->previous()))
                                <a class="btn btn-warning btn-xs" href="{{ $website->previous()->id }}"> Previous </a>
                            @endif 
                            @if(!empty($website->next()))
                                &nbsp;&nbsp;|&nbsp;&nbsp;<a class="btn btn-warning btn-xs" href="{{ $website->next()->id }}"> Next </a>
                            @endif 
                             
                            
                          </div>  
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
									<div class="col-md-6">
                                   <div class="table-responsive">
                                         <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th><td>{{ $website->id }}</td>
                                                </tr>
                                                <tr><th> @lang('website.label.domain') </th><td> {{ $website->domain }} </td></tr>
                                                <tr>
                                                    <th> @lang('moduleunit.label.unit')</th>
                                                    <td>{{ $website->units }} <a href='#' class='credit_unit' title='@lang('moduleunit.label.creadit_unit')' data-id="{{$website->id}}"><i class='fa fa-plus action_icon'></i></a></td>
                                                </tr>
                                                <tr><th> @lang('comman.label.description') </th><td> {{ $website->description }} </td></tr>
                                                <tr><th> @lang('comman.label.name') </th><td> {{ $website->name }} </td></tr>
                                            </tbody>
                                        </table>
                                    </div>
									</div>
									<div class="col-md-6">
                                         @if($website->qrcode)
                                            @include('partials.qrcode',['qrcode' => $website->qrcode]) 
                                        @endif
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

@include("admin.moduleunit.unitcreditmodel")

@endsection








