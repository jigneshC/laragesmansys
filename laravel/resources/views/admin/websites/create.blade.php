@extends('layouts.apex')

@section('title',trans('website.label.create_website'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('website.label.create_website') </div>
                {{-- @include('partials.page_tooltip',['model' => 'website','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/websites') }}" title="Back"><button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')</button></a>
                        
	                
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
							
							<div class="col-xl-6 col-sm-12">

                            {!! Form::open(['url' => '/admin/websites', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}

                            @include ('admin.websites.form')

                            {!! Form::close() !!}
							
							</div>
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection






@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                      <div class="box-header blue-background">
                          <div class="title">
                              <i class="icon-circle-blank"></i>
                              
                          </div>
                          <div class="actions">
                              
                          </div>

                      </div>
                      <div class="box-content ">




                        
                        <br />
                        <br />

                        




                    </div>
                </div>
            </div>
        </div>
@endsection


