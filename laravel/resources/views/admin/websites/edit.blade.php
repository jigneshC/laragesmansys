@extends('layouts.apex')
@section('title',trans('website.label.edit_website'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('website.label.edit_website') # {{ $website->name }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'website','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/websites') }}" title="Back"><button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')</button></a>
                         <div class="next_previous pull-right">
                            @if(!empty($website->previous()))
                            
                                <a class="btn-link-back" href="{{ url('/admin/websites/' . $website->previous()->id . '/edit') }}"> Previous </a> |
                            @endif 
                            @if(!empty($website->next()))
                                &nbsp;&nbsp;|&nbsp;&nbsp;<a class="btn-link-back" href="{{ url('/admin/websites/' . $website->next()->id . '/edit') }}"> Next </a>
                            @endif 
                        </div>   
                        
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($website, [
                                'method' => 'PATCH',
                                'url' => ['/admin/websites', $website->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.websites.form', ['submitButtonText' => trans('comman.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection

