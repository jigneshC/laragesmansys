<div class="form-group row {{ $errors->has('username') ? 'has-error' : ''}}">
    <label for="username" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.username')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('email') ? ' has-error' : ''}}">
    <label for="email" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('user.label.email')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('password') ? ' has-error' : ''}}">
    <label for="password" class="col-xl-4 col-sm-4 label-control">
        @if(!isset($user_roles))
            <span class="field_compulsory">*</span>
        @endif
        @lang('user.label.password')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('language') ? ' has-error' : ''}}">
    {!! Form::label('language', trans('people.label.language'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('language',$languages, null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('language', '<p class="help-block">:message</p>') !!}
    </div>
</div>
