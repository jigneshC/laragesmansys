@extends('layouts.apex')

@section('title',trans('website.label.websites'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('website.label.websites') </div>
        {{--    @include('partials.page_tooltip',['model' => 'website','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                            @if(Auth::user()->can('access.website.create'))
                            <a href="{{ url('/admin/websites/create') }}" class="btn btn-success btn-sm  ticket-add-new "
                               title="Add New Website">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                            </a>
                            @endif
							
							@include("admin.for_master.export_pdf",["module"=>"_websites","fields"=>['is_deleted_record']]) 
							
                            
                          </div>
                         </div>
                        <div class="col-9">
                          
                            @if(_MASTER)
								 @include("admin.for_master.filter_deleted") 
                            @endif
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('comman.label.id')</th>
                                <th data-priority="2">@lang('website.label.master')</th>
                                <th data-priority="3">@lang('website.label.domain')</th>
                                <th data-priority="5">@lang('moduleunit.label.unit')</th>
                                <th data-priority="6">@lang('comman.label.name')</th>
                                <th data-priority="7">@lang('user.label.role')</th>
                                <th data-priority="8">@lang('comman.label.description')</th>
                                <th data-priority="4">@lang('comman.label.action')</th>
                            </thead>

                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@include("admin.moduleunit.unitcreditmodel")  

@endsection





@push('js')
<script>

    var url = "{{url('admin/websites')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            {
                "data": null,
                "name": "master",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    if(o.master){
                        return '<span class="label label-success"><i class="fa fa-check"></i></span>';
                    }else{
                        return '<span class="label label-default"><i class="fa fa-remove"></i></span>';
                    }
                }
            },
            {
                "data": null,
                "name": "domain",
                "render": function (o) {
                    return '<a href="//'+o.domain+'" target="_blank">'+o.domain+'</a>';
                }
            },
            { 
                "data": null,
                "name":"units",
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    @if(Auth::user()->can('access.canaddcredit'))
                        return "<a href='#' class='credit_unit' title='@lang('moduleunit.label.creadit_unit')' data-id="+o.id+"><i class='fa fa-plus action_icon'></i></a>"+o.units;
                    @else
                         return o.units;
                    @endif
                }
            },
            { "data": "name","name":"name"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o.roles){
                        var rol = "";
                        for(var i=0;i<o.roles.length;i++){
                            rol = rol + o.roles[i].label;
                            if(i!=o.roles.length-1){
                                rol = rol+" , ";
                            }
                        }
                        return rol;
                    }else{
                        return "";
                    }
                }
            },
            { "data": "description","name":"description"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.website.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.website.delete'))
                        d = "<a href='javascript:void(0);' deleted_at="+o.deleted_at+" class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";


					if(o.deleted_at && o.deleted_at!=""){
						var jio = "<a href='javascript:void(0);' class='recover-item' moduel='website' data-id="+o.id+" title='@lang('tooltip.common.icon.recover')'><i class='fa fa-repeat action_icon'></i></a>"+d;
						
						return jio;
					}else{
						return v+d+e;
					}
                    

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/website/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
				@if(_MASTER)
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
                // d.filter_website = $('#filter_website').val();
            }
        }
    });

	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		var deleted_at = $(this).attr('deleted_at');
		
		var r = false;
		if(deleted_at && deleted_at != "" && deleted_at != "undefined" ){
			r = confirm("@lang('comman.js_msg.confirm_for_delete_perminent',['item_name'=>'Website'])");	
		}else{
			r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Website'])");
		}
		if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete_recover',['item_name'=>'Website'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush
