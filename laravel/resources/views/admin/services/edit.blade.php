@extends('layouts.apex')
@section('title',trans('service.label.edit_service'))

@section('content')


   <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('service.label.edit_service') #{{ $service->name }}</div>
                {{--   @include('partials.page_tooltip',['model' => 'services','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/services') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($service, [
                            'method' => 'PATCH',
                            'url' => ['/admin/services', $service->id],
                            'class' => 'form-horizontal',
                            'files' => true,
                            'autocomplete'=>'off'
                        ]) !!}

                        @include ('admin.services.form', ['submitButtonText' => trans('comman.label.update')])

                        {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection

