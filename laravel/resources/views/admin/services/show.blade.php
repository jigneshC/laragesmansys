@extends('layouts.apex')

@section('title',trans('service.label.show_service'))

@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('service.label.show_service') </h4>
       {{-- @include('partials.page_tooltip',['model' => 'services','page'=>'index']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title">{{ $service->name }}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            <ul class="nav nav-tabs">
              <li class="nav-item">
              <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#service" aria-expanded="true"><i class="fa fa-cubes"></i> @lang('service.label.service')</a>
              </li>
              <li class="nav-item">
              <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#peoples" aria-expanded="false"><i class="fa fa-users"></i> @lang('service.label.people')</a>
              </li>
              <li class="nav-item">
              <a class="nav-link " id="baseIcon-tab3" data-toggle="tab" aria-controls="tabIcon3" href="#subjects" aria-expanded="false"><i class="fa fa-tags"></i> @lang('subject.label.subject')</a>
              </li>
            </ul>
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="service" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                
                                <div class="card-body">
                                    <div class="px-3">
                                       <div class="box-content clearfix ">
                                       
									   <div class="pull-left">
										 <a href="{{ url('/admin/services') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left"
                                                                                  aria-hidden="true"></i> @lang('comman.label.back')
                                        </button>
                                    </a>
									</div>
									
									<div class="pull-right">

                                    @if(Auth::user()->can('access.service.edit'))
                                    <a href="{{ url('/admin/services/' . $service->id . '/edit') }}" title="Edit Service">
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                  aria-hidden="true"></i>
                                            @lang('comman.label.edit')
                                        </button>
                                    </a>
                                    @endif

                                    @if(Auth::user()->can('access.service.delete'))
                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/services', $service->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Service',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Service'])."')"
                                    ))!!}
                                    {!! Form::close() !!}
                                    @endif
									</div>



                                            <br/>
                                            <br/>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                @if(_MASTER)
                                                                <tr>
                                                                    <th> @lang('website.label.website')</th>
                                                                    <td>{{ $service->_website->domain OR NULL }}</td>
                                                                </tr>
                                                                @endif
                                                                <tr>
                                                                    <th>@lang('comman.label.id')</th>
                                                                    <td>#{{ $service->id }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th> @lang('service.label.site')</th>
                                                                    <td> {{ $service->site->name or null }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <th> @lang('comman.label.status')</th>
                                                                    <td> {{ $service->name }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <th> @lang('service.label.description')</th>
                                                                    <td> {{ $service->description }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('service.label.manager')</th>
                                                                    <td> {{ $service->manager->name or null }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <th> @lang('comman.label.status') </th>
                                                                    <td> {{ $service->active==true?'True':'False' }} </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    @if($service->qrcode)
                                                        @include('partials.qrcode',['qrcode' => $service->qrcode])
                                                    @endif 
                                                </div>
                                            </div>



                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
              <div class="tab-pane" id="peoples" aria-labelledby="baseIcon-tab2" aria-expanded="false">
                
                  
                  <div class="row ">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                    
                    <div class="box-content ">


                        <div class="row">
                            <div class="col-md-6">

                                @if(Auth::user()->can('access.people.create'))
                                <a href="{{ url('/admin/peoples/create') }}" class="btn btn-success btn-sm"
                                   title="Add New People">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                                @endif


                            </div>
                            <div class="col-md-6">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/peoples', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="@lang('comman.label.search')"
                                           value="{!! request()->get('search') !!}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default no-radious" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>@lang('comman.label.id')</th>
                                        <th>@lang('people.label.user')</th>
                                        <th>@lang('people.label.title')</th>
                                        <th>@lang('people.label.first_name')</th>
                                        <th>@lang('comman.label.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($service->peoples))
                                    @foreach($service->peoples as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->user->name or null }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->first_name }}</td>
                                        <td>
                                            <a href="{{ url('/admin/peoples/' . $item->id) }}"
                                               title="View People">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i>
                                                    @lang('comman.label.view')
                                                </button>
                                            </a>

                                            @if(Auth::user()->can('access.people.edit'))

                                            <a href="{{ url('/admin/peoples/' . $item->id . '/edit') }}"
                                               title="Edit People">
                                                <button class="btn btn-primary btn-xs"><i
                                                        class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> @lang('comman.label.edit')
                                                </button>
                                            </a>
                                            @endif

                                            @if(Auth::user()->can('access.people.delete'))

                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/peoples', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete People',
                                            'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'People'])."')"
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {{--<div class="pagination-wrapper"> {!! $peoples->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
                  
                  
              </div>
              <div class="tab-pane" id="subjects" aria-labelledby="baseIcon-tab3" aria-expanded="false">
                    
                    <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                    
                    <div class="box-content ">


                        <div class="row">
                            <div class="col-md-6">

                                @if(Auth::user()->can('access.subject.create'))
                                <a href="{{ url('/admin/subjects/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Subject">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                                @endif


                            </div>
                            <div class="col-md-6">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/subject', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="@lang('comman.label.search')"
                                           value="{!! request()->get('search') !!}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default no-radious" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>@lang('comman.label.id')</th>
                                        <th>@lang('subject.label.subject')</th>
                                        <th>@lang('subject.label.type')</th>
                                        <th>@lang('subject.label.subject_hrs')</th>
                                        <th>@lang('comman.label.action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($service->subjects))
                                    @foreach($service->subjects as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name or null }}</td>
                                        <td>{{ $item->subject_type }}</td>
                                        <td>{{ $item->time or null}}</td>
                                        <td>
                                            <a href="{{ url('/admin/subjects/' . $item->id) }}"
                                               title="View People">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i>
                                                    @lang('comman.label.view')
                                                </button>
                                            </a>

                                            @if(Auth::user()->can('access.subject.edit'))

                                            <a href="{{ url('/admin/subjects/' . $item->id . '/edit') }}"
                                               title="Edit People">
                                                <button class="btn btn-primary btn-xs"><i
                                                        class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> @lang('comman.label.edit')
                                                </button>
                                            </a>
                                            @endif

                                            @if(Auth::user()->can('access.subject.delete'))

                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/subjects', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete Subject',
                                            'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Subject'])."')"
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {{--<div class="pagination-wrapper"> {!! $peoples->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
                  
                  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>







@endsection
