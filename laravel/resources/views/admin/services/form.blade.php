

<div class="row">
    <div class="col-xl-6 col-sm-12">
        @include('admin.for_master.site_input3')


<div class="form-group row {{ $errors->has('company_id') ? 'has-error' : ''}}">
        <label for="service_id" class="col-xl-4 col-sm-4 label-control">@lang('site.label.company')</label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('company_id',[], null, ['class' => 'form-control','required'=>'required','id'=>'company_id']) !!}
            {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group row {{ $errors->has('site_id') ? 'has-error' : ''}}">
    <label for="site_id" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('service.label.site')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.services.form_field.site')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('site_id[]',[], [], ['class' => '','id'=>'site_id', 'multiple' => true]) !!}
        {!! $errors->first('site_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-xl-4 col-sm-4 label-control line-height-two">
        <span class="field_compulsory">*</span>@lang('service.label.name')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('service.label.description'), ['class' => 'col-xl-4 col-sm-4 label-control line-height-two']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('description', null, ['class' => 'form-control ','autocomplete'=>'off']) !!}
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('manager_id') ? 'has-error' : ''}}">
    <label for="manager_id" class="col-xl-4 col-sm-4 label-control">@lang('service.label.manager')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.services.form_field.manager')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('manager_id',$users, null, ['class' => 'form-control','id'=>'manager_id']) !!}
        {!! $errors->first('manager_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('service.label.active'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rd1']) !!} <label for="rd1">{{ trans('service.label.yes') }}</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false ,['id' => 'rd2']) !!} <label for="rd2">{{ trans('service.label.no') }}</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
        <lable class="col-xl-4 col-sm-4 label-control"></lable>
        <div class="col-xl-8 col-sm-8">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
    </div>

 </div>
 <div class="col-md-6"></div>
    </div>
        
@push('js')
<script>
    var sub_search_url ="{{url('admin/servicesoptionfilterbywebsite')}}";
    var selected_site = [];
    var selected_user = "0";
    var selected_company = "0";
    var data_site = [];

    @if(isset($service))
        selected_user = "{{$service->manager_id}}";
        
    @endif

    @if(isset($service))
        selected_site = <?php echo json_encode($selected_sites); ?>;
        selected_company = "{{$service->company_id}}";
    @endif

    initSelect();
    function initSelect(){
        @if(_MASTER)
        var filter_id = $('#_website_id').val();
        @else
        var filter_id = "{{_WEBSITE_ID}}";
        @endif


        
        $("#manager_id").html("");
        $("#company_id").html("");

        $.ajax({
            type: "get",
            url: sub_search_url,
            data:{_website_id:filter_id},
            success: function (result) {
                    data = result.data.compoanies;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_company) { selected = "selected=selected"; }
                        $("#company_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    data_site = result.data.site;
                    
                data = result.data.user;
                for(var i=0;i<data.length;i++){
                    var selected="";
                    if (data[i]['id'] == selected_user) { selected = "selected=selected"; }
                    $("#manager_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                }

                data = result.data.site;
                initSelectSite();
                
            },
            error: function (xhr, status, error) {
            }
        });
    }
    function initSelectSite(){
		$('#site_id').select2("destroy");
                $("#site_id").html("");
        
		var selected_se = $("#company_id").val();
		data = data_site;
                for(var i=0;i<data.length;i++){
                    var selected="";
                    if(data[i]['company_id'] == selected_se){
                        if(jQuery.inArray(data[i]['id'], selected_site) !== -1){
                            selected = "selected=selected";
                        }
			$("#site_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
               }
               $('#site_id').select2({
                    tokenSeparators: [",", " "]
               });
               
	}

    $('#_website_id').change(function() {
        initSelect();
    });
    $('#company_id').change(function() {
        initSelectSite();
    });


</script>


@endpush

