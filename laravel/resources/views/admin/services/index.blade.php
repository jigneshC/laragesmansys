@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('ticket.label.ticket'))

@push('css')

@endpush

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('service.label.service')</div>
        {{--  @include('partials.page_tooltip',['model' => 'services','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                           @if(Auth::user()->can('access.service.create'))
                                <a href="{{ url('/admin/services/create') }}" class="btn btn-success btn-sm ticket-add-new"
                                   title="Add New Service">
                                    <i class="fa fa-plus" aria-hidden="true"></i>  @lang('comman.label.add_new')

                                </a>
                            @endif
							
							@include("admin.for_master.export_pdf",["module"=>"services","fields"=>['is_deleted_record','filter_website']])
							
                          </div>
                         </div>
                        <div class="col-9">
                          
                             @if(_MASTER)
								@include("admin.for_master.filter_deleted") 
							
                               {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter pull-right','required'=>'required','id'=>'filter_website','style'=>'width:200px']) !!}
                            @endif
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <table class="table table-striped table-bordered base-style responsive datatable">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('comman.label.id')</th>
                                <th data-priority="4">@lang('service.label.site')</th>
                                <th data-priority="2">@lang('comman.label.name')</th>
                                <th data-priority="6">@lang('service.label.description')</th>
                                @if(_MASTER)
                                    <th data-priority="5">@lang('website.label.website')</th>
                                @endif
                                <th data-priority="3">@lang('comman.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection







@push('js')
<script>

    var url = "{{url('admin/services')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            {
                "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o.sites){
                        var rol = "";
                        for(var i=0;i<o.sites.length;i++){
                            rol = rol + o.sites[i].name;
                            if(i!=o.sites.length-1){
                                rol = rol+" , ";
                            }
                        }
                        return rol;
                    }else{
                        return "";
                    }
                }
            },
            { "data": "name","name":"name"},
            { "data": "description","name":"description"},
            @if(_MASTER)
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o._website){
                        return o._website.domain;
                    }else{
                        return "";
                    }
                }
            },
            @endif
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";
					

                    @if(Auth::user()->can('access.service.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.service.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' deleted_at="+o.deleted_at+" data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";

					if(o.deleted_at && o.deleted_at!=""){
						return "<a href='javascript:void(0);' class='recover-item' moduel='services' data-id="+o.id+" title='@lang('tooltip.common.icon.recover')'><i class='fa fa-repeat action_icon'></i></a>" + d;
					}else{
						return v+d+e;	
					}

                    

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/services/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                 @if(_MASTER)
					d.filter_website = $('#filter_website').val();
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
            }
        }
    });
    $('.filter').change(function() {
        datatable.fnDraw();
    });
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });
    $("#filter_website").select2();


    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var deleted_at = $(this).attr('deleted_at');
		
		var r = false;
		if(deleted_at && deleted_at != "" && deleted_at != "undefined" ){
			r = confirm("@lang('comman.js_msg.confirm_for_delete_perminent',['item_name'=>'Service'])");	
		}else{
			r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Service'])");
		}
        
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
	
	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete_recover',['item_name'=>'Service'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

