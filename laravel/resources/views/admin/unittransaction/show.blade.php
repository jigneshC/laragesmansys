@extends('layouts.apex')

@section('title',trans('unittransaction.label.unittransactions'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> </div>
                {{--  @include('partials.page_tooltip',['model' => 'subject','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            
	           <div class="card-body p-3">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-6 text-left">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800"><strong> {{$transaction->website->domain}} </strong></li>
                            <li>@lang('unittransaction.label.domain_id') :  <strong>{{ $transaction->website_id }}</strong> </li>
                            <li>@lang('unittransaction.label.reference') : <strong>{{ $transaction->reference }}</strong> </strong></li>
                            <li>@lang('unittransaction.label.transaction_date') : <strong>{{ $transaction->created_at->format('Y-m-d') }}</strong></li>
                            
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <h3>Tranasction Detail : # {{$transaction->id}}</h3>
                        <p class="pb-3">
                            @if($transaction->unit_type=="credit") @lang('unittransaction.label.credited_unit')  @else @lang('unittransaction.label.debited_unit') @endif  :  {{$transaction->unit}}
                        </p>
                        
                    </div>
                </div>
               @if($transaction->itemsobj && isset($itemsobj))
                @php( $unit_charge_lables = trans('moduleunit.unit_charge_lable'))
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>@lang('unittransaction.label.module_items')</th>
                                        <th>@lang('unittransaction.label.unit_charge_per_day')</th>
                                        <th class="text-right">@lang('unittransaction.label.free_items')</th>
                                        <th class="text-right">@lang('unittransaction.label.total_items')</th>
                                        <th class="text-right">@lang('unittransaction.label.total_charge')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($itemsobj as $mod => $item)
                                        <tr>
                                          <td> {{$unit_charge_lables[$mod]}}</td>
                                          <td>{{$item->unit_charge_per_item}}</td>
                                          <td class="text-right">
                                          <div >{{$item->free_qty}}</div>
                                          </td>
                                          <td class="text-right">
                                          <div >{{$item->total_qty}}</div>
                                          </td>
                                          <td class="text-right">
                                            <div >{{$item->net_unit_charge}}</div>
                                          </td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-right" > @lang('unittransaction.label.net_chargable_units_day') : <strong> {{$transaction->unit}} </strong></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               @endif
            </div>
        </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection

