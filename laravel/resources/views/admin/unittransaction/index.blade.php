@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('unittransaction.label.unittransaction'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('unittransaction.label.unittransaction') </div>
        {{--   @include('partials.page_tooltip',['model' => 'unittransaction','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                          
                          </div>
                         </div>
                        <div class="col-9">
                          
                              @if(_MASTER)
                                {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter pull-right','required'=>'required','id'=>'filter_website','style'=>'width:200px']) !!}
                            @endif
                           
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" width="100%">
                                <thead>
                                <tr>
                                    <th data-priority="1">@lang('comman.label.id')</th>
                                    <th data-priority="2">@lang('unittransaction.label.unit_type')</th>
                                    <th data-priority="3">@lang('unitpackage.label.unit')</th>
                                    <th data-priority="4">@lang('comman.label.actioner')</th>
                                    <th data-priority="6">@lang('unittransaction.label.reference')</th>
                                    @if(_MASTER)
                                        <th data-priority="7">@lang('website.label.website')</th>
                                    @endif
                                    <th data-priority="8">@lang('unittransaction.label.comment')</th>
                                    <th data-priority="6">@lang('comman.label.created')</th>
                                    <th>@lang('comman.label.action')</th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection








@push('js')

<script>

    var url = "{{url('admin/unit-history')}}";
    

    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            { "data": "id","name":"id"},
            { "data": "unit_type","name":"unit_type"},
            { "data": "unit","name":"unit"},
            {
                "data": null,
                "name": 'created_by',
                "searchable": false,
                "render": function (o) {
                    var creatorname = "";
                    if(o.creator){
                        creatorname = o.creator.name;
                    }
                    return creatorname;
                }
            },
            { "data": "reference","name":"reference"},
            @if(_MASTER)
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o.website){
                        return o.website.domain;
                    }else{
                        return "";
                    }
                }
            },
            @endif
            { "data": "comment","name":"comment"},
            {
                "data": null,
                "name": 'created_at',
                "searchable": false,
                "render": function (o) {
                    return getLangFullDate(o.created_at,"short");
                }
            },
            { 
                "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";
                    return v;
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/unit-history/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                d.filter_website = $('#filter_website').val();
           }
        }
    });
    $('.filter').change(function() {
        datatable.fnDraw(false);
    });
    $("#filter_website").select2();

</script>


@endpush

