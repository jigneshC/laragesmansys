@extends('layouts.apex')

@section('title',trans('setting.label.create_setting'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('setting.label.create_setting') </div>
                {{--  @include('partials.page_tooltip',['model' => 'setting','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/settings') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
                        
	                <div class="actions pull-right">
                             <a href="javascript:document.getElementById('module_form').submit();" title="Update" class="navbar-right btn btn-primary"> @lang('comman.label.create')</a>
                        </div>
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif

                            {!! Form::open(['url' => '/admin/settings', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}

                            @include ('admin.settings.form')

                            {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection



