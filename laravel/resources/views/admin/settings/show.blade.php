@extends('layouts.apex')

@section('title',trans('setting.label.show_setting'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('setting.label.show_setting') # {{ $setting->id }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'setting','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/settings') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                            <a href="{{ url('/admin/settings/' . $setting->id . '/edit') }}" title="Edit Setting">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    @lang('comman.label.edit')
                                </button>
                            </a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/settings', $setting->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Setting',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Setting'])."')"
                            ))!!}
                            {!! Form::close() !!}
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $setting->id }}</td>
                                                </tr>
                                                @if($setting->type == "subject_for_duty")
                                                 <tr>
                                                    <th> @lang('comman.label.name')</th>
                                                    <td> {{ $setting->name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('setting.label.value')</th>
                                                    <td> @if($setting->website) {{$setting->website->domain}} @endif - @if($setting->subjects) {{$setting->subjects->name}} @endif</td>
                                                </tr>

                                                @else
                                                <tr>
                                                    <th> @lang('comman.label.name')</th>
                                                    <td> {{ $setting->name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('setting.label.value')</th>
                                                    <td> {{ $setting->value }} </td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


