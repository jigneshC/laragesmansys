
<div class="form-group row {{ $errors->has('subject_id') ? 'has-error' : ''}}">
            {!! Form::label('subject_id',"Type Of Value", ['class' => 'col-md-4 label-control']) !!}
            <div class="col-md-6">
                {!! Form::select('type',['key_value'=>'Key-Value'], null, ['class' => 'form-control', 'id' => 'setting_type']) !!}
                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
            </div>
    </div>
   

<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.name')
    </label>
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group row {{ $errors->has('key') ? 'has-error' : ''}}">
    <label for="key" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('setting.label.key')
    </label>
    <div class="col-md-6">
        {!! Form::text('key', null, ['class' => 'form-control']) !!}
        {!! $errors->first('key', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row {{ $errors->has('value') ? 'has-error' : ''}}">
    <label for="value" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('setting.label.value')
    </label>
    <div class="col-md-6">
        {!! Form::text('value', null, ['class' => 'form-control']) !!}
        {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
    <lable class="col-md-4 label-control"></lable>
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>





