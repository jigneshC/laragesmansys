@extends('layouts.apex')

@section('title',trans('setting.label.setting'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('setting.label.setting') </div>
        {{--   @include('partials.page_tooltip',['model' => 'setting','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                             @if(Auth::user()->can('access.setting.create'))
                                <a href="{{ url('/admin/settings/create') }}" class="btn btn-success btn-sm ticket-add-new"
                                   title="Add New Setting">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                            @endif
							
							@include("admin.for_master.export_pdf",["module"=>"settings","fields"=>[]])
							
                          </div>
                         </div>
                        <div class="col-9">
                          
                              @if(_MASTER)
                                {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter pull-right','required'=>'required','id'=>'filter_website','style'=>'width:200px']) !!}
                            @endif
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>@lang('comman.label.name')</th>
                                <th>@lang('setting.label.key')</th>
                                <th>@lang('setting.label.value')</th>
                                <th>@lang('setting.label.type')</th>
                                <th>@lang('comman.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection









@push('js')
<script>

    var url = "{{url('admin/settings')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "asc"],
        columns: [
            { "data": "name","name":"name"},
            {
                "data": null,
                "name": "key",
                "render": function (o) {
                    if(o.type == "subject_for_duty" &&  o.website){
                        return  o.website.domain;
                    }else{
                        return o.key;
                    }
                }
            },
            {
                "data": null,
                "name": "value",
                "render": function (o) {
                    if(o.type == "subject_for_duty" &&  o.subjects){
                        return  o.subjects.name;
                    }else{
                        return o.value;
                    }
                }
            },
            { "data": "type","name":"type"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.setting.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.setting.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')' ><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'><i class='fa fa-eye' aria-hidden='true'></i></a>";


                    return v+d+e;

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/settings/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                // d.filter_website = $('#filter_website').val();
            }
        }
    });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Setting'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

