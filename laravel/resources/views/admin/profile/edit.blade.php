@extends('layouts.apex')
@section('title',trans('people.label.edit_profile'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">   @lang('people.label.edit_profile') </div>
                {{-- @include('partials.page_tooltip',['model' => 'profile','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ route('profile.index') }}" title="{{__('Back')}}">
                            <button class="btn-link-back"><i class="fa fa-arrow-left"
                                                                      aria-hidden="true"></i> Back </button>
                        </a>
						@include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            
							{!! Form::model($user,[
								'method' => 'PATCH',
								'class' => 'form-horizontal',
								'files'=>true,
								'autocomplete'=>'off'
							]) !!}

                    <div class="form-group row {{ $errors->has('first_name') ? ' has-error' : ''}}">
                        <label for="first_name" class="col-md-4 label-control">
                            <span class="field_compulsory">*</span>@lang('people.label.first_name')
                        </label>
                        <div class="col-md-6">
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
                            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group row {{ $errors->has('last_name') ? ' has-error' : ''}}">
                        <label for="last_name" class="col-md-4 label-control">
                            <span class="field_compulsory">*</span>@lang('people.label.last_name')
                        </label>
                        <div class="col-md-6">
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
                            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                    <div class="form-group row {{ $errors->has('email') ? ' has-error' : ''}}">
                        {!! Form::label('email',trans('people.label.email'), ['class' => 'col-md-4 label-control']) !!}
                        <div class="col-md-6">
                            {{--<p style="border: 1px solid #d0d0d0; padding: 5px">{{$user->email}}</p>--}}
                            {!! Form::email('email', isset($user->email)?$user->email:old('email'), ['class' => 'form-control', 'required' => 'required','disabled'=>true]) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('language') ? ' has-error' : ''}}">
                        {!! Form::label('language', trans('people.label.language'), ['class' => 'col-md-4 label-control']) !!}
                        <div class="col-md-6">
                            {!! Form::select('language',$languages, null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
                            {!! $errors->first('language', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
					<div class="form-group row {{ $errors->has('enable_sms') ? 'has-error' : ''}}">
						{!! Form::label('enable_sms', trans('user.label.enable_sms_notification'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
						<div class="col-xl-8 col-sm-8 status_rad">
							<div class="radio">
								{!! Form::radio('enable_sms', '1',($user->enable_sms)? true : false, ['id' => 'rd1']) !!} <label for="rd1">Yes</label>
							</div>
							<div class="radio">
								{!! Form::radio('enable_sms', '0',($user->enable_sms)? false : true ,['id' => 'rd2']) !!} <label for="rd2">No</label>
							</div>
							{!! $errors->first('enable_sms', '<p class="help-block">:message</p>') !!}
						</div>
					</div>

					<div class="form-group row {{ $errors->has('timezone') ? ' has-error' : ''}}">
                        {!! Form::label('timezone', trans('people.label.timezone'), ['class' => 'col-md-4 label-control']) !!}
                        <div class="col-md-6">
							<select class="form-control" id="timezone" name="timezone">
							@foreach(trans('language.label.time_zone') as $k => $val)
                                 <option value="{{$val}}" @if($user->timezone && $user->timezone == $val) selected="selected" @endif>{{$k}}</option>
                            @endforeach
							</select>
							{!! $errors->first('timezone', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
					
                    @if($user->people)
                        @include('admin.profile.peopleForm')
                    @endif
                    <div class="form-group row">
						<label for="first_name" class="col-md-4 label-control">
                            
                        </label>
                        <div class="col-md-offset-4 col-md-4">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('people.label.update_profile'), ['class' => 'btn btn-primary']) !!}
                            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-primary']) }}
                        </div>
                    </div>
					

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection



@push('js')

<script>
   

    $("#timezone").select2();
    

   

   
</script>


@endpush


