@extends('layouts.apex')

@section('title',trans('people.label.my_profile'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('people.label.my_profile') </div>
                {{--  @include('partials.page_tooltip',['model' => 'website','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
				
							<a href="{{ route('profile.edit') }}" class="btn btn-success btn-sm" title="Edit Profile">
                                <i class="fa fa-edit" aria-hidden="true"></i> @lang('people.label.edit_profile')
                            </a>
                            <a href="{{ route('profile.password') }}" class="btn btn-success btn-sm" title="Change Password">
                                <i class="fa fa-lock" aria-hidden="true"></i> @lang('people.label.change_password')
                            </a>
							
                       
                          
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                   <div class="table-responsive">
                                         <table class="table table-borderless">
                                            <tbody>
                                                 <tr>
														<th>@lang('comman.label.name')</th>
														<td>{{$user->name}}</td>
													</tr>

													<tr>
														<th>@lang('people.label.email')</th>
														<td>{{$user->email}}</td>
													</tr>
													<tr>
														<th>@lang('people.label.language')</th>
														<td>{{$user->lang->lang_code or null}}</td>
													</tr>
													<tr>
														<th>@lang('people.label.timezone')</th>
														<td>{{$user->timezone}}</td>
													</tr>
													<tr>
														<th>@lang('people.label.joined')</th>
														<td>{{$user->created_at->diffForHumans()}}</td>
													</tr>
													<tr>
														<th>@lang('user.label.enable_sms_notification')</th>
														<td>@if($user->enable_sms) Yes @else No @endif</td>
													</tr>
													@if($user->people)
													<tr>
														<th colspan="2"><h2> @lang('people.label.person_details') </h2></th>
													
													</tr>
													
													<tr>
														<th>@lang('people.label.first_name')</th>
														<td>{{$user->people->first_name}}</td>
													</tr>
													<tr>
														<th>@lang('people.label.last_name')</th>
														<td>{{$user->people->last_name}}</td>
													</tr>

													<tr>
														<th>@lang('people.label.phone_number_1')</th>
														<td>{{$user->people->phone_number_1}}</td>
													</tr>

													<tr>
														<th>@lang('people.label.phone_type_1')</th>
														<td>{{$user->people->phone_type_1}}</td>
													</tr>


													<tr>
														<th>@lang('people.label.phone_number_2')</th>
														<td>{{$user->people->phone_number_2 or null}}</td>
													</tr>

													<tr>
														<th>@lang('people.label.phone_type_2')</th>
														<td>{{$user->people->phone_type_2 or null}}</td>
													</tr>


													<tr>
														<th>@lang('people.label.photo')</th>
														<td>
															@if($user->photo)
																<img src="{!! asset('uploads/'.$user->photo) !!}" alt="" width="150">
															@endif
														</td>
													</tr>
													@endif
                                            </tbody>
                                        </table>
                                    </div>
							
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>



@endsection