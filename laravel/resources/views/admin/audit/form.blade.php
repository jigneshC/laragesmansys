<div class="form-group row {{ $errors->has('table_name') ? 'has-error' : ''}}">
    <label for="table_name" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('audit.label.table_name')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.audit.form_field.table_name')])


    </label>
    <div class="col-md-6">
        {!! Form::text('table_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('table_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('table_row_id') ? 'has-error' : ''}}">
    <label for="table_row_id" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('audit.label.table_row_id')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.audit.form_field.table_row_id')])

    </label>
    <div class="col-md-6">
        {!! Form::number('table_row_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('table_row_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('old_values') ? 'has-error' : ''}}">
    <label for="old_values" class="col-md-4 label-control"><span class="field_compulsory">*</span>
        @lang('audit.label.old_values')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.audit.form_field.old_values')])

    </label>
    <div class="col-md-6">
        {!! Form::textarea('old_values', null, ['class' => 'form-control']) !!}
        {!! $errors->first('old_values', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group row {{ $errors->has('audit_by') ? 'has-error' : ''}}">
    <label for="audit_by" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('audit.label.audit_by')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.audit.form_field.audit_by')])
    </label>
    <div class="col-md-6">
        {!! Form::number('audit_by', null, ['class' => 'form-control']) !!}
        {!! $errors->first('audit_by', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group row">
    <label class="col-md-4 label-control"></label>
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>
