@extends('layouts.apex')

@section('title',trans('audit.label.show_audit'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  {{ trans('audit.label.show_audit') }} {{ $audit->id }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'audit','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                         <a href="{{ url('/admin/audits') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                           
                            @if(Auth::user()->can('access.audit.edit'))
                                <a href="{{ url('/admin/audits/' . $audit->id . '/edit') }}" title="Edit Audit">
                                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                              aria-hidden="true"></i>
                                        @lang('comman.label.edit')
                                    </button>
                                </a>

                            @endif
                            @if(Auth::user()->can('access.audit.delete'))

                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/audits', $audit->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Audit'])."')"
                                ))!!}
                                {!! Form::close() !!}
                            @endif
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $audit->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('audit.label.table_name')</th>
                                                    <td> {{ $audit->table_name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('audit.label.table_row_id')</th>
                                                    <td> {{ $audit->table_row_id }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('audit.label.old_values')</th>
                                                    <td> {{ $audit->old_values }} </td>
                                                </tr>
                                                 @if($audit->qrcode)
                                                <tr>
                                                    <th>@lang('comman.label.qrcode')</th>
                                                    <td>  {!!  DNS2D::getBarcodeHTML($audit->qrcode->qrcode, "QRCODE")  !!} </td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection




