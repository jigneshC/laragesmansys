@extends('layouts.apex')

@section('title',trans('dropdownvalue.label.show_dropdown_value'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('dropdownvalue.label.show_dropdown_value') # {{ $dropdownvalue->name }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'dropdownvalue','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                         <a href="{{ url('/admin/dropdown-values') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                           
                            @if(Auth::user()->can('access.dropdownsvalue.edit'))

                                <a href="{{ url('/admin/dropdown-values/' . $dropdownvalue->id . '/edit') }}"
                                   title="Edit DropdownValue">
                                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                              aria-hidden="true"></i>
                                        @lang('comman.label.edit')
                                    </button>
                                </a>

                            @endif
                    
                            @if(Auth::user()->can('access.dropdownsvalue.delete'))

                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/dropdown-values', $dropdownvalue->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete DropdownValue',
                                        'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Dropdown Value'])."')"
                                ))!!}
                                {!! Form::close() !!}
                                
                            @endif
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $dropdownvalue->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.name')</th>
                                                    <td> {{ $dropdownvalue->name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('dropdownvalue.label.type_id')</th>
                                                    <td> {{ $dropdownvalue->type['name'] }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.active')</th>
                                                    <td> {{ $dropdownvalue->active }} </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection




