@extends('layouts.apex')

@section('title',trans('dropdownvalue.label.Add_Languages_in_DropDown_Value'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('dropdownvalue.label.add_languages') #{{ $dropdownvalue->id }} </div>
                {{--  @include('partials.page_tooltip',['model' => 'dropdownvalue','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                     <a href="{{ url('/admin/dropdown-values') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                        </button>
                    </a>

                    <a href="javascript:document.getElementById('module_form').submit();" title="Update" class="navbar-right btn btn-primary"> @lang('comman.label.update')</a>
                     
                     @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($dropdownvalue, [
                                'method' => 'POST',
                                'class' => 'form-horizontal',
                                'files' => true,
                                'id' => "module_form"
                            ]) !!}

                             <input type="hidden" name="parent_id" value="{{$parent_id}}">

                            @foreach($languages as $code=>$language)

                                <div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
                                    {!! Form::label($code, $language.' || '.$code, ['class' => 'col-md-4 col-md-offset-1 label-control']) !!}
                                    <div class="col-md-3">
                                        {!! Form::text('lang['.$code.']', $getLine($code), ['class' => 'form-control']) !!}
                                        {!! $errors->first($code, '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group row">
                                <label class="col-md-4 label-control"></label>
                                <div class="col-md-offset-4 col-md-4">
                                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.update'), ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>


                            {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection





