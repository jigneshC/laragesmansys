@extends('layouts.apex')

@section('title',trans('dropdownvalue.label.dropdown_value'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">@lang('dropdownvalue.label.dropdown_value') </div>
        {{--    @include('partials.page_tooltip',['model' => 'dropdownvalue','page'=>'index'])  --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                            
                              @if(Auth::user()->can('access.dropdownsvalue.create'))

                                <a href="{{ url('/admin/dropdown-values/create') }}" class="btn btn-success btn-sm ticket-add-new"
                                   title="Add New DropdownValue">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                              
                               @endif
                          </div>
                         </div>
                        <div class="col-9">
                          
                             
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                        <table class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th>@lang('comman.label.id')</th>
                                <th>@lang('comman.label.name')</th>
                                <th>@lang('language.label.language')</th>
                                <th>@lang('comman.label.type')</th>
                                <th>@lang('comman.label.active')</th>
                                <th>@lang('comman.label.action')</th>
                            </tr>
                            </thead>

                        </table>
                            
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection



@push('js')
<script>

    var url = "{{url('admin/dropdown-values')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [1, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            { "data": "name","name":"name"},
            { "data": "lang_code","name":"lang_code"},
            {
                "data": null,
                "name": "type_id",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    if(o.type){
                        return o.type.name;
                    }else{
                        return '';
                    }
                }
            },
            {
                "data": null,
                "name": "active",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    if(o.active ==1){
                        return '<span class="label label-success">True</span>';
                    }else{
                        return '<span class="label label-danger">False</span>';
                    }
                }
            },
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.dropdownsvalue.edit'))
                        e= "<a href='"+url+"/"+o.id+"/edit' title='Edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.dropdownsvalue.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' title='Delete' data-id="+o.parent_id+" title='@lang('tooltip.common.icon.delete')'><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  "<a href='"+url+"/"+o.id+"' title='View' data-id="+o.id+"><i class='fa fa-eye' aria-hidden='true' title='@lang('tooltip.common.icon.eye')'></i></a>";

                    var l =  "<a href='"+url+"/"+o.id+"/add-languages' title='Add Language' data-id="+o.id+"><i class='fa fa-language' aria-hidden='true'></i></a>";


                    return v+e+l+d;

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/dropdown-values/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                // d.filter_website = $('#filter_website').val();
            }
        }
    });

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Dropdown-value'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

