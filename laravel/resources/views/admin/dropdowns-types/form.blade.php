<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.name')
    </label>
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('comman.label.active'), ['class' => 'col-md-4 label-control']) !!}
    <div class="col-md-6 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rd1']) !!} <label for="rd1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false ,['id' => 'rd2']) !!} <label for="rd2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
    <label class="col-md-4 label-control"></label>
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>
