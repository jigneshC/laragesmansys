@extends('layouts.apex')

@section('title',trans('dropdowntype.label.show_dropdowns_types'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('dropdowntype.label.show_dropdowns_types') # {{ $dropdownstype->name }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'dropdowntype','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                         <a href="{{ url('/admin/dropdowns-types') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                             @if(Auth::user()->can('access.dropdownsvalue.edit'))
                                <a href="{{ url('/admin/dropdowns-types/' . $dropdownstype->id . '/edit') }}"
                                   title="Edit DropdownsType">
                                    <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                              aria-hidden="true"></i>
                                        @lang('comman.label.edit')
                                    </button>
                                </a>
                            @endif

                            @if(Auth::user()->can('access.dropdownsvalue.delete'))

                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/dropdowns-types', $dropdownstype->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete DropdownsType',
                                        'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Dropdown Type'])."')"
                                ))!!}
                                {!! Form::close() !!}
                            @endif
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $dropdownstype->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.name')</th>
                                                    <td> {{ $dropdownstype->name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.active')</th>
                                                    <td> {{ $dropdownstype->active }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.created_by')</th>
                                                    <td>{{$dropdownstype->user['name']}}</td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


