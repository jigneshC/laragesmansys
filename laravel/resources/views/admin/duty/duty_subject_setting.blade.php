<div class="modal modal-static fade" id="duty_setting_modal" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('duty.label.add_subject_title')</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="row rental_option_form_row">
                        {!! Form::open(['url' => 'admin/duty/addsubject', 'class' => 'form-horizontal add_subject_form', 'files' => true]) !!}

                        {!! Form::hidden('ticket_id',0, ['class' => 'form-control','id'=>'action_ticket_id']) !!}



                        <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                            {!! Form::label('status', trans('duty.label.select_subject'), ['class' => 'col-md-4 col-xs-5 control-label']) !!}
                            <div class="col-md-6 col-xs-6">
                                {!! Form::select('filter_subject',$subjects,($setting && $setting->dutysubject)? $setting->dutysubject->id:null, ['class' => 'filter full-width','id'=>'filter_subject']) !!}
                                <p class="add_subject_error text-error"></p>
                            </div>
                        </div>

                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-primary']) !!}

                        <button type="button" class="btn btn-warning" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('js')

<script>
/***************action model******************/
    var url = "{{url('admin/duty')}}";

    var action_model = "#duty_setting_modal";
        $(document).on('click', '#open_model', function (e) {
            $(action_model).modal('show');
            return false;
        });

    $("#filter_subject").select2();

    $('.add_subject_form').submit(function(event) {
        var subject_id = $("#filter_subject").val();
        if(subject_id==""){
            var error_msg ="*Please select a subject";
            $(".add_subject_error").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }
        var formData = {
            'subject_id': subject_id,
        };

        $.ajax({
            type: "POST",
            url: url + "/addsubject",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {
                $('#subject_id').val(subject_id);
                $(action_model).modal('hide');
                toastr.success('Action Success!', data.message);

            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush