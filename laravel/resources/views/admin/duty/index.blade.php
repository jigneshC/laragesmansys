@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('ticket.label.ticket'))

@push('css')

@endpush

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">
            <label id="timer"></label>
        </div>
        {{--  @include('partials.page_tooltip',['model' => 'duty','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                   
                        <div class="col-12">
                           @can('access.ticket.create')
                            <div class="row">
								<div class="col-xl-3 col-sm-4">
									<div class="ticket-select-process">
                                        {!! Form::select('company_id',$companies,null, ['class' => 'filter full-width','id'=>'company_id']) !!}
                                    </div>
								</div>
								<div class="col-xl-3 col-sm-4">
									 <div class="ticket-select-process">
                                        <input type="text" name="filter_users" class="filter  full-width" id="filter_users">
                                        <p class="add_duty_error text-error"></p>
                                    </div>
								</div>
								<div class="col-xl-3 col-sm-4">
									<a href="#" class="btn btn-success btn-sm ticket-add-new" id="add_new"
                                           title="Add New Audit">
                                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                        </a>
										@include("admin.for_master.export_pdf",["module"=>"duty","fields"=>[]])
								</div>
							

                            </div>
					@endcan
                       
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped  datatable responsive" width="100%">
                                <thead>
                                <tr>
                                    <th data-priority="1">@lang('ticket.label.subject')</th>
                                    <th data-priority="2">@lang('ticket.label.assign')</th>
                                    <th data-priority="4">@lang('comman.label.created')</th>
                                    <th data-priority="3" class="noExport">@lang('duty.label.close')</th>
                                </tr>
                                </thead>
                            </table>
						</div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection




@push('js')

<script>

    var url = "{{url('admin/duty')}}";
    <?php $today = getdate(); ?>
    var d = new Date(<?php echo $today['year'].",".$today['mon'].",".$today['mday'].",".$today['hours'].",".$today['minutes'].",".$today['seconds']; ?>);

    setInterval(function() {
        d.setSeconds(d.getSeconds() + 1);
        $('#timer').text((("0"+d.getHours()).slice(-2) +' : ' + ("0"+d.getMinutes()).slice(-2) + ' : ' + ("0"+d.getSeconds()).slice(-2) ));
    }, 1000);

    $("#company_id").select2();
    $("#filter_users").select2({
        placeholder: "@lang('user.label.select_user')",
        allowClear: true,
        ajax: {
            url: "{{ url('admin/users/search') }}?_website_id={{_WEBSITE_ID}}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_subject: params, // search term
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    });






    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [2, "desc"],
        columns: [
            { "data": "subject_name","name":"subjects.name"},
            { "data": "user_name","name":"users.name"},
            {
                "data": null,
                "name": 'created_at',
                "searchable": false,
                "render": function (o) {
                    return getLangFullDate(o.created_at,"short");
                }
            },
            { "data": null,
                "searchable": false,
                "orderable": false,
                "visible": <?php echo (Auth::user()->can('access.ticket.action')) ? 'true' : 'false' ?>,
                "render": function (o) {
                    return "<a href='"+url+"/"+o.id+"'  class='more-action' data-status="+o.status+" data-id="+o.id+" title='@lang('tooltip.duty.icon.checked')'><i class='fa fa-check-circle-o action_icon'></i></a>"
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/duty/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {

            }
        }
    });

    $(document).on('click', '#add_new', function (e) {
        var user_id = $('#filter_users').val();
        var company_id = $('#company_id').val();
        var error_msg = "";
        if(user_id==""){ error_msg = "*Please select a User"; }
        else if(company_id==""){ error_msg = "*No Company Selected"; }

        if(error_msg!=""){
            $(".add_duty_error").html(error_msg).show().delay(5000).fadeOut();
        }else{
            var formData = {
                'company_id': company_id,
                'user_id': user_id,
            };

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message);
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }



        return false;
    });

    $(document).on('click', '.more-action', function (e) {
        var id=$(this).attr('data-id');
        var r = confirm("@lang('duty.js_msg.confirm_for_close_ticket')");
        if (r == true) {
            var formData = {
                'ticket_id': id,
            };
            var url = "{{url('admin/duty/closeticket')}}";
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message);
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }

        return false;
    });



</script>


@endpush

