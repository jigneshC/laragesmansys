<div class="modal fade text-left" id="unitpackage_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                     <h3 class="modal-title title_form_model"></h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                   
                
            </div>
            {!! Form::open(['url' => '','id'=>'unitpackage_form','class' => 'form-horizontal unitpackage_form', 'files' => true]) !!}
            {!! Form::hidden('package_id',0, ['class' => 'form-control','id'=>'package_id']) !!}
                        
            <div class="modal-body">
                {!! Form::label('name', trans('unitpackage.label.name'),['class' => '']) !!}
                <div class="form-group position-relative">
                    <input type="text" name="name" class="filter form-control"  id="name" style="" autocomplete='off'>    
                </div>
                
                {!! Form::label('desc', trans('unitpackage.label.desc'),['class' => '']) !!}
                <div class="form-group position-relative">
                    <input type="textarea" name="desc" class="filter form-control" rows="3" id="desc" style="" autocomplete='off'>
                </div>
                
                {!! Form::label('price', trans('unitpackage.label.price'),['class' => '']) !!}
                <div class="form-group position-relative">
                    <input type="number" name="price" class="filter form-control" min="0" id="price" style="" autocomplete='off'>
                </div>
                
                {!! Form::label('price_currency', trans('unitpackage.label.price_currency'),['class' => '']) !!}
                <div class="form-group position-relative">
                    <select class="form-control" id="price_currency" name="price_currency">
                                                @foreach(\config('settings.currency') as $k => $val)
                                                    <option value="{{$k}}">{{$k}} ({{$val['symbole']}})</option>
                                                @endforeach
                                        </select>
                </div>
                
                {!! Form::label('package_class', trans('unitpackage.label.package_class'),['class' => '']) !!}
                <div class="form-group position-relative">
                    <select class="form-control" id="package_class" name="package_class">
                                                @foreach(\config('settings.package_class') as $k => $val)
                                                    <option value="{{$k}}">{{$k}}</option>
                                                @endforeach
                                        </select>
                </div>
                
                {!! Form::label('unit', trans('unitpackage.label.unit'),['class' => '']) !!}
                <div class="form-group position-relative">
                     <input type="number" name="unit" class="filter form-control" min="0" id="unit" style="" autocomplete='off'>
                </div>
                
            </div>
            <div class="modal-footer">
                <p class="form_submit_error text-error"></p>
                <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-light']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@push('js')


<script>

 
/***************action model******************/
    var _model = "#unitpackage_modal";
        $(document).on('click', '.unitpackage_form_open', function (e) {
            var id=$(this).attr('data-id');
            $('#unitpackage_form')[0].reset();
            $("#package_id").val(id);
            if(id!=0){
                $(".title_form_model").html("@lang('unitpackage.label.update_a_package')");
                var url = "{{url('admin/units-packages')}}/"+id+"/edit";   
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (result) {
                        var data=result.data;
                        $(".unitpackage_form #name").val(data.name);
                        $(".unitpackage_form #desc").val(data.desc);
                        $(".unitpackage_form #price").val(data.price);
                        $(".unitpackage_form #price_currency").val(data.price_currency);
                        $(".unitpackage_form #unit").val(data.unit);
                        $(".unitpackage_form #package_class").val(data.package_class);
                        
                    }
                });
            }else{
                $(".title_form_model").html("@lang('unitpackage.label.create_a_package')");
            }
            
            $(_model).modal('show');
            return false;
        });


    $('.unitpackage_form').submit(function(event) {

        var error_msg = "";
        if($(".unitpackage_form #name").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>trans('unitpackage.label.name')])";
        }else if($(".unitpackage_form #desc").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>trans('unitpackage.label.desc')])";
        }else if($(".unitpackage_form #price").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>trans('unitpackage.label.price')])";
        }else if($(".unitpackage_form #price_currency").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>trans('unitpackage.label.price_currency')])";
        }else if($(".unitpackage_form #unit").val()==""){
            error_msg = "@lang('comman.validation.field_required',['field_name'=>trans('unitpackage.label.unit')])";
        }

        if(error_msg!=""){
            $(".form_submit_error").html(error_msg).show().delay(5000).fadeOut();
            return false;
        }
        
        var formData = $("#unitpackage_form").serialize();
        var url = "{{url('admin/units-packages')}}";
        var method = "POST";

        if($(".unitpackage_form #package_id").val()!="" && $(".unitpackage_form #package_id").val()!=0){
            var url = "{{url('admin/units-packages')}}/"+$(".unitpackage_form #package_id").val();
            var method = "PUT";
        }
        $.ajax({
            type: method,
            url: url,
            data: formData,
            success: function (data) {
                $(_model).modal('hide');
                toastr.success('Action Success!', data.message);
                datatable.fnDraw(false);
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush