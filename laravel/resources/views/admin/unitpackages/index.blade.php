@extends('layouts.apex')

@section('title',trans('unitpackage.label.unitpackages'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">@lang('unitpackage.label.unitpackages') </div>
        {{--    @include('partials.page_tooltip',['model' => 'unitpackage','page'=>'index'])  --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                            
                                <a href="#" class="btn btn-success btn-sm unitpackage_form_open ticket-add-new" data-id="0" title="Add New Packages">
                                     <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                          </div>
                         </div>
                        <div class="col-9">
                          
                             
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                        <table class="table table-borderless datatable responsive" width="100%">
                                <thead>
                                <tr>
                                    <th data-priority="7">@lang('comman.label.id')</th>
                                    <th data-priority="1">@lang('unitpackage.label.name')</th>
                                    <th data-priority="2">@lang('unitpackage.label.price')</th>
                                    <th data-priority="3">@lang('unitpackage.label.unit')</th>
                                    <th data-priority="4">@lang('unitpackage.label.package_class')</th>
                                    <th data-priority="6">@lang('unitpackage.label.desc')</th>
                                    <th data-priority="5">@lang('comman.label.action')</th>
                                </tr>
                                </thead>
                            </table>
                            
                    </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@include("admin.unitpackages.unitpackageForm")

@endsection


@push('js')

<script>

    var url = "{{url('admin/units-packages')}}";
    $("#action").select2();

    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: true,
        order: [0, "desc"],
        columns: [
            { "data": "id","name":"id"},
            { "data": "name","name":"name"},
            {
                "data": null,
                "name": 'price',
                "searchable": false,
                "render": function (o) {
                    return o.price+" "+o.price_currency;
                }
            },
            { "data": "unit","name":"unit"},
            { "data": "package_class","name":"package_class"},
            { "data": "desc","name":"desc"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "visible": <?php echo (Auth::user()->can('access.moduleunits.delete')) ? 'true' : 'false' ?>,
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.moduleunits.edit'))
                        e= "<a href='javascript:void(0);' data-id='"+o.id+"' class='unitpackage_form_open' title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                    @if(Auth::user()->can('access.moduleunits.delete'))
                        d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" title='@lang('tooltip.common.icon.delete')' ><i class='fa fa-trash action_icon '></i></a>";
                    @endif
                    var v =  "";
                    return v+d+e;
                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/units-packages/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {

            }
        }
    });





    
        $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Package'])");
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });




</script>


@endpush

