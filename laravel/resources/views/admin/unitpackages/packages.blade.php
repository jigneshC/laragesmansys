@extends('layouts.apex')

@section('title',trans('unitpackage.label.unitpackages'))

@section('pageHeader')
<div class="row">
        <div class="col-md-3">
           

        </div>
        <div class="col-md-3 info">
               
                      
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3 text-right">
                
            <h3 class="">
                <i class="icon-money"></i>
                <span>@lang('unittransaction.label.available_units')</span>
                <span class="text-muted">:{{_WEBSITE_UNITS}}</span>
            </h3>
           
            <br/>
            <a href="{{ url('/admin/order') }}" class="" title="@lang('unitorder.label.view_order')">
                <span class="btn btn-danger btn-lg">@lang('unitorder.label.view_order')</span>
            </a>
               
        </div>
    </div>
@endsection

@section('content')

@php( $curOb = \config('settings.currency'))  
@php( $package_cls =\config('settings.package_class'))

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">@lang('unitpackage.label.unitpackages')
            {{-- <span> @include('partials.page_tooltip',['model' => 'unitpackage','page'=>'index'])</span> --}}
        </div>
         <div class=" pull-right">
                
            <h3 class="">
                <i class="icon-money"></i>
                <span>@lang('unittransaction.label.available_units')</span>
                <span class="text-muted">:{{_WEBSITE_UNITS}}</span>
            </h3>
           
            <br/>
            <a href="{{ url('/admin/order') }}" class="" title="@lang('unitorder.label.view_order')">
                <span class="btn btn-danger btn-sm">@lang('unitorder.label.view_order')</span>
            </a>
               
        </div>
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                            
                                <a href="#" class="btn btn-success btn-sm unitpackage_form_open" data-id="0" title="Add New Packages">
                                     <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                                </a>
                          </div>
                         </div>
                        <div class="col-9">
                          
                             
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard card-block-new">
                       
                        @foreach($packages->chunk(6) as $chunk) 
                                  <div class="row pricing-tables">
                                  @foreach($chunk as $item)
                                  <div class="col-xl-2 col-sm-4">
                                        <div class="pricing-table">
                                        <div class="header">{{$item->package_class}}</div>
                                          <div class="price @if(isset($package_cls[$item->package_class])) {{$package_cls[$item->package_class]}} @endif">
                                            <span> {{$curOb[$item->price_currency]['symbole']}} {{$item->price}}</span>
                                          </div>
                                          <ul class="list-unstyled features">
                                            
                                            <li>
                                              <strong>{{$item->unit}}</strong>
                                              @lang("unitpackage.label.unit")
                                            </li>
                                            <li>
                                              <strong>{{$item->name}}</strong>
                                            </li>
                                            <li>
                                                
                                                <strong class="badge badge-warning badge-light">{{$item->desc}}</strong>
                                                    
                                            </li>
                                          </ul>
                                          <div class="footer footer-btn">
                                            <a class="btn btn-primary" href="{{url('admin/order-confirm/'.$item->id)}}"><i class="icon-signin"></i>  @lang("unitorder.label.order_now") </a>
                                          </div>
                                        </div>
                                      </div>
                                  @endforeach
                                  </div>
                                 <!-- <div class="group-header">
               
                                  </div>-->
                                  @endforeach
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@endsection


@push('js')

<script>

   
</script>


@endpush





<!--@section('pageHeader')
<div class="row">
        <div class="col-md-3">
           

        </div>
        <div class="col-md-3 info">
               
                      
        </div>
        <div class="col-md-3">

        </div>
        <div class="col-md-3 text-right">
                
            <h3 class="">
                <i class="icon-money"></i>
                <span>@lang('unittransaction.label.available_units')</span>
                <span class="text-muted">:{{_WEBSITE_UNITS}}</span>
            </h3>
           
            <br/>
            <a href="{{ url('/admin/order') }}" class="" title="@lang('unitorder.label.view_order')">
                <span class="btn btn-danger btn-lg">@lang('unitorder.label.view_order')</span>
            </a>
               
        </div>
    </div>
@endsection

@section('content')

@php( $curOb = \config('settings.currency'))  
@php( $package_cls =\config('settings.package_class'))
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('unitpackage.label.unitpackages')
                    </div>
                    <div class="actions">
                        @include('partials.page_tooltip',['model' => 'unitpackage','page'=>'index'])
                    </div>

                </div>
               

                <div class="box-content ">
                        <div class="row" id="content-wrapper">
                                <div class="col-xs-12">
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div class="page-header">
                                        <h1 class="pull-left">
                                          <i class="icon-table"></i>
                                          <span>Pricing tables</span>
                                        </h1>
                                      </div>
                                    </div>
                                  </div>
                                     
                                  @foreach($packages->chunk(4) as $chunk) 
                                  <div class="row pricing-tables">
                                  @foreach($chunk as $item)
                                  <div class="col-sm-6 col-md-3">
                                        <div class="pricing-table">
                                        <div class="header">{{$item->package_class}}</div>
                                          <div class="price @if(isset($package_cls[$item->package_class])) {{$package_cls[$item->package_class]}} @endif">
                                            <span> {{$curOb[$item->price_currency]['symbole']}} {{$item->price}}</span>
                                          </div>
                                          <ul class="list-unstyled features">
                                            
                                            <li>
                                              <strong>{{$item->unit}}</strong>
                                              @lang("unitpackage.label.unit")
                                            </li>
                                            <li>
                                              <strong>{{$item->name}}</strong>
                                            </li>
                                            <li>
                                                
                                                <strong class="badge badge-warning">{{$item->desc}}</strong>
                                                    
                                            </li>
                                          </ul>
                                          <div class="footer">
                                            <a class="btn btn-primary" href="{{url('admin/order-confirm/'.$item->id)}}"><i class="icon-signin"></i>  @lang("unitorder.label.order_now") </a>
                                          </div>
                                        </div>
                                      </div>
                                  @endforeach
                                  </div>
                                  <div class="group-header">
               
                                  </div>
                                  @endforeach
                              
                    

                </div>


        </div>
    </div>

  
   

@endsection-->
