@extends('layouts.apex')
@section('title',trans('site.label.edit_site'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('site.label.edit_site') # {{ $site->name }}</div>
                {{-- @include('partials.page_tooltip',['model' => 'site','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/sites') }}" title="{{__('Back')}}">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left"
                                                                      aria-hidden="true"></i> @lang('comman.label.back') </button>
                        </a>
	                
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($site, [
                                'method' => 'PATCH',
                                'url' => ['/admin/sites', $site->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.sites.form', ['submitButtonText' =>trans('comman.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection



