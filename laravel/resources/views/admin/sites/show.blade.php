@extends('layouts.apex')

@section('title',trans('site.label.show_site'))

@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('site.label.show_site') </h4>
       {{-- @include('partials.page_tooltip',['model' => 'site','page'=>'index']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title">@lang('site.label.show_site') # {{ $site->name }}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            <ul class="nav nav-tabs">
              
              <li class="nav-item">
                   <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#site" aria-expanded="true" ><i class="fa fa-user"></i> @lang('site.label.site') </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#building" aria-expanded="false"><i class="fa fa-cubes"></i> @lang('building.label.buildings') </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#services" aria-expanded="false"><i class="fa fa-cubes"></i> @lang('site.label.service') </a>
              </li>
            </ul>
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="site" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a href="{{ url('/admin/sites') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('comman.label.back')
                                        </button>
                                    </a>
                                    @if(Auth::user()->can('access.site.edit'))


                                    <a href="{{ url('/admin/sites/' . $site->id . '/edit') }}" title="{{__('Edit Site')}}">
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                  aria-hidden="true"></i>
                                            @lang('comman.label.edit')
                                        </button>
                                    </a>
                                    @endif

                                    @if(Auth::user()->can('access.site.delete'))

                                    {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['admin/sites', $site->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => __('Delete Site'),
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Site'])."')"
                                    ))!!}
                                    {!! Form::close() !!}
                                    @endif
                                    
                                    

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                       <div class="box-content ">
                                       
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                         <table class="table table-borderless">
                                        <tbody>
                                            @if(_MASTER)
                                            <tr>
                                                <th> @lang('website.label.website')</th>
                                                <td>{{ $site->_website->domain OR NULL }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <th class="col-md-3">@lang('comman.label.id')</th>
                                                <td>#{{ $site->id }}</td>
                                            </tr>
                                            <tr>
                                                <th> @lang('comman.label.name')</th>
                                                <td> {{ $site->name }} </td>
                                            </tr>
											 <tr>
                                                <th> @lang('site.label.default_email') </th>
                                                <td> {{ $site->default_email }} </td>
                                            </tr>

                                            <tr>
                                                <th> @lang('site.label.company') </th>
                                                <td> @if($site->company) {{ $site->company->name }} @endif</td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.address')</th>
                                                <td> {{ $site->address }} </td>
                                            </tr>

                                            <tr>
                                                <th> @lang('site.label.city')</th>
                                                <td> {{ $site->city }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.postcode')</th>
                                                <td> {{ $site->postcode }} </td>
                                            </tr>


                                            <tr>
                                                <th>@lang('site.label.country')</th>
                                                <td> {{ $site->country->name or null }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.phone_number_1')</th>
                                                <td> {{ $site->phone_number_1 }} </td>
                                            </tr>

                                           


                                            <tr>
                                                <th> @lang('site.label.phone_type_1')</th>
                                                <td>  {{ \App\DropdownValue::getValue($site->phone_type_1) }} </td>
                                            </tr>


                                           


                                            <tr>
                                                <th> @lang('site.label.site_type') </th>
                                                <td>{{ \App\DropdownValue::getValue($site->site_type) }}</td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.logo')</th>
                                                <td>
                                                    @if($site->logo)
                                                    <img src="{!! asset('uploads/'.$site->logo) !!}" alt="" width="120">
                                                    @endif
                                                </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.access_conditions')</th>
                                                <td> {{ $site->access_conditions }} </td>
                                            </tr>


                                           




                                            <tr>
                                                <th> @lang('site.label.guard_presence') </th>
                                                <td> {{ $site->guard_presence }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.digicode')</th>
                                                <td> {{ $site->digicode }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.keybox_presence') </th>
                                                <td> {{ $site->keybox_presence }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.keybox_code') </th>
                                                <td> {{ $site->keybox_code }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.keybox_place')</th>
                                                <td> {{ $site->keybox_place }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.keybox_issue')</th>
                                                <td> {{ $site->keybox_issue }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.known_issue')</th>
                                                <td> {{ $site->known_issue }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.instructions_tasks_file') </th>
                                                <td> {{ $site->instructions_tasks_file }} </td>
                                            </tr>


                                            <tr>
                                                <th> @lang('site.label.order_file')</th>
                                                <td> {{ $site->oder_file }} </td>
                                            </tr>
                                            <tr>
                                                <th> @lang('site.label.procedures_file')</th>
                                                <td> {{ $site->procedures_file }} </td>
                                            </tr>
                                            <tr>
                                                <th> @lang('site.label.sitemaps_file') </th>
                                                <td> {{ $site->sitemaps_file }} </td>
                                            </tr>
                                            <tr>
                                                <th> @lang('site.label.active')</th>
                                                <td> {{ $site->active }} </td>
                                            </tr>

                                            
                                        </tbody>
                                    </table>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                     @if($site->qrcode)
                                                        @include('partials.qrcode',['qrcode' => $site->qrcode]) 
                                                     @endif
                                                </div>
                                            </div>



                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 
              <div class="tab-pane" id="building" aria-labelledby="baseIcon-tab2" aria-expanded="false">
                
                  
                        <div class="row ">
                  <div class="col-md-12">
                      <div class="box bordered-box blue-border">
                          
                          <div class="box-content ">

                              
                              <div class="row">
                                  <div class="col-md-6">

                                      {{__('Buildings')}}


                                  </div>
                                  

                              </div>

                              <div class="table-responsive">
                                  <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Site</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($site->buildings)
                                    @foreach($site->buildings as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->address }}</td>
                                        <td>{{ $item->site->name or null}}</td>
                                        <td>
                                            <a href="{{ url('/admin/buildings/' . $item->id) }}" title="View Building">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/buildings/' . $item->id . '/edit') }}"
                                               title="Edit Building">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/buildings', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete Building',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>

                              </div>

                          </div>
                      </div>
                  </div>
              </div>
                  
                  
              </div>
                
                <div class="tab-pane" id="services" aria-labelledby="baseIcon-tab2" aria-expanded="false">
                
                  
                        <div class="row ">
                  <div class="col-md-12">
                      <div class="box bordered-box blue-border">
                          
                          <div class="box-content ">

                              
                              <div class="row">
                                  <div class="col-md-6">

                                      {{__('Services')}}


                                  </div>
                                  

                              </div>

                              <div class="table-responsive">
                                  <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Site</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($site->services)
                                    @foreach($site->services as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            <a href="{{ url('/admin/services/' . $item->id) }}" title="View Service">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>

                                            @if(Auth::user()->can('access.service.edit'))

                                            <a href="{{ url('/admin/services/' . $item->id . '/edit') }}"
                                               title="Edit Service">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>

                                            @endif

                                            @if(Auth::user()->can('access.service.delete'))

                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/services', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete Service',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>

                              </div>

                          </div>
                      </div>
                  </div>
              </div>
                  
                  
              </div>
               
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection







