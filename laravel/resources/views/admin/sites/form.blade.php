
@include('admin.for_master.site_input3')


<div class="form-group row {{ $errors->has('company_id') ? 'has-error' : ''}}">
    <label for="company_id" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('site.label.company')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.company')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('company_id',$companies, null, ['class' => 'form-control','required'=>'required','id'=>'company_id']) !!}
        <p class="add pull-right" id="add_company"></p>

        {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('site.label.name')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('default_email') ? 'has-error' : ''}}">
    <label for="default_email" class="col-xl-4 col-sm-4 label-control">@lang('site.label.default_email')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.default_email')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::email('default_email', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('default_email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', trans('site.label.address'), ['class' => 'col-xl-4 col-sm-4 label-control fill_address']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('address', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
  
</div>

<div class="form-group row {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', trans('site.label.city'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('postcode') ? 'has-error' : ''}}">
    {!! Form::label('postcode', trans('site.label.postcode'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('postcode', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', trans('site.label.country'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('country',$countries, null, ['class' => 'form-control']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_number_1', trans('site.label.phone_number_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {{ Form::hidden('code_phone_number_1',null, array('id' => 'code_phone_number_1')) }}
        {!! Form::text('phone_number_1', null, ['class' => 'form-control','style'=>'padding-left: 84px !important','autocomplete'=>'off']) !!}
        {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_type_1', trans('site.label.phone_type_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('phone_type_1',$phoneTypes, null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('site_type') ? 'has-error' : ''}}">
    <label for="site_type" class="col-xl-4 col-sm-4 label-control">@lang('site.label.site_type')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.site_type')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('site_type',$siteTypes, null, ['class' => 'form-control']) !!}
        {!! $errors->first('site_type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($site) && $site->logo != '')


    <div class="form-group row {{ $errors->has('logo') ? 'has-error' : ''}}">
        <label for="logo" class="col-xl-4 col-sm-4 label-control">@lang('site.label.change_logo')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.logo')])
        </label>
        <div class="col-xl-8 col-sm-8">

            <img src="{!! asset('uploads/'.$site->logo) !!}" alt="" width="150">
            <br><br>
            {!! Form::file('logo', null, ['class' => 'form-control']) !!}
            {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@else
    <div class="form-group row {{ $errors->has('logo') ? 'has-error' : ''}}">
        <label for="logo" class="col-xl-4 col-sm-4 label-control">@lang('site.label.logo')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.logo')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('logo', null, ['class' => 'form-control']) !!}
            {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif

<div class="form-group row {{ $errors->has('access_conditions') ? 'has-error' : ''}}">
    <label for="access_conditions" class="col-xl-4 col-sm-4 label-control">@lang('site.label.access_conditions')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.access_conditions')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('access_conditions', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('access_conditions', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('guard_presence') ? 'has-error' : ''}}">
    <label for="guard_presence" class="col-xl-4 col-sm-4 label-control">@lang('site.label.guard_presence')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.guard_presence')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::number('guard_presence', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('guard_presence', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('digicode') ? 'has-error' : ''}}">
    <label for="digicode" class="col-xl-4 col-sm-4 label-control">@lang('site.label.digicode')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.digicode')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('digicode', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('digicode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('keybox_presence') ? 'has-error' : ''}}">
    <label for="keybox_presence" class="col-xl-4 col-sm-4 label-control">@lang('site.label.keybox_presence')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.keybox_presence')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::number('keybox_presence', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('keybox_presence', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('keybox_code') ? 'has-error' : ''}}">
    <label for="keybox_code" class="col-xl-4 col-sm-4 label-control">@lang('site.label.keybox_code')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.keybox_code')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('keybox_code', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('keybox_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('keybox_place') ? 'has-error' : ''}}">
    <label for="keybox_place" class="col-xl-4 col-sm-4 label-control">@lang('site.label.keybox_place')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.keybox_place')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('keybox_place', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('keybox_place', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('keybox_issue') ? 'has-error' : ''}}">
    <label for="keybox_issue" class="col-xl-4 col-sm-4 label-control">@lang('site.label.keybox_issue')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.keybox_issue')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('keybox_issue', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('keybox_issue', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('known_issue') ? 'has-error' : ''}}">
    <label for="known_issue" class="col-xl-4 col-sm-4 label-control">@lang('site.label.known_issue')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.known_issue')])
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::textarea('known_issue', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('known_issue', '<p class="help-block">:message</p>') !!}
    </div>
</div>




@if(isset($site) && $site->instructions_tasks_file != '')

    <div class="form-group row {{ $errors->has('instructions_tasks_file') ? 'has-error' : ''}}">
        <label for="instructions_tasks_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.update_instructions_tasks_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.instructions_tasks_file')])
        </label>
        <div class="col-xl-8 col-sm-8">


            <a href="{!! url('/uploads/files/'.$site->instructions_tasks_file) !!}" target="_blank">
                <img src="{!! asset('img/pdf.png') !!}" alt="" width="25">
                @lang('comman.label.download_file')
            </a>
            <br>
            <br>
            {!! Form::file('instructions_tasks_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('instructions_tasks_file', '<p class="help-block">:message</p>') !!}
            <br>
            <br>

        </div>
    </div>
@else
    <div class="form-group row {{ $errors->has('instructions_tasks_file') ? 'has-error' : ''}}">
        <label for="instructions_tasks_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.instructions_tasks_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.instructions_tasks_file')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('instructions_tasks_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('instructions_tasks_file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif



@if(isset($site) && $site->order_file != '')


    <div class="form-group row {{ $errors->has('order_file') ? 'has-error' : ''}}">
        <label for="order_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.order_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.order_file')])
        </label>
        <div class="col-xl-8 col-sm-8">

            <a href="{!! url('/uploads/files/'.$site->order_file) !!}" target="_blank">
                <img src="{!! asset('img/pdf.png') !!}" alt="" width="25">
                @lang('comman.label.download_file')
                
            </a>
            <br>
            <br>
            {!! Form::file('order_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('order_file', '<p class="help-block">:message</p>') !!}
            <br>
            <br>

        </div>
    </div>
@else
    <div class="form-group row {{ $errors->has('order_file') ? 'has-error' : ''}}">
        <label for="order_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.order_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.order_file')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('order_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('order_file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif





@if(isset($site) && $site->procedures_file != '')


    <div class="form-group row {{ $errors->has('procedures_file') ? 'has-error' : ''}}">
        <label for="procedures_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.procedures_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.procedures_file')])
        </label>
        <div class="col-xl-8 col-sm-8">

            <a href="{!! url('/uploads/files/'.$site->procedures_file) !!}" target="_blank">
                <img src="{!! asset('img/pdf.png') !!}" alt="" width="25">
                @lang('comman.label.download_file')
                </b>
            </a>
            <br>
            <br>
            {!! Form::file('procedures_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('procedures_file', '<p class="help-block">:message</p>') !!}
            <br>
            <br>

        </div>
    </div>
@else
    <div class="form-group row {{ $errors->has('procedures_file') ? 'has-error' : ''}}">
        <label for="procedures_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.procedures_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.procedures_file')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('procedures_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('procedures_file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif



@if(isset($site) && $site->sitemaps_file != '')

    <div class="form-group row {{ $errors->has('sitemaps_file') ? 'has-error' : ''}}">
        <label for="sitemaps_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.sitemaps_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.sitemaps_file')])
        </label>
        <div class="col-xl-8 col-sm-8">

            <a href="{!! url('/uploads/files/'.$site->sitemaps_file) !!}" target="_blank">
                <img src="{!! asset('img/pdf.png') !!}" alt="" width="25">
                 @lang('comman.label.download_file')
                </b>
            </a>
            <br>
            <br>
            {!! Form::file('sitemaps_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('sitemaps_file', '<p class="help-block">:message</p>') !!}
            <br>
            <br>

        </div>
    </div>

@else

    <div class="form-group row {{ $errors->has('sitemaps_file') ? 'has-error' : ''}}">
        <label for="sitemaps_file" class="col-xl-4 col-sm-4 label-control">@lang('site.label.sitemaps_file')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.site.form_field.sitemaps_file')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::file('sitemaps_file', null, ['class' => 'form-control']) !!}
            {!! $errors->first('sitemaps_file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

@endif

<div class="form-group row {{ $errors->has('geolocation') ? 'has-error' : ''}}">
    {!! Form::label('geolocation', "geolocation", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
		 {!! Form::hidden('longitude', null, ['class' => 'form-control search_longitude','id'=>"longitude"]) !!}
         {!! Form::hidden('latitude', null, ['class' => 'form-control search_latitude','id'=>"latitude"]) !!}
		 
         {!! Form::text('geolocation', null, ['class' => 'form-control','id'=>"geolocation_search"]) !!}
		  <div id="map-canvas" class="gmaps" style="height: 300px;width: 100%"></div>	
         {!! $errors->first('geolocation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('site.label.active'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rd1']) !!} <label for="rd1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false ,['id' => 'rd2']) !!} <label for="rd2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row">
    <lable class="col-xl-4 col-sm-4 label-control"></lable>
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText :__('Create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>


@push('js')
<script src="{!! asset('js/google-search-location-map.js')!!}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\config('settings.GOOGLE_MAP_API')}}&libraries=places&callback=initAutocomplete"
            async defer></script>
			
<script>

    var old_code1 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";
    var old_code2 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";;
    @if(isset($site) && $site->code_phone_number_1 != '')
        old_code1 ="{{$site->code_phone_number_1}}";
    @endif
    @if(isset($site) && $site->code_phone_number_2 != '')
        old_code2 ="{{$site->code_phone_number_2}}";
    @endif

    $("#phone_number_2").intlTelInput({
        preferredCountries:[old_code2],
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });
    $("#phone_number_1").intlTelInput({
        preferredCountries:[old_code1],
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });

    $("#phone_number_1").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_1").val(code);
    });
    $("#phone_number_2").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_2").val(code);
    });

    companySelect();
   
    function companySelect(){
        var companies = <?php echo json_encode($companies);?> ;
        companies =  Object.keys(companies).length;
        if(companies <= 1){
            $(".add").show();
            var url = "{{url('admin/companies/create')}}";
            var element = $('add_company');
            $('#add_company').html($("<a></a>").attr("href", url).text('Add New Company'));
        }else{
            $(".add").hide();
        }
    }

</script>

@endpush


@if(_MASTER)
    @push('js')
    <script>
        var sub_search_url ="{{url('admin/siteoptionfilterbywebsite')}}";
        var selected_company = "0";


        @if(isset($site))
            selected_company = "{{$site->company_id}}";
        @endif

        initSelect();
        function initSelect(){
            var filter_id = $('#_website_id').val();
           

            $("#company_id").html("");

            $.ajax({
                type: "get",
                url: sub_search_url,
                data:{_website_id:filter_id},
                success: function (result) {
                    data = result.data.company;
                    if(data.length == 0){
                        $(".add").show();
                        var url = "{{url('admin/companies')}}"+"?filter_website="+filter_id;
                        var element = $('add_company');
                        $('#add_company').html($("<a></a>").attr("href", url).text('Add New Company for this Website'));
                    }else{
                        $(".add").hide();
                        for(var i=0;i<data.length;i++){
                            var selected="";
                            if (data[i]['id'] == selected_company) { selected = "selected=selected"; }
                            $("#company_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                        }
                    }
                    
                },
                error: function (xhr, status, error) {
                }
            });
        }

        $('#_website_id').change(function() {
            initSelect();

        });
        

    </script>


    @endpush

@endif
