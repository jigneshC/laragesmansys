<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('comman.label.name')
    </label>

    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('lang_code') ? 'has-error' : ''}}">
    <label for="lang_code" class="col-md-4 label-control">
        <span class="field_compulsory">*</span>@lang('language.label.lang_code')
    </label>

    <div class="col-md-6">
        {!! Form::text('lang_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('lang_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('company.label.active'), ['class' => 'col-md-4 label-control']) !!}
    <div class="col-md-6 status_rad">
        <div class="radio">
           {!! Form::radio('active', '1',true, ['id' => 'rd1']) !!} <label for="rd1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false ,['id' => 'rd2']) !!} <label for="rd2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <label class="col-md-4 label-control"></label>
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
    </div>
</div>
