@extends('layouts.backend')

@section('title',trans('language.label.show_language'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('language.label.show_language') # {{ $language->name }}
                    </div>
                    <div class="actions">
                        @include('partials.page_tooltip',['model' => 'language','page'=>'index'])
                    </div>

                </div>
                <div class="box-content ">

                    <a href="{{ url('/admin/languages') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.language.edit'))
                    <a href="{{ url('/admin/languages/' . $language->id . '/edit') }}" title="Edit Language">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('comman.label.edit')
                        </button>
                    </a>
                    @endif

                    @if(Auth::user()->can('access.language.delete'))
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/languages', $language->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Language',
                            'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Language'])."')"
                    ))!!}
                    {!! Form::close() !!}

                    @endif
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <th>@lang('comman.label.id')</th>
                                <td>{{ $language->id }}</td>
                            </tr>
                            <tr>
                                <th> @lang('comman.label.name')</th>
                                <td> {{ $language->name }} </td>
                            </tr>
                            <tr>
                                <th> @lang('language.label.lang_code')</th>
                                <td> {{ $language->lang_code }} </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
