<div class="content-wrapper1"><div class="chat-application" style="height: 427px;">
  <div class="content-overlay"></div>
  
  
  @if($ticket->logs)
  <section class="chat-app-window">
    <div class="badge badge-dark mb-1"></div>
    <div class="chats">
      <div class="chats">
         @foreach($ticket->logs as $log)  
        <div class="chat chat-left">
          <div class="chat-avatar">
            <a class="avatar" data-toggle="tooltip" href="javascript:void(0)" data-placement="right" title="" data-original-title="">
                <img alt="Avatar" class="rounded-circle mr-1" height="37" src="{!! asset('assets/images/avatar.jpg') !!}" width="37">
                
                {{ $log->user->name}}
                
                @if($log->history_type == "action") <b style="color: #9c27b0"> @lang('ticket.label.mark_as') </b> @endif
                @if($log->history_type == "file") <b style="color: #9c27b0"> @lang('ticket.label.added_a_file') </b> @endif
                    @if($log->history_type == "assign") <b style="color: #9c27b0"> @lang('ticket.label.assign_ticket_to') </b> @endif
            </a>
          </div>
          <div class="chat-body">
              <div class="chat-content">
                  @if($log->history_type == "comment")
                  <p>{{ $log->desc}}</p>
                  @elseif($log->history_type == "action")
                  <p>{{ $log->desc}}</p>
                  @elseif($log->history_type == "assign")
                  <p>{{ $log->desc}}</p>
                  @elseif($log->history_type == "file")
                  @php( $path_info = pathinfo($log->desc) )
                  @php( $clas = "view_in_popup")
                  @if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG','pdf','PDF']))
                  @php( $clas = "view_in_popup")
                  @endif

                  @if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG']) && $log->file_url)
				
					 <a class="example-image-link" href="{!! asset('uploads/'.$log->desc) !!}" data-lightbox="example-2" data-title="{{$log->note}}">
						<img class="example-image" @if($log->image_thumb) src="{{$log->image_thumb}}" @else src="{{$log->file_url}}" @endif alt="{{$log->note}}" height="100px" />
					 </a>
				 
                 
                  @elseif(isset($path_info['extension']) && in_array($path_info['extension'],['docx','docm','docb','dotx','dotm','dotb']) && $log->file_url)
					<a href="{!! asset('uploads/'.$log->desc) !!}" target="_blank" file_type="{{$path_info['extension']}}" class="{{$clas}}"> <img src="{!! asset('img/word.png') !!}" height="100px"> </a>
				  @elseif(isset($path_info['extension']) && in_array($path_info['extension'],['pdf']) && $log->file_url)
					<a href="{!! asset('uploads/'.$log->desc) !!}" target="_blank" file_type="{{$path_info['extension']}}" class="{{$clas}}"> <img src="{!! asset('img/apdf.png') !!}" height="100px"> </a>
				  @else	  
					    <a href="{!! asset('uploads/'.$log->desc) !!}" target="_blank" file_type="{{$path_info['extension']}}" class="{{$clas}}"> <img src="{!! asset('assets/images/file.png') !!}" height="100px"> </a>
                  @endif
				  	

                  @endif

                  @if($log->note != "")  <p>{{$log->note}} </p> @endif
              </div>
              <p class="time pull-right">{{ $log->created_at->diffForHumans() }}</p>
          </div>
            
        </div>
         
        @endforeach
      </div>
    </div>
  </section>
  @endif
  
  @if(1 || Auth::user()->can('access.ticket.action'))
      @include ('admin.tickets.form_comment', ['submitButtonText' => 'Comment'])
  @endif                                        
</div>
</div>





@push('js')

<script src="{!! asset('apex/js/chat.js') !!}" type="text/javascript"></script>
 
@endpush

