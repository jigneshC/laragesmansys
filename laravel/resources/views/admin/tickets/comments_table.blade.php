



 <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                        <h4 class="card-title">@lang('ticket.label.action_log') </h4>
                    </div>
                    <div class="col-9">
                        <div class="actions pull-right">
							 <div class="form-group">
                                              <select class="full-width filter form-control" id="filter_status" name="filter">
                                                <option value="">@lang('ticket.label.all')</option>
                                                @foreach(trans('ticket.history_type') as $k => $val)
                                                    <option value="{{$k}}" >{{$val}}</option>
                                                @endforeach
                                            </select>
                                          </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <table class="table table-striped table-bordered base-style responsive datatable">
                            
                          <thead>
                            <tr>
                                <th data-priority="1" >@lang('comman.label.id')</th>
                                <th data-priority="2" title="@lang('ticket.label.actioner')">@lang('ticket.label.actioner')</th>
                               
                                <th data-priority="4" title="@lang('ticket.label.action')">@lang('ticket.label.action')</th>
                                <th data-priority="5" title="@lang('ticket.label.detail')">@lang('ticket.label.detail') </th>
                                <th data-priority="6" title="@lang('comman.label.created')">@lang('comman.label.created')</th>
							</tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
	


	
@push('js')
<script>

    var type_dropdown = <?php echo json_encode(trans('subject.subject_type')); ?>;
    
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "desc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            { "data": "name","name":"name"},
            {
                "data": null,
                "name": "history_type",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
					var action = "";
                    if(o.history_type=="action"){
                        action ="@lang('ticket.label.mark_as')";
                    }else if(o.history_type=="assign"){
                        action ="@lang('ticket.label.assign_ticket_to')";
                    }else if(o.history_type=="file"){
                        action ="@lang('ticket.label.added_a_file')";
                    }else{
                        action ="@lang('ticket.label.comments')";
                    }
                    return action;
                }
            },
            {
                "data": null,
                "name": "desc",
                "searchable": true,
                "orderable": false,
                "render": function (o) {
					var des= o.desc;
                    if(o.history_type=="file"){
						var ext = o.desc.split('.').pop();
						var iimg= "<img src = '{!! asset('img/image.png') !!}' height='100'>";
								
						if(ext != ""){ ext = ext.toLowerCase(); }
								
						if(ext=="pdf"){
							iimg= "<img src = '{!! asset('img/apdf.png') !!}' height='100'>";
						}else if(ext=="docx" || ext=="docm" || ext=="dotx" || ext=="dotm" || ext=="docb"){
							iimg= "<img src = '{!! asset('img/word.png') !!}' height='100'>";
						}else if(o.image_thumb && o.image_thumb !=""){
							iimg= "<img src = '"+o.image_thumb+"' height='100'>";	
						}
								
						if(ext=="jpg" || ext=="jpeg" || ext=="png" || ext=="gif"){
							des= "<a class='example-image-link' href='{!! asset('uploads') !!}/"+o.desc+"' data-lightbox='example-1' data-title=''>"+iimg+"</a>";
						}else{
							des= "<a href='{!! asset('uploads') !!}/"+o.desc+"' file_type='"+ext+"' class='link_file view_in_popup' target='_blank'> "+iimg+" </a>";
						}
					}
                    return des;
                }
            },
            {
                "data": null,
                "name": "created_at",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
					return getLangFullDate(o.created_tz,"short");
                }
            }
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/tickets/datatable') }}/{{$ticket->id}}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                d.history_type = $("#filter_status").val();
            }
        }
    });
    $('.filter').change(function() {
        datatable.fnDraw();
    });
	

</script>


@endpush



