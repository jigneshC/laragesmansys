<div class="modal fade text-left" id="file_upload_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('ticket.label.add_an_evidence')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => 'admin/tickets/action', 'class' => 'form-horizontal file_upload_form', 'files' => true]) !!}

            {!! Form::hidden('ticket_id',0, ['class' => 'form-control','id'=>'file_upload_ticket_id']) !!}
            <div class="modal-body">
                <label>@lang('ticket.label.evidence') </label>
                <div class="form-group position-relative">
                    {!! Form::file('file',  ['class' => '']) !!}
                    <div class="form-control-position">
                        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <label>@lang('ticket.label.note') </label>
                <div class="form-group position-relative has-icon-left">
                    {!! Form::text('note','' , ['class' => 'form-control']) !!}
                    <!--div class="form-control-position">
                        <i class="fa icon-notebook font-large-1 line-height-1 text-muted icon-align"></i>
                    </div-->
                    {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-light']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



@push('js')

<script>
    /***************action model******************/
    var file_upload_model = "#file_upload_modal";
    $(document).on('click', '.file_upload', function (e) {
        var id = $(this).attr('data-id');
        $("#file_upload_ticket_id").val(id);
        $('.file_upload_form')[0].reset();
        $(file_upload_model).modal('show');

        return false;
    });
</script>
@endpush