<div class="modal fade text-left" id="action_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('ticket.label.mark_ticket_as')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => 'admin/tickets/action', 'class' => 'form-horizontal action_form', 'files' => true]) !!}
            {!! Form::hidden('ticket_id',0, ['class' => 'form-control','id'=>'action_ticket_id']) !!}
                        
            <div class="modal-body">
                <label>@lang('ticket.label.mark_as') </label>
                <div class="form-group position-relative">
                     <select class="form-control" id="mark_status" name="status">
                                    <option value="" selected="selected">@lang('comman.label.select_from_dropdown')</option>
                                    @foreach(trans('dashboard.status_dropdown') as $k => $val)
                                        <option value="{{$k}}">{{$val}}</option>
                                    @endforeach
                                </select>
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
                
                <label>@lang('ticket.label.place_a_comment') </label>
                <div class="form-group position-relative has-icon-left">
                    {!! Form::textarea('comment','' , ['class' => 'form-control','id'=>'status_comment' ,'rows' => 4, 'cols' => 40]) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-light']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>
/***************action model******************/
    var action_model = "#action_modal";
        $(document).on('click', '.more-action', function (e) {
        var id=$(this).attr('data-id');
        var status=$(this).attr('data-status');


        $('.action_form')[0].reset();
        //$("#mark_status").val(status);
        $("#action_ticket_id").val(id);
        var isfound=0;
        $('#mark_status option').each(function(index,element){
            $(element).attr("disabled", false);
            if(isfound==0){
                $(element).attr("disabled","disabled");
            }
            if($(element).val()==status){
                isfound=1;
            }
        });
        $(action_model).modal('show');

        return false;
    });
	
	$('.action_form').submit(function(event) {


        var formData = {
            'ticket_id': $("#action_ticket_id").val(),
            'status': $("#mark_status").val(),
            'comment': $("#status_comment").val(),
        };

        $.ajax({
            type: "POST",
            url: url + "/action",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {

                $(action_model).modal('hide');
                @if($isReload==0)
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                @else
                    window.location.reload();
                @endif
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush