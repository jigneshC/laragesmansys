<div class="row">
    <div class="col-xl-6 col-sm-12">
        @include('admin.for_master.site_input3')

        {{-- <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        <label for="title" class="col-xl-4 col-sm-4 control-label">
            <span class="field_compulsory">*</span>@lang('ticket.label.title')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::text('title', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div> --}}
    
    <div class="form-group row {{ $errors->has('site_id') ? 'has-error' : ''}}">
        <label for="site_id" class="col-xl-4 col-sm-4 label-control">
            @lang('service.label.site')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('site_id',[], [], ['class' => 'form-control','id'=>'site_id']) !!}
            {!! $errors->first('site_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('subject_id') ? 'has-error' : ''}}">
        {{--<label for="subject_id" class="col-xl-4 col-sm-4 label-control">
                <span class="field_compulsory">*</span>@lang('ticket.label.subject')
            </label>--}}
        {!! Form::label('subject_id',trans('ticket.label.subject'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('subject_id',[], null, ['class' => 'full-width full-height-input filter', 'required' => 'required']) !!}
            {!! $errors->first('subject_id', '<p class="help-block">:message</p>') !!}

        </div>
    </div>



    <div class="form-group row {{ $errors->has('equipment_id') ? 'has-error' : ''}}">
        {!! Form::label('equipment_id',trans('ticket.label.equipment_id'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('equipment_id',[], null, ['class' => 'form-control']) !!}
            {!! $errors->first('equipment_id', '<p class="help-block">:message</p>') !!}

        </div>
    </div>


    <div class="form-group row {{ $errors->has('content') ? 'has-error' : ''}}">
        <label for="content" class="col-xl-4 col-sm-4 label-control line-height-two">
            <span class="field_compulsory">*</span>@lang('ticket.label.content')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::textarea('content', null, ['class' => 'form-control textarea-height-two','id'=>'else','autocomplete'=>'off']) !!}
            {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group row {{ $errors->has('file') ? 'has-error' : ''}}">
        <label for="emergancy_password" class="col-xl-4 col-sm-4 label-control">@lang('ticket.label.evidence')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.ticket.form_field.evidence')])
        </label>

        <div class="col-xl-8 col-sm-8">
            {!! Form::file('file',  ['class' => '']) !!}
            {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    {{--<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">--}}

    {{--{!! Form::label('content', , ['class' => 'col-xl-4 col-sm-4 control-label']) !!}--}}
    {{--<div class="col-xl-8 col-sm-8">--}}
    {{--{!! Form::textarea('content', null, ['class' => 'form-control']) !!}--}}
    {{--{!! $errors->first('content', '<p class="help-block">:message</p>') !!}--}}
    {{--</div>--}}

    {{--</div>--}}
    @if(0 &&  isset($ticket))
    <div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
        {!! Form::label('status', trans('ticket.label.status'), ['class' => 'col-md-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('status',$status, null, ['class' => 'form-control']) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @endif




    <div class="form-group row">
        <lable class="col-xl-4 col-sm-4 label-control"></lable>
        <div class="col-xl-8 col-sm-8">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
    </div>
</div>
<div class="col-md-6">


    <div class="form-group">
        <div class="">
            <div id="caller_list"></div>
        </div>
    </div>
    <div class="form-group">
        <div class="">
            <div id="fileContainer"></div>
        </div>
    </div>
</div>
</div>

@push('js')
<script>
    
    
    var _setUp = function (value) {
        var fileContainer = $('#fileContainer');
        if (value == '' || !value) {
            fileContainer.empty();
        } else {
            var _url = "{!! url('admin/subjects/get-view/') !!}";
            fileContainer.load(_url + '/' + value);
        }
    };

    var _setUpCallerList = function (value) {
        var url ="{{url('admin/subjects/get-caller')}}";
        var listContainer = $('#caller_list');
        var title ="<h3> Caller list</h3>";
        listContainer.html("");
        if (value == '' || !value) {
            return false;
        } else {
            $.ajax({
                type: "get",
                url: url + "/" + value,
                success: function (result) {
                   
                    data = result.caller_list;
                    var table= title+"<hr/><table width='100%'> <tr><th>Name</th><th>Phone1</th><th>Phone2</th></tr>";
                    for(var i=0;i<data.length;i++){

                        var tr = "<tr><td>"+ data[i].first_name+" "+data[i].last_name+"</td>";
                             if(data[i].phone_number_1){ tr = tr+"<td>"+data[i].phone_number_1+"</td>"; }else { tr= tr+"<td></td>"}
                             if(data[i].phone_number_1){ tr = tr+"<td>"+data[i].phone_number_1+"</td>"; }else { tr= tr+"<td></td>"}
                        tr=tr+"</tr>";
                        table = table+tr;
                    }
                    table= table+"</tr></table><hr/>";
                    listContainer.html(table);
                },
                error: function (xhr, status, error) {
                }
            });
        }
    };

    $(document).ready(function () {
        var subject = $('#subject_id');
        if(subject.val()){
            _setUp(subject.val());
            _setUpCallerList(subject.val());
        }
        subject.change(function () {
            var value = $(this).val();
            _setUp(value);
            _setUpCallerList(subject.val());
        });

       

    });
</script>
@endpush



@push('js')
<script>
    var sub_search_url ="{{url('admin/ticketsoptionfilterbywebsite')}}";
    var selected_sub = "0";
    var selected_equ = "0";
    var selected_site = "0";
    var data_subject = [];

    @if(isset($ticket))
        selected_sub = "{{$ticket->subject_id}}";
        selected_equ = "{{$ticket->equipment_id}}";
	selected_site = "{{$ticket->site_id}}";
    @endif

    initSelect();
    function initSelect(){
        @if(_MASTER)
	var filter_id = $('#_website_id').val();
        @else
	var filter_id = "{{_WEBSITE_ID}}";
        @endif

        $("#subject_id").html("");
	$("#site_id").html("");
        $("#equipment_id").html("");

        $.ajax({
            type: "get",
            url: sub_search_url,
            data:{_website_id:filter_id},
            success: function (result) {
                    data = result.data.sites;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_site) { selected = "selected=selected"; }
                        $("#site_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    
                    data = result.data.equipment;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_equ) { selected = "selected=selected"; }
                        $("#equipment_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['equipment_id'] + "</option>");
                    }
				
                    initSelectSubject();

            },
            error: function (xhr, status, error) {
            }
        });
    }
	
	function initSelectSubject(){
		$('#subject_id').select2("destroy");
		$("#subject_id").html("");
                
		var site_id =$("#site_id").val();
                
                if(site_id){
                
                    $.ajax({
                    type: "get",
                    url: "{{ url('admin/subjects/search') }}",
                   data:{site_id:site_id,tickettype:'ticket'},
                    success: function (result) {
                            data = result.data;
                            for(var i=0;i<data.length;i++){
                                var selected="";
                                if (data[i]['id'] == selected_sub) { selected = "selected=selected"; }
                                $("#subject_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] +" ("+data[i]['services']['name']+") </option>");
                            }
							$('#subject_id').select2({
							  allowClear: true
							});
                            if(1){
                                 _setUpCallerList($("#subject_id").val());
                                 _setUp($("#subject_id").val());
                            }

                            },
                            error: function (xhr, status, error) {
                            }
                    });
                
                }
        }

    $('#_website_id').change(function() {
        initSelect();
    });
    $('#site_id').change(function() {
        initSelectSubject();
    });


</script>


@endpush

