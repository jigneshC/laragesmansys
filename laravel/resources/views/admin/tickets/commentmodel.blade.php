<div class="modal fade text-left" id="comment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('ticket.label.place_a_comment')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => 'admin/tickets/action', 'class' => 'form-horizontal comment_form', 'files' => true]) !!}
            {!! Form::hidden('ticket_id',0, ['class' => 'form-control','id'=>'comment_ticket_id']) !!}
                        
            <div class="modal-body">
               
                
                <label>@lang('ticket.label.comment') </label>
                <div class="form-group position-relative has-icon-left">
                    {!! Form::textarea('comment','' , ['class' => 'form-control', 'rows' => 4, 'cols' => 40]) !!}
                    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-light']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>
/***************action model******************/
    var comment_modal = "#comment_modal";
        $(document).on('click', '.comment-action', function (e) {
        var id=$(this).attr('data-id');
        var status=$(this).attr('data-status');


        $('.comment_form')[0].reset();
        //$("#mark_status").val(status);
        $("#comment_ticket_id").val(id);
        $(comment_modal).modal('show');

        return false;
    });
</script>
@endpush