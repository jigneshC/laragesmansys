<html>

<head>
    <meta charset="utf-8">


    <style>
        .page-break {
            page-break-after: always;
        }
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Montserrat', sans-serif;
            color: #555;
        }

        * {
            font-size: 16px;
            line-height: 20px;
            color: #666;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }



        .td-bordered{
            border: 1px solid black;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .bold1{
            font-weight: bold;
            font-size: 20px;
        }

        td.text-right {
            text-align: right !important;
            vertical-align: middle !important;
        }
        .red_color {
            color: red;
        }




    </style>
</head>
<body>
<div class="invoice-box">

    <table class="table">
    @php( $website=0 )
    @foreach($tickets as $item)

            @if($item->_website_id != $website)
                <tr>
                    <td class ="td-bordered">
                        <table class="table site_detail">
                            <tr>
                                <td rowspan="2">
                                    @if($item->_website->refimages && @getimagesize(asset('uploads/website/'.$item->_website->refimages->image)))
                                         <img height="100" src="{!! asset('uploads/website/'.$item->_website->refimages->image) !!}"/>
                                    @elseif(_WEBSITE_LOGO && @getimagesize(asset('uploads/website/'._WEBSITE_LOGO)))
                                        <img height="100" src="{!! asset('uploads/website/'._WEBSITE_LOGO) !!}"/>
                                    @else
                                        <img height="100" src="{!! asset('assets/images/logo_lg.png') !!}"/>
                                    @endif
                                </td>
                                <td class="text-right"><span class="bold1"> {{ $item->_website->name }} </span></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    @if(isset($filter['range_start']) && isset($filter['range_end']))
                                        <span class="bold1">
                                            {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$filter['range_start'])->format("jS M, Y") }}
                                            to
                                            {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$filter['range_end'])->format("jS M, Y") }}
                                        </span>
                                    @endif
                                </td>
                            </tr>
                        </table>

                     </td>
                </tr>
            @endif
            <tr>
                <td class ="td-bordered">

                    {{--<table class="table">
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->subject_name }}</td>
                            <td >{{ $item->content }}</td>
                        </tr>
                    </table>--}}
                    <table class="table" border="1">
                        <tr>
                            <td>{{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->format("jS M, Y g:i a") }} </td>
                            <td><i>Subjet:</i> <strong>{{$item->subject_name}}</strong></td>
                            <td><i>Type:</i> </td>
                            <td ><i>Ref:</i> {{$item->id}} </td>
                        </tr>
                        <tr>
                            <td><i>Site:</i> {{$item->site_name}}</td>
							<td colspan="3" rowspan="4" class="text-right">
                               @foreach($item->evidence as $log)  
							   
							   
									@php( $path_info = pathinfo($log->desc) )
									@if(isset($path_info['extension']) && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG']) && $log->file_url != "")
				
									 <a class="example-image-link" href="{!! asset('uploads/'.$log->desc) !!}" >
										<img class="example-image" src="{!! asset('uploads/'.$log->desc) !!}" alt="{{$log->note}}" height="100px" />
									 </a>
									 
									 
									  @else
									  <a href="{!! asset('uploads/'.$log->desc) !!}" target="_blank" file_type="{{$path_info['extension']}}" > 
										<img src="{!! asset('assets/images/file.png') !!}" height="100px">
										</a>
									  @endif


									
							   @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td ><i>Lieu:</i> -</td>
                        </tr>
                        <tr>
                            <td ><i>Commentaire:</i> <strong class="red_color">****** {{$item->status}} ******</strong></td>
                        </tr>
                        <tr>
                            <td >
                                <p>
                                    {{$item->title}} <br/>
                                    {{$item->content}}
                                </p>
                            </td>
                        </tr>
						
                        <tr>
                            <td colspan="4" class="text-right">
                                {{$item->user_name}}
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

        @php( $website=$item->_website_id )
    @endforeach
    </table>




</div>
</body>

</html>
