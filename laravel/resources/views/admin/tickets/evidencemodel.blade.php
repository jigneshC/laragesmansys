<div class="modal fade text-left" id="evidence_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('ticket.label.evidence')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
           
            <div class="modal-body">
                <div class="text-center">
                    <div class="row rental_option_form_row">
						<object id="object" data="" width="100%"
								height="600" type=''>
							<p>@lang('comman.error_msg.pdf_file_coule_not_loaded') </p>
							<p>@lang('comman.label.download') : <a id= "object_link" href="">File</a></p>
						</object>

                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>



@push('js')

<script>
/***************action model******************/
    var evidence_modal = "#evidence_modal";
        $(document).on('click', '.view_in_popup', function (e) {
            var link = $(this).attr("href");
            var type = $(this).attr("file_type").toLowerCase();;
            if(type=="jpg" || type=="jpeg" || type=="png" || type=="gif"){
                type ="image/"+type;
            }else if(type=="pdf"){
                type ="application/"+type;
            }else{
                window.location.href = link;
                return false;
            }

            $("#object").attr("data",link);
            $("#object").attr("type",type);
            $("#object_link").attr("href",link);
            $(evidence_modal).modal('show');

        return false;
    });
</script>
@endpush