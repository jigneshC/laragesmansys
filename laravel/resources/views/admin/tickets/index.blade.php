@extends('layouts.backend')

@section('title',__('Tickets'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        {{__('Tickets')}}
                    </div>

                </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                            @if(Auth::user()->can('access.ticket.create'))
                                <a href="{{ url('/admin/tickets/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Ticket">
                                    <i class="fa fa-plus" aria-hidden="true"></i> {{__('Add New')}}
                                </a>
                            @endif
                        </div>
                        <div class="col-md-6">

                            {!! Form::open(['method' => 'GET', 'url' => '/admin/tickets', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="{{__('Search...')}}">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Subject')}}</th>
                                <th>{{__('Content')}}</th>
                                <th>{{__('Actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tickets as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->subject->name or null }}</td>
                                    <td class="col-md-3">{{ $item->content }}</td>
                                    <td>
                                        @if( \Auth::user()->id == $item->user_id)
                                        <a href="{{ url('/admin/tickets/' . $item->id) }}" title="{{__('View')}}">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> {{__('View')}}
                                            </button>
                                        </a>
                                        @endif
                                        @if(Auth::user()->can('access.ticket.edit') && \Auth::user()->id == $item->user_id)
                                        <a href="{{ url('/admin/tickets/' . $item->id . '/edit') }}"
                                           title="{{__('Edit')}}">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i> {{__('Edit')}}
                                            </button>
                                        </a>
                                        @endif
                                        @if(Auth::user()->can('access.ticket.delete') && \Auth::user()->id == $item->user_id)
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/tickets', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.__('Delete'), array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Ticket',
                                                'onclick'=>'return confirm("'.__('Confirm delete?').'")'
                                        )) !!}
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
						 </div>
                        <div class="pagination-wrapper"> {!! $tickets->appends(['search' => Request::get('search')])->render() !!} </div>
                   

                </div>
            </div>
        </div>
    </div>
@endsection
