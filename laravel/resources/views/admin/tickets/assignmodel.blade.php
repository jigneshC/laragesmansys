<div class="modal fade text-left the_assign_modal" id="assign_modal"  role="dialog" aria-labelledby="myModalLabel34" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel34">@lang('ticket.label.assign_to')</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! Form::open(['url' => 'admin/tickets/action', 'class' => 'form-horizontal assign_form', 'files' => true]) !!}

            {!! Form::hidden('ticket_id',0, ['class' => 'form-control','id'=>'assign_ticket_id']) !!}
            {!! Form::hidden('website_id',_WEBSITE_ID, ['class' => 'form-control','id'=>'website_id']) !!}
            <div class="modal-body">
                <label>@lang('ticket.label.select_user') </label>
                <div class="form-group position-relative">
                    <input type="text" name="user_id" class="filter" id="assign_user_id" style="width: 100%;height: 32px;margin: 0 !important;">
                     {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="float: none;" data-dismiss="modal" aria-hidden="true">@lang('comman.label.cancel')</button>
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.submit'), ['class' => 'btn btn-light']) !!}

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@push('js')

<script>

    var url = "{{url('admin/tickets')}}";
/***************action model******************/
    var _model = "#assign_modal";
        $(document).on('click', '.assign_to', function (e) {
            var website_id = $(this).attr('data-web-id');

            var id=$(this).attr('data-id');
            var user_id=$(this).attr('data-uid');

            $("#assign_ticket_id").val(id);
            $("#website_id").val(website_id);
            $("#assign_user_id").select2('val', '');
            $(_model).modal('show');

            return false;
        });
	$(document).ready(function(){ 
		$("#assign_user_id").select2({
			dropdownParent: $(".the_assign_modal"),
			placeholder: "@lang('user.label.select_user')",
			allowClear: true,
			ajax: {
				url: "{{ url('admin/users/search') }}",
				dataType: 'json',
				type: "GET",
				data: function (params) {
					return {
						filter_user: params, // search term
						_website_id:$("#website_id").val()
					};
				},
				results: function (result) {
					var data = result.data;
					console.log(data);
					return {
						results: $.map(data, function (obj) {
							return {id: obj.id, text: obj.name};
						})
					};
				},
				cache: false
			}
		});
	});

    $('.assign_form').submit(function(event) {


        var formData = {
            'ticket_id': $("#assign_ticket_id").val(),
            'user_id': $("#assign_user_id").val(),
            'is_reload': "{{$isReload}}",
        };

        $.ajax({
            type: "POST",
            url: url + "/assignuser",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            success: function (data) {

                $(_model).modal('hide');
                @if($isReload==0)
                    datatable.fnDraw(false);
                    toastr.success('Action Success!', data.message)
                @else
                    window.location.reload();
                @endif
            },
            error: function (xhr, status, error) {
                var erro = ajaxError(xhr, status, error);
                toastr.error('Action Not Procede!',erro)
            }
        });

        return false;
    });
</script>
@endpush