
<section class="chat-app-form bg-blue-grey bg-lighten-5">
    {!! Form::open(['url' => '/admin/tickets/comment', 'class' => 'hat-app-input row', 'files' => true,'autocomplete'=>'off']) !!}
	<input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
	<input type="hidden" name="parent_id" value="0">
	<input type="hidden" name="history_type" value="comment">
      <fieldset class="form-group position-relative has-icon-left col-lg-10 col-12 m-0">
        <div class="form-control-position">
          <i class="icon-emoticon-smile"></i>
        </div>
        <input type="text" id="comment" name="desc" class="form-control"  placeholder="@lang('ticket.label.add_your_comment_placeorder')">
        <div class="form-control-position control-position-right">
          <i class="ft-image"></i>
        </div>
      </fieldset>
      <fieldset class="form-group position-relative has-icon-left col-lg-2 col-12 m-0 mmt10 pull-right text-right">
          <button type="submit" class="btn btn-raised btn-primary width100">
          <i class="fa fa-paper-plane-o hidden-lg-up"></i> Send</button>
      </fieldset>
    {!! Form::close() !!}
  </section>