@extends('layouts.apex')

@section('title',trans('ticket.label.show_ticket'))




@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('ticket.label.show_ticket') # {{ $ticket->title }}</div>
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/tickets') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                            @if(Auth::user()->can('access.ticket.edit') && $ticket->isEditable)
                            <a href="{{ url('/admin/tickets/' . $ticket->id . '/edit') }}" title=" @lang('comman.label.edit')">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    @lang('comman.label.edit')
                                </button>
                            </a>
                            @endif
                            @if(Auth::user()->can('access.ticket.delete') && $ticket->isEditable)
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/tickets', $ticket->id],
                                'style' => 'display:inline'

                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Ticket',
                                    'onclick'=>'return confirm("'.trans('comman.js_msg.confirm_for_delete',['item_name'=>'Ticket']).'")'
                            ))!!}
                            {!! Form::close() !!}
                            @endif
                            
                            @if(Auth::user()->can('access.ticket.action'))
								
								
								<a href="#" title="@lang('ticket.label.comment')" data-id="{{ $ticket->id }}" data-status="{{ $ticket->status }}" class='comment-action'>
                                    <button class="btn-link">
                                        <i class="fa fa-edit" aria-hidden="true"></i> @lang('ticket.label.comment')
                                    </button>
                                </a>
								
                                <a href="#" title="@lang('comman.label.action')" data-id="{{ $ticket->id }}" data-status="{{ $ticket->status }}" class='more-action'>
                                    <button class="btn-link">
                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i> @lang('comman.label.action')
                                    </button>
                                </a>

                                <a href="#" title="@lang('ticket.label.evidence')" data-id="{{ $ticket->id }}" class='file_upload'>
                                    <button class="btn-link">
                                        <i class="fa fa-file" aria-hidden="true"></i> @lang('ticket.label.evidence')
                                    </button>
                                </a>
								

                                <a href="#" title="@lang('ticket.label.assign')" data-id="{{ $ticket->id }}" data-web-id="{{ $ticket->_website_id }}" data-uid="{{ $ticket->user_id }}" class='assign_to'>
                                    <button class="btn-link">
                                        <i class="fa fa-user" aria-hidden="true"></i> @lang('ticket.label.assign')
                                    </button>
                                </a>
                            @endif

                          </div>  
                         <div class="next_previous-div clearfix">
                   
                        @if(!empty($ticket->previous()))
                        <a class="btn-link-back" href="{{ $ticket->previous()->id }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous  </a>
                        @endif 						
                        @if(!empty($ticket->next()))
                        &nbsp;&nbsp;|&nbsp;&nbsp;<a class="btn-link-back" href="{{ $ticket->next()->id }}">  Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        @endif  
                    </div>  
                        
                        
	            </div>
				
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">

                    <div class="table-responsive tbl_ticket1">
                        <table class="table table-bordered" width="100%">
							<tbody>
							
							
								
                            @if(_MASTER)
                                <tr>
                                    <td width="10%"><strong>@lang('website.label.website')</strong></td>
                                    <td width="90%">{{ $ticket->_website->domain OR NULL }}</td>
                                </tr>
                            @endif
                            <tr>
                                <td><strong>@lang('ticket.label.id')</strong></td>
                                <td>#{{ $ticket->id }}</td>
                            </tr>
                          {{--  <tr>
                                <td><strong>@lang('ticket.label.title')</strong></td>
                                <td> {{ $ticket->title }} </td>
                            </tr> --}}
							
							
							<tr>
                                <td><strong>@lang('service.label.site')</strong></td>
                                <td> @if($ticket->site) {{ $ticket->site->name or null }} @endif</td>
                            </tr>
							
                            <tr>
                                <td><strong>@lang('ticket.label.subject')</strong></td>
                                <td> {{ $ticket->subject->name or null }} </td>
                            </tr>
                            <tr>
                                <td><strong>@lang('ticket.label.content')</strong></td>
                                <td> {{ $ticket->content }} </td>
                            </tr>
                            @if($ticket->view_on_date)
                            
                                <tr>
                                    <td colspan="2" class="text-center">@lang('ticket.label.planned_activity_detail')</td>
                                </tr>
                                
                                <tr>
                                    <td><strong>@lang('ticket.label.activity_time')</strong></td>
                                    <td> {{ $ticket->activity_time_opt }} </td>
                                </tr>
                                
                                 @if($ticket->activity_time_opt !="daily")
                                    <tr>
                                        <td><strong>@lang('ticket.label.activity_date')</strong></td>
                                        <td> @if($ticket->activity_time_opt =="weekly")
                                                @php( $dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'))
                                                {{ $dowMap[$ticket->view_on_date_val]}}
                                            @else
                                                {{ $ticket->view_on_date_val }}
                                            @endif    
                                        </td>
                                    </tr>
                                 @endif
                                
                                <tr>
                                    <td><strong>@lang('ticket.label.view_before_date')</strong></td>
                                    <td> {{ $ticket->view_before_date }} </td>
                                </tr>
                                
                                <tr>
                                    <td><strong>@lang('ticket.label.planned_start_date')</strong></td>
                                    <td> {{ $ticket->view_start_date }} </td>
                                </tr>
                                
                                <tr>
                                    <td><strong>@lang('ticket.label.planned_end_date')</strong></td>
                                    <td> {{ $ticket->view_end_date }} </td>
                                </tr>
                            
                            @endif
                            
                            @if($ticket->qrcode)
                            <tr>
                                <td><strong>@lang('comman.label.qrcode')</strong></td>
                                <td>  {!!  DNS2D::getBarcodeHTML($ticket->qrcode->qrcode, "QRCODE")  !!} </td>
                            </tr>
                            @endif
							
							@if($ticket->status == 'new')
								
								<tr>
									<td><strong>@lang('subject.label.caller_list')</strong></td>
                                    <td> 
										<table width='100%'> 
											
											@if($ticket->subject && $ticket->subject->caller_list)
												
												<tr><th>@lang('user.label.name')</th><th>@lang('people.label.contact_no')</th></tr>
												@foreach($ticket->subject->caller_list as $caller)
													<tr><th> {{$caller->first_name}} {{ $caller->last_name}} </th>
													<th>
														@if($caller->code_phone_number_1 && $caller->code_phone_number_1 !="" && $caller->phone_number_1 !="")
															({{ strtoupper($caller->code_phone_number_1) }})
														@endif {{$caller->phone_number_1}}
													</th>
													
													</tr>
												@endforeach
											@endif
										</table>
									</td>
                                  
                                </tr>
				
								<tr>
									<td><strong>@lang('subject.label.instruction_file')</strong></td>
                                    <td> @include("admin.subjects.fileview",['subject'=>$ticket->subject,'height'=>600]) </td>
                                  
                                </tr>
								
								
							@endif
				
                            </tbody>
                        </table>
                    </div>
				</div>
	                </div>
	            </div>
				
	        </div>
	    </div>
	</div>

	

	

	
</section>




    @if(Auth::user()->can('access.ticket.action') || \Auth::user()->id == $ticket->user_id)
	{{-- @include ('admin.tickets.comments') --}}
@if($ticket->status != 'new')
	@include ('admin.tickets.comments_table',['ticket'=>$ticket])
@endif
		@include("admin.tickets.commentmodel")
        @include("admin.tickets.actionmodel",['isReload' => 1])
        @include("admin.tickets.assignmodel",['isReload' => 1])
        @include("admin.tickets.fileuploadmodel")
        
    @endif
    @include("admin.tickets.evidencemodel")
@endsection
