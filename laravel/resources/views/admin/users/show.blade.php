@extends('layouts.apex')

@section('title',trans('user.label.show_user'))

@section('content')

<section id="tabs-with-icons">
  <div class="row">
    <div class="col-12 mt-3 mb-1">
      <h4 class="text-uppercase"> @lang('user.label.show_user') </h4>
       {{-- @include('partials.page_tooltip',['model' => 'services','page'=>'index']) --}}
    </div>
  </div>
  <div class="row match-height">
    <div class="col-xl-12 col-lg-12">
      <div class="card" >
        <div class="card-header">
          <h4 class="card-title">@lang('user.label.user') # {{$user->name}}</h4>
        </div>
        <div class="card-body">
          <div class="card-block">
            <ul class="nav nav-tabs">
              <li class="nav-item">
              <a class="nav-link active" id="baseIcon-tab1" data-toggle="tab" aria-controls="tabIcon1" href="#people" aria-expanded="true" ><i class="fa fa-user"></i> @lang('user.label.user') # {{$user->name}}</a>
              </li>
              @if($people)
                     <li class="nav-item">
                        <a class="nav-link" id="baseIcon-tab2" data-toggle="tab" aria-controls="tabIcon2" href="#services" aria-expanded="false"><i class="fa fa-cubes"></i> @lang('people.label.services')</a>
                      </li>
               @endif
             
            </ul>
            <div class="tab-content px-1 pt-1">
              <div role="tabpanel" class="tab-pane active" id="service" aria-expanded="true" aria-labelledby="baseIcon-tab1">
                <div class="row">
                    
                    
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <a href="{{ url('/admin/users') }}" title="Back">
                                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('comman.label.back')
                                        </button>
                                    </a>
                                    @if(Auth::user()->can('access.user.edit'))
                                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            @lang('comman.label.edit')
                                        </button>
                                    </a>
                                    @endif
                                    {!! Form::open([
                                    'method' => 'DELETE',
                                    'url' => ['/admin/users', $user->id],
                                    'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete User',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'User'])."')"
                                    ))!!}
                                    {!! Form::close() !!}
                                    
                                    <div class="next_previous">

                                        @if(!empty($user->previous()))
                                        <a class="btn btn-warning btn-xs" href="{{ $user->previous()->id }}">Previous</a>
                                        @endif 
                                        @if(!empty($user->next()))
                                        &nbsp;&nbsp;|&nbsp;&nbsp;<a class="btn btn-warning btn-xs" href="{{ $user->next()->id }}">Next</a>
                                        @endif  
                                    </div>  

                                </div>
                                <div class="card-body">
                                    <div class="px-3">
                                       <div class="box-content ">
                                       
                                            <?php
                                                $role = join(' + ', $user->roles()->pluck('label')->toArray());
                                             ?>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="table-responsive">
                                                        <table class="table table-borderless">
                                                            <tbody>

                                                                <tr>
                                                                    <th> @lang('user.label.id')</th>
                                                                    <td>{{ $user->id }}</td>
                                                                </tr>


                                                                @if($people)
                                                                <tr>
                                                                    <th> @lang('people.label.first_name') </th>
                                                                    <td> {{ $people->first_name }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.last_name') </th>
                                                                    <td> {{ $people->last_name }} </td>
                                                                </tr>

                                                                @else

                                                                <tr>
                                                                    <td> @lang('user.label.name')</td>
                                                                    <td>{{ $user->name }}</td>
                                                                </tr>

                                                                @endif


                                                                <tr>
                                                                    <td> @lang('user.label.email')</td>
                                                                    <td>{{ $user->email }}</td>
                                                                </tr>

                                                                <tr>
                                                                    <td> @lang('user.label.role')</td>
                                                                    <td>{{ $role }}</td>
                                                                </tr>

                                                                <tr>
                                                                    <td> @lang('people.label.language')</td>
                                                                    <td>{{ $user->language }}</td>
                                                                </tr>

                                                                @if(_MASTER)
                                                                <tr>
                                                                    <td> @lang('website.label.website')</td>
                                                                    <td>{{ $user->_website->domain OR NULL }}</td>
                                                                </tr>
                                                                @endif

                                                                @if($people)
                                                                <tr>

                                                                    <th>@lang('people.label.services')</th>
                                                                    <td>
                                                                        @if($people->services)
                                                                        @foreach($people->services as $k=>$itm)
                                                                        {{ $itm->name }} @if(count($people->services) != $k+1) ,  @endif
                                                                        @endforeach

                                                                        @endif
                                                                    </td>
                                                                </tr>



                                                                <tr>
                                                                    <th>  @lang('people.label.title')</th>
                                                                    <td> {{ $people->title }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.quality') </th>
                                                                    <td> {{ $people->quality }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.phone_number_1')</th>
                                                                    <td>
                                                                        @if($people->phone_number_1)
                                                                        <input type="text" class="phone_number" style="padding-left: 90px !important;     border: 0px solid #ddd !important;" disabled="disabled" value="{{ $people->phone_number_1 }}" id="phone_number_1">
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th> @lang('people.label.phone_number_2')</th>
                                                                    <td>
                                                                        @if($people->code_phone_number_2)
                                                                        <input type="text" class="phone_number" style="padding-left: 90px !important;     border: 0px solid #ddd !important;" disabled="disabled" value="{{ $people->phone_number_2 }}" id="phone_number_2">
                                                                        @endif
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.phone_type_1')</th>
                                                                    <td> {{ $people->dropdownvalue->name OR '' }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.phone_type_2')</th>
                                                                    <td> {{ $people->dropdownvalueM->name OR '' }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.emergancy_password') </th>
                                                                    <td> {{ $people->emergency_password }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.availability')</th>
                                                                    <td> {{ $people->availability }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.hold_key') </th>
                                                                    <td> {{ $people->hold_key }} </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.photo') </th>
                                                                    <td>
                                                                        @if($people->photo)
                                                                        <img src="{!! asset('uploads/'.$people->photo) !!}" alt="" width="150">
                                                                        @endif
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <th> @lang('people.label.active')</th>
                                                                    <td> {{ $people->active == true?"True":'False' }} </td>
                                                                </tr>
                                                                @endif


                                                            </tbody>
                                                        </table>                 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    @if($user->qrcode)
                                                    @include('partials.qrcode',['qrcode' => $user->qrcode])
                                                    @endif
                                                </div>
                                            </div>



                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   
                </div>
              </div>
                 @if($people)
              <div class="tab-pane" id="services" aria-labelledby="baseIcon-tab2" aria-expanded="false">
                
                  
                  <div class="row ">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                    
                    <div class="box-content ">


                        <div class="row">
                            <div class="col-md-6">

                                @if(Auth::user()->can('access.service.create'))
                                <a href="{{ url('/admin/services/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Service">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New

                                </a>
                                @endif


                            </div>
                            <div class="col-md-6">
                                {!! Form::open(['method' => 'GET', 'url' => '/admin/services', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search..."
                                           value="{!! request()->get('search') !!}">

                                    <span class="input-group-btn">
                                        <button class="btn btn-default no-radious" type="submit" style="
                                                padding: 7.5px 15px;
                                                ">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Site</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($people->services))
                                    @foreach($people->services as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->site->name or null}}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            <a href="{{ url('/admin/services/' . $item->id) }}" title="View Service">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>

                                            @if(Auth::user()->can('access.service.edit'))

                                            <a href="{{ url('/admin/services/' . $item->id . '/edit') }}"
                                               title="Edit Service">
                                                <button class="btn btn-primary btn-xs"><i
                                                        class="fa fa-pencil-square-o"
                                                        aria-hidden="true"></i> Edit
                                                </button>
                                            </a>

                                            @endif

                                            @if(Auth::user()->can('access.service.delete'))

                                            {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/services', $item->id],
                                            'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => 'Delete Service',
                                            'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif

                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {{--<div class="pagination-wrapper"> {!! $services->appends(['search' => Request::get('search')])->render() !!} </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
                  
                  
              </div>
                @endif
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>


@endsection


@push('js')
<script>


    $(".phone_number").intlTelInput({
    allowDropdown: false,
            separateDialCode: true,
            utilsScript: "{!! asset('/theme/build/js/utils.js')!!}"
    });
            // var intlNumber = $("#provider_phone").intlTelInput("getNumber");
</script>



<script>
            @if ($people)
            $("#phone_number_1").intlTelInput("setCountry", '{{$people->code_phone_number_1}}');
            $("#phone_number_2").intlTelInput("setCountry", '{{$people->code_phone_number_2}}');
            @endif
</script>
@endpush
