@push('css')
<style>
    .intl-tel-input{
        display: block;
    }
</style>
@endpush

<div class="row ">

    <lable class="col-md-1"></lable>
    <div class="col-md-4">
        @include('admin.for_master.site_input2')
        
        

        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
            <label for="first_name" class="">
                <span class="field_compulsory">*</span>@lang('people.label.first_name')
            </label>
                {!! Form::text('first_name', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
            <label for="last_name" class="">
                <span class="field_compulsory">*</span>@lang('people.label.last_name')
            </label>
                {!! Form::text('last_name', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('language') ? 'has-error' : ''}}">
            <label for="language" class="">
                <span class="field_compulsory">*</span>@lang('people.label.language')
            </label>
                {!! Form::select('language',$languages, null, ['class' => 'form-control']) !!}
                {!! $errors->first('language', '<p class="help-block">:message</p>') !!}

        </div>
        
        
        
        
        <div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
            <label for="role" >
                <span class="field_compulsory">*</span>@lang('user.label.role')
            </label>
            <div >
                {!! Form::select('roles[]',[],[], ['class' => 'full-width selectTag2', 'multiple' => true]) !!}
            </div>
        </div>

        @if(_MASTER)

            <?php
            $_id = isset($website_ids) ? $website_ids : [];

            ?>


            <div class="form-group {{ $errors->has('accessible_websites[]') ? 'has-error' : ''}}">
                {!! Form::label('accessible_websites[]',trans('user.label.accessible_website'), ['class' => '']) !!}
                <div class="">
                    <select class="selectTag" id="accessible_websites" name="accessible_websites[]" multiple = true>
                        @foreach($accessible_websites as $website)
                            <option value="{{$website->id}}" @if(in_array($website->id,$user_accessible_website)) selected="selected" @endif>{{$website->domain}}</option>
                        @endforeach
                    </select>
                    {!! $errors->first('accessible_websites[]', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

        @endif
        
        <div class="form-group {{ $errors->has('services') ? 'has-error' : ''}}">
            <label for="services" class="">@lang('people.label.services')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Services')])
            </label>
            {!! Form::select('services[]',$services, isset($user_services) ? $user_services : [], ['class' => 'selectTag','multiple'=>true,'id'=>'services_selection']) !!}
            {!! $errors->first('services', '<p class="help-block">:message</p>') !!}

        </div>

        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            <label for="title" class="">
                @lang('people.label.title')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Title')])
            </label>
            {!! Form::text('title', null, ['class' => 'form-control','max'=>10,'autocomplete'=>'off']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

        </div>
        
       
        <div class="form-group {{ $errors->has('quality') ? 'has-error' : ''}}">
            <label for="quality" class="">@lang('people.label.quality')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Quality')])
            </label>

                {!! Form::select('quality',$people_quality, null, ['class' => 'form-control ']) !!}
                {!! $errors->first('quality', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
            {!! Form::label('photo', trans('people.label.change_photo'), ['class' => '']) !!}


                @if(isset($people) && $people->photo != '')
                     <br>
                    <img src="{!! asset('uploads/'.$people->photo) !!}" alt="" width="150">
                    <br><br>
                @endif
                    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('hold_key') ? 'has-error' : ''}}">
            <label for="hold_key" class="">@lang('people.label.hold_key')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Hold_Key')])
            </label>
            {!! Form::select('hold_key',['yes'=>'Yes','no'=>"No"], null, ['class' => 'form-control ']) !!}
            {!! $errors->first('hold_key', '<p class="help-block">:message</p>') !!}
        </div>
        
    </div>
     <lable class="col-md-1"></lable>
    <div class="col-md-4">
        
        <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
            <label for="email" class="">
                <span class="field_compulsory">*</span>@lang('user.label.email')
            </label>
                 {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
            <label for="password" class="">
                @if(!isset($user_roles))
                    <span class="field_compulsory">*</span>
                @endif
                @lang('user.label.password')
            </label>
                 {!! Form::password('password', ['class' => 'form-control','autocomplete'=>'new-password']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}

        </div>
        
        <div class="form-group {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
            {!! Form::label('phone_number_1',  trans('people.label.phone_number_1'), ['class' => ""]) !!}
            {{ Form::hidden('code_phone_number_1',null, array('id' => 'code_phone_number_1')) }}
            {!! Form::text('phone_number_1', null, ['class' => 'form-control','style'=>'padding-left: 84px !important','autocomplete'=>'off']) !!}
            {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
            {!! Form::label('phone_type_1', trans('people.label.phone_type_1'), ['class' => '']) !!}
            {!! Form::select('phone_type_1',$phoneTypes, null, ['class' => 'form-control']) !!}
            {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}

        </div>
        <div class="form-group {{ $errors->has('phone_number_2') ? 'has-error' : ''}}">
            {!! Form::label('phone_number_2',  trans('people.label.phone_number_2'), ['class' => '']) !!}
            {{ Form::hidden('code_phone_number_2',null, array('id' => 'code_phone_number_2')) }}
            {!! Form::text('phone_number_2', null, ['class' => 'form-control','style'=>'padding-left: 84px !important','autocomplete'=>'off']) !!}
            {!! $errors->first('phone_number_2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('phone_type_2') ? 'has-error' : ''}}">
            {!! Form::label('phone_type_2', trans('people.label.phone_type_2'), ['class' => '']) !!}
            {!! Form::select('phone_type_2',$phoneTypes, null, ['class' => 'form-control']) !!}
            {!! $errors->first('phone_type_2', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('emergency_password') ? 'has-error' : ''}}">
            <label for="emergancy_password" class="">@lang('people.label.emergancy_password')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Emergancy_Password')])
            </label>

            {!! Form::text('emergency_password',null, ['class' => 'form-control','autocomplete'=>'off']) !!}
            {!! $errors->first('emergency_password', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('availability') ? 'has-error' : ''}}">
            <label for="availability" class="">@lang('people.label.availability')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Availability')])
            </label>
            {!! Form::select('availability',[''=>'select',true=>'True',false=>'False'], null, ['class' => 'form-control drop_down','id'=>'availability']) !!}
            {!! $errors->first('availability', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group {{ $errors->has('status_text') ? 'has-error' : ''}}" id="dropbox">
            {!! Form::label('status_text',  'status-Text', ['class' => ''],['id'=>'statusvalue']) !!}
            {!! Form::text('status_text', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
            {!! $errors->first('status_text', '<p class="help-block">:message</p>') !!}
        </div>
        

        <div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
            {!! Form::label('active', trans('people.label.active'), ['class' => '']) !!}
            {!! Form::select('active',[1=>'Active',0=>'Inactive'], null, ['class' => 'form-control']) !!}
        </div>
        
        <div class="form-group {{ $errors->has('master') ? 'has-error' : ''}}">
            <label for="master" class="">@lang('people.label.sendpassword')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.sendpassword')])
            </label>
          
            <div class="status_rad">
                <div class="radio">
                    {!! Form::radio('sendpassword', '1',(isset($user) && $user)? false:true,['id' => 'rd2']) !!} <label for="rd2">Yes</label>
                </div>
                <div class="radio">
                    {!! Form::radio('sendpassword', '0', (isset($user) && $user)? true:false, ['id' => 'rd1']) !!} <label for="rd1">No</label>
                </div>
                {!! $errors->first('sendpassword', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
		
		 <div class="form-group {{ $errors->has('login_from_date') ? 'has-error' : ''}}" id="dropbox">
            {!! Form::label('login_from_date', trans('user.label.login_from_date'), ['class' => '']) !!}
            {!! Form::text('login_from_date', null, ['class' => 'form-control','id'=>'login_from_date','placeholder'=>trans('user.label.login_from_date')]) !!}
            {!! $errors->first('login_from_date', '<p class="help-block">:message</p>') !!}
        </div>
		<div class="form-group {{ $errors->has('login_to_date') ? 'has-error' : ''}}" id="dropbox">
            {!! Form::label('login_to_date', trans('user.label.login_to_date'), ['class' => '']) !!}
            {!! Form::text('login_to_date', null, ['class' => 'form-control','id'=>'login_to_date','placeholder'=>trans('user.label.login_to_date')]) !!}
            {!! $errors->first('login_to_date', '<p class="help-block">:message</p>') !!}
        </div>
		
		
    </div>
</div>
<div class="row availability_head_row">
     <lable class="col-md-1"></lable>   
	<div class="col-md-9 ">
	<div class="form-group">
        
		{!! Form::label('availability', trans('people.label.availability'), ['class' => 'control-label']) !!}
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.people.form_field.Availability')])
        </div>
	</div>
</div>
<div class="row">
     <lable class="col-md-1"></lable>
      <div class="col-md-9">
    <div class="form-group">

       
       

            <p>
                <button type="button" class="btn btn-info btn-xs" id="cp_day_btn" style="display: none">Copy from monday
                </button>
            </p>

			<div class="table-responsive">
            <table class="tbl_avialability table table-bordered table-striped" width="80%" border="1" bordercolor="#cccccc" cellpadding="5" cellspacing="5">

                <thead>
                <tr>
                    <th>Day</th>
                    <th>From</th>
                    <th>To</th>
                </tr>
                </thead>

                <tbody>

                <tr>
                    <th>Monday</th>

                    <td> {!! Form::text('av[monday][from]', isset($av['monday']['from'])?$av['monday']['from']:null, ['class' => 'form-control time_pick mon_from']) !!}</td>

                    <td> {!! Form::text('av[monday][to]', isset($av['monday']['to'])?$av['monday']['to']:null, ['class' => 'form-control time_pick mon_to']) !!}</td>
                </tr>


                {{--foreach $ev as $k=>v--}}

                {{--day from--}}
                {{--k v[from] v[to]--}}

                <tr class="tuesday tr">
                    <th>
                        Tuesday
                    </th>

                    <td > {!! Form::text('av[Tuesday][from]', isset($av['tuesday']['from'])?$av['monday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Tuesday][to]', isset($av['tuesday']['to'])?$av['tuesday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="wednesday tr">

                    <th>
                        Wednesday
                    </th>

                    <td> {!! Form::text('av[Wednesday][from]', isset($av['wednesday']['from'])?$av['wednesday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Wednesday][to]', isset($av['wednesday']['to'])?$av['wednesday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="thursday tr">

                    <th>
                        Thursday
                    </th>

                    <td> {!! Form::text('av[Thursday][from]', isset($av['thursday']['from'])?$av['thursday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Thursday][to]', isset($av['thursday']['to'])?$av['thursday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="friday tr">

                    <th>
                        Friday
                    </th>

                    <td> {!! Form::text('av[Friday][from]', isset($av['friday']['from'])?$av['friday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Friday][to]', isset($av['friday']['to'])?$av['friday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>

                <tr class="saturday tr">

                    <th>
                        Saturday
                    </th>

                    <td> {!! Form::text('av[Saturday][from]', isset($av['saturday']['from'])?$av['saturday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Saturday][to]', isset($av['saturday']['to'])?$av['saturday']['to']:null, ['class' => 'form-control time_pick']) !!}</td>
                </tr>
                <tr class="sunday tr">

                    <th>
                        Sunday
                    </th>
                    <td> {!! Form::text('av[Sunday][from]', isset($av['sunday']['from'])?$av['sunday']['from']:null, ['class' => 'form-control time_pick']) !!}</td>

                    <td> {!! Form::text('av[Sunday][to]', isset($av['sunday']['to'])?$av['sunday']['to']:null, ['class' => 'form-control time_pick ']) !!}</td>

                </tr>

                </tbody>
            </table>
			</div>

        </div>

    </div>


  

</div>




<div class="row">


    <lable class="col-md-4"></lable>
    <div class="col-md-offset-4">
        <div class="form-group">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
    </div>

    
</div>


@push('js')
<script>

    var sub_search_url ="{{url('admin/useroptionfilterbywebsite')}}";
    var selected_services = "0";

    @if(isset($user_services))
        selected_services = <?php echo json_encode($user_services); ?>;
    @endif
    
    var master = 0;
    @if(_MASTER)
        master = 1;
    @endif

    var roles = <?php echo json_encode($roles); ?>;
    @if(isset($user_roles))
    var selected_roles = <?php echo json_encode($user_roles); ?>;
    @else
    var selected_roles = [];
    @endif

    initSelect();

    function initSelect(){
        var filter_id = $('#_website_id').val();
        
        //--------------------------------User role change on select website---------------------------//
        $('.selectTag2').select2("destroy");
        $(".selectTag2").html("");
        if(filter_id || !master){
            for(var i=0;i<roles.length;i++) {
                if ((roles[i]['website_id'] == filter_id) ||  !master || roles[i]['name'] == "GSA"){
                    var selected="";
                    if(jQuery.inArray(roles[i]['name'], selected_roles) !== -1){
                        selected = "selected=selected";
                    }
                    $(".selectTag2").append("<option value='" + roles[i]['name'] + "' "+selected+">" + roles[i]['label'] + "</option>");
                }
            }
        }
        $('.selectTag2').select2({
            tokenSeparators: [",", " "]
        });
        
        //--------------------------------------Service select2 ---------------------//
        
        if (master){
            $('#services_selection').select2("destroy");
            $("#services_selection").html("");
            $.ajax({
                type: "get",
                url: sub_search_url,
                data:{_website_id:filter_id},
                success: function (result) {
                    data = result.data.service;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if(jQuery.inArray(data[i]['id'], selected_services) !== -1){  selected = "selected=selected"; }
                        $("#services_selection").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    $('#services_selection').select2({
                        tokenSeparators: [",", " "]
                    });

                },
                error: function (xhr, status, error) {
                }
            });
        }
    }

    $('#_website_id').change(function() {
        initSelect();
    });


</script>


@endpush




@push('js')

<script>

    $(function () {
        $("#dropbox").hide()
        $("#availability").change(function () {
            if ($(this).val() == false) {
                $("#dropbox").show();
            } else {
                $("#dropbox").hide();
            }
        });


        var cp_day_btn = $('#cp_day_btn');

        var mon_from = $('.mon_from');
        var mon_to = $('.mon_to');

        $('.mon_from, .mon_to').on('click', function () {
            if (mon_from.val() != '' && mon_to.val() != '') {
                cp_day_btn.show();
            }
            else {
                cp_day_btn.hide();
            }
        });


        //copy button click event
        cp_day_btn.click(function () {

            var from = mon_from.val();
            var to = mon_to.val();

            var first = $(".tr td:nth-child(2)").find('input').val(from);
            var second = $(".tr td:nth-child(3)").find('input').val(to);

        });


    });


</script>


{{--<script>--}}

{{--$("#slider-example2").slider({--}}
{{--range: true,--}}
{{--min: 0,--}}
{{--max: 500,--}}
{{--values: [12am, 12pm],--}}
{{--slide: function (event, ui) {--}}
{{--return $("#slider-example2-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);--}}
{{--}--}}
{{--});--}}

{{--$("#slider-example2-amount").text("$" + $("#slider-example2").slider("values", 0) + " - $" + $("#slider-example2").slider("values", 1));--}}
{{--</script>--}}

@endpush


@push('js')
<script>

    var old_code1 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";
    var old_code2 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";;
    @if(isset($user) && $user->code_phone_number_1 != '')
        old_code1 ="{{$user->code_phone_number_1}}";
    @endif
    @if(isset($user) && $user->code_phone_number_2 != '')
        old_code2 ="{{$user->code_phone_number_2}}";
    @endif

    $("#phone_number_2").intlTelInput({
        preferredCountries:[old_code2],
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });
    $("#phone_number_1").intlTelInput({
        preferredCountries:[old_code1],
        separateDialCode: true,
        utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
    });
    
    $("#phone_number_1").intlTelInput("setCountry", old_code1);
    $("#phone_number_2").intlTelInput("setCountry",old_code2);

    $("#phone_number_1").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_1").val(code);
    });
    $("#phone_number_2").on("countrychange", function(e, countryData) {
        var code =  countryData.iso2;
        $("#code_phone_number_2").val(code);
    });



</script>




@endpush


@push('js')
<script>
    var format_v = "YYYY-MM-DD";

    $(document).ready(function () {
        

        $('#login_from_date').datetimepicker({
            format: format_v,
            useCurrent: false,
        });
        $('#login_to_date').datetimepicker({
            format: format_v,
            useCurrent: false
        });
        $("#login_from_date").on("dp.change", function (e) {
            $('#login_to_date').data("DateTimePicker").minDate(e.date);
        });
        $("#login_to_date").on("dp.change", function (e) {
            $('#login_from_date').data("DateTimePicker").maxDate(e.date);
        });

    });
</script>
@endpush


