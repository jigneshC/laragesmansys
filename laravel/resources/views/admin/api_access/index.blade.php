@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('api_access.label.api_access'))

@push('css')

@endpush

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">
            <label id="timer">@lang('api_access.label.api_access')</label>
        </div>
        {{--  @include('partials.page_tooltip',['model' => 'api_access','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                   
                        <div class="col-9">
                          
                       
                        </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                       <div class="table-responsive"> 
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <th>@lang('api_access.label.accesskey')</th>
                                <td>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="key_text" disabled>
                                        </div>
                                        <div class="form-group">
                                        <button type="button" class="btn btn-default " id="key_btn">@lang('api_access.label.show_access_key')
                                        </button>
                                            </div>

                                        <button type="button" class="btn btn-success" id="regen_btn"
                                                style="display: none" data-user_id="{!! Auth::user()->id !!}">@lang('api_access.label.regenerate_key')
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>@lang('api_access.label.document')</th>
                                <td>
                                    @markdown
									
                                    #APIs

                                    ##Ticket
									
									> **{site}/api/v1.0/ticket/get-sites?api_token=Your api token **

                                    - method : GET
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)
									
                                    > **{site}/api/v1.0/ticket/get-subjects?api_token=Your api token **

                                    - method : GET
                                    - request : 
									- site_id(int|required),
									- response
                                    - messages
                                    - data
                                    - status(true|false)

                                    > **{site}/api/v1.0/ticket/get-subject/{subject_id}?api_token=Your api token **

                                    - method : GET
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)

                                    > **{site}/api/v1.0/ticket/get-tickets?api_token=Your api token **

                                    - method : GET
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)


                                    > **{site}/api/v1.0/ticket/get-ticket/{ticket_id}?api_token=Your api token **

                                    - method : GET
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)


                                    > **{site}/api/v1.0/ticket/create-ticket?api_token=Your api token **

                                    - method : POST
                                    - request :
									- site_id(int|required),
                                    - subject_id(int|required),
                                    - content(string|required),
                                    - equipment_id(string)
                                    - status(['new','open','pending','on-hold','solved','closed']|required)
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)

                                    > **{site}/api/v1.0/ticket/edit-ticket/{ticket_id}?api_token=Your api token **

                                    - method : PUT
                                    - request :
                                    - subject_id(int|optional),
                                    - content(string|optional),
                                    - equipment_id(string)
                                    - status(['new','open','pending','on-hold','solved','closed']|optional)
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)


                                    > **{site}/api/v1.0/ticket/delete-ticket/{ticket_id}?api_token=Your api token **

                                    - method : DELETE
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)
                                    @endmarkdown
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

<div class="modal fade" id="passModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    
                    <h3 class="modal-title">@lang('api_access.label.enter_your_password')</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger _error" style="display: none"></div>
                    <form class="form-horizontal" id="AccessForm" data-toggle="validator">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">@lang('user.label.email'):</label>
                            <div class="col-sm-10">
                                <div class="form-control" disabled="">{{Auth::user()->email}}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">@lang('user.label.password'):</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="pwd"
                                       placeholder="Enter password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">@lang('api_access.label.show_access_key')</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('comman.label.close')</button>
                </div>
            </div>

        </div>
    </div>
@endsection


@push('js')
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
    }


    $(document).ready(function () {

        var access = false;
        var btn = $('#key_btn');
        var input = $('#key_text');
        var passModal = $('#passModal');

        var AccessForm = $('#AccessForm');
        var _error = $('._error');

        var regenBtn = $('#regen_btn');

        regenBtn.hide();//for double sure

        _error.hide();

        //show modal
        btn.click(function (e) {
            e.preventDefault();

            if (access == true) {
                copyToClipboard(input);
                alert('Access key Copied');

                btn.text('Copied');
//                btn.prop('disabled', true);
                return;
            }

            passModal.modal('show');
        });


        regenBtn.click(function (e) {
            e.preventDefault();
            if (access == true) {
//                alert('regen btn clicked');
                var user_id = $(this).data('user_id');
                regenToken(user_id);
            }

        });


        /**
         *
         * @param user_id
         */
        var regenToken = function (user_id) {

            var _url = "{!! url('admin/api-access/regen') !!}";
            $.post(_url, {},
                function (data, status) {

                    if (data.status == true) {
                        input.prop('disabled', false);
                        input.val(data.api_token);
                        btn.text('Copy');
                        alert("@lang('api_access.notification_msg.access_key_regenerated')")
                    } else {
                        alert("@lang('api_access.notification_msg.something_wrong')")
                        input.val('');
                        input.prop('disabled', true);
                    }
                    console.log(status);
                });

        }


        //access form submit
        AccessForm.submit(function (e) {
            e.preventDefault();

            var data = $(this).serializeArray();

            if (data['password'] == '') {
                _error.text("@lang('api_access.notification_msg.please_enter_your_password')").show();
            } else {
                _error.hide();
            }

            var url = "{!! url('admin/api-access') !!}";
            $.post(url, data,
                function (data, status) {

                    if (data.status == true) {
                        //enable
                        input.prop('disabled', false);
                        input.val(data.api_token);
                        btn.text('Copy');
                        access = true;
                        passModal.modal('hide');
                        _error.hide();

                        regenBtn.show();

                    } else {
                        //desable
                        _error.text("@lang('api_access.notification_msg.your_password_is_wrong')").show();
                        input.val('');
                        input.prop('disabled', true);
                        btn.text("@lang('api_access.label.show_access_key')");
                        access = false;
                    }
//                    console.log(data);
                    console.log(status);
//                alert("Data: " + data + "\nStatus: " + status);
                });

        });

    });

</script>
@endpush