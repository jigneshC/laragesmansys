<div class="row">

    <div class="col-xl-6 col-sm-12">
        @include('admin.for_master.site_input3')
    
     <div class="form-group row {{ $errors->has('company_id') ? 'has-error' : ''}}">
        <label for="company_id" class="col-xl-4 col-sm-4 label-control">@lang('site.label.company')</label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('company_id',[], null, ['class' => 'form-control','required'=>'required','id'=>'company_id']) !!}
            {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
     <div class="form-group row {{ $errors->has('service_id') ? 'has-error' : ''}}">
        <label for="service_id" class="col-xl-4 col-sm-4 label-control">@lang('subject.label.service')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.subject.form_field.service')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('service_id',$services, null, ['class' => 'form-control','required'=>'required','id'=>'service_id']) !!}
            {!! $errors->first('service_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>   
	<div class="form-group row {{ $errors->has('is_geolocation') ? 'has-error' : ''}}">
    {!! Form::label('is_geolocation', trans('subject.label.is_geolocation'), ['class' => 'col-md-4 label-control']) !!}
	
	@php( $isg = false )
	 @if(isset($subject) && $subject->geolocation_id && $subject->geolocation_id > 0)
		 @php( $isg = true )
	 @endif
		<div class="col-md-6 status_rad">
			<div class="radio">
				{!! Form::radio('is_geolocation', '0',!$isg, ['id' => 'rd1']) !!} <label for="rd1">No</label>
			</div>
			<div class="radio">
				{!! Form::radio('is_geolocation', '1',$isg ,['id' => 'rd2']) !!} <label for="rd2">Yes</label>
			</div>
			{!! $errors->first('is_geolocation', '<p class="help-block">:message</p>') !!}
		</div>
	</div>
		<div class="form-group is_geolocation_enable row {{ $errors->has('geolocation_id') ? 'has-error' : ''}}">
			<label for="geolocation_id" class="col-xl-4 col-sm-4 label-control">
				<span class="field_compulsory"></span>@lang('subject.label.geolocation_id')
			</label>

			<div class="col-xl-8 col-sm-8">
				{!! Form::select('geolocation_id',$geolocation_ids, null, ['class' => 'form-control']) !!}
				{!! $errors->first('geolocation_id', '<p class="help-block">:message</p>') !!}
			</div>
		</div>
     <div class="form-group row {{ $errors->has('subject_type') ? 'has-error' : ''}}">
        {!! Form::label('subject_type',trans('subject.label.type'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
         <div class="col-xl-8 col-sm-8">
             <select class="form-control" id="subject_type" name="subject_type">
                 @foreach(trans('subject.subject_type') as $k => $val)
                     <option value="{{$k}}" @if(isset($subject) && $subject->subject_type==$k) selected @endif>{{$val}}</option>
                 @endforeach
             </select>
             {!! $errors->first('subject_type', '<p class="help-block">:message</p>') !!}
         </div>
     </div>

    <div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
        <label for="name" class="col-xl-4 col-sm-4 label-control line-height-two">
            <span class="field_compulsory">*</span>@lang('comman.label.name')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    

    <div class="form-group row {{ $errors->has('time') ? 'has-error' : ''}}">
        <label for="time" class="col-xl-4 col-sm-4 label-control">@lang('subject.label.time_to_resolve')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.subject.form_field.time_to_resolve')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::number('time', null, ['class' => 'form-control','min'=>0,'autocomplete'=>'off']) !!}
            {!! $errors->first('time', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group row {{ $errors->has('caller_list') ? ' has-error' : ''}}">
        <label for="caller_list" class="col-xl-4 col-sm-4 label-control">@lang('subject.label.caller_list')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.subject.form_field.caller_list')])
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('xx', $peoples,null,  ['class' => ' selectTag clsel', 'multiple' => true,'id'=>'people_selection']) !!}
            <p>@lang('subject.label.instruction')</p>
        </div>
        {{--caller_list[]--}}

        <input type="hidden" name="caller_list" id="caller_list">
        <p id="demo"></p>
    </div>

    <div class="form-group row">
        <lable class="col-md-4 label-control"></lable>
        <div class="col-md-offset-4">
            <div id="cl">
                <ul id="sort-people">
                    @if(isset($users))
                        @foreach($users as $user)
                            <li id="{{$user->id}}">
                                <img src="{!! asset('css/arrow.png') !!}" alt="move" width="16" height="16"
                                     class="handle"/>
                                <strong>{{$user->full_name}}</strong>

                                {{--<a href="{!! url('/admin/events/'.$event->id.'/menu/'.$menu->id.'/remove') !!}"--}}
                                {{--class="btn btn-danger btn-xs pull-right">Remove</a>--}}
                                {{--<a href="{!! url('/admin/events/'.$event->id.'/menu/'.$menu->id.'/edit') !!}"--}}
                                {{--class="btn btn-info btn-xs pull-right">Edit</a>--}}
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <div class="form-group row {{ $errors->has('file') ? 'has-error' : ''}}">
        <label for="file" class="col-xl-4 col-sm-4 label-control">@lang('comman.label.upload_PDF')
            @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.subject.form_field.upload_PDF')])
        </label>

        <div class="col-xl-8 col-sm-8">
            {!! Form::file('file', ['class' => 'form-control']) !!}
            {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    

    <div class="form-group row ">
        <lable class="col-md-4 label-control"></lable>
        <div class="col-md-offset-4 col-md-4">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
    </div>

    </div>

    <div class="col-xl-6 col-sm-12">
        <div class="form-group row">
            <div class="">
                <div id="caller_list"></div>
            </div>
        </div>
        <div class="form-group">
            @if(isset($subject) && $subject->file)
                <object data="{!! url('/uploads/files/'.$subject->file) !!}" width="100%" height="400"
                        type='application/pdf'>
                    <p>@lang('comman.error_msg.pdf_file_coule_not_loaded')  :(</p>
                </object>

            @endif
        </div>
    </div>
</div>


@push('js')



<script>
    $(document).ready(function () {
		
		@if(isset($subject) && $subject->geolocation_id && $subject->geolocation_id > 0)
		
		@else
			$(".is_geolocation_enable").hide();
		@endif
		
		 $('input[name="is_geolocation"').change(function(){
			if( $(this).val() == 1 ){
				$(".is_geolocation_enable").show();
			}else{
				$(".is_geolocation_enable").hide();
			} 
		  
		 });
		 
		 

        $('.clsel').change(function () {

            var values = $(this).val();
            if (!values) {
                $('#cl').empty();
                return;
            }
            var url = "{!! url('/admin/subjects/get-caller') !!}";
            $.post(url,
                {
                    ids: values,
                },
                function (data, status) {

                    $('#cl').html(data);

                });

        });

        $("ul#sort-people").sortable({
            opacity: 0.6,
            cursor: 'move',
            update: loadStringOnHiddenField
        });

        loadStringOnHiddenField();

    });


    var loadStringOnHiddenField = function () {

        console.log('called');

        var strItems = "";
        $("ul#sort-people").children().each(function (i) {
            var li = $(this);
            strItems += li.attr("id") + ':' + i + ',';
        });
//                var order = $(this).sortable("serialize") + '&action=updateRecordsListings';
        console.log(strItems);

        $('#caller_list').val(strItems);

    };

    $(document).ajaxSuccess(function () {

        loadStringOnHiddenField();

        $("ul#sort-people").sortable({
            opacity: 0.6,
            cursor: 'move',
            update: loadStringOnHiddenField
        });

    });
</script>

@endpush


    @push('js')
    <script>
        var sub_search_url ="{{url('admin/subjectoptionfilterbywebsite')}}";
        var selected_service = "0";
        var selected_people = "0";
        var selected_company = "0";
        var selected_stype = "";
        var data_service = [];

        @if(isset($subject))
            selected_service = "{{$subject->service_id}}";
            selected_people = <?php echo json_encode($subject->xx); ?>;
            selected_company = "{{$subject->company_id}}";
            selected_stype = "{{$subject->subject_type}}";
        @endif


        initSelect();
        function initSelect(){
            @if(_MASTER)
            var filter_id = $('#_website_id').val();
            @else
            var filter_id = "{{_WEBSITE_ID}}";
            @endif
            $("#service_id").html("");
            $("#company_id").html("");
            $('.selectTag').select2("destroy");
            $('#people_selection').select2("destroy");
            $("#people_selection").html("");

            $.ajax({
                type: "get",
                url: sub_search_url,
                data:{_website_id:filter_id},
                success: function (result) {
                    data_service = result.data.service;
                    data = result.data.compoanies;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_company) { selected = "selected=selected"; }
                        $("#company_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    data = result.data.people;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if(jQuery.inArray(data[i]['id'], selected_people) !== -1){  selected = "selected=selected"; }
                        $("#people_selection").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['first_name'] + " "+ data[i]['last_name'] + "</option>");
                    }
                    $('#people_selection').select2({
                        tokenSeparators: [",", " "]
                    });
                    
                    initSelectService();
                    initSubjectType();

                },
                error: function (xhr, status, error) {
                }
            });
        }

        function initSelectService(){
		$("#service_id").html("");
		var selected_se = $("#company_id").val();
		data = data_service;
                for(var i=0;i<data.length;i++){
                    var selected="";
                    if(data[i]['company_id'] == selected_se){
                        if (data[i]['id'] == selected_service) { selected = "selected=selected"; }
                        $("#service_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                }
        }
        
        function initSubjectType(){
		$("#subject_type").html("");
		var selected_comp = $("#company_id").val();
		$.ajax({
                    type: "get",
                    url: "{{url('admin/subjects-type')}}",
                    data:{company_id:selected_comp},
                    success: function (result) {
                        data  = result.data;
                        Object.keys(data).forEach(function(key) {
                            var selected="";
                            if (key == selected_stype) { selected = "selected=selected"; }
                            $("#subject_type").append("<option value='" + key + "' "+selected+">" + data[key] + "</option>");
                        });
                    },
                    error: function (xhr, status, error) {
                    }
                });
        }
        $('#_website_id').change(function() {
            initSelect();
        });
        $('#company_id').change(function() {
            initSelectService();
            initSubjectType();
        });


    </script>


    @endpush

