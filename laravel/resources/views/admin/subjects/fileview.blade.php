<object data="{!! url('/uploads/files/'.$subject->file) !!}" width="100%"
        height=" {{ isset($height)? $height : '400'}}" type='application/pdf'>
    <p>@lang('comman.error_msg.pdf_file_coule_not_loaded')</p>
    <p>@lang('comman.label.download') : <a href="{!! url('/uploads/files/'.$subject->file) !!}">PDF</a></p>
</object>