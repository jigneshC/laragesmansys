@extends('layouts.apex')

@section('title',trans('subject.label.create_subject'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">@lang('subject.label.create_subject')</div>
                {{--  @include('partials.page_tooltip',['model' => 'subject','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/tickets') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                <div class="actions pull-right">
                             @include('partials.page_tooltip',['model' => 'subject','page'=>'form'])
                        </div>
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
	                    {!! Form::open(['url' => '/admin/subjects', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}

                            @include ('admin.subjects.form')

                            {!! Form::close() !!}
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection





