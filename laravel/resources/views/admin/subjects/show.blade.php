@extends('layouts.apex')

@section('title',trans('subject.label.show_subject'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('subject.label.show_subject') # {{ $subject->name }}</div>
                {{--  @include('partials.page_tooltip',['model' => 'subject','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/subjects') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                            @if(Auth::user()->can('access.subject.edit'))
                            <a href="{{ url('/admin/subjects/' . $subject->id . '/edit') }}" title="{{__('Edit Subject')}}">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    @lang('comman.label.edit')
                                </button>
                            </a>
                            @endif
                            @if(Auth::user()->can('access.subject.delete'))
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/subjects', $subject->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => __('Delete Subject'),
                            'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Subject'])."')"
                            ))!!}
                            {!! Form::close() !!}
                            @endif
                            
                          </div> 
					
                         <div class="next_previous">
                   
                         @if(!empty($subject->previous()))
                            <a class="btn btn-warning btn-xs" href="{{ $subject->previous()->id }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous  </a>
                            @endif 
                            @if(!empty($subject->next()))
								 &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a class="btn btn-warning btn-xs" href="{{ $subject->next()->id }}">  Next <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            @endif  
                    </div>  
                        
                        
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    @if(_MASTER)
                                    <tr>
                                        <th> @lang('website.label.website')</th>
                                        <td>{{ $subject->_website->domain OR NULL }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th >@lang('comman.label.id')</th>
                                        <td>#{{ $subject->id }}</td>
                                    </tr>
                                    <tr>
                                        <th >@lang('subject.label.type')</th>
                                        <td>{{ $subject->subject_type }}</td>
                                    </tr>
									 <tr>
                                        <th >@lang('subject.label.geolocation_id')</th>
                                        <td>
										@if($subject->geoid)
											{{ $subject->geoid->name }}
										@endif
										</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('comman.label.name')</th>
                                        <td> {{ $subject->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('subject.label.services')</th>
                                        <td> {{ $subject->services->name or null }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('subject.label.caller_list')</th>
                                        <td>

                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th>
                                                            @lang('comman.label.name')
                                                        </th>
                                                        <th>
                                                            @lang('subject.label.available')
                                                        </th>
                                                        <th>
                                                            @lang('comman.label.time')
                                                        </th>
                                                    </tr>
                                                </tbody>

                                                @foreach($subject->caller_list as $people)
                                                <tr>
                                                    <td>{{$people->full_name}} <br>
                                                        ({{$people->title or null}})
                                                    </td>

                                                    <td>
                                                        @if($people->availability == true)
                                                        <span class="label label-success">@lang('subject.label.available')</span>
                                                        @else
                                                        <span class="label label-danger">@lang('subject.label.unavailable')</span>
                                                        @endif
                                                    </td>

                                                    <td>

                                                        @if($people->availability_time)
                                                        <table class="table">

                                                            @foreach($people->availability_time as $av)
                                                            <tr>
                                                                <th>{{ucfirst($av->day)}}</th>
                                                                <td>{{ucfirst($av->time)}}</td>
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                        @endif
                                                    </td>

                                                </tr>

                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th >@lang('comman.label.file')</th>
                                        <td >
                                            @if(isset($subject) && $subject->file)


                                            <object data="{!! url('/uploads/files/'.$subject->file) !!}" width="100%"
                                                    height="400" type='application/pdf'>
                                                <p>Sorry, the PDF couldn't be displayed :(</p>
                                            </object>

                                            {{--<a href="{!! url('/uploads/files/'.$subject->file) !!}" target="_blank">--}}
                                            {{--<img src="{!! asset('img/pdf.png') !!}" alt="" width="25">--}}
                                            {{--<b>{{$subject->file}} </b>--}}
                                            {{--</a>--}}

                                            @endif
                                        </td>
                                    </tr>

                                    

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                           @if($subject->qrcode)
                               @include('partials.qrcode',['qrcode' => $subject->qrcode]) 
                           @endif
                    </div>
                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection
