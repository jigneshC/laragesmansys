@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('subject.label.subjects'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('subject.label.subjects') </div>
        {{--  @include('partials.page_tooltip',['model' => 'subject','page'=>'index']) --}}
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                           @if(Auth::user()->can('access.subject.create'))
                            <a href="{{ url('/admin/subjects/create') }}" class="btn btn-success btn-sm ticket-add-new"
                               title="Add New Subject">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                            </a>
                            @endif
							
							@include("admin.for_master.export_pdf",["module"=>"subjects","fields"=>['is_deleted_record','filter_website']])
							
                          </div>
                         </div>
                        <div class="col-9">
                          
                               @if(_MASTER)
								@include("admin.for_master.filter_deleted") 
							
                               {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter pull-right','required'=>'required','id'=>'filter_website','style'=>'width:200px']) !!}
                            @endif
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered datatable responsive">
                                <thead>
                                <tr>
                                    <th>@lang('comman.label.id')</th>
                                    <th>@lang('comman.label.name')</th>
                                    <th>@lang('subject.label.type')</th>
                                    <th>@lang('subject.label.services')</th>
                                    <th>@lang('subject.label.subject_hrs')</th>
                                    @if(_MASTER) <th>@lang('website.label.website')</th> @endif
                                    <th>@lang('comman.label.action')</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection





@push('js')
<script>

    var type_dropdown = <?php echo json_encode(trans('subject.subject_type')); ?>;
    var url = "{{url('admin/subjects')}}";
    datatable = $('.datatable').dataTable({
        pagingType: "full_numbers",
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [1, "asc"],
        columns: [
            { "data": "id","name":"id","searchable": false},
            { "data": "name","name":"name"},
            {
                "data": null,
                "name": "subject_type",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    var res = (o.subject_type in type_dropdown) ? type_dropdown[o.subject_type] : o.subject_type;
                    return res;
                }
            },
            {
                "data": null,
                "name": "service_id",
                "searchable": false,
                "orderable": true,
                "render": function (o) {
                    if(o.services){
                        return o.services.name;
                    }else{
                        return '';
                    }
                }
            },
            { "data": "time","name":"time"},
            @if(_MASTER)
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    if(o._website){
                        return o._website.domain;
                    }else{
                        return "";
                    }
                }
            },
            @endif
            { "data": null,
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "render": function (o) {
                    var e=""; var d=""; var v="";

                    @if(Auth::user()->can('access.subject.edit'))
                        e= " <a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'><i class='fa fa-edit action_icon'></i></a>";
                    @endif
                            @if(Auth::user()->can('access.subject.delete'))
                        d = " <a href='javascript:void(0);' class='del-item' deleted_at="+o.deleted_at+" data-id="+o.id+" title='@lang('tooltip.common.icon.delete')' ><i class='fa fa-trash action_icon '></i></a>";
                            @endif

                    var v =  " <a href='"+url+"/"+o.id+"' data-id="+o.id+" title='@lang('tooltip.common.icon.eye')'> <i class='fa fa-eye' aria-hidden='true'></i></a>";


					if(o.deleted_at && o.deleted_at!=""){
						var jio = "<a href='javascript:void(0);' deleted_at="+o.deleted_at+" class='recover-item' moduel='subject' data-id="+o.id+" title='@lang('tooltip.common.icon.recover')'><i class='fa fa-repeat action_icon'></i></a>"+d;
						
						return jio;
					}else{
						return v+d+e;
					}
					
                    

                }
            }

        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/subjects/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
                @if(_MASTER)
					d.filter_website = $('#filter_website').val();
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
            }
        }
    });
    $('.filter').change(function() {
        datatable.fnDraw();
    });
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });
    $("#filter_website").select2();

    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		var deleted_at = $(this).attr('deleted_at');
		
		var r = false;
		if(deleted_at && deleted_at != "" && deleted_at != "undefined" && deleted_at!="null"){
			r = confirm("@lang('comman.js_msg.confirm_for_delete_perminent',['item_name'=>'Subject'])");	
		}else{
			r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Subject'])");
		}
		
        if (r == true) {
            $.ajax({
                type: "DELETE",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
	
	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete_recover',['item_name'=>'Subject'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


</script>


@endpush

