@extends('layouts.apex')
@section('title',trans('subject.label.edit_subject'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  @lang('subject.label.edit_subject') # {{ $subject->name }}</div>
                {{-- @include('partials.page_tooltip',['model' => 'subject','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/subjects') }}" title="{{__('Back')}}">
                            <button class="btn-link-back"><i class="fa fa-arrow-left"
                                                                      aria-hidden="true"></i> @lang('comman.label.back') </button>
                        </a>
	                
                        
                          <div class="next_previous pull-right">
                   
                             @if(!empty($subject->previous()))
                                <a class="btn-link-back" href="{{ url('/admin/subjects/' . $subject->previous()->id . '/edit') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous  </a>&nbsp;&nbsp;|&nbsp;&nbsp;
                             @endif 
                             @if(!empty($subject->next()))
                                <a class="btn-link-back" href="{{ url('/admin/subjects/' . $subject->next()->id . '/edit') }}">  Next <i class="fa fa-arrow-right" aria-hidden="true"></i> </a>
                             @endif 
                          </div>  
                        
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            
	                    {!! Form::model($subject, [
                                'method' => 'PATCH',
                                'url' => ['/admin/subjects', $subject->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.subjects.form', ['submitButtonText' => trans('comman.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection

