{{--<ul>--}}

{{--@foreach($users as $user)--}}

{{--<li>{{$user->full_name}}</li>--}}

{{--@endforeach--}}

{{--</ul>--}}


<ul id="sort-people">

    @foreach($users as $user)

        <li id="{{$user->id}}">
            <img src="{!! asset('css/arrow.png') !!}" alt="move" width="16" height="16"
                 class="handle"/>
            <strong>{{$user->full_name}}</strong>

            {{--<a href="{!! url('/admin/events/'.$event->id.'/menu/'.$menu->id.'/remove') !!}"--}}
            {{--class="btn btn-danger btn-xs pull-right">Remove</a>--}}
            {{--<a href="{!! url('/admin/events/'.$event->id.'/menu/'.$menu->id.'/edit') !!}"--}}
            {{--class="btn btn-info btn-xs pull-right">Edit</a>--}}
        </li>


    @endforeach
</ul>


