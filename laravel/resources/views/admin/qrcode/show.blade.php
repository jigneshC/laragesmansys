@extends('layouts.apex')

@section('title',"QR-CODE")

@push('css')

<style>
 .display_in_print{
    display: none !important;
}
.content-wrapper{
    display: block !important;
}
.footer {
    position: inherit !important;
}


.qr-code-box {display: flex; justify-content: center; align-items: center;    flex-direction: column;
    padding: 100px 0;}
.qr-code-box h2 {color: #009da0; margin-bottom: 50px; font-weight: 600;}
@media print {
    .hide-on-print{
        visibility: hidden;
    }
    .displaynone_on_print{
        display: none !important;
    }
    .display_in_print{
        display: block !important;
    }
    .app-sidebar{
        display: none !important;
    }
   
   body {-webkit-print-color-adjust: exact; }
   
   html, body {
      height:100vh; 
      margin: 0 !important; 
      padding: 0 !important;
      overflow: hidden;
    }
}


</style>
@endpush

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header hide-on-print">  QR-CODE  </div>
                
            </div>
        </div>
	<div class="row">
	    <div class="col-lg-12 col-md-12">
	        <div class="card">
	                  
	                
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content">
                               <div class="row">
                                    <div class="col-lg-12 qr-code-box">
                                     
											@if($module)
                                                
                                                    <h2> {{ strtoupper($module->getTable()) }} : {{ $title }}</h2>
                                                
											@endif	
												
													<p>
													{!!  DNS2D::getBarcodeHTML(\config('settings.qrcode.domain_URL')."/".$qrcode->qrcode, "QRCODE")  !!}
													</p>
													
                                                
                
                                    </div>

									
                                   
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


@push('js')


<script type="text/javascript" >

            
$(".print_qr_code").click(function () {
    window.print();
});

 window.print();

</script>

@endpush



