@extends('layouts.apex')

@section('title',trans('qrcode.label.generate_bulck_qrcode'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> {{trans('qrcode.label.generate_bulck_qrcode')}}  </div>
                {{--  @include('partials.page_tooltip',['model' => 'building','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
							
							<div class="col-xl-6 col-sm-12">

                            {!! Form::open(['url' => '/admin/qrcode', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}
                            

                                <div class="form-group row {{ $errors->has('_website_id') ? 'has-error' : ''}}">
                                        {!! Form::label('_website_id',trans('ticket.label.website'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
                                        <div class="col-xl-8 col-sm-8">
                                            {!! Form::select('_website_id',$websites, null,['class' => 'form-control']) !!}
                                            {!! $errors->first('_website_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                <div class="form-group row {{ $errors->has('module_name') ? 'has-error' : ''}}">
                                    <label for="module_name" class="col-xl-4 col-sm-4 label-control">@lang('qrcode.label.select_module')</label>
                                    <div class="col-xl-8 col-sm-8">
                                        {!! Form::select('module_name',['users'=>"Users",'companies'=>"Companies",'sites'=>"Sites",'services'=>"Services",'buildings'=>"Buildings",'subjects'=>"Subjects"], null, ['class' => 'form-control', 'required' => 'required','id'=>'module_name']) !!}
                                        {!! $errors->first('module_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>


                                <div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
                                    {!! Form::label('active', trans('qrcode.label.check_item_type'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
                                    <div class="col-xl-8 col-sm-8 status_rad">
                                        <div class="radio">
                                            {!! Form::radio('active', '1',true, ['id' => 'rad1']) !!} <label for="rad1">@lang('qrcode.label.only_active')</label>
                                        </div>
                                        <div class="radio">
                                            {!! Form::radio('active', '0',false, ['id' => 'rad2']) !!} <label for="rad2">@lang('qrcode.label.any_active_type')</label>
                                        </div>
                                        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                            <div class="form-group row">
                                <label class="col-xl-4 col-sm-4 label-control"></label>
                                <div class="col-md-offset-4 col-xl-4 col-sm-4">
                                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('qrcode.label.generate_bulck_qrcode'), ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>


                            {!! Form::close() !!}
							
							</div>
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection






