@extends('layouts.apex')

@section('title',trans('building.label.show_building'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> {{ trans('building.label.show_building') }} # {{ $building->name }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'building','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/buildings') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                 <div class="next_previous pull-right">
                   
                            <a href="{{ url('/admin/buildings/' . $building->id . '/edit') }}" title="Edit Building">
                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    @lang('comman.label.edit')
                                </button>
                            </a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/buildings', $building->id],
                                'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '.trans('comman.label.delete'), array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Building',
                                    'onclick'=>"return confirm('".trans('comman.js_msg.confirm_for_delete',['item_name'=>'Building'])."')"
                            ))!!}
                            {!! Form::close() !!}
                            
                          </div>  
                         
                    </div>
	            <div class="card-body">
	                <div class="px-3">
                           <div class="box-content ">
                               <div class="row">
                                    <div class="col-md-6">
                                        <div class="table-responsive">
                                            <table class="table table-borderless">
                                                <tbody>
                                                @if(_MASTER)
                                                    <tr>
                                                        <th> @lang('website.label.website')</th>
                                                        <td>{{ $building->_website->domain OR NULL }}</td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <th>@lang('comman.label.id')</th>
                                                    <td>{{ $building->id }}</td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('comman.label.name')</th>
                                                    <td> {{ $building->name }} </td>
                                                </tr>
                                                <tr>
                                                    <th> @lang('site.label.address')</th>
                                                    <td> {{ $building->address }} </td>
                                                </tr>

                                                <tr>
                                                    <th> @lang('site.label.site')</th>
                                                    @foreach($building->sites as $site)
                                                    <td> {{ $site->name }} </td>
                                                    @endforeach
                                                </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            @if($building->qrcode)
                                            @include('partials.qrcode',['qrcode' => $building->qrcode])
                                            @endif
                                    </div>
                                </div>
                            </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


