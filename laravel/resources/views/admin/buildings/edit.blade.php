@extends('layouts.apex')
@section('title',trans('building.label.edit_building'))

@section('content')


    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">  {{trans('building.label.edit_building')}} # {{ $building->name }} </div>
                {{-- @include('partials.page_tooltip',['model' => 'building','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/buildings') }}" title="{{__('Back')}}">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left"
                                                                      aria-hidden="true"></i> @lang('comman.label.back') </button>
                        </a>
	                
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                             @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            {!! Form::model($building, [
                                'method' => 'PATCH',
                                'url' => ['/admin/buildings', $building->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.buildings.form', ['submitButtonText' => trans('comman.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>

   
@endsection

