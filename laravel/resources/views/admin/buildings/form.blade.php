
@include('admin.for_master.site_input3')

<div class="form-group row {{ $errors->has('company_id') ? 'has-error' : ''}}">
        <label for="service_id" class="col-xl-4 col-sm-4 label-control">@lang('site.label.company')</label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('company_id',[], null, ['class' => 'form-control','required'=>'required','id'=>'company_id']) !!}
            {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group row {{ $errors->has('site_id') ? 'has-error' : ''}}">
    <label for="site_id" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>
        @lang('building.label.site')
        @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.building.form_field.site')])

    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('site_id',$sites, null, ['class' => 'form-control', 'required' => 'required','id'=>'site_id']) !!}
        {!! $errors->first('site_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-xl-4 col-sm-4 label-control">
        <span class="field_compulsory">*</span>@lang('building.label.name')
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required','autocomplete'=>'off']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-xl-4 col-sm-4 label-control">@lang('user.label.email')
      
    </label>
    <div class="col-xl-8 col-sm-8">
        {!! Form::email('email', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', trans('site.label.address'), ['class' => 'col-xl-4 col-sm-4 label-control fill_address']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('address', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
  
</div>

<div class="form-group row {{ $errors->has('city') ? 'has-error' : ''}}">
    {!! Form::label('city', trans('site.label.city'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('city', null, ['class' => 'form-control']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('postcode') ? 'has-error' : ''}}">
    {!! Form::label('postcode', trans('site.label.postcode'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::text('postcode', null, ['class' => 'form-control','autocomplete'=>'off']) !!}
        {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', trans('site.label.country'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('country',$countries, null, ['class' => 'form-control']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_number_1', trans('site.label.phone_number_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {{ Form::hidden('code_phone_number_1',null, array('id' => 'code_phone_number_1')) }}
        {!! Form::text('phone_number_1', null, ['class' => 'form-control','style'=>'padding-left: 84px !important','autocomplete'=>'off']) !!}
        {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_type_1', trans('site.label.phone_type_1'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8">
        {!! Form::select('phone_type_1',$phoneTypes, null, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group row {{ $errors->has('geolocation') ? 'has-error' : ''}}">
    {!! Form::label('geolocation', "geolocation", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
		 {!! Form::hidden('longitude', null, ['class' => 'form-control search_longitude','id'=>"longitude"]) !!}
         {!! Form::hidden('latitude', null, ['class' => 'form-control search_latitude','id'=>"latitude"]) !!}
		 
         {!! Form::text('geolocation', null, ['class' => 'form-control','id'=>"geolocation_search"]) !!}
		  <div id="map-canvas" class="gmaps" style="height: 300px;width: 100%"></div>	
         {!! $errors->first('geolocation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group row {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('building.label.active'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
    <div class="col-xl-8 col-sm-8 status_rad">
        <div class="radio">
            {!! Form::radio('active', '1',true, ['id' => 'rad1']) !!}<label for="rad1">Yes</label>
        </div>
        <div class="radio">
            {!! Form::radio('active', '0',false, ['id' => 'rad2']) !!} <label for="rad2">No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group row">
     <label class="col-xl-4 col-sm-4 label-control"></label>
    <div class="col-md-offset-4 col-md-4 btn-submit-edit-s">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
        {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-primary']) }}
    </div>
</div>



    @push('js')
	
	<script src="{!! asset('js/google-search-location-map.js')!!}" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{\config('settings.GOOGLE_MAP_API')}}&libraries=places&callback=initAutocomplete"
            async defer></script>
			
    <script>
        var sub_search_url ="{{url('admin/buildingoptionfilterbywebsite')}}";
        var selected_site = "0";
        var selected_company = "0";
        var data_site = [];


        @if(isset($building))
        selected_site = "{{$building->site_id}}";
        selected_company = "{{$building->company_id}}";
        @endif

        initSelect();
        function initSelect(){
            @if(_MASTER)
            var filter_id = $('#_website_id').val();
            @else
            var filter_id = "{{_WEBSITE_ID}}";
            @endif


            $("#site_id").html("");
            $("#company_id").html("");

            $.ajax({
                type: "get",
                url: sub_search_url,
                data:{_website_id:filter_id},
                success: function (result) {
                    
                    data = result.data.compoanies;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_company) { selected = "selected=selected"; }
                        $("#company_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    data_site = result.data.site;
                    
                    initSelectSite();
                },
                error: function (xhr, status, error) {
                }
            });
        }
         function initSelectSite(){
		$("#site_id").html("");
		var selected_se = $("#company_id").val();
		data = data_site;
                for(var i=0;i<data.length;i++){
                    var selected="";
                    if(data[i]['company_id'] == selected_se){
			if (data[i]['id'] == selected_site) { selected = "selected=selected"; }
                       	$("#site_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
               }
               
	}

        $('#_website_id').change(function() {
            initSelect();
        });
        $('#company_id').change(function() {
            initSelectSite();
        });

		
		    var old_code1 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";
			var old_code2 = "{{ env('DEFAULT_PHONE_COUNTRY_CODE', 'fr') }}";;
			@if(isset($building) && $building->code_phone_number_1 != '')
				old_code1 ="{{$building->code_phone_number_1}}";
			@endif
			@if(isset($building) && $building->code_phone_number_2 != '')
				old_code2 ="{{$building->code_phone_number_2}}";
			@endif

			$("#phone_number_2").intlTelInput({
				preferredCountries:[old_code2],
				separateDialCode: true,
				utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
			});
			$("#phone_number_1").intlTelInput({
				preferredCountries:[old_code1],
				separateDialCode: true,
				utilsScript: "{!! asset('/assets/build/js/utils.js')!!}"
			});

			$("#phone_number_1").on("countrychange", function(e, countryData) {
				var code =  countryData.iso2;
				$("#code_phone_number_1").val(code);
			});
			$("#phone_number_2").on("countrychange", function(e, countryData) {
				var code =  countryData.iso2;
				$("#code_phone_number_2").val(code);
			});

    </script>


    @endpush

