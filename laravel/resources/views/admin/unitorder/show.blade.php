@extends('layouts.apex')

@section('title',trans('unitorder.label.order'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> </div>
                {{--  @include('partials.page_tooltip',['model' => 'subject','page'=>'index']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            
	           <div class="card-body p-3">
            <div id="invoice-template" class="card-block">
                <!-- Invoice Company Details -->
                <div id="invoice-company-details" class="row">
                    <div class="col-6 text-left">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800"><strong> {{$order->website->domain}} / {{$package->name}}</strong></li>
                            <li>@lang('unitorder.label.payment_status') : <strong>{{ $order->payment_status }}</strong></strong> </li>
                            <li>@lang('unitorder.label.invoicedate') : <strong>{{ $order->created_at->format('Y-m-d') }}</strong></strong> </strong></li>
                            <li>@lang('unitorder.label.reference_id') : <strong>{{ $order->payment_request_id }}</strong></strong></li>
                            <li>@lang('unitorder.label.payment_type') : <strong>{{ $order->payment_type }}</strong></strong></strong></li>
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <h3>@lang('unitorder.label.invoice') : # {{$order->id}}</h3>
                        <p class="pb-3">
                            <strong>{{ $order->payment_status }}</strong>
                        </p>
                        
                    </div>
                </div>
                
                <div id="invoice-customer-details" class="row pt-2">
                    <div class="col-sm-12 text-left">
                        <p class="text-muted">@lang('unitorder.label.to')</p>
                    </div>
                    <div class="col-6 text-left">
                        <ul class="px-0 list-unstyled">
                            <li class="text-bold-800">{{$order->buyer_name}}</li>
                            <li>{{$order->website->domain}}</li>
                            <li>{{$order->buyer_email}}</li>
                            <li>{{$order->buyer_contact}}</li>
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <strong>@lang('unitpackage.label.package')</strong>
                        <p><span class="text-muted">Invoice Date :</span>{{$package->name}} </p>
                        <p><span class="text-muted">@lang('unitpackage.label.unit') :</span> {{$package->unit}}</p>
                        <p><span class="text-muted">@lang('unitpackage.label.price') : :</span> {{\config('settings.currency')[$order->price_currency]['symbole']}} {{$package->price}} </p>
                    </div>
                </div>
                
              
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>@lang('unitpackage.label.package_code')</th>
                                        <th> @lang('unitorder.label.item')</th>
                                        <th class="text-right">@lang('unitorder.label.item_qty')</th>
                                        <th class="text-right">@lang('unitpackage.label.price')</th>
                                        <th class="text-right"> @lang('unitorder.label.price_total')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                        <tr>
                                          <td> {{$package->id}}</td>
                                          <td>{{$package->name}}</td>
                                          <td class="text-right">
                                          {{$order->item_qty}}
                                          </td>
                                          <td class="text-right">
                                          {{\config('settings.currency')[$order->price_currency]['symbole']}}{{$order->item_price}}
                                          </td>
                                          <td class="text-right">
                                           {{\config('settings.currency')[$order->price_currency]['symbole']}}{{$order->item_price * $order->item_qty}}
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="5"></td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-right" > @lang('unitorder.label.discount') (%): {{$order->discount}}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-right" > @lang('unitorder.label.net_payable_amount') : {{\config('settings.currency')[$order->price_currency]['symbole']}} {{$order->order_total}}</td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">
                          <div class="text-right text-contrast subtotal">
                              @lang('unitorder.label.paidamount') : @if($order->payment_status=="completed") {{\config('settings.currency')[$order->price_currency]['symbole']}} {{$order->order_total}} @else {{\config('settings.currency')[$order->price_currency]['symbole']}} 0.00 @endif
                          </div>
                        </div>
                      </div>
                </div>
               
            </div>
        </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection


