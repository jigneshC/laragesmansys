@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('unitorder.label.order'))

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="content-header"> @lang('unitorder.label.order') </div>
        
    </div>
</div>

    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    
                    <div class="col-3">
                        <div class="actions pull-left">
                          
                          </div>
                         </div>
                        <div class="col-9">
                          
                              @if(_MASTER)
                                {!! Form::select('filter_website',$_websites_pluck,null, ['class' => 'filter pull-right','required'=>'required','id'=>'filter_website','style'=>'width:200px']) !!}
                            @endif
                           
                    </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        @if(count($orders)<=0)
                            <div class="row">
                                <div class="text-center">
                                    <h2>@lang('unitorder.responce_msg.no_order_found')</h2>
                                </div>
                            </div>
                        @endif
                        <ul class="list-group">
                            @foreach($orders as $order)
                            @php($package = json_decode($order->unit_package))
                            <li class="list-group-item list-group-item-action flex-column align-items-start box-group-01">
                                <div class="d-flex w-100 justify-content-between">
                                <h5 class="h5-product"> 
                                    <p>#{{$order->id}}</p><p> {{\config('settings.currency')[$order->price_currency]['symbole']}} {{$order->order_total}} </p> 
                                        <span class="@if($order->payment_status=='completed') 
                                            badge badge-success
                                                        @elseif($order->payment_status=='failed')
                                                        badge badge-default
                                                        @elseif($order->payment_status=='cancelled')
                                                        badge badge-info
                                                        @else
                                                        badge badge-info
                                                        @endif
                                        ">
                                     {{ $order->payment_status }}
                                </span>
                                    
                                </h5>
                                <small class="last-status">{{ trans("{$order->created_at->diffForHumans()}") }}</small>
                                </div>
                                <p class="mb-2 percentag-p">{{$package->desc}}</p>
                                <small class="package-name">{{$package->name}}</small>
                                
                                 <a href="{{ url('/admin/order') }}/{{$order->id}}" class="btn btn-01" title="@lang('unitorder.label.view_order')">
                                    <span class="label label-warning"> @lang('unitorder.label.view_order')</span>
                                </a>
                            </li>
                            @endforeach
                           
                        </ul>
                        
                        <div class="text-center">
                            {!! $orders->appends($_GET)->links() !!}
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection



{{--

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                    <div class="box-contento ">
                            
                    </div>
                <div class="box-header blue-background">
                    <div class="title">
                            <i class="icon-circle-blank"></i>
                            
                    </div>
                    

                </div>
                <div class="box-content ">
					<div class="row">
                        <div class="col-md-9">
                           
                        </div>
                       
                        <div class="col-md-3">
                           
                        </div>



                    </div>

                </div>
               
				<div class="box-content">

                        @if(count($orders)<=0)
                            <div class="row">
                                <div class="text-center">
                                    <h2>@lang('unitorder.responce_msg.no_order_found')</h2>
                                </div>
                            </div>
                        @endif
                    @foreach($orders as $order)
                        @php($package = json_decode($order->unit_package))
                        <div class="pull-left">
                            <p>
                              <a href="#detail">
                               
                                {{$package->name}}
                              </a>
                            </p>
                            <p>
                              <span class="label @if($order->payment_status=='completed') 
                                    label-success
                                                @elseif($order->payment_status=='failed')
                                                label-important
                                                @elseif($order->payment_status=='cancelled')
                                                label-warning
                                                @else
                                                label-warning
                                                @endif
                                ">
                                {{ $order->payment_status }}
                                </span>
                            </p>
                           
                          </div>
                          <div class="text-right pull-right">
                            <h4 class="contrast price">${{$order->order_total}}</h4>
                            <p>
                              <span class="timeago fade has-tooltip in" data-placement="top" title="{{$order->created_at}}">{{ trans("{$order->created_at->diffForHumans()}") }}</span>
                              <i class="icon-time"></i>
                            </p>
                          
                          </div>
                          <div class="clearfix"></div>
                          <hr class="hr-normal">
                    @endforeach

                    <div class="text-center">
                            {!! $orders->appends($_GET)->links() !!}
                    </div>
                        
                </div>       
            </div>
        </div>
    </div>
@endsection



--}}

@push('js')

<script>
    var url = "{{url('admin/order')}}";

    @if(_MASTER)
    $('#filter_website').change(function() {
        window.location.href=url+"?website_id="+$(this).val();
    });
    @endif
   

</script>


@endpush

