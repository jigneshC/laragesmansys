@extends('layouts.apex')

@section('title',trans('unitorder.label.confirm_order'))

@section('content')

    <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header"> @lang('unitorder.label.confirm_order') </div>
                
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <h2>{{$package->name}}</h2>
                              <small class="text-muted">{{$package->desc}}</small>
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
							
							<div class="col-xl-6 col-sm-12">

                            {!! Form::open(['url' => '/admin/order', 'class' => 'form-horizontal', 'files' => true,'autocomplete'=>'off']) !!}

                            {!! Form::hidden('unit_package_id',$package->id, ['class' => 'form-control','id'=>'unit_package_id']) !!}

                            <div class="form-group row {{ $errors->has('buyer_name') ? 'has-error' : ''}}">
                                {!! Form::label('buyer_name',"Buyer Name", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
                                <div class="col-xl-8 col-sm-8">
                                    {!! Form::text('buyer_name', $user->name, ['class' => 'form-control', 'required' => 'required']) !!}
                                    {!! $errors->first('buyer_name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('buyer_contact') ? 'has-error' : ''}}">
                                {!! Form::label('buyer_contact',"Buyer Contact", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
                                <div class="col-xl-8 col-sm-8">
                                    {!! Form::text('buyer_contact', $user->phone_number_1, ['class' => 'form-control', 'required' => 'required']) !!}
                                    {!! $errors->first('buyer_contact', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('buyer_email') ? 'has-error' : ''}}">
                                {!! Form::label('buyer_email',"Buyer Email", ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
                                <div class="col-xl-8 col-sm-8">
                                    {!! Form::text('buyer_email', $user->email, ['class' => 'form-control', 'required' => 'required']) !!}
                                    {!! $errors->first('buyer_email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>



                            <div class="form-group row">
                                <label class="col-xl-4 col-sm-4 label-control"></label>
                            <div class="col-md-offset-4 col-xl-4 col-sm-4">
                            @php( $curOb = \config('settings.currency'))  
                            {!! Form::submit(trans('unitorder.label.confirm_and_pay_amount',['amount'=>$curOb[$package->price_currency]['symbole']." ".$package->price]), ['class' => 'btn btn-primary']) !!}

                            </div>
                            </div>

                            {!! Form::close() !!}
							
							</div>
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>
   
@endsection





{{--



@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                            <i class="icon-circle-blank"></i>
                            
                    </div>
                   

                </div>
               
                <div class="box-content ">     
                    <div class="group-header">
                        <div class="row">
                          <div class="col-sm-6 col-sm-offset-3">
                            <div class="text-center">
                            
                            </div>
                          </div>
                        </div>
                      </div>

                    
                  

                   
                  

                  {!! Form::close() !!}
                
                </div>
              </div>
        </div>
    </div>

  
   

@endsection


@push('js')

<script>

   
</script>


@endpush

--}}