@extends('layouts.apex')

@section('body_class',' pace-done')

@section('title',trans('ticket.label.ticket'))

@push('css')

@endpush

@section('content')


@php( $is_expanded= 0)
@if(Session::has('filter_website') || Session::has('range_start') || Session::has('filter_status') || Session::has('filter_site') || Session::has('filter_services') || Session::has('filter_subject') || Session::has('filter_company')) 
@php( $is_expanded= 1)
@endif

<div class="row">
    <div class="col-sm-12">
        <div class="content-header">@lang('ticket.label.planned_activity')</div>
    </div>
</div>






    <section id="configuration">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   
                    <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-12">
					    @if(Auth::user()->can('access.ticket.create'))
                            <a href="{{ url('/admin/planned-activity/create') }}" class="btn btn-success btn-sm ticket-add-new"
                               title="Add New Ticket">
                                <i class="fa fa-plus" aria-hidden="true"></i> @lang('comman.label.add_new')
                            </a>
                        @endif
                       @include("admin.for_master.export_pdf",["module"=>"master_tickets","fields"=>['is_deleted_record','filter_website','filter_company','filter_site','filter_services','filter_range_start','filter_range_end']]) 
                    </div>
                    <div class="col-9">
                        <div class="actions pull-right">
							@if(_MASTER)
								@include("admin.for_master.filter_deleted") 
							
                            @endif

                          </div>
                         </div>
                    </div>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="card-block card-dashboard">
                       
                        
                        <table class="table table-striped table-bordered base-style responsive datatable">
                            
                          <thead>
                            <tr>
                                <th data-priority="1" >@lang('ticket.label.id')</th>
                                <th data-priority="2" title="@lang('tooltip.ticket.label.subject')">@lang('ticket.label.subject')</th>
                                <th data-priority="3" title="Activity Time">@lang('ticket.label.activity_time')</th>
                                <th data-priority="4" title="Desplay Before Hours">@lang('ticket.label.view_before_date')</th>
                                <th data-priority="5" title="Duration">@lang('ticket.label.duration') Duration</th>
                                <th data-priority="10" title="@lang('tooltip.ticket.label.content')">@lang('ticket.label.content')</th>

                                @if(Auth::user()->can('access.ticket.edit'))<th data-priority="5" class="noExport" title="@lang('tooltip.ticket.label.action')">@lang('comman.label.action')</th> @endif
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>


@endsection



@push('js')


<script>
	$("#filter_website").select2();
	$("#filter_company").select2({
        placeholder: "@lang('comman.label.select_company')",
        allowClear: true,
        @if(Session::has('filter_company'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_company')}}, text: "{{Session::get('filter_company_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/companies/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	
    $("#filter_site").select2({
        placeholder: "@lang('comman.label.select_site')",
        allowClear: true,
        @if(Session::has('filter_site'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_site')}}, text: "{{Session::get('filter_site_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        ajax: {
            url: "{{ url('admin/sites/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_site: params, // search term
                    _website_id: $("#filter_website").val(),
                     company_id: $("#filter_company").val(),
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);

    $("#filter_services").select2({
        placeholder: "@lang('comman.label.select_services')",
        allowClear: true,
        @if(0 && Session::has('filter_services'))
            initSelection: function (element, callback) {
                callback({id: {{Session::get('filter_services')}}, text: "{{Session::get('filter_services_text')}}" });
            },
        @else   
            initSelection: function (element, callback) {
                callback({id: null, text: "" });
            },
        @endif
        
        ajax: {
            url: "{{ url('admin/services/search') }}",
            dataType: 'json',
            type: "GET",
            data: function (params) {
                return {
                    filter_services: params, // search term
                    site_id: $("#filter_site").val()
                };
            },
            results: function (result) {
                var data = result.data;
                console.log(data);
                return {
                    results: $.map(data, function (obj) {
                        return {id: obj.id, text: obj.name};
                    })
                };
            },
            cache: false
        }
    }).select2('val', []);
	
	var range_start ="";
    var range_end ="";
    
    var auth_id = "{{\Auth::user()->id}}";
    var days = ['sunday','monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saterday'];


    var url = "{{url('admin/planned-activity')}}";
    datatable = $('.datatable').dataTable({
        lengthMenu: [[10, 50,100,200, -1], [10, 50,100,200, "All"]],
        pageLength: 10,
        "language": {
            "emptyTable":"@lang('comman.datatable.emptyTable')",
            "infoEmpty":"@lang('comman.datatable.infoEmpty')",
            "search": "@lang('comman.datatable.search')",
            "sLengthMenu": "@lang('comman.datatable.show') _MENU_ @lang('comman.datatable.entries')",
            "sInfo": "@lang('comman.datatable.showing') _START_ @lang('comman.datatable.to') _END_ @lang('comman.datatable.of') _TOTAL_ @lang('comman.datatable.small_entries')",
            paginate: {
                next: '@lang('comman.datatable.paginate.next')',
                previous: '@lang('comman.datatable.paginate.previous')',
                first:'@lang('comman.datatable.paginate.first')',
                last:'@lang('comman.datatable.paginate.last')',
            }
        },
        responsive: true,
        pagingType: "full_numbers",
        processing: true,
        serverSide: true,
        autoWidth: false,
        stateSave: false,
        order: [0, "desc"],
        columns: [
            { "data": "id","name":"id","searchable": false,"width":"5%"},
            { "data": "subject_name","name":"subjects.name"},
            {
                "data": null,
                "name": 'activity_time_opt',
                "searchable": false,
                "render": function (o) {
                    var dt = o.view_on_date_val;
                    if(o.activity_time_opt =="weekly"){
                        dt = days[dt];
                        return o.activity_time_opt +" | "+dt ;
                    }
                    if(o.activity_time_opt =="daily"){
                        return o.activity_time_opt;
                    }else{
                        return o.activity_time_opt +" | "+dt ;
                    }
                    
                }
            },
            { "data": "view_before_date","name":"view_before_date"},
            {
                "data": null,
                "name": 'created_at',
                "searchable": false,
                "render": function (o) {
                 
                    return getLangDate(o.view_start_date,"short")+" to "+getLangDate(o.view_end_date,"short") ;
                }
            },
            { "data": "content","name":"content"},
            { "data": null,
                "searchable": false,
                "orderable": false,
                "render": function (o) {
                    var e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+" title='@lang('tooltip.common.icon.edit')'> <i class='fa fa-edit action_icon'></i></a>";
                    
                    var d = " <a href='javascript:void(0);' class='del-item' deleted_at="+o.deleted_at+" data-id="+o.id+" title='@lang('tooltip.common.icon.delete')'> <i class='fa fa-trash action_icon '></i></a>";
                    
					
					if(o.deleted_at && o.deleted_at!="" ){
						var jio = "<a href='javascript:void(0);' deleted_at="+o.deleted_at+" class='recover-item' moduel='planned_activity' data-id="+o.id+" title='@lang('tooltip.common.icon.recover')'><i class='fa fa-repeat action_icon'></i></a>"+d;
						
						return jio;
					}else{
						return d+e;
					}

                }
            },
        ],
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            $('td', nRow).attr('nowrap', 'nowrap');
            return nRow;
        },
        ajax: {
            url: "{{ url('admin/planned-activity/datatable') }}", // json datasource
            type: "get", // method , by default get
            data: function (d) {
				 @if(_MASTER)
					d.filter_website = $('#filter_website').val();
					d.enable_deleted = ($('#is_deleted_record').is(":checked")) ? 1 : 0;
				@endif
                
				d.filter_site = $('#filter_site').val();
                d.filter_subject = $('#filter_subject').val();
                d.filter_services = $('#filter_services').val();
                d.filter_company = $("#filter_company").val();

                d.range_start = range_start;
                d.range_end = range_end;
                
            }
        }
    });
    $('.filter').change(function() {
        var filter_id = $(this).attr('id');
        if(filter_id=="filter_website"){
			$("#filter_company").select2("val", "");
            $("#filter_site").select2("val", "");
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
		if(filter_id=="filter_company"){
			$("#filter_site").select2("val", "");
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
        if(filter_id=="filter_site"){
            $("#filter_services").select2("val", "");
            $("#filter_subject").select2("val", "");
        }
        if(filter_id=="filter_services"){
            $("#filter_subject").select2("val", "");
        }
        datatable.fnDraw();
    });
	$('#is_deleted_record').change(function() {
		datatable.fnDraw();
    });
	
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		var deleted_at = $(this).attr('deleted_at');
		
		var r = false;
		if(deleted_at && deleted_at != "" && deleted_at != "undefined" && deleted_at!="null"){
			r = confirm("@lang('comman.js_msg.confirm_for_delete_perminent',['item_name'=>'Planned Activity'])");	
		}else{
			r = confirm("@lang('comman.js_msg.confirm_for_delete',['item_name'=>'Planned Activity'])");
		}
		
		if (r == true) {
            $.ajax({
                type: "delete",
                url: url + "/" + id,
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

	$(document).on('click', '.recover-item', function (e) {
        var id = $(this).attr('data-id');
        var moduel = $(this).attr('moduel');
        var r = confirm("@lang('comman.js_msg.confirm_for_delete_recover',['item_name'=>'Planned Activity'])");
        if (r == true) {
            $.ajax({
                type: "POST",
                url: "{{ url('admin/recover-item') }}",
				data: {item:moduel,id:id},
                headers: {
                    "X-CSRF-TOKEN": "{{ csrf_token() }}"
                },
                success: function (data) {
                    datatable.fnDraw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


	/*************************daterange selection*****************/



    var start = moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment();
    @if(Session::has('range_start') && Session::has('range_end'))
     start = moment.utc("{{Session::get('range_start')}}",'YYYY-MM-DD');
     end = moment.utc("{{Session::get('range_end')}}",'YYYY-MM-DD');
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');
        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');

			 datatable.fnDraw();
        }
		
		$("#filter_range_start").val(range_start);
		$("#filter_range_end").val(range_end);


    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment()],
            "@lang('comman.daterange.today')": [moment(), moment()],
            "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            "@lang('comman.daterange.last7day')": [moment().subtract(6, 'days'), moment()],
            "@lang('comman.daterange.last30day')": [moment().subtract(29, 'days'), moment()],
            "@lang('comman.daterange.thismonth')": [moment().startOf('month'), moment().endOf('month')],
            "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            "@lang('comman.daterange.thisyear')": [moment().startOf('year'), moment().endOf('year')],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

     cb(start, end);

    $(".range_inputs").find(".applyBtn").html("@lang('comman.daterange.applyBtn')");
    $(".range_inputs").find(".cancelBtn").html("@lang('comman.daterange.cancelBtn')");
    $(".ranges").find('ul li:last-child').html("@lang('comman.daterange.customeRange')");

     $(document).on('click', '#reset_filter', function (e) {
        
        $("#filter_company").select2("val", "");
        $("#filter_site").select2("val", "");
        $("#filter_services").select2("val", "");
        $("#filter_subject").select2("val", "");
        $("#filter_website").select2("val", "");
		view_assign = "";
        
        var start_r = moment.utc('2015-01-01','YYYY-MM-DD');
        var end_r = moment();

        cb(start_r, end_r);
        return false;
    });
	






</script>


@endpush
