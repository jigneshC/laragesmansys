<div class="row">
    <div class="col-xl-6 col-sm-12">
        @include('admin.for_master.site_input3')
     <div class="form-group row {{ $errors->has('site_id') ? 'has-error' : ''}}">
        <label for="site_id" class="col-xl-4 col-sm-4 label-control">
            @lang('service.label.site')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('site_id',[], [], ['class' => 'form-control','id'=>'site_id']) !!}
            {!! $errors->first('site_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
        
     <div class="form-group row {{ $errors->has('subject_id') ? 'has-error' : ''}}">
        {{--<label for="subject_id" class="col-xl-4 col-sm-4 label-control">
                <span class="field_compulsory">*</span>@lang('ticket.label.subject')
            </label>--}}
        {!! Form::label('subject_id',trans('ticket.label.subject'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('subject_id',[], null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('subject_id', '<p class="help-block">:message</p>') !!}

        </div>
    </div>

     <div id="planned_activity_options" >   
        <div class="form-group row {{ $errors->has('activity_time_opt') ? 'has-error' : ''}}">
            <label for="activity_option" class="col-xl-4 col-sm-4 label-control">
                <span class="field_compulsory">*</span>
                @lang('ticket.form.activity_option')
                @include('partials.form_field_tooltip',['tooltip' =>trans('tooltip.ticket.form_field.activity_option')])
            </label>
            <div class="col-xl-8 col-sm-8">
                <select class="form-control" id="activity_time_opt" name="activity_time_opt">
                    @foreach(trans('subject.subject_planned_activity_time') as $k => $val)
                    <option value="{{$k}}" @if(isset($ticket) && $ticket->activity_time_opt==$k) selected @endif>{{$val}}</option>
                    @endforeach
                </select>
                {!! $errors->first('activity_time_opt', '<p class="help-block">:message</p>') !!}
            </div>
        </div>  
        
        <div class="form-group row {{ $errors->has('activity_time_opt') ? 'has-error' : ''}}">

            <label for="" class="col-xl-4 col-sm-4 label-control">
                <span class="field_compulsory">*</span>
                @lang('ticket.form.planned_activity_range')
            </label>
            <div class="col-xl-4 col-sm-4">
                {!! Form::text('view_start_date', null, ['class' => 'form-control','id'=>'view_start_date','placeholder'=>trans('ticket.label.planned_start_date')]) !!}
                {!! $errors->first('view_start_date', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="col-xl-4 col-sm-4">
                {!! Form::text('view_end_date', null, ['class' => 'form-control','id'=>'view_end_date','placeholder'=>trans('ticket.label.planned_end_date')]) !!}
                {!! $errors->first('view_end_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div> 
        
        <div class="form-group row {{ $errors->has('activity_time_opt') ? 'has-error' : ''}}">

            <label for="" class="col-xl-4 col-sm-4 label-control">
                @lang('ticket.label.view_before_date')
            </label>
            <div class="col-xl-8 col-sm-8">
                {!! Form::number('view_before_date', null, ['min'=>0,'class' => 'form-control','id'=>'view_before_date','placeholder'=>trans('ticket.label.view_before_date')]) !!}
                {!! $errors->first('view_before_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

         <div id="view_on_date_val_container" @if(isset($ticket) && $ticket->view_on_date==1 && ($ticket->activity_time_opt!="daily" )) style="display: block;" @else style="display:none" @endif>
            <div class="form-group row {{ $errors->has('activity_time_opt') ? 'has-error' : ''}}"  >

                 <label for="" class="col-xl-4 col-sm-4 label-control">
                    @lang('ticket.label.activity_date')
                </label>
                <div class="col-xl-8 col-sm-8">
                    {!! Form::text('view_on_date_val', null, ['class' => 'form-control','id'=>'view_on_date_val','placeholder'=>trans('ticket.label.view_on_date')]) !!}
                    {!! $errors->first('view_on_date_val', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
         
        
    </div>

    <div class="form-group row {{ $errors->has('equipment_id') ? 'has-error' : ''}}">
        {!! Form::label('equipment_id',trans('ticket.label.equipment_id'), ['class' => 'col-xl-4 col-sm-4 label-control']) !!}
        <div class="col-xl-8 col-sm-8">
            {!! Form::select('equipment_id',[], null, ['class' => 'form-control']) !!}
            {!! $errors->first('equipment_id', '<p class="help-block">:message</p>') !!}

        </div>
    </div>


    <div class="form-group row {{ $errors->has('content') ? 'has-error' : ''}}">
        <label for="content" class="col-xl-4 col-sm-4 label-control line-height-two">
            <span class="field_compulsory">*</span>@lang('ticket.label.content')
        </label>
        <div class="col-xl-8 col-sm-8">
            {!! Form::textarea('content', null, ['class' => 'form-control textarea-height-two','id'=>'else']) !!}
            {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    




    <div class="form-group row">
        <lable class="col-xl-4 col-sm-4 label-control"></lable>
        <div class="col-md-4">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('comman.label.create'), ['class' => 'btn btn-primary']) !!}
            {{ Form::reset(trans('comman.label.clear_form'), ['class' => 'btn btn-light']) }}
        </div>
    </div>
</div>
<div class="col-xl-8 col-sm-8">


    <div class="form-group">
        <div class="">
            <div id="caller_list"></div>
        </div>
    </div>
    <div class="form-group">
        <div class="">
            <div id="fileContainer"></div>
        </div>
    </div>
</div>
</div>




@push('js')
<script>
    var format_v = "YYYY-MM-DD";

    @if(isset($ticket) && $ticket->view_on_date==1)
        @if($ticket->activity_time_opt=="yearly" || $ticket->activity_time_opt=="monthly_3" || $ticket->activity_time_opt=="monthly_6")
        format_v = "MM-DD";
            $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_yearly_select_')")
        @endif
        @if($ticket->activity_time_opt=="monthly")
            format_v = "DD";
            $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_monthly_select_')")
        @endif
        @if($ticket->activity_time_opt=="weekly")
            format_v = "d";
            $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_weekly_select_')")
        @endif
    @endif
    var _setSubjectTypeOption = function (value) {
            console.log(value);
        var container = $('#planned_activity_options');
        if (value == 'planned_activity') {
            container.css("display","block");
        }else{
            container.css("display","none");
        }
    };

    var _setUp = function (value) {
        var fileContainer = $('#fileContainer');
        if (value == '') {
            fileContainer.empty();
        } else {
            var _url = "{!! url('admin/subjects/get-view/') !!}";
            fileContainer.load(_url + '/' + value);
        }
    };

    var _setUpCallerList = function (value) {
        var url ="{{url('admin/subjects/get-caller')}}";
        var listContainer = $('#caller_list');
        var title ="<h3> Caller list</h3>";
        listContainer.html("");
        if (value == '') {
            return false;
        } else {
            $.ajax({
                type: "get",
                url: url + "/" + value,
                success: function (result) {
                    if(result.subject){
                    _setSubjectTypeOption(result.subject.subject_type);
                    }

                    data = result.caller_list;
                    if(data){
                    var table= title+"<hr/><table width='100%'> <tr><th>Name</th><th>Phone1</th><th>Phone2</th></tr>";
                    for(var i=0;i<data.length;i++){

                        var tr = "<tr><td>"+ data[i].first_name+" "+data[i].last_name+"</td>";
                             if(data[i].phone_number_1){ tr = tr+"<td>"+data[i].phone_number_1+"</td>"; }else { tr= tr+"<td></td>"}
                             if(data[i].phone_number_1){ tr = tr+"<td>"+data[i].phone_number_1+"</td>"; }else { tr= tr+"<td></td>"}
                        tr=tr+"</tr>";
                        table = table+tr;
                    }
                    table= table+"</tr></table><hr/>";
                    listContainer.html(table);
                    }
                },
                error: function (xhr, status, error) {
                }
            });
        }
    };

    $(document).ready(function () {
        var subject = $('#subject_id');
        if(subject.val()){
            _setUp(subject.val());
            _setUpCallerList(subject.val());
        }
        subject.change(function () {
            var value = $(this).val();
            if(value){
            _setUp(value);
            _setUpCallerList(subject.val());
            }
        });

        $('#activity_time_opt').change(function () {
            var value = $(this).val();
            if(value=="daily"){
                $('#view_on_date_val_container').css("display","none");
            }else{
                $('#view_on_date_val_container').css("display","block");
                $('#view_on_date_val').datetimepicker('destroy');
                var format = "YYYY-MM-DD";
                if(value=="yearly") {
                    format = "MM-DD";
                    $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_yearly_select_')")
                }
                if(value=="monthly") {
                    format = "DD";
                    $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_monthly_select_')")
                }
                if(value=="weekly") {
                    format = "d";
                    $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_weekly_select_')")
                }
                if(value=="monthly_3" || value=="monthly_6") {
                    format = "MM-DD";
                    $("#view_on_date_val").attr("placeholder","@lang('ticket.form.activity_op_yearly_select_')")
                }

                $('#view_on_date_val').datetimepicker({
                    format: format,
                    useCurrent: false
                });
            }
        });

        $('#view_on_date_val').datetimepicker({
            format: format_v,
            useCurrent: false,
        });
        $('#view_start_date').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false,
        });
        $('#view_end_date').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $("#view_start_date").on("dp.change", function (e) {
            $('#view_end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#view_end_date").on("dp.change", function (e) {
            $('#view_start_date').data("DateTimePicker").maxDate(e.date);
        });

    });
</script>
@endpush



@push('js')
<script>
    var sub_search_url ="{{url('admin/activityoptionfilterbywebsite')}}";
    var selected_sub = "0";
    var selected_equ = "0";
    var selected_site = "0";
    var data_subject = [];

    @if(isset($ticket))
        selected_sub = "{{$ticket->subject_id}}";
        selected_equ = "{{$ticket->equipment_id}}";
        selected_site = "{{$ticket->site_id}}";
    @endif

    initSelect();
    function initSelect(){
        @if(_MASTER)
        var filter_id = $('#_website_id').val();
        @else
        var filter_id = "{{_WEBSITE_ID}}";
        @endif

        $("#subject_id").html("");
        $("#equipment_id").html("");
        $("#site_id").html("");

        $.ajax({
            type: "get",
            url: sub_search_url,
            data:{_website_id:filter_id},
            success: function (result) {
                    data = result.data.sites;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_site) { selected = "selected=selected"; }
                        $("#site_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] + "</option>");
                    }
                    
                    data_subject = result.data.subject;
                    data = result.data.equipment;
                    for(var i=0;i<data.length;i++){
                        var selected="";
                        if (data[i]['id'] == selected_equ) { selected = "selected=selected"; }
                        $("#equipment_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['equipment_id'] + "</option>");
                    }
				
                    initSelectSubject();

            },
            error: function (xhr, status, error) {
            }
        });
    }
    function initSelectSubject(){
		$("#subject_id").html("");
                
		var site_id =$("#site_id").val();
                
                if(site_id){
                
                    $.ajax({
                    type: "get",
                    url: "{{ url('admin/subjects/search') }}",
                   data:{site_id:site_id,tickettype:'planned_activity'},
                    success: function (result) {
                            data = result.data;
                            for(var i=0;i<data.length;i++){
                                var selected="";
                                if (data[i]['id'] == selected_sub) { selected = "selected=selected"; }
                                $("#subject_id").append("<option value='" + data[i]['id'] + "' "+selected+">" + data[i]['name'] +" ("+data[i]['services']['name']+") </option>");
                            }
                            if(1){
                               //  _setUpCallerList($("#subject_id").val());
                               //  _setUp($("#subject_id").val());
                            }

                            },
                            error: function (xhr, status, error) {
                            }
                    });
                
                }
        }

    $('#_website_id').change(function() {
        initSelect();
    });
    $('#site_id').change(function() {
        initSelectSubject();
    });


</script>


@endpush

