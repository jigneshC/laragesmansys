@extends('layouts.apex')
@section('title',trans('ticket.label.edit_ticket'))

@section('content')


   <section id="basic-form-layouts">
	<div class="row">
            <div class="col-sm-12">
                <div class="content-header">@lang('ticket.label.edit_planned_activity') # {{ $ticket->id }}</div>
                {{--   @include('partials.page_tooltip',['model' => 'ticket','page'=>'form']) --}}
            </div>
        </div>
	<div class="row">
	    <div class="col-md-12">
	        <div class="card">
	            <div class="card-header">
                        <a href="{{ url('/admin/tickets') }}" title="Back">
                            <button class="btn-link-back"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('comman.label.back')
                            </button>
                        </a>
	                
                        @include('partials.form_notification')
	            </div>
	            <div class="card-body">
	                <div class="px-3">
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
	                    {!! Form::model($ticket, [
                                'method' => 'PATCH',
                                'url' => ['/admin/planned-activity', $ticket->id],
                                'class' => 'form-horizontal',
                                'files' => true,
                                'autocomplete'=>'off'
                            ]) !!}

                            @include ('admin.master-ticket.form', ['submitButtonText' => trans('comman.label.update')])

                            {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	

	

	
</section>


@endsection
