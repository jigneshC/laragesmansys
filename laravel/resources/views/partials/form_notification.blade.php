@if (Session::has('form_unit_charge_notification') && Session::get('form_unit_charge_notification')!="")
<div class="alert  alert-warning alert-important">
    <a class="close" data-dismiss="alert" href="#">×</a>
    {{ Session::get('form_unit_charge_notification') }}
</div>
@endif