<?php
return [


    'label' => [

        'setting' => 'સેટિંગ',
        'settings' => 'સેટિંગ્સ',
        'show_setting' => 'સેટિંગ બતાવો',
        'edit_setting' => 'સેટિંગ સેટ કરો',
        'create_setting' => 'સેટિંગ બનાવો',
        'key' => 'કી',
        'value' => 'મૂલ્ય',

    ]

];