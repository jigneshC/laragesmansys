<?php
return [


    'label' => [

        'dropdown_value' => 'ડ્રોપડાઉન મૂલ્ય',
        'dropdown_values' => 'ડ્રોપડાઉન મૂલ્ય',
        'show_dropdown_value' => 'ડ્રોપડાઉન મૂલ્ય બતાવો',
        'edit_dropdown_value' => 'ડ્રોપડાઉન મૂલ્ય સંપાદિત કરો',
        'create_dropdown_value' => 'ડ્રોપડાઉન મૂલ્ય બનાવો',
        'Add_Languages_in_DropDown_Value' => 'ડ્રોપડાઉન મૂલ્યમાં ભાષા ઉમેરો',
        'add_languages' => 'ભાષા ઉમેરો',
        'type_id' => 'ટાઈપ આઈડી',


    ]

];