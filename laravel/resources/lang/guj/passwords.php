<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'પાસવર્ડ્સ ઓછામાં ઓછા છ અક્ષરો હોવા જોઈએ અને ખાતરી સાથે મેળ ખાશે.',
    'reset' => 'તમારો પાસવર્ડ ફરીથી સેટ કરવામાં આવ્યો છે!',
    'sent' => 'અમે તમારા પાસવર્ડ રીસેટ લિંકને ઈ-મેઇલ કર્યો છે!',
    'token' => 'આ પાસવર્ડ રીસેટ ટોકન અમાન્ય છે.',
    'user' => 'અમે તે ઈ-મેલ સરનામાથી વપરાશકર્તા શોધી શકતા નથી.',

];
