<?php
return [


    'label' => [

        'role' => 'ભૂમિકા',
        'roles' => 'ભૂમિકાઓ',
        'show_role' => 'રોલ્સ બતાવો',
        'edit_role' => 'રોલ્સ સંપાદિત કરો',
        'create_role' => 'ભૂમિકાઓ બનાવો',


    ]

];