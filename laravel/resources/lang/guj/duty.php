<?php

return [
    'label' =>
          array (
              'duty' => 'Devoir',
              'add_subject_title' => 'Ajouter un sujet pour le devoir',
              'select_subject' => 'Sélectionnez le sujet',
              'close' => 'Fermer',
          ),
    'data' =>
        array (
            'default_title' => 'Devoir',
            'default_content' => 'Devoir',
        ),
    'responce_msg' =>
        array (
            'something_went_wrong' => 'Quelque chose c\'est mal passé. Merci d\'essayer plus tard.',
            'subject_add_success' => 'Sujet ajouté succès',
            'subject_update_success' => 'Sujet mis à jour',
        ),
    'js_msg' =>
        array (
            'confirm_for_close_ticket' => 'Êtes-vous sûr de fermer le billet?',
        ),
];