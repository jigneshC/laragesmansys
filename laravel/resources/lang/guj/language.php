<?php
return [


    'label' => [

        'language' => 'ભાષા',
        'languages' => 'ભાષાઓ',
        'show_language' => 'ભાષા બતાવો',
        'edit_language' => 'ભાષા સંપાદિત કરો',
        'create_language' => 'ભાષા બનાવો',
        'lang_code' => 'લેંગ કોડ',


    ]

];