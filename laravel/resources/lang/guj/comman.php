<?php

return array (
    'label' =>
        array (
            'select_from_dropdown' => 'પસંદ કરો ..',
            'add_new' => 'નવો ઉમેરો',
            'mark' => 'ચિહ્ન',
            'action' => 'ક્રિયા',
            'cancel' => 'રદ કરો',
            'save' => 'સાચવો',
            'update' => 'અપડેટ કરો',
            'submit' => 'સબમિટ કરો',
            'create' => 'બનાવો',
            'status' => 'સ્થિતિ',
            'back' => 'પાછળ',
            '_back'=>'Back',
            'delete' => 'કાઢી નાંખો',
            'created' => 'બનાવ્યું',
            'select_subject' => 'વિષય પસંદ કરો',
            'select_site' => 'સાઇટ પસંદ કરો',
            'select_services' => 'સેવાઓ પસંદ કરો',
            'view' => 'જુઓ',
            'created_by' => 'દ્વારા બનાવવામાં',
            'time' => 'સમય',
            'edit' => 'સંપાદિત કરો',
            'logo_image' => 'લોગો છબી',
            'pdf' => 'પીડીએફ',
            'download_pdf' => "DOWNLOAD PDF",
            'download_file' => "DOWNLOAD FILE",
            'actioner' => 'ઍક્શનર',
            'detail' => 'વિગતવાર',
            'description' => 'વર્ણન',
            'label' => 'લેબલ',
            'name' => 'નામ',
            'id' => 'ID',
            'file' => 'ફાઇલ',
            'upload_PDF' => 'PDF અપલોડ કરો',
            'download' => 'ડાઉનલોડ કરો',
            'search' => 'શોધ ..',
            'active' => 'સક્રિય',
            'type' => 'પ્રકાર',
            'close' => 'બંધ',
            'add_or_update_units' => 'Add Or Update Units',
        ),
    'datatable' =>
        array (
            'search' => 'શોધો',
            'show' => 'બતાવો',
            'entries' => 'પ્રવેશો',
            'showing' => 'દર્શાવે',
            'to' => 'થી',
            'of' => 'ના',
            'small_entries' => 'પ્રવેશો',
            'paginate' =>
                array (
                    'next' => 'આગળ',
                    'previous' => 'અગાઉના',
                    'first' => 'પ્રથમ',
                    'last' => 'છેલ્લા',
                ),
        ),
    'daterange' =>
        array (
            'all' => 'બધા',
            'today' => 'આજે',
            'yesterday' => 'ગઇકાલે',
            'last7day' => 'છેલ્લા 7 દિવસ',
            'last30day' => 'છેલ્લાં 30 દિવસ',
            'thismonth' => 'આ મહિને',
            'lastmonth' => 'ગયા મહિને',
            'thisyear' => 'આ વર્ષ',
            'lastyear' => 'ગયું વરસ',
            'customeRange' => 'કસ્ટમ્સ રેંજ',
            'applyBtn' => 'લાગુ કરો',
            'cancelBtn' => 'કાન્કલ',
        ),
    'responce_msg' =>
        array (
            'something_went_wrong' => 'કંઈક ખોટું થયું. પછીથી ફરી પ્રયત્ન કરો.',
            'you_have_no_permision_to_delete_record' => 'તમારી પાસે આ રેકોર્ડને કાઢી નાખવાની પરવાનગી નથી',
            'record_deleted_succes' => 'રેકોર્ડ કાઢી સફળતા',
        ),
    'js_msg' =>
        array (
            'confirm_for_delete' => 'શું તમે ખાતરી કરો કે કાઢી નાખો છો :item_name ?',
        ),
    'error_msg' =>
        array (
            'pdf_file_coule_not_loaded' => 'માફ કરશો, પીડીએફ પ્રદર્શિત કરી શકાયું નથી',
        ),
);

