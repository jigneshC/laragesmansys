<?php

return [


    'label'=>[
        'users' => 'વપરાશકર્તાઓ',
        'user' => 'વપરાશકર્તા',
        'id' => 'ID',
        'name' => 'નામ',
        'email' => 'ઇમેઇલ',
        'role' => 'ભૂમિકા',
        'password' => 'પાસવર્ડ',
        'website' => 'વેબસાઇટ',
        'create_new_user' => 'નવું વપરાશકર્તા બનાવો',
        'create_user' => 'વપરાશકર્તા બનાવો',
        'edit_user' => 'વપરાશકર્તા સંપાદિત કરો',
        'show_user' => 'વપરાશકર્તા બતાવો',
        'select_user' => 'Sélectionner un utilisateur',
    ]
];