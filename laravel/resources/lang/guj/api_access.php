<?php

return [
    'label' => [
        'api_access' => 'Api Access',
        'accesskey' => 'AccessKey',
        'show_access_key' => 'ઍક્સેસ કી બતાવો',
        'regenerate_key' => 'કી પુનઃપેદા',
        'document' => 'દસ્તાવેજ',
        'enter_your_password' => 'તમારો પાસવર્ડ નાખો',

    ],
    'notification_msg'=>[
        'access_key_regenerated' => 'ઍક્સેસ કી પુનઃજનિત',
        'please_enter_your_password..' => 'મહેરબાની કરીને તમારો પાસવર્ડ નાખો.',
        'your_password_is_wrong' => 'તમારો પાસવર્ડ ખોટો છે.',
        'access_key_copied' => 'ઍક્સેસ કી કૉપિ કરેલ',
        'something_wrong' => 'કંઈક ખોટું!, ફરી પ્રયાસ કરો',
    ]
];