<?php

return [
    'label' => [
        'Users' => 'વપરાશકર્તાઓ',
        'Roles' => 'ભૂમિકાઓ',
        'Permissions' => 'પરવાનગીઓ',
        'Give_Role_Permissions' => 'ભૂમિકા પરવાનગીઓ આપો',
        'Peoples' => 'લોકો',
        'Tickets' => 'ટિકિટ્સ',
        'Duty' => 'ફરજ',
        'Services' => 'સેવાઓ',
        'Subjects' => 'વિષયો',
        'Location' => 'સ્થાન',
        'Create_Wizard' => 'વિઝાર્ડ બનાવો',
        'Companies' => 'કંપનીઓ',
        'Sites' => 'સાઇટ્સ',
        'Buildings' => 'ઇમારતો',
        'Settings' => 'સેટિંગ્સ',
        'Constants' => 'સતત',
        'Equipments' => 'સાધનો',
        'Audits' => 'ઓડિટ',
        'DropdownsTypes' => 'નીચે આવતા પ્રકારો',
        'DropdownValues' => 'ડ્રોપડાઉન મૂલ્ય',
        'Languages' => 'ભાષાઓ',
        'Websites' => 'વેબસાઈટસ',
        'Api_Access' => 'Api Access',
        'ModulesAndUnits' => "Modules et unités"
    ],

];