<?php

return array (
    'label' =>
        array (
            'name' => 'prénom',
            'address' => 'Adresse',
            'city' => 'Ville',
            'postcode' => 'Code postal',
            'country' => 'Pays',
            'phone_number_1' => 'Numéro de téléphone 1',
            'phone_number_2' => 'Numéro de téléphone 2',
            'phone_type_1' => 'Type de téléphone 1',
            'phone_type_2' => 'Type de téléphone 2',
            'business_type' => 'Type d\'entreprise',
            'logo' => 'Logo',
            'change_logo' => 'Changer le logo',
            'reference' => 'Référence',
            'active' => 'actif',
            'company' => 'Compagnie',
            'companies' => 'Entreprises',
            'show_company' => 'Afficher la société',
            'edit_company' => 'Modifier l\'entreprise',
            'create_company' => 'Créer une entreprise',
        ),
);
