<?php
return [


    'label' => [

        'site' => 'સાઇટ',
        'name' => 'નામ',
        'description' => 'વર્ણન',
        'manager' => 'મેનેજર',
        'active' => 'સક્રિય',
        'show_service' => 'સેવા બતાવો',
        'edit_service' => 'સેવા સંપાદિત કરો',
        'create_service' => 'સેવા બનાવો',
        'service' => 'સેવાઓ',
        'people' => 'લોકો',
        'yes' => 'હા',
        'no' => 'ના',

    ]

];