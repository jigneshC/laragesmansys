<?php


return [

    'label' => [

        'company' => 'કંપની',
        'name' => 'નામ',
        'address' => 'સરનામું',
        'city' => 'શહેર',
        'postcode' => 'પોસ્ટકોડ',
        'country' => 'દેશ',
        'phone_number_1' => 'ફોન નંબર 1',
        'phone_number_2' => 'ફોન નંબર 2',
        'phone_type_1' => 'ફોન પ્રકાર 1',
        'phone_type_2' => 'ફોન પ્રકાર 2',
        'site_type' => 'સાઇટનો પ્રકાર',
        'logo' => 'લૉગો',
        'change_logo' => 'લોગો બદલો',
        'access_conditions' => 'ઍક્સેસ શરતો',
        'default_email' => 'ડિફૉલ્ટ ઇમેઇલ',
        'guard_presence' => 'ગાર્ડની હાજરી',
        'digicode' => 'ડિજીકોડ',
        'keybox_presence' => 'કીબૉક્સ હાજરી',
        'keybox_code' => 'કીબોક્સ કોડ',
        'keybox_place' => 'કીબોક્સ પ્લેસ',
        'keybox_issue' => 'કીબોક્સ અંક',
        'known_issue' => 'જાણીતા અંક',
        'instructions_tasks_file' => 'સૂચનાઓ કાર્ય ફાઇલ',
        'update_instructions_tasks_file' => 'સુધારા સૂચનાઓ કાર્યો ફાઇલ',
        'order_file' => 'ઓર્ડર ફાઇલ',
        'update_order_file' => 'અપડેટ ઓર્ડર ફાઇલ',
        'procedures_file' => 'કાર્યવાહી ફાઇલ',
        'update_procedures_file' => 'સુધારા કાર્યવાહી ફાઇલ',
        'sitemaps_file' => 'સાઇટમેપ્સ ફાઇલ',
        'update_sitemaps_file' => 'સાઇટમેપ્સ ફાઇલ અપડેટ કરો',
        'active' => 'સક્રિય',
        'show_site' => 'સાઇટ બતાવો',
        'edit_site' => 'સાઇટ સંપાદિત કરો',
        'create_site' => 'સાઇટ બનાવો',
        'site' => 'સાઇટ',
        'site_id' => 'સાઇટ Id',
        'sites' => 'સાઇટ્સ',
        'service' => 'સેવા',

    ]
];