<?php
return array (
    'label' =>
        array (
            'permissions' => 'પરવાનગીઓ',
            'permission' => 'પરવાનગી',
            'nopermission' => 'કોઈ પરવાનગી નથી',
            'show_permission' => 'પરવાનગી બતાવો',
            'edit_permission' => 'પરવાનગી સંપાદિત કરો',
            'create_permission' => 'પરવાનગી બનાવો',
            'Give_Permission_to_Role' => 'ભૂમિકા માટે પરવાનગી આપો',
            'Parent_Permission' => 'પિતૃ પરવાનગી',
        ),
    'notification_msg' =>
        array (
            'Please_select_role_to_give_permission' => 'કૃપા કરીને પરવાનગી આપવા માટે ભૂમિકા પસંદ કરો',
        ),
)
    ;