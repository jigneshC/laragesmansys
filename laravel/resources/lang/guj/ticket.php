<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label' =>
        array (
            'mark_ticket_as' => 'ટિકિટ માર્ક કરો ...',
            'mark_as' => 'તરીકે ચિહ્નિત કરો',
            'evidence' => 'પુરાવા',
            'note' => 'નૉૅધ',
            'place_a_comment' => 'ટિપ્પણી મૂકો',
            'id' => 'Id',
            'name' => 'નામ',
            'title' => 'શીર્ષક',
            'subject' => 'વિષય',
            'content' => 'સામગ્રી',
            'status' => 'સ્થિતિ',
            'site' => 'સાઇટ',
            'equipment_id' => 'સાધન Id',
            'website' => 'વેબસાઇટ',
            'create_ticket' => 'ટિકિટ બનાવો',
            'edit_ticket' => 'ટિકિટ સંપાદિત કરો',
            'added_a_file' => 'એક ફાઇલ ઉમેરાઈ',
            'show_ticket' => 'ટિકિટ બતાવો',
            'add_your_comment_placeorder' => 'તમારી ટિપ્પણી અહીં લખો ....',
            'comments' => 'ટિપ્પણીઓ',
            'assign' => 'સોંપો',
            'assign_to' => 'માટે સોંપો..',
            'select_user' => 'વપરાશકર્તા પસંદ કરો',
            'assign_ticket_to' => 'ટિકિટ પર સોંપો',
            'add_an_evidence' => 'એક પુરાવા ઉમેરો',
            'advance_search' => 'એડવાન્સ શોધ',
            'all_notification' => 'તમામ સૂચના',
            'ticket' => 'ટિકિટ',
        ),
    'form' =>
        array (
            'activity_option' => 'પ્રવૃત્તિ ઑપ્ટ',
        ),
    'notification' =>
        array (
            'assign_user_success' => 'ટિકિટ સોંપેલ :user_name',
            'actioner_has_assing_you_ticket' => ':actioner_name તમને ટિકિટ સોંપી છે',
            'ticket_closed' => 'ટિકિટ બંધ કરવામાં આવી છે',
            'no_permission_to_access' => 'તમારી પાસે પગલાં લેવાની પૂરતી પરવાનગી નથી',
            'view_assign_task'=>"View Assign Task",
            'actioner_has_comment_on_ticket'=>":actioner_name a ajouté un nouveau commentaire sur ticket",
            'actioner_has_add_evidence_on_ticket'=>":actioner_name a ajouté une preuve dans le ticket",
            'actioner_has_change_ticket_status_to'=>":actioner_name has change ticket status to :new_status",
        ),
];
