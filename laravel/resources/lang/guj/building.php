<?php

return [
    'label' => [
        'site' => 'સાઇટ',
        'name' => 'નામ',
        'address' => 'સરનામું',
        'active' => 'સક્રિય',
        'buildings' => 'ઇમારતો',
        'building' => 'મકાન',
        'show_building' => 'બિલ્ડીંગ બતાવો',
        'edit_building' => 'બિલ્ડીંગ સંપાદિત કરો',
        'create_building' => 'મકાન બનાવો',
    ]
];