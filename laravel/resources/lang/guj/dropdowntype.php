<?php
return [


    'label' => [

        'dropdowns_type' => 'ડ્રોપડાઉન પ્રકાર',
        'dropdowns_types' => 'ડ્રોપડાઉન પ્રકારો',
        'show_dropdowns_types' => 'ડ્રોપડાઉન પ્રકાર બતાવો',
        'edit_dropdowns_types' => 'ડ્રોપડાઉન પ્રકાર સંપાદિત કરો',
        'create_dropdowns_types' => 'ડ્રોપડાઉન પ્રકાર બનાવો',


    ]

];