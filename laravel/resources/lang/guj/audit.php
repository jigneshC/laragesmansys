<?php

return [
    'label' => [
        'audit' => 'ઓડિટ',
        'audits' => 'ઓડિટ',
        'show_audit' => 'ઑડિટ બતાવો',
        'edit_audit' => 'ઓડિટ સંપાદિત કરો',
        'create_audit' => 'ઑડિટ બનાવો',
        'table_name' => 'કોષ્ટકનું નામ',
        'table_row_id' => 'કોષ્ટક પંક્તિ id',
        'old_values' => 'જૂના મૂલ્યો',
        'audit_by' => 'દ્વારા ઑડિટ',
    ]
];