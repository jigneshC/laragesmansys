<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'આ ઓળખાણપત્ર અમારા રેકોર્ડ સાથે મેળ ખાતા નથી.',
    'throttle' => 'ઘણા બધા લૉગિન પ્રયાસો કૃપા કરીને ફરીથી પ્રયાસ કરો: સેકંડ સેકંડ.',

];
