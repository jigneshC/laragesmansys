<?php
return [


    'label' => [

        'equipment' => 'સાધનો',
        'equipment_id' => 'સાધન Id',
        'website_id' => 'વેબસાઈટ આઈડી',
        'equipments' => 'સાધનો',
        'show_equipment' => 'સાધનો બતાવો',
        'edit_equipment' => 'સાધન સંપાદિત કરો',
        'create_equipment' => 'સાધન બનાવો',

    ]

];