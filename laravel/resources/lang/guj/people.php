<?php

return array (
    'label' =>
        array (
            'user' => 'વપરાશકર્તા',
            'services' => 'સેવાઓ',
            'title' => 'શીર્ષક',
            'first_name' => 'પ્રથમ નામ',
            'last_name' => 'છેલ્લું નામ',
            'quality' => 'જાત',
            'phone_number_1' => 'ફોન નંબર 1',
            'phone_number_2' => 'ફોન નંબર 2',
            'phone_type_1' => 'ફોન પ્રકાર 1',
            'phone_type_2' => 'ફોન પ્રકાર 2',
            'emergancy_password' => 'ઉભરતા પાસવર્ડ',
            'availability' => 'ઉપલબ્ધતા',
            'hold_key' => 'કી રાખો',
            'change_photo' => 'ફોટો બદલ',
            'photo' => 'ફોટો',
            'email' => 'ઇમેઇલ',
            'my_profile' => 'મારી પ્રોફાઈલ',
            'edit_profile' => 'પ્રોફાઇલ સંપાદિત કરો',
            'update_profile' => 'પ્રોફાઇલ અપડેટ કરો',
            'change_password' => 'પાસવર્ડ બદલો',
            'current_password' => 'અત્યારનો પાસવર્ડ',
            'password_confirmation' => 'પાસવર્ડ ખાતરી',
            'password' => 'પાસવર્ડ',
            'language' => 'ભાષા',
            'joined' => 'જોડાયા',
            'person_details' => 'વ્યક્તિ વિગતો',
            'show_people' => 'લોકો બતાવો',
            'people' => 'લોકો',
            'peoples' => 'લોકો',
            'edit_peoples' => 'લોકો સંપાદિત કરો',
            'create_peoples' => 'લોકો બનાવો',
            'active' => 'સક્રિય',
        ),
    'tool_tip' =>
        array (
            'input_availability' => 'ચેક ઉપલબ્ધ છે?',
        ),
);