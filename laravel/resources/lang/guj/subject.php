<?php


return array (
    'label' =>
        array (
            'subjects' => 'વિષયો',
            'subject' => 'વિષય',
            'type' => 'પ્રકાર',
            'services' => 'સેવાઓ',
            'service' => 'સેવા',
            'subject_hrs' => 'સમય (કલાક)',
            'time_to_resolve' => 'ઉકેલવા માટેનો સમય (કલાક)',
            'show_subject' => 'વિષય બતાવો',
            'edit_subject' => 'વિષય સંપાદિત કરો',
            'create_subject' => 'વિષય બનાવો',
            'caller_list' => 'કૉલર સૂચિ',
            'available' => 'ઉપલબ્ધ',
            'unavailable' => 'અનુપલબ્ધ',
            'instruction' => 'પ્રાધાન્યતા સેટ કરવા માટે ખેંચો અને છોડો.',
        ),
    'subject_type' =>
        array (
            'ticket' => 'ટિકિટ',
            'alarm' => 'એલાર્મ',
            'event' => 'ઇવેન્ટ',
            'planned_activity' => 'આયોજન પ્રવૃત્તિ',
        ),
    'subject_planned_activity_time' =>
        array (
            'daily' => 'દૈનિક',
            'weekly' => 'સાપ્તાહિક',
            'monthly' => 'માસિક',
            'monthly_3' => 'દર 3 માસિક',
            'monthly_6' => 'દરેક 6 માસિક',
            'yearly' => 'વાર્ષિક',
        ),
);