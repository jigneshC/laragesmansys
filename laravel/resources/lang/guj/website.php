<?php
return [


    'label' => [

        'website' => 'વેબસાઇટ',
        'websites' => 'વેબસાઈટસ',
        'create_website' => 'વેબસાઇટ બનાવો',
        'show_website' => 'વેબસાઇટ બતાવો',
        'edit_website' => 'વેબસાઇટ સંપાદિત કરો',
        'domain' => 'ડોમેન',
        'master' => 'માસ્ટર',

    ]

];