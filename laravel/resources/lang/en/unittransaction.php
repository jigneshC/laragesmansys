<?php

return [
    'label' => [
        'unittransaction'=>'Unit Transaction',
        'unittransactions'=>'Unit Transactions',
        'unit_type'=>'Transaction Type',
        'reference'=>'Reference',
        'comment'=>'Comment',
        'available_units'=>'Available Units',
        'get_more_units'=>'Get More Units',
        'order_list'=>'My Orders',
        'transaction_detail'=>"Transaction Detail",
        'transaction_id'=>"Transaction Id",
        'domain_id'=>"Domain Id",
        'transaction_date'=>"Date",
        'module_items'=>"Module / Items",
        'unit_charge_per_day'=>"Unit Charge / Day",
        'free_items'=>"Free Items",
        'total_items'=>"Total Items",
        'total_charge'=>"Total Charge",
        'net_chargable_units_day'=>'Net Chargable Units / Day',
        'credited_unit'=>"Credited Units",
        'debited_unit'=>"Debited Units",
        'in_day'=>"in :day days",
        'today'=>"Today",
        
       
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        
    ],
    'js_msg' =>[
        
    ],
    'notification'=>[
        'unit_is_low'=>'Domain has very low units (credit) :unit .Please purchase more units to countinue domain services.',
        'unit_will_end_in_check_more_plan'=>"Domain has very low units (credit), it should be end :in_number_days.",
        'domin_will_be_charge_per_day'=>"Domain will be charged :units units / day for item",
        'domin_charge_per_day'=>"Domain will be charged :units units / day.",
        'allow_free_tiems'=>"(* :total_free Free :module_name)"
    ]
];