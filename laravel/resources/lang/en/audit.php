<?php

return [
    'label' => [
        'audit' => 'Audit',
        'audits' => 'Audits',
        'show_audit'=>'Show Audit',
        'edit_audit'=>'Edit Audit',
        'create_audit'=>'Create Audit',
        'table_name'=>'Table Name',
        'table_row_id'=>'Table Row Id',
        'old_values'=>'Old Values',
        'audit_by'=>'Audit By',
    ]
];