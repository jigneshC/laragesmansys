<?php

return [
    'label' => [
        'api_access' => 'Api Access',
        'accesskey' => 'AccessKey',
        'show_access_key' => 'Show Access Key',
        'regenerate_key' => 'Regenerate Key',
        'document' => 'Document',
        'enter_your_password' => 'Enter Your Password',

    ],
    'notification_msg'=>[
        'access_key_regenerated'=>'Access key regenerated.',
        'please_enter_your_password..'=>'Please, Enter your password.',
        'your_password_is_wrong'=>'Your password is wrong.',
        'access_key_copied'=>'Access key Copied',
        'something_wrong'=>'Something wrong!, try again',
    ]
];