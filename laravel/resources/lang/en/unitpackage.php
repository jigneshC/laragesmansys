<?php

return [
    'label' => [
        'unitpackages'=>'Unit Packages',
        'unitpackage'=>'Unit Packages',
        'package'=>'Package',
        'unit'=>'Unit',
        'name'=>'Package Name',
        'desc'=>'Description',
        'price'=>'Price',
        'price_currency'=>'Price Currnecy',
        'create_a_package'=>'Create a package',
        'update_a_package'=>'Update a package',
        'package_class'=>'Package Class',
        'package_code'=>'Package Code',
       
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        'no_packages_available_for_purchase_an_units'=>'Sorry , No any packages avilabel , Please try after some time'
      
    ],
    'js_msg' =>[
        
    ],
];