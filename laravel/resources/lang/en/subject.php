<?php


return [

    'label' => [
        'subjects'=>'Subjects',
        'subject'=>'Subject',
        'type'=>'Type',
        'services'=>'Services',
        'service'=>'Service',
        'subject_hrs'=>'Time (Hours)',
        'time_to_resolve'=>'Time to resolve (Hours)',
        'show_subject'=>'Show Subject',
        'edit_subject'=>'Edit Subject',
        'create_subject'=>'Create Subject',
        'caller_list'=>'Caller List',
        'available'=>'Available',
        'unavailable'=>'Unavailable',
        'instruction' => 'Drag and drop to set priority.',
		'geolocation_id'=>'Geolocation Id',
        'is_geolocation'=>'Enable Geolocation',
        'instruction_file'=>'Instruction File',

    ],
    'subject_type'=>[
        'ticket'=>'Ticket',
        'alarm'=>'Alarm',
        'event'=>'Event',
        'planned_activity'=>'Planned Activity',
        'duty'=>"Duty",
    ],
    'subject_planned_activity_time'=>[
        'daily'=>'Daily',
        'weekly'=>'Weekly',
        'monthly'=>'Monthly',
        'monthly_3'=>'Every 3 Monthly',
        'monthly_6'=>'Every 6 Monthly',
        'yearly'=>'Yearly',
    ]

];