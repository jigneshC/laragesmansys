<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    'label' => [

        'name' => 'Name',
        'address' => 'Address',
        'city' => 'City',
        'postcode' => 'Postcode',
        'country' => 'Country',
        'phone_number_1' => 'Phone number 1',
        'phone_number_2' => 'Phone number 2',
        'phone_type_1' => 'Phone type 1',
        'phone_type_2' => 'Phone type 2',
        'business_type' => 'Business type',
        'logo' => 'Logo',
        'change_logo' => 'Change Logo',
        'reference' => 'Reference',
        'active' => 'Active',
        'company' => 'Company',
        'companies' => 'Companies',
        'show_company'=>'Show Company',
        'edit_company'=>'Edit Company',
        'create_company'=>'Create Company',
        'Assign_Company_Modules'=>"Assign Company-Modules",
        'Assigned_Modules'=>"Assigned Modules",
        'Non_Assigned_Modules'=>"Non-Assigned Modules",
        'to_process_company_module_plaease_complete_company_form_first'=>"To process company module,please fillup & complete company form first !",
        'move_user_to_domain'=>"Move user from domain :olddomain to  :newdomain  ",
        'already_used_domain_user_as'=>"Already Used as Caller/Manager in [:olddomain]",
        'other_domain_user'=>"Other Users in [:olddomain]",
        'move_selected_users_to_new_domain'=>"Move selected users to new sub-domain",
    ]


];
