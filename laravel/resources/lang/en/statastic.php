<?php

return [
    'label' => [
        'Overall_Ticket_Report' => 'Overall Ticket Report',
        'Tickets_Report'=>"Tickets Report",
        'Tickets' => 'Tickets',
        'planned_activity' => 'Planned activity',
        'alarm' => 'Alarm',
        'event' => 'Event',
        'Top Twenty' => 'Top Twenty',
        'subjects'=>'Subjects',
        'services'=>'Services',
        'sites'=>'Sites',
        'companies'=>'Companies',
        'create_image'=>"Create Report Image",
        'duration'=>"Duration"
    ],

];