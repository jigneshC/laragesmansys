<?php
return [


    'label' => [

        'equipment' => 'Equipment',
        'equipment_id' => 'Equipment Id',
        'website_id' => 'Website Id',
        'equipments' => 'Equipments',
        'show_equipment'=>'Show Equipment',
        'edit_equipment'=>'Edit Equipment',
        'create_equipment'=>'Create Equipment',


    ]

];