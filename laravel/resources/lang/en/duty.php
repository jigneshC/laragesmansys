<?php

return [
    'label' => [
        'duty' => 'Duty',
        'add_subject_title' => 'Add Subject For Duty',
        'select_subject' => 'Select Subject',
        'close' => 'Close',
    ],
    'data'=>[
        'default_title'=>"Duty",
        'default_content'=>"Duty"
    ],
    'responce_msg' =>[
        'something_went_wrong'=>'Something went wrong , Please try again later.',
        'subject_add_success'=>'Subject added success',
        'subject_update_success'=>'Subject updated success',
        'no_duty_subject_for_given_company'=>'No any subject found for given company , please create new subject with type duty'
    ],
    'js_msg' =>[
        'confirm_for_close_ticket'=>'Are You To Sure Close Ticket ?',
    ],
];