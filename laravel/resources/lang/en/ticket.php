<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label'=>[
        'mark_ticket_as'=>'Mark ticket as ...',
        'mark_as'=>'Mark as',
        'evidence'=>'Evidence',
        'note'=>'Note',
        'place_a_comment'=>'Place a Comment',
        'id'=>'Id',
        'name'=>'Name',
        'title'=>'Title',
        'subject'=>'Subject',
        'content'=>'Content',
        'status'=>'Status',
        'site'=>'Site',
        'equipment_id'=>'Equipment Id',
        'website'=>'Website',
        'create_ticket'=>'Create Ticket',
        'edit_ticket'=>'Edit Ticket',
        'added_a_file'=>'Added a File',
        'show_ticket'=>'Show Ticket',
        'add_your_comment_placeorder'=>'Type your comment here....',
        'comments'=>'Comments',
        'assign'=>'Assign',
        'assign_to'=>'Assign to..',
        'select_user' => 'Select User',
        'assign_ticket_to' => 'Assign Ticket to',
        'add_an_evidence' => 'Add an Evidence',
        'assign' => 'Assign',
        'advance_search'=>'Advance Search',
        'all_notification'=>'All Notification',
        'ticket'=>'Ticket',
        'planned_start_date'=>'Start Date',
        'planned_end_date'=>'End Date',
        'action_log'=>'Action Logs',
        'view_on_date'=>'Activity Date',
        'view_before_date'=>'Desplay Before Hours',
        'activity_date'=>"Activity Date or Day",
        'planned_activity'=>"Planned Activity",
        'activity_time'=>'Activity Time',
        'duration'=>"Duration",
        'edit_planned_activity'=>'Edit Planned Activity',
        'create_planned_activity' => 'Create Planned Activity',
        'show_planned_activity'=>"Show Planned Activity",
        'planned_activity_detail'=>"Planned Activity Detail",
        'actioner'=>"Actioner",
        'action'=>"Action",
        'detail'=>"Detail",
        'all'=>"All",
		'comment'=>'Comment',
    ],
	'history_type'=>[
		'action'=>'Status',
		'assign'=>'Assign',
		'file'=>'Document',
		'comment'=>'Comment',
	],
    'form'=>[
        'activity_option'=>"Activity Opt.",
        'planned_activity_range'=>"Date Limit",
        'activity_op_yearly_select_'=>"Select Day And Month Of Year",
        'activity_op_monthly_select_'=>"Select Day Of Month",
        'activity_op_weekly_select_'=>"Select Day Of Week"

    ],
    'notification'=>[
        'ticket_created_success' =>"New ticket has been created!",
        'assign_user_success' =>"Ticket assigned to :user_name",
        'actioner_has_assing_you_ticket'=>":actioner_name has assigned you a ticket",
        'ticket_closed'=>"Ticket has been closed",
        'no_permission_to_access'=>"You have not enough permission to take an action",
        'actioner_has_comment_on_ticket'=>":actioner_name has added new comment on ticket",
        'actioner_has_add_evidence_on_ticket'=>":actioner_name has added an evidence in ticket",
        'actioner_has_change_ticket_status_to'=>":actioner_name has changed ticket status to :new_status",
		'actioner_has_assing_ticket_to_user'=>":actioner_name has assigned ticket to :assinee",
		'actioner_has_created_tikcet'=>":actioner_name has created ticket",
		'actioner_has_created_call'=>":actioner_name has created call",
		
		'error_tikcet_not_found'=>"Ticket data not found!",
		
		'sucess_comment_added'=>'You have posted comment successfully!',
		'sucess_file_added'=>'You have added file successfully!',
		'sucess_calllog_added'=>'Call log created successfully!',
        
    ],
    'tool_tip'=>[
        'status'=>[
            'new' => 'Not viewed ever',
            'open' => 'Once viewed by operator',
            'pending' => 'Pending',
            'on-hold' => 'On-hold',
            'solved' => 'Solved',
            'closed' => 'Closed'
        ]
    ],
];
