<?php
return [


    'label' => [

        'site' => 'Site',
        'name' => 'Name',
        'description' => 'Description',
        'manager' => 'Manager',
        'active' => 'Active',
        'show_service'=>'Show Service',
        'edit_service'=>'Edit Service',
        'create_service'=>'Create Service',
        'service'=>'service',
        'servicies'=>'services',
        'people'=>'People',
        'yes' => 'YES',
        'no' => 'NO',

    ]

];