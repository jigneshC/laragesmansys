<?php
return [


    'label' => [

        'website' => 'Subdomain',
        'websites' => 'Subdomains',
        'create_website' => 'Create Subdomain',
        'show_website' => 'Show Subdomain',
        'edit_website' => 'Edit Subdomain',
        'domain' => 'Domain',
        'master' => 'Master',
        'enable_charge'=>"Enable Charge",
        'enable_qrcode'=>"Enable Qrcode",
		'domain_name_not_valid'=>"The domain name format is not valid",

    ]

];