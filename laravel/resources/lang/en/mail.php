<?php

return [


    'label'=>[
        'thank_you_for_using_our_application'=>'Thank you for using our application!',
        'view_assign_task'=>"View Assign Task",
        'view_detail'=>"View Ticket",
        'ticket_has_been_marked_as'=>"ticket has been marked as ':status'",
        'tickets_acknowledged_text'=>"Click here to send your Acknowledgement",
        'tickets_acknowledged_comment'=>"Email has been acknowledged",
    ],
	'subject'=>[
		'login_otp'=>"Gesmansys Login One tiem password (Otp)",
		'reset_password'=>'Forgot Password!',
	],
	'slug'=>[
		'you_have_request_to_forgot_password'=>'You have recently request to reset password  for :app account.',	
	'your_temp_password_to_reset_password_is'=>'You can login with new password :password . After login you can change/reset password as you want.',	
		'your_otp_to_reset_password_is'=>'Your one time password (otp) to reset your password is',	
		'if_you_did_not_request'=>"If you didn't request this then ignor this email or let us know.",	
		'otp_will_be_expire_within'=>"And will be expire within :minuts minuts.",	
	],
    

];
