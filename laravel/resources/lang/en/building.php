<?php

return [
    'label' => [
        'site' => 'Site',
        'name' => 'Name',
        'address' => 'Address',
        'active' => 'Active',
        'buildings' => 'Buildings',
        'building' => 'Building',
        'show_building'=>'Show Building',
        'edit_building'=>'Edit Building',
        'create_building'=>'Create Building',
    ]
];