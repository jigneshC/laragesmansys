<?php

return [
    'label' => [
       'moduleunit'=>'Module And Unit',
       'action'=>'Action',
       'unit'=>'Unit',
       'units'=>'Units',
       'units_day'=>"Units / Day",
       'creadit_unit'=>"Credit an Units",
       'unit_charge'=>'Units Charge',
       'unit_charge_per_day'=>"Units Charge Per Day",
       'unit_charge_per_month'=>"Units Charge Per Month",
       'chargable_module'=>"Chargable Module",
        
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        'moduel_unit_add_success'=>'Module Unit  Added Success',
        'moduel_unit_updated_success'=>'Module Unit  Updated Success',
        'unit_credited_to_website_success'=>'Units credited in to Subdomain success',
        'you_have_not_enough_credit'=>"You don't have enough credit/units,to take an action !",
        'click_to_check_packages'=>'To check available unit Packages',
    ],
    'js_msg' =>[
        
    ],
    'unit_charge_lable' =>[
        'license'=>" license (1 customer, 1 Site, 1 Buidling, 1 Service, 10 Subjects)",
        'users'=>'User Item',
        'users_module'=>'User Module',
        'companies'=>'Company Item',
        'companies_module'=>'Company Module',
        'sites'=>'Site Item',
        'sites_module'=>'Site Module',
        'services'=>'Services Item',
        'services_module'=>'Services Module',
        'audits'=>'Audit Item',
        'audits_module'=>'Audit Module',
        'buildings'=>'Building Item',
        'buildings_module'=>'Building Module',
        'subjects'=>'Subject Item',
        'subjects_module'=>'Subject Module',
        'tickets'=>'Ticket Item',
        'tickets_module'=>'Ticket Module',
        'duty_module'=>'Duty Module',
        'plannedactivity_module'=>'Planned Activity Module',
    ]
];