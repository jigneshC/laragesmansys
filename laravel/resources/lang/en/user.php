<?php

return [


    'label'=>[
        'users'=>'Users',
        'user'=>'User',
        'id'=>'ID',
        'name'=>'Name',
        'email'=>'Email',
        'role'=>'Role',
        'password'=>'Password',
        'website'=>'Website',
        'create_new_user'=>'Create New User',
        'create_user'=>'Create User',
        'edit_user'=>'Edit User',
        'show_user'=>'Show User',
        'select_user'=>'Select User',
        'accessible_website' => 'Accessible Website',
        'enable_sms_notification' => 'Enable SMS Notification',
        'login_from_date' => 'Enable Login From Date',
        'login_to_date' => 'Enable Login To Date',
    ]
];