<?php

return [
	'responce_msg' => [
		'success_default' => 'Action Success !',
        'success_login' => 'Login Success.',
        'success_logout' => 'You have successfully logged out!',
        'success_forgot_password_send_to_email' => 'One time password (otp) to reset your password has been sent to your registered email address.',
        'success_otp_to_login_sent_on_email' => 'One time password (otp) to login has been sent to your registered email address.',
        'success_otp_to_login_sent_on_phone' => 'One time password (otp) to login has been sent to your registered Phone number.',
        'success_otp_to_login_sent_on_phone_email' => 'One time password (otp) to login has been sent to your registered Phone number and Email.',
        'success_new_password_generated' => 'New password has bee generated! and sent to your registered email address.',
        'success_forgot_password_send_to_phone_no' => 'One time password has been sent on your phone number.',
        'success_password_changed' => 'Success! Your Password has been changed!',
        'success_register' => 'User Register Successfully.',
        'success_profile_updated' => 'Profile Updated Successfully.',
        
		
        'warning_user_data_not_found' => 'User data not found .',
        'warning_no_data_found' => 'No Record Found.',
        'warning_your_account_not_activated_yet' => 'Your account has not activated Yet.',
        'warning_incorrect_email_or_password' => 'Invalid email or password',
        'warning_require_unique_phone_number' => 'Phone number already exists with another account. Try with another number',
        'warning_require_unique_user_name' => 'Username already exists. please try another one.',
        'warning_current_password_incorrect' => 'Your current password does not match with our records.',
        'warning_invalid_qrcode' => 'Qr-code is not valid!',
        'warning_user_have_not_valid_phone_no' => 'User have no phone number to send One time password (otp)',
        'warning_otp_token_expired' => 'Otp token has been expired !',
        'warning_otp_token_is_not_valid' => 'Otp token is not valid !',
        'warning_you_have_no_permission_to_access_this_site' => 'You have no permission to access this site!',
        'warning_your_account_not_activated_yet_or_disabled' => 'Your account is either deactivated or no access permission. Please contact admin for more information',
		
		
        'error_default'=>'Something went wrong !',
		
		
	],
];