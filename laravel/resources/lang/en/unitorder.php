<?php

return [
    'label' => [
        'order_now'=>'Order Now',
        'confirm_order'=>"Confirm Order",
        'confirm_and_pay_amount'=>"Confirm And Pay :amount",
        'invoice'=>'Invoice',
        'to'=>'To',
        'orderstatus'=>'Status',
        'payment_status'=>'Payment Status',
        'reference_id  '=>'Payment Refrence Id',
        'payment_type'=>'Payment Type',
        'item'=>'Item Name',
        'item_qty'=>'Qty',
        'price_total'=>'Price total',
        'discount'=>'Discount',
        'net_payable_amount'=>'Net Payable Amount',
        'paidamount'=>"Paid Amount",
        'order'=>'Order',
        'view_order'=>'View Order',
        'buyer_detail'=>'Buyer Detail',
        'buyer_name'=>"Buyer Name",
        'buyer_contact'=>"Buyer Contact",
        'buyer_email'=>"Buyer Email",
        'payment_detail'=>"Payment Detail",
        'invoicedate'=>"Invoice Date",
        'reference_id'=>"Reference Id",
        
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
       'order_place_successfully'=>'Order Place succefully !',
       'order_canceled'=>'Order Cancelled !!',
       'no_order_found'=>'There are no any order in subdomain',
      
    ],
    'js_msg' =>[
        
    ],
    'mail_admin' =>[
            'order_place_success_subject'=>'Congratulation for order!',
            'order_place_success_headerline1'=>'The following order has been payed:',

            'order_place_failed_subject'=>'Order Failed !',
            'order_place_failed_headerline1'=>'Please check the reason and follow up.',


            'order_place_canceled_subject'=>'Order Cancelled !',
            'order_place_canceled_headerline1'=>'The following order has been cancelled at checkout:'
    ],
    'mail_user' =>[
        'order_place_success_subject'=>'Congratulation for your order!',
        'order_place_success_headerline1'=>'You have successfully place order , following is order detail :',

        'order_place_failed_subject'=>'Order Failed !',
        'order_place_failed_headerline1'=>'Please check the reason and follow up.',


        'order_place_canceled_subject'=>'Order Cancelled !',
        'order_place_canceled_headerline1'=>'The following order has been cancelled at checkout:'
    ]
];