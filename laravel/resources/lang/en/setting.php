<?php
return [


    'label' => [

        'setting' => 'Setting',
        'settings' => 'Settings',
        'show_setting'=>'Show Setting',
        'edit_setting'=>'Edit Setting',
        'create_setting'=>'Create Setting',
        'key'=>'Key',
        'value'=>'Value',
        'type'=>'Type',

    ]

];