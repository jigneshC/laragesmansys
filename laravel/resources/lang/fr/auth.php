<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Ces identifiants ne correspondent pas à nos enregistrements',
    'throttle' => 'Trop de tentatives de connexion. Veuillez essayer de nouveau dans :seconds secondes.',
	'here_is_otp_to_login' => ":otp est votre mot de passe à usage unique (otp) pour vous connecter à l'application gesmansys.",
	'email_or_phone_selection_required' => "Veuillez sélectionner l'option email ou sms pour OTP.",

];
