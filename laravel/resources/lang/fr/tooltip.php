<?php

return [

    'common'=>[
        'label'=>[
            'action' => 'Afficher Modifier Supprimer',
            'description' => 'Description',
        ],
        'icon' =>[
             'edit' => 'Modifier l\'enregistrement',
                'delete' => 'Supprimer l\'enregistrement',
                'eye' => 'Voir les détails',
				'recover'=>"Récupérer l'enregistrement supprimé",
        ],
    ],
    'ticket'=>[
        'status'=>[
            'new' => 'Jamais vu',
            'open' => 'Assigné à une personne',
            'pending' => 'En attente de résolution',
            'on-hold' => 'En attente de décision',
            'solved' => 'Résolu, en attente de confirmation',
            'closed' => 'Fermé',
        ],

        'label'=>[
            'subject' => 'Sujet',
            'title' => 'Titre',
            'site' => 'Site',
            'assign' => 'Assigne',
            'content' => 'Contenu',
            'mark' => 'Plus d\'actions',
            'action' => 'Afficher Modifier Supprimer',
            'close' => 'Fermer le ticket',
        ],
        'icon' =>[
            'checked' => 'Changer le statut pour',
            'file' => 'Ajouter des preuves',
        ],
        'form_field' =>[
            'activity_option' => 'Le Ticket sera créé seulement à l\'heure indiquée.',
            'evidence' => 'Preuve du problème / rapport / photo',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Billets, événements, activités du plan, alarmes, etc.',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Billets, événements, activités du plan, alarmes, etc.',
    ],


    'duty'=>[
        'icon' =>[
            'subject_add_icon' => 'Ajouter un sujet pour la prise de service',
            'checked' => 'Fermer le Ticket associé',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Billets, événements, activités du plan, alarmes, etc.',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Billets, événements, activités du plan, alarmes, etc.',
    ],

    'moduleunit'=>[
        'index_info_header'=>'Comment ça marche',
        'index_info'=>'Définir le prix unitaire pour chaque module',
        'form_info_header'=>'Comment ça marche',
        'form_info'=>'Définir le prix unitaire pour chaque module'
    ],
    'unitpackage'=>[
        'index_info_header'=>'Comment ça marche',
        'index_info'=>"L'utilisateur peut commander et acheter un forfait et obtenir / créditer des unités de forfait",
    ],
    'unittransaction'=>[
        'index_info_header'=>'Comment ça marche',
        'index_info'=>"Transaction / Historique des unités du domaine",
    ],

    'services'=>[
        'form_field' =>[
            'site' => 'Ajouter les sites sur les quels le service sera actif',
            'manager' => 'Selectionnez le Gestionnaire du service',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Le sujet appartient aux services',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Le sujet appartient aux services',
    ],


    'subject'=>[
        'form_field' =>[
            'service' => 'Service pour le sujet',
            'time_to_resolve' => 'Temps (en minutes) alloué pour résoudre le problème',
            'caller_list' => 'Personnes affectées au sujet',
            'upload_PDF' => 'Fichier d\'instructions',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Sujets pour Ticket',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Sujets pour Ticket',
    ],


    'companies'=>[
        'form_field' =>[
            'business_type' => 'Type d\'entreprise',
            'logo' => 'Logo des entreprises',
            'reference' => 'Référence des entreprises',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste des entreprises',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Entreprises',
    ],
    
    'companies_module'=>[
        'form_info_header'=>'How it\'s work',
        '_info_header'=>"La société peut sélectionner un ou plusieurs modules facturables, en fonction de la société du module d'affectation serait facturé"
    ],


    'site'=>[
        'form_field' =>[
            'company' => 'Le site appartient à l\'entreprise',
            'site_type' => 'Type de site, c.-à-d.',
            'logo' => 'Logo du site',
            'access_conditions' => 'Conditions d\'accès',
            'default_email' => 'Email par défaut',
            'guard_presence' => 'Présence de garde',
            'digicode' => 'Digicode',
            'keybox_presence' => 'Keybox Présence',
            'keybox_code' => 'Code de la Keybox',
            'keybox_place' => 'Keybox Place',
            'keybox_issue' => 'Question Keybox',
            'known_issue' => 'Problème connu',
            'instructions_tasks_file' => 'Fichier de tâches d\'instructions',
            'order_file' => 'Commander un fichier',
            'procedures_file' => 'Fichier de procédures',
            'sitemaps_file' => 'Fichier sitemaps',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Le bâtiment appartient au site',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Le bâtiment appartient au site',
    ],


    'building'=>[
        'form_field' =>[
            'site' => 'Le bâtiment appartient au site',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Bâtiments, Cela appartient au site',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Bâtiments, Cela appartient au site',
    ],


    'equipment'=>[
        'form_field' =>[
            'equipment_id' => 'ID unique pour l\'équipement',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste des équipements disponibles',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Liste des équipements disponibles',
    ],


    'audit'=>[
        'form_field' =>[
            'table_name' => 'Nom de la table',
            'table_row_id' => 'Table Row Id',
            'old_values' => 'Anciennes valeurs',
            'audit_by' => 'Audit ajouté par quelqu\'un',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste d\'audit de table',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Liste d\'audit de table',
    ],

    'people'=>[
        'form_field' =>[
            'User' => 'Les gens appartiennent à certains utilisateurs - Manager',
            'Services' => 'Prestations de service',
            'Title' => 'Titre',
            'Quality' => 'Qualité',
            'Emergancy_Password' => 'Mot de passe d\'urgence',
            'Availability' => 'Disponibilité',
            'Hold_Key' => 'Maintenir la touche',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste des personnes',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Liste des personnes',
    ],

    'permission'=>[
        'form_field' =>[
            'Parent_Permission' => 'Permission des parents',
            'name' => 'Nom de l\'autorisation',
            'label' => 'Nom du jeu d\'autorisations',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste des autorisations, Autorisation appartient au rôle',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Liste des autorisations, Autorisation appartient au rôle',
    ],


    'role'=>[
        'form_field' =>[
            'name' => 'Nom de rôle',
      'label' => 'Nom du rôle desplay',
      'permission' => 'Rôle Peut avoir beaucoup de permission',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'L\'utilisateur peut avoir un ou plusieurs rôles, et selon l\'autorisation de rôle l\'utilisateur peut accéder aux modules',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'L\'utilisateur peut avoir un ou plusieurs rôles, et selon l\'autorisation de rôle l\'utilisateur peut accéder aux modules',
    ],


    'website'=>[
        'form_field' =>[
            'domain' => 'Domaine du site',
      'logo_image' => 'Logo Image pour le site Web',
      'master' => 'Site principal, Autoriser tous les accès',
        ],
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste de tous les sites Web disponibles et avec permission. Le site Web principal a accès à tous les sites Web. Le site Web non principal n\'a accès qu\'à ses propres données',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Liste de tous les sites Web disponibles et avec permission. Le site Web principal a accès à tous les sites Web. Le site Web non principal n\'a accès qu\'à ses propres données',
    ],


    'api_access'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Liste Api et étapes de processus .., Générer et Obtenir la clé d\'accès',
        'form_info_header' => '',
        'form_info' => '',
    ],

    'dropdownvalue'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Valeur de Dropdown',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Valeur de Dropdown',
    ],

    'dropdowntype'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Type de liste déroulante',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Type de liste déroulante',
    ],

    'language'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'language Disponible pour le site',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'language Disponible pour le site',
    ],

    'profile'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Profil Détail',
        'form_info_header' => 'Comment ça marche',
        'form_info' => '',
    ],

    'setting'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Stocker une valeur constante',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Stocker une valeur constante',
    ],

    'user'=>[
        'index_info_header' => 'Comment ça marche',
        'index_info' => 'Utilisateur du site Web Auth, l\'utilisateur peut avoir un ou plusieurs rôles et peut accéder aux modules selon le rôle.',
        'form_info_header' => 'Comment ça marche',
        'form_info' => 'Utilisateur du site Web Auth, l\'utilisateur peut avoir un ou plusieurs rôles et peut accéder aux modules selon le rôle.',
    ],

];
