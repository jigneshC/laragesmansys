<?php

return [


    'label'=>[
        'users' => 'Utilisateurs',
        'user' => 'Utilisateur',
        'id' => 'ID',
        'name' => 'Nom',
        'email' => 'Email',
        'role' => 'Rôle',
        'password' => 'Mot de passe',
        'website' => 'Sous-domaine',
        'create_new_user' => 'Créer un nouvel utilisateur',
        'create_user' => 'Créer un utilisateur',
        'edit_user' => 'Modifier l\'utilisateur',
        'show_user' => 'Afficher l\'utilisateur',
        'select_user' => 'Sélectionner un utilisateur',
		'accessible_website' => 'Accessible Website',
		'enable_sms_notification' => 'Activer la notification par SMS',
		'login_from_date' => 'Activer la connexion à partir de la date',
        'login_to_date' => 'Activer la connexion à la date',
    ]
];