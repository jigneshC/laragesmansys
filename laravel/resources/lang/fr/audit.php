<?php

return [
    'label' => [
        'audit' => 'Audit',
        'audits' => 'Audits',
        'show_audit' => 'Afficher l\'audit',
        'edit_audit' => 'Modifier l\'audit',
        'create_audit' => 'Créer un audit',
        'table_name' => 'Nom de la table',
        'table_row_id' => 'Ligne ID de la table',
        'old_values' => 'Anciennes valeurs',
        'audit_by' => 'Audité par',
    ]
];