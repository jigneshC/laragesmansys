<?php
return [


    'label' => [

        'permissions' => 'Autorisations',
        'permission' => 'Autorisation',
        'nopermission' => 'Aucune autorisation',
        'show_permission' => 'Afficher la permission',
        'edit_permission' => 'Modifier la permission',
        'create_permission' => 'Créer une autorisation',
        'Give_Permission_to_Role' => 'Donner la permission à un rôle',
        'Parent_Permission' => 'Permission des parents',


    ],
    'notification_msg'=>[
        'Please_select_role_to_give_permission' => 'Veuillez sélectionner le rôle pour donner la permission',
    ]

];