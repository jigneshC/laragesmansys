<?php

return [
    'label' => [
        'api_access' => 'Accès Api',
        'accesskey' => 'Clef d\'accès',
        'show_access_key' => 'Afficher la clé d\'accès',
        'regenerate_key' => 'Régénérer la clé',
        'document' => 'Document',
        'enter_your_password' => 'Tapez votre mot de passe',

    ],
    'notification_msg'=>[
        'access_key_regenerated' => 'Clé d\'accès régénérée.',
        'please_enter_your_password..' => 'S\'il vous plait entrez votre mot de passe.',
        'your_password_is_wrong' => 'Votre mot de passe est erroné.',
        'access_key_copied' => 'Clé d\'accès copiée',
        'something_wrong' => 'Quelque chose ne va pas!',
    ]
];