<?php

return [
    'label' => [
        'Users' => 'Utilisateurs',
        'Roles' => 'Rôles',
        'Permissions' => 'Autorisations',
        'Give_Role_Permissions' => 'Attribuer des autorisations aux rôles',
        'Peoples' => 'Personnes',
        'Tickets' => 'Tickets',
        'Duty' => 'Prise de Service',
        'Services' => 'Services',
        'Subjects' => 'Sujets',
        'Location' => 'Emplacements',
        'Create_Wizard' => 'Assistant de création',
        'Companies' => 'Entreprises',
        'Sites' => 'Sites',
        'Buildings' => 'Bâtiments',
        'Settings' => 'Paramètres',
        'Constants' => 'Constantes',
        'Equipments' => 'Équipements',
        'Audits' => 'Audits',
        'DropdownsTypes' => 'Types de listes déroulantes',
        'DropdownValues' => 'Valeures de listes déroulantes',
        'Languages' => 'Langues',
        'Websites' => 'Sous-domaines',
        'Api_Access' => 'Accès Api',
        'Planned_Activity' => 'Tâches Planifiées',
        'ModulesAndUnits' => 'Modules et unités',
        'UnitPackage'=>'Forfaits unitaires',
        'DomainUnits'=>'Unités de domaine',
        'UnitsHistory'=>"Historique des unités",
        'Units'=>'Unités',
        'Statistics'=>'Statistics',
        'Qrcode'=>'QRCodes',
		'Admin'=>'Admin'
    ],

];

