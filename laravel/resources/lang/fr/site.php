<?php


return [

    'label' => [

        'company' => 'Compagnie',
        'name' => 'Nom',
        'address' => 'Adresse',
        'city' => 'Ville',
        'postcode' => 'Code postal',
        'country' => 'Pays',
        'phone_number_1' => 'Numéro de téléphone 1',
        'phone_number_2' => 'Numéro de téléphone 2',
        'phone_type_1' => 'Type de téléphone 1',
        'phone_type_2' => 'Type de téléphone 2',
        'site_type' => 'Type de site',
        'logo' => 'Logo',
        'change_logo' => 'Changer le logo',
        'access_conditions' => 'Conditions d\'accès',
        'default_email' => 'Email par défaut',
        'guard_presence' => 'Présence de garde',
        'digicode' => 'Digicode',
        'keybox_presence' => 'Présence Keybox',
        'keybox_code' => 'Code de la Keybox',
        'keybox_place' => 'Place de la Keybox',
        'keybox_issue' => 'Question Keybox',
        'known_issue' => 'Problème connu',
        'instructions_tasks_file' => 'Fichier d\'instructions des tâches',
        'update_instructions_tasks_file' => 'Mettre à jour le fichier d\'instructions des tâches',
        'order_file' => 'Fichier de Consignes',
        'update_order_file' => 'Mettre à jour le fichier de consignes',
        'procedures_file' => 'Fichier de procédures',
        'update_procedures_file' => 'Mettre à jour le fichier de procédures',
        'sitemaps_file' => 'Plans du site',
        'update_sitemaps_file' => 'Mettre à jour le plan du site',
        'active' => 'actif',
        'show_site' => 'Afficher le site',
        'edit_site' => 'Modifier le site',
        'create_site' => 'Créer un site',
        'site' => 'Site',
        'site_id' => 'ID du site',
        'sites' => 'Des sites',
        'service' => 'Un service',

    ]
];