<?php

return [
    'label' => [
        'site' => 'Site',
        'name' => 'Nom',
        'address' => 'Adresse',
        'active' => 'actif',
        'buildings' => 'Bâtiments',
        'building' => 'Bâtiment',
        'show_building' => 'Afficher le bâtiment',
        'edit_building' => 'Modifier le bâtiment',
        'create_building' => 'Créer un bâtiment',
    ]
];