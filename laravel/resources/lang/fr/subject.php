<?php


return [

    'label' => [
        'subjects' => 'Sujets',
        'subject' => 'Sujet',
        'type' => 'Type',
        'services' => 'Services',
        'service' => 'Service',
        'subject_hrs' => 'Temps (heures)',
        'time_to_resolve' => 'Temps de résolution (heures)',
        'show_subject' => 'Afficher le sujet',
        'edit_subject' => 'Modifier le sujet',
        'create_subject' => 'Créer un sujet',
        'caller_list' => 'Cascade d\'appel',
        'available' => 'Disponible',
        'unavailable' => 'Indisponible',
        'instruction' => 'Glissez et déposez pour définir la priorité.',
		'geolocation_id'=>'Geolocation Id',
        'is_geolocation'=>'Enable Geolocation',
		'instruction_file'=>"Fichier d'instruction",

    ],
    'subject_type'=>[
        'ticket' => 'Ticket',
        'alarm' => 'Alarme',
        'event' => 'Evénement',
        'planned_activity' => 'Tâche planifiée',
        'duty'=>"Prise de Services",
    ],
    'subject_planned_activity_time'=>[
        'daily' => 'Quotidien',
        'weekly' => 'Hebdomadaire',
        'monthly' => 'Mensuel',
        'monthly_3' => 'Trimestriel',
        'monthly_6' => 'Semestriel',
        'yearly' => 'Annuel',
    ]

];