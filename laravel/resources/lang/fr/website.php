<?php
return [


    'label' => [

        'website' => 'Sous-domaine',
        'websites' => 'Sous-domaines',
        'create_website' => 'Créer un sous-domaine',
        'show_website' => 'Afficher le sous-domaine',
        'edit_website' => 'Modifier le sous-domaine',
        'domain' => 'Sous-domaine',
        'master' => 'Maître',
        'enable_charge'=>"Activer les frais",
        'enable_qrcode'=>"Activer le QRCode",
        'domain_name_not_valid'=>"Le format du nom de domaine n'est pas valide",

    ]

];