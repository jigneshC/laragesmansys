<?php

return [
	'responce_msg' => [
		'success_default' => 'Action réussie!',
        'success_login' => 'Connexion réussie.',
        'success_logout' => 'Vous vous êtes déconnecté avec succès!',
        'success_forgot_password_send_to_email' => 'Un mot de passe unique (otp) pour réinitialiser votre mot de passe a été envoyé à votre adresse email enregistrée.',
        'success_otp_to_login_sent_on_email' => 'Un mot de passe (otp) pour vous connecter a été envoyé à votre adresse e-mail enregistrée.',
        'success_otp_to_login_sent_on_phone' => 'Un mot de passe unique (otp) pour vous connecter a été envoyé à votre numéro de téléphone enregistré.',
		'success_otp_to_login_sent_on_phone_email' => 'Un mot de passe unique (otp) pour vous connecter a été envoyé à votre numéro de téléphone et à votre adresse e-mail enregistrés.',
        'success_new_password_generated' => 'Un nouveau mot de passe a été généré! et envoyé à votre adresse e-mail enregistrée.',
        'success_forgot_password_send_to_phone_no' => 'Un mot de passe unique a été envoyé sur votre numéro de téléphone.',
        'success_password_changed' => 'Succès! Votre mot de passe a été changé!',
        'success_register' => "Enregistrement d'utilisateur avec succès.",
        'success_profile_updated' => 'Mise à jour du profil réussie.',
        
		
        'warning_user_data_not_found' => "Données d'utilisateur non trouvées.",
        'warning_no_data_found' => 'Aucun Enregistrement Trouvé.',
        'warning_your_account_not_activated_yet' => "Votre compte n'a pas encore été activé.",
        'warning_incorrect_email_or_password' => 'email ou mot de passe invalide',
        'warning_require_unique_phone_number' => 'Le numéro de téléphone existe déjà avec un autre compte. Essayez avec un autre numéro',
        'warning_require_unique_user_name' => "Ce nom d'utilisateur existe déjà. S'il vous plaît, essayez-en un autre.",
        'warning_current_password_incorrect' => 'Votre mot de passe actuel ne correspond pas à nos enregistrements.',
        'warning_invalid_qrcode' => "Qr-code n'est pas valide!",
        'warning_user_have_not_valid_phone_no' => "L'utilisateur n'a pas de numéro de téléphone à envoyer Un mot de passe à usage unique (otp)",
        'warning_otp_token_expired' => 'Le jeton Otp a expiré!',
        'warning_otp_token_is_not_valid' => "Le jeton Otp n'est pas valide!",
		'warning_you_have_no_permission_to_access_this_site' => "Vous n'avez pas la permission d'accéder à ce site!",
		'warning_your_account_not_activated_yet_or_disabled' => "Votre compte est soit désactivé, soit sans autorisation d'accès. S'il vous plaît contacter admin pour plus d'informations",
		
		
        'error_default'=>"Quelque chose s'est mal passé!",
		
		
	],
];