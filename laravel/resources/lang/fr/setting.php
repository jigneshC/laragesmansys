<?php
return [


    'label' => [

        'setting' => 'Réglage',
        'settings' => 'Paramètres',
        'show_setting' => 'Afficher le paramètre',
        'edit_setting' => 'Modifier le paramètre',
        'create_setting' => 'Créer un paramètre',
        'key' => 'Clé',
        'value' => 'Valeur',
        'type'=>'Type',

    ]

];