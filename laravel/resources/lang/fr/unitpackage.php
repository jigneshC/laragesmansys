<?php

return [
    'label' => [
        'unitpackages'=>'Forfaits unitaires',
        'unitpackage'=>'Unité du Forfait',
        'package'=>'Forfait',
        'unit'=>'Unité',
        'name'=>'Nom du Forfait',
        'desc'=>'Description',
        'price'=>'Prix',
        'price_currency'=>'Devise du prix',
        'create_a_package'=>'Créer un Forfait',
        'update_a_package'=>'Mettre à jour un Forfait',
        'package_class'=>'Classe du Forfait',
        'package_code'=>"Code du Forfait",
       
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        'no_packages_available_for_purchase_an_units'=>"Désolé, aucun Forfait disponible, s'il vous plaît essayer après un certain temps",
      
    ],
    'js_msg' =>[
        
    ],
];