<?php

return [
    'label' => [
        'Overall_Ticket_Report' => 'Rapport global des tickets',
        'Tickets_Report'=>"Rapport des tickets",
        'Tickets' => 'Tickets',
        'planned_activity' => 'Activité planifiée',
        'alarm' => 'Alarme',
        'event' => 'Evénement',
        'Top Twenty' => 'Top Vingt',
        'subjects'=>'Sujets',
        'services'=>'Service',
        'sites'=>'Sites',
        'companies'=>'Companies',
        'create_image'=>"Create Report Image",
        'duration'=>"Durée"
    ],

];