<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label'=>[
        'mark_ticket_as' => 'Marquez le ticket comme ...',
        'mark_as' => 'Marquer comme',
        'evidence' => 'Preuve',
        'note' => 'Remarque',
        'place_a_comment' => 'Placez un commentaire',
        'id' => 'Id',
        'name' => 'prénom',
        'title' => 'Titre',
        'subject' => 'Sujet',
        'content' => 'Contenu',
        'status' => 'Statut',
        'site' => 'Site',
        'equipment_id' => 'Equipement',
        'website' => 'Sous-domaine',
        'create_ticket' => 'Créer un ticket',
        'edit_ticket' => 'Modifier le ticket',
        'added_a_file' => 'Ajout d\'un fichier',
        'show_ticket' => 'Afficher le ticket',
        'add_your_comment_placeorder' => 'Tapez votre commentaire ici ....',
        'comments' => 'commentaires',
        'assign' => 'Attribuer',
        'assign_to' => 'Affecter à..',
        'select_user' => 'Sélectionner un utilisateur',
        'assign_ticket_to' => 'Affecter un ticket à',
        'add_an_evidence' => 'Ajouter une preuve',
        'advance_search' => 'Recherche avancée',
        'all_notification' => 'Toute notification',
        'ticket' => 'Ticket',
        'planned_start_date'=>'Date de début',
        'planned_end_date'=>'Date de fin',
        'action_log'=>'Action Logs',
        'view_before_date'=>'Rappel (h)',
        'activity_date'=>"Jour ou Date d'activité",
        'planned_activity'=>"Tâches Planifiées",
        'activity_time'=>'Temps d\'activité',
        'duration'=>"Durée",
        'edit_planned_activity'=>'Editer l\'activité planifiée',
        'create_planned_activity' => 'Créer une activité planifiée',
        'show_planned_activity'=>"Montrer l\'activité planifiée",
        'planned_activity_detail'=>"Details de l\'activité planifiée",
		'actioner'=>"Acteur",
        'action'=>"action",
        'detail'=>"Détail",
        'all'=>"Tout",
		'comment'=>'Commentaire',
    ],
	'history_type'=>[
		'action'=>'Statut',
		'assign'=>'Attribuer',
		'file'=>'Document',
		'comment'=>'Commentaire',
	],
	
    'form'=>[
        'activity_option' => 'Option d\'activité',
        'planned_activity_range'=>"Période",
        'activity_op_yearly_select_'=>"Sélectionnez le jour et le mois de l'année",
        'activity_op_monthly_select_'=>"Sélectionnez le jour du mois",
        'activity_op_weekly_select_'=>"Sélectionnez le jour de la semaine"
    ],
    'notification'=>[
		'ticket_created_success' =>"Nouveau ticket a été créé!",
        'assign_user_success' => 'Ticket attribué à :user_name',
        'actioner_has_assing_you_ticket' => ':actioner_name vous a attribué un ticket',
        'ticket_closed' => 'Le ticket a été fermé',
        'no_permission_to_access' => 'Vous n\'avez pas assez de permission pour prendre cette action',
        'actioner_has_comment_on_ticket'=>":actioner_name a ajouté un nouveau commentaire sur ticket",
        'actioner_has_add_evidence_on_ticket'=>":actioner_name a ajouté une preuve dans le ticket",
        'actioner_has_change_ticket_status_to'=>":actioner_name  a changé le statut du ticket à :new_status",
		'actioner_has_assing_ticket_to_user'=>":actioner_name a attribué un billet à :assine_name",
		'actioner_has_created_tikcet'=>":actioner_name a créé un billet",
		'actioner_has_created_call'=>":actioner_name a créé l'appel",
		
		'error_tikcet_not_found'=>"Données de ticket non trouvées!",
		
		'sucess_comment_added'=>'Vous avez posté un commentaire avec succès!',
		'sucess_file_added'=>'Vous avez ajouté le fichier avec succès!',
		'sucess_calllog_added'=>'Journal des appels créé avec succès!',
    ]
];
