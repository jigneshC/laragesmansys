<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status_dropdown' => [
        'new' => 'Nouveau',
        'open' => 'Ouvert',
        'pending' => 'En attente',
        'on-hold' => 'En suspens',
        'solved' => 'Résolu',
        'closed' => 'Clos',
    ],
    'label'=>[
        'dashboard' => 'Tableau de bord',
        'all_tickets' => 'Tous les tickets',
    ],
];
