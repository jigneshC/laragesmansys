<?php
return [


    'label' => [

        'site' => 'Site',
        'name' => 'Nom',
        'description' => 'Description',
        'manager' => 'Chef du Service',
        'active' => 'actif',
        'show_service' => 'Afficher le service',
        'edit_service' => 'Modifier le service',
        'create_service' => 'Créer un service',
        'service' => 'Service',
        'servicies'=>'Services',
        'people' => 'Persones',
        'yes' => 'OUI',
        'no' => 'NON',

    ]

];