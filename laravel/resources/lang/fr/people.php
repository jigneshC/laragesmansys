<?php

return [
    'label' => [
        'user' => 'Utilisateur',
        'services' => 'Services',
        'title' => 'Titre',
        'first_name' => 'Prénom',
        'last_name' => 'Nom de famille',
        'quality' => 'Qualité',
        'phone_number_1' => 'Numéro de téléphone 1',
        'phone_number_2' => 'Numéro de téléphone 2',
		'contact_no' => 'Numéro de contact',
        'phone_type_1' => 'Type de téléphone 1',
        'phone_type_2' => 'Type de téléphone 2',
        'emergancy_password' => 'Mot de passe d\'urgence',
        'availability' => 'Disponibilité',
        'hold_key' => 'Détient les clés',
        'change_photo' => 'Changer la photo',
        'photo' => 'Photo',
        'email' => 'Email',
        'my_profile' => 'Mon profil',
        'edit_profile' => 'Editer le profil',
        'update_profile' => 'Mettre à jour le profil',
        'change_password' => 'Changer le mot de passe',
        'current_password' => 'Mot de passe actuel',
        'password_confirmation' => 'Confirmation mot de passe',
        'password' => 'Mot de passe',
        'language' => 'Langue',
        'joined' => 'A rejoint le système',
        'person_details' => 'Détails de la personne',
        'show_people' => 'Afficher les personnes',
        'people' => 'Personne',
        'peoples' => 'Personnes',
        'edit_peoples' => 'Modifier les Personnes',
        'create_peoples' => 'Créer une personne',
        'active' => 'actif',
        'timezone' => 'Timezone'
    ],
    'tool_tip'=>[
        'input_availability' => 'Contrôle si disponible?',
    ]

];