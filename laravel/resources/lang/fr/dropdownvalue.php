<?php
return [


    'label' => [

        'dropdown_value' => 'Valeur de liste déroulante',
        'dropdown_values' => 'Valeur de liste déroulante',
        'show_dropdown_value' => 'Afficher la valeur de liste déroulante',
        'edit_dropdown_value' => 'Modifier la valeur de la liste déroulante',
        'create_dropdown_value' => 'Créer une valeur de liste déroulante',
        'Add_Languages_in_DropDown_Value' => 'Ajouter des langues dans la valeur de liste déroulante',
        'add_languages' => 'Ajouter des langues',
        'type_id' => 'Identifiant de type',


    ]

];