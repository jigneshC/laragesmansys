<?php

return [


    'label'=>[
        'thank_you_for_using_our_application'=>"Merci d'utiliser notre application!",
        'view_assign_task'=>"Afficher la tâche assignée",
        'view_detail'=>"Voir les détails",
        'ticket_has_been_marked_as'=>"le ticket a été marqué comme ':status'",
        'tickets_acknowledged_text'=>"Cliquez ici pour envoyer votre accusé de réception",
		'tickets_acknowledged_comment'=>"Email a été reconnu",
    ],
	'subject'=>[
		'login_otp'=>"Gesmansys Login Un mot de passe (Otp)",
		'reset_password'=>'Mot de passe oublié!',
	],
	'slug'=>[
		'you_have_request_to_forgot_password'=>"Vous avez récemment demandé à réinitialiser le mot de passe du compte :app .",	
	'your_temp_password_to_reset_password_is'=>"Vous pouvez vous connecter avec un nouveau mot de passe :password. Après la connexion, vous pouvez changer / réinitialiser le mot de passe comme vous le souhaitez.",	
		'your_otp_to_reset_password_is'=>'Votre mot de passe à usage unique pour réinitialiser votre mot de passe est',	
		'if_you_did_not_request'=>"Si vous ne l'avez pas demandé, ignorez ce courrier ou laissez-nous savoir",	
		'otp_will_be_expire_within'=>"Et expirera dans :minuts minuts.",	
	],
    

];
