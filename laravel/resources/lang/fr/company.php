<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    'label' => [

        'name' => 'Nom',
        'address' => 'Adresse',
        'city' => 'Ville',
        'postcode' => 'Code Postal',
        'country' => 'Pays',
        'phone_number_1' => 'Numéro de Téléphone 1',
        'phone_number_2' => 'Numéro de Téléphone 2',
        'phone_type_1' => 'Type de Téléphone 1',
        'phone_type_2' => 'Type de Téléphone 2',
        'business_type' => 'Genre d\'entreprise',
        'logo' => 'Logo',
        'change_logo' => 'Changer le Logo',
        'reference' => 'Référence',
        'active' => 'Actif',
        'company' => 'Entreprise',
        'companies' => 'Entreprises',
        'show_company'=>'Voir l\'entreprise',
        'edit_company'=>'Editer l\'entreprise',
        'create_company'=>'Créer une entreprise',
        'Assign_Company_Modules'=>"Attribuer des sociétés-modules",
        'Assigned_Modules'=>"Modules assignés",
        'Non_Assigned_Modules'=>"Modules non assignés",
        'to_process_company_module_plaease_complete_company_form_first'=>"Pour traiter le module de l'entreprise, remplissez et complétez le formulaire de l'entreprise en premier",
		'move_user_to_domain'=>"Déplacer l'utilisateur du domaine :olddomain à :newdomain",
        'already_used_domain_user_as'=>"Déjà utilisé comme appelant / gestionnaire dans [:olddomain]",
        'other_domain_user'=>"Autres utilisateurs dans [:olddomain]",
        'move_selected_users_to_new_domain'=>"Déplacer les utilisateurs sélectionnés vers un nouveau sous-domaine",
    ]
  

];
