<?php

return [
    'label' => [
        'moduleunit'=>'Module et unité',
        'action'=>"Nom de l'action",
        'unit'=>'Unité',
        'units'=>'Unité',
        'units_day'=>"Unités / Jour",
        'creadit_unit'=>"Créditer les unités",
        'unit_charge'=>'Unités chargées',
        'unit_charge_per_day'=>"Unités chargées par jour",
        'unit_charge_per_month'=>"Unités chargées par mois",
        'chargable_module'=>"Module rechargeable",
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        'moduel_unit_add_success'=>'Module Unit succès ajouté',
        'moduel_unit_updated_success'=>'Unité Module Mise à jour réussie',
        'unit_credited_to_website_success'=>'Unités du sous-domaine créditées avec succès',
        'you_have_not_enough_credit'=>"Vous n'avez pas assez de crédits / unités pour effectuer cette action !",
        'click_to_check_packages'=>'Vérifier les unités disponibles',
    ],
    'js_msg' =>[
        
    ],
    'unit_charge_lable' =>[
        'license' => 'licence (1 client, 1 site, 1 bâtiment, 1 service, 10 sujets)',
        'users' => 'Elément utilisateur',
        'users_module' => 'Module utilisateur',
        'companies' => 'Entreprises',
        'companies_module' => 'Module d\'entreprises',
        'sites' => 'Sites',
        'sites_module' => 'Module de site',
        'services' => 'Services',
        'services_module' => 'Module de services',
        'audits' => 'Audit',
        'audits_module' => 'Module d\'audit',
        'buildings' => 'Bâtiments',
        'buildings_module' => 'Module de Bâtiments',
        'subjects' => 'Sujets',
        'subjects_module' => 'Module sujets',
        'tickets' => 'Tickets',
        'tickets_module' => 'Module de ticket',
        'duty_module' => 'Module Prise de service',
        'plannedactivity_module' => 'Module Tâches plannifiées',
    ]
];