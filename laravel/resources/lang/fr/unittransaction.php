<?php

return [
    'label' => [
        'unittransaction'=>"Transaction d'unité",
        'unittransactions'=>'Transactions unitaires',
        'unit_type'=>'Type de transactio',
        'reference'=>'Référence',
        'comment'=>'Commentaire',
        'available_units' => 'Unités disponibles',
        'get_more_units' => 'Obtenez plus d\'unités',
        'order_list' => 'Mes commandes',
        'transaction_detail' => 'Détail de la transaction',
        'transaction_id' => 'Identifiant de transaction',
        'domain_id' => 'Identifiant du domaine',
        'transaction_date' => 'Rendez-vous amoureux',
        'module_items' => 'Module / Articles',
        'unit_charge_per_day' => 'Charge unitaire / jour',
        'free_items' => 'Articles gratuits',
        'total_items' => 'Articles au total',
        'total_charge' => 'Charge totale',
        'net_chargable_units_day' => 'Unités Net Chargables / Jour',
        'credited_unit' => 'Unités créditées',
        'debited_unit' => 'Unités débitées',
        'in_day'=>"dans :day jours",
        'today'=>"Aujourd'hui",
        
       
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
      
    ],
    'js_msg' =>[
        
    ],
    'notification'=>[
        'unit_is_low'=>"Votre Domaine a des unités très faibles (crédit) :unit . S'il vous plaît acheter plus d'unités pour continuer vos services dans ce domaine.",
        'unit_will_end_in_check_more_plan'=>"Le domaine a des unités très faibles (crédit), il devrait se terminer :in_number_days .",
        'domin_will_be_charge_per_day'=>"Le domaine sera facturé :units unités / jour pour cet article",
        'domin_charge_per_day'=>"Le domaine sera facturé :units unités / jour.",
        'allow_free_tiems'=>" (* :total_free gratuit :module_name)"
    ]
];