<?php

return [
    'label' => [
        'order_now'=>'Commandez maintenant',
        'confirm_order'=>"Confirmer la commande",
        'confirm_and_pay_amount'=>"Confirmez et payez :amount",
        'invoice'=>"Facture d'achat",
        'to'=>'À',
        'orderstatus'=>'Statut',
        'payment_status'=>'Statut de paiement',
        'reference_id  '=>'Identifiant de référence de paiement',
        'payment_type'=>'Type de paiement',
        'item'=>"Nom de l'article",
        'item_qty'=>'Qté',
        'price_total'=>'Prix total',
        'discount'=>'Remise',
        'net_payable_amount'=>'Montant net payable',
        'paidamount'=>"montant payé",
        'order'=>'Commande',
        'view_order'=>"Voir l'ordre",
        'buyer_detail'=>"Détail de l'acheteur",
        'buyer_name'=>"Nom de l'acheteur",
        'buyer_contact'=>"Contact de l'acheteur",
        'buyer_email'=>"Email de l'acheteur",
        'payment_detail'=>"Détail du paiement",
        'invoicedate'=>"Date de facturation",
        'reference_id'=>"Pièce d'identité",
       
    ],
    'data'=>[
        
    ],
    'responce_msg' =>[
        'order_place_successfully'=>'Commande placée avec succès !',
        'order_canceled'=>'Commande annulée !!',
        'no_order_found'=>"Il n'y a pas d'ordre dans le sous-domaine",
      
    ],
    'js_msg' =>[
        
    ],
    'mail_admin' =>[
        'order_place_success_subject'=>"Félicitations pour votre commande !",
        'order_place_success_headerline1'=>'La commande suivante a été payée:',

        'order_place_failed_subject'=>'Échec de la commande !',
        'order_place_failed_headerline1'=>"S'il vous plaît vérifier la raison et le suivi.",


        'order_place_failed_subject'=>'Commande annulée !',
        'order_place_failed_headerline1'=>'La commande suivante a été annulée:'
    ],
    'mail_user' =>[
        'order_place_success_subject'=>'Félicitation pour votre commande !',
        'order_place_success_headerline1'=>'Vous avez passé la commande avec succès, voici le détail de la commande:',

        'order_place_failed_subject'=>'Échec de la commande !',
        'order_place_failed_headerline1'=>"S'il vous plaît vérifier la raison et le suivi.",


        'order_place_failed_subject'=>'Commande annulée !',
        'order_place_failed_headerline1'=>'La commande suivante a été annulée:'
    ]
];