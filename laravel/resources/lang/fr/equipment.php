<?php
return [


    'label' => [

        'equipment' => 'Équipement',
        'equipment_id' => 'Identifiant d\'équipement',
        'website_id' => 'Identifiant du site',
        'equipments' => 'Équipements',
        'show_equipment' => 'Afficher l\'équipement',
        'edit_equipment' => 'Modifier l\'équipement',
        'create_equipment' => 'Créer un équipement',


    ]

];