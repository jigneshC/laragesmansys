<?php
return [


    'label' => [

        'dropdowns_type' => 'Type de liste déroulante',
        'dropdowns_types' => 'Types de liste déroulante',
        'show_dropdowns_types' => 'Afficher le type de liste déroulante',
        'edit_dropdowns_types' => 'Modifier le type de liste déroulante',
        'create_dropdowns_types' => 'Créer un type de liste déroulante',


    ]

];