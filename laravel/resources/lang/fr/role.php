<?php
return [


    'label' => [

        'role' => 'Rôle',
        'roles' => 'Les rôles',
        'show_role' => 'Afficher les rôles',
        'edit_role' => 'Modifier les rôles',
        'create_role' => 'Créer un rôle',


    ]

];