<?php

return [
    'label' =>
      array (
          'duty' => 'Prise de Service',
          'add_subject_title' => 'Ajouter un sujet pour la Prise de Service',
          'select_subject' => 'Sélectionnez le sujet',
          'close' => 'Fermer',
      ),
    'data' =>
        array (
            'default_title' => 'Prise de Service',
            'default_content' => 'Prise de Service',
        ),
    'responce_msg' =>
        array (
            'something_went_wrong' => 'Quelque chose c\'est mal passé. Merci d\'essayer plus tard.',
            'subject_add_success' => 'Sujet ajouté avec succès',
            'subject_update_success' => 'Sujet mis à jour',
            'no_duty_subject_for_given_company'=>"Aucun sujet trouvé pour une entreprise donnée , s'il vous plaît créer un nouveau sujet avec le type Prise de Service"
        ),
    'js_msg' =>
        array (
            'confirm_for_close_ticket' => 'Êtes-vous sûr de fermer le Ticket?',
        ),
];

