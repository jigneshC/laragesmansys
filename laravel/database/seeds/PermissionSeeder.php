<?php

use Illuminate\Database\Seeder;

use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected function insertAndReturnId($string, $parent_id = 0)
    {
        $a = explode('|', $string);

        $ins = [
            'name' => trim($a[0]),
            'label' => trim($a[1]),
            'parent_id' => $parent_id
        ];

        $permission = Permission::create($ins);

        return $permission->id;
    }

    public function run()
    {


        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Permission::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

//manage.users
//manage.roles

//manage.permissions
//user.edit
//manage.generator
//manage.companies

//manage.sites
//manage.services
//manage.peoples

//manage.
//manage.dropdownsvalues
//manage.audit

        $arr = [
            'access.users|Can Access Users' =>
                [
                    'access.user.create|Can Create User',
                    'access.user.edit|Can Edit User',
                    'access.user.delete| Can Delete User',
                    'access.user.all| Can Access All User (Or will access only own)',
                    'access.user.changes.all| Can Access And Update All User  (Or will access only own)'
                ],


            'access.roles|Can Access Roles' =>
                [
                    'access.role.create|Can Create Role',
                    'access.role.edit|Can Edit Role',
                    'access.role.delete| Can Delete Role'
                ],

            'access.permissions|Can Access Permission' =>
                [
                    'access.permission.create|Can Create Permission',
                    'access.permission.edit|Can Edit Permission',
                    'access.permission.delete| Can Delete Permission'
                ],


            'access.websites|Can Access Websites' =>
                [
                    'access.website.create|Can Create Website',
                    'access.website.edit|Can Edit Website',
                    'access.website.delete| Can Delete Website'
                ],

            'access.companies|Can Access Companies' =>
                [
                    'access.company.create|Can Create Company',
                    'access.company.edit|Can Edit Company',
                    'access.company.delete| Can Delete Company',
                    'access.company.all| Can Access All Company (Or will access only own)',
                    'access.company.changes.all | Can Access Changes in All Company (Or will access only own)',
                    'access.componiesmodule | Can access Componies Module Enable/Disable ',
                ],

            'access.sites|Can Access Sites' =>
                [
                    'access.site.create|Can Create Site',
                    'access.site.edit|Can Edit Site',
                    'access.site.delete| Can Delete Site',
                    'access.site.all| Can Access All Site (Or will access only own)',
                    'access.site.changes.all| Can Access Changes in All Site (Or will access only own)',
                ],


            'access.services|Can Access Services' =>
                [
                    'access.service.create|Can Create Service',
                    'access.service.edit|Can Edit Service',
                    'access.service.delete| Can Delete Service',
                    'access.service.all| Can Access All Service (Or will access only own)',
                    'access.service.changes.all| Can Access Changes in All Service (Or will access only own)',
                ],


            'access.peoples|Can Access Peoples' =>
                [
                    'access.people.create|Can Create People',
                    'access.people.edit|Can Edit People',
                    'access.people.delete| Can Delete People',
                    'access.people.all| Can Access All People (Or will access only own)',
                    'access.people.changes.all| Can Access Changes in All People (Or will access only own)',
                ],

            'access.dropdowns|Can Access Dropdowns' =>
                [
                    'access.dropdown.create|Can Create Dropdown',
                    'access.dropdown.edit|Can Edit Dropdown',
                    'access.dropdown.delete| Can Delete Dropdown'
                ],

            'access.dropdownsvalues|Can Access Dropdown Values' =>
                [
                    'access.dropdownsvalue.create|Can Create Dropdown Value',
                    'access.dropdownsvalue.edit|Can Edit Dropdown Value',
                    'access.dropdownsvalue.delete| Can Delete Dropdown Value'
                ],


            'access.audits|Can Access Audits' =>
                [
                    'access.audit.create|Can Create Audit',
                    'access.audit.edit|Can Edit Audit',
                    'access.audit.delete| Can Delete Audit',
                    'access.audit.all| Can Access All Audit (Or will access only own)',
                    'access.audit.changes.all| Can Access Changes in Audit  (Or will access only own)',
                ],


            'access.settings|Can Access Settings' =>
                [
                    'access.setting.create|Can Create Setting',
                    'access.setting.edit|Can Edit Setting',
                    'access.setting.delete| Can Delete Setting'
                ],

            'access.buildings|Can Access Buildings' =>
                [
                    'access.building.create|Can Create Building',
                    'access.building.edit|Can Edit Building',
                    'access.building.delete| Can Delete Building',
                    'access.building.all| Can Access All Building (Or will access only own)',
                    'access.building.changes.all| Can Access Changes in All building (Or will access only own)',
                ],
            'access.equipments|Can Access Equipments' =>
                [
                    'access.equipment.create|Can Create Equipment',
                    'access.equipment.edit|Can Edit Equipment',
                    'access.equipment.delete| Can Delete Equipment'
                ],
            'access.languages|Can Access Languages' =>
                [
                    'access.language.create|Can Create Language',
                    'access.language.edit|Can Edit Language',
                    'access.language.delete| Can Delete Language'
                ],
            'access.subjects|Can Access Subjects' =>
                [
                    'access.subject.create|Can Create Subject',
                    'access.subject.edit|Can Edit Subject',
                    'access.subject.delete| Can Delete Subject',
                    'access.subject.all| Can Access All Subject (Or will access only own)',
                    'access.subject.changes.all| Can Access Changes in All subject (Or will access only own)',
                ],

            'access.tickets|Can Access Tickets' =>
                [
                    'access.ticket.create|Can Create Ticket',
                    'access.ticket.edit|Can Edit Ticket',
                    'access.ticket.delete| Can Delete Ticket',
                    'access.ticket.all| Can Access All Ticket (Or will access only own)',
                    'access.ticket.changes.all| Can Access Changes in All Ticket (Or will access only own)',
                    'access.ticket.action| Can Action on Ticket (Make it close , open ,on-hold etc)',
                    'access.ticket.comment.all| Can Access All Ticket Comment(Or will access only own)',
                    'access.ticket.changes.comment.all| Can Access Changes in All Ticket Comment(Or will access only own)',
                ],
                'access.moduleunits|Can Access Module And Units' =>
                [
                    'access.moduleunits.create|Can Create Module And Units',
                    'access.moduleunits.edit|Can Edit Module And Units',
                    'access.moduleunits.delete| Can Delete Module And Units'
                ],
                'access.unitpackages|Can Access Packages ' => [
                    'access.canaddcredit|Can Credit Domain units'
                ],
                'access.domainunits|Can Access Domain units' => [],
                
                'access.api|Can Access Api' => []
            ,


        ];


//        \DB::table('permissions')->delete();


        foreach ($arr as $k => $v) {


            $id = $this->insertAndReturnId($k);


            foreach ($v as $string) {
                $this->insertAndReturnId($string, $id);

            }


        }


    }
}
