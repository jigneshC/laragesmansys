<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReffeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refe_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('refe_table_field_name', 55);
            $table->integer('ref_field_id')->default(0);
            $table->string('image', 150)->nullable();
            $table->string('image_ref_code', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refe_images');
    }
}
