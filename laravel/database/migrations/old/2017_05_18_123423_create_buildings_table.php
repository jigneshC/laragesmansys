<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->integer('_website_id')->default(0);

            $table->string('name');
            $table->text('address')->nullable();
            $table->string('site_id');
            $table->boolean('active')->default(false);

            $table->integer('created_by');
            $table->integer('updated_by');
			
			$table->text('geolocation')->nullable()->default(null);

            $table->string('longitude')->nullable()->default(0);
            $table->string('latitude')->nullable()->default(0);
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('buildings');
    }
}
