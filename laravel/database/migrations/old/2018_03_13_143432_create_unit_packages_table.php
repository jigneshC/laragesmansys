<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('desc')->nullable()->default(null);
            $table->float('price')->default(0);
            $table->enum('price_currency', ['USD','EURO'])->default('EURO');
            $table->float('unit')->nullable()->default(0)->comment("Unit Total");
            $table->string('package_class')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_packages');
    }
}
