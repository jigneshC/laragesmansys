<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_transaction', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('website_id');
			$table->enum('unit_type', ['credit', 'debit'])->default('credit');
			$table->float('unit')->default(0)->comment("Unit amount");
			$table->enum('reference', ['self_debit','admin_credit','paypal_credit','daily_charge']);
			$table->string('comment')->nullable();
			$table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0)->nullable();
            $table->longText('itemsobj')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_transaction');
    }
}
