<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_websites', function (Blueprint $table) {
            $table->increments('id');
            $table->softDeletes();
            $table->string('domain');
            $table->longText('description')->nullable();
            $table->string('name');
            $table->boolean('master')->default(false);
            $table->tinyInteger('enable_charge')->nullable()->default(0);
            $table->tinyInteger('enable_qrcode')->nullable()->default(0);
            $table->text('qrcode_apitoken')->nullable();
            $table->enum('status', ['active', 'inactive']);
            $table->float('units')->nullable()->default(0)->comment("Wallet available units");
//            $table->integer('created_by');
//            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('_websites');
    }
}
