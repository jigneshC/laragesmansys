<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('subject_type')->nullable()->default("");
            $table->integer('service_id');
            $table->string('time')->nullable();
            $table->dateTime('action_date')->nullable();
            $table->integer('action_id')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0);
			
			$table->integer('geolocation_id')->nullable()->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
    }
}
