<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('subject_id');
            $table->integer('equipment_id')->nullable();
            $table->integer('user_id');
            $table->longText('content');
            $table->enum('status', [
                'new',
                'open',
                'pending',
                'on-hold',
                'solved',
                'closed'
            ]);
            $table->integer('_website_id')->default(0);

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();
            $table->timestamps();

            $table->integer('view_on_date')->nullable()->default(0)->comment("Flag 0 or 1");
            $table->string('view_on_date_val')->nullable()->default(null)->comment("Actual date or day  to display");
            $table->integer('view_before_date')->nullable()->default(null)->comment("Totoal hours befor actual date to display");

            $table->string('activity_time_opt')->nullable()->default(null)->comment("dayly , weekly , yearly , monthly...");
            $table->date('view_start_date')->nullable()->default(null);
            $table->date('view_end_date')->nullable()->default(null);
            $table->dateTime('display_from_time')->nullable()->default(null);
            $table->dateTime('display_end_time')->nullable()->default(null);
            $table->integer('master_ticket_id')->nullable()->default(0);



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }
}
