<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeoplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

//Table People
//ID -> int(10) Auto Numbered,
//UserID -> Integer (from Users) DEFAULT NULL,
//Title -> varchar(10) NULL,
//FirstName -> varchar(40) NOT NULL,
//LastName -> varchar(40) NOT NULL,
//Quality -> int(11) NULL,
//PhoneNumber1 -> varchar(15) NULL,
//PhoneNumber2 -> varchar(15) NULL,
//PhoneType1 -> int(10) NULL (Link to DropDown ID)
//PhoneType2 -> int(10) NULL (Link to DropDown ID)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//
//Table PeopleExtended
//ID -> int(10) Auto Numbered,
//PeopleID -> int(10) (Link to People),
//EmergencyPasswod -> varchar(50) NULL,
//Availability -> int(11) NULL,
//HoldKey -> int(11) NULL,
//Photo -> varchar(100) NULL,
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)


//thanks
    public function up()
    {
        Schema::create('peoples', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);//static 1

            $table->integer('user_id'); // static 0 for now
            $table->string('title', 10)->nullable();
            $table->string('first_name', 40);
            $table->string('last_name', 40);
            $table->string('quality',55)->nullable();
            $table->string('phone_number_1', 15)->nullable();
            $table->string('phone_number_2', 15)->nullable();

            $table->string('code_phone_number_1', 10)->nullable();
            $table->string('code_phone_number_2', 10)->nullable();

            $table->integer('phone_type_1')->nullable();
            $table->integer('phone_type_2')->nullable();
            $table->string('emergency_password')->nullable();
            $table->boolean('availability')->default(false)->nullable();
            $table->text('status_text')->nullable();
            $table->string('hold_key',55)->nullable();
            $table->string('photo', 100)->nullable();
            $table->boolean('active')->default(false);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();

            $table->timestamps();
        });


        //many to many relation using $this->services()->sync([ids])
        Schema::create('people_service', function (Blueprint $table) {
            $table->integer('people_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->timestamps();
//            $table->foreign('role_id')
//                ->references('id')
//                ->on('roles')
//                ->onDelete('cascade');
//
//            $table->foreign('user_id')
//                ->references('id')
//                ->on('users')
//                ->onDelete('cascade');

            $table->primary(['people_id', 'service_id']);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peoples');
        Schema::dropIfExists('people_service');
    }
}
