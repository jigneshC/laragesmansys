<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_package_id')->nullable()->default(0);
            $table->integer('website_id')->default(0)->nullable();

            $table->float('unit')->nullable()->default(0)->comment("Unit Total");

            $table->string('buyer_name')->nullable()->default(null);
            $table->string('buyer_contact')->nullable()->default(null);
            $table->string('buyer_email')->nullable()->default(null);
            


            $table->float('item_price')->nullable()->default(0);
            $table->enum('price_currency', ['USD'])->default('USD');
            $table->integer('item_qty')->nullable()->default(0);
            $table->float('discount')->nullable()->default(0);
            

            $table->float('order_total')->nullable()->default(0);

           
			
            $table->string('payment_request_id')->nullable()->default(null);
            $table->enum('payment_type', ['paypal'])->default('paypal');
            $table->enum('payment_status', ['pending','failed','cancelled','completed'])->default('pending');
           


            $table->longText('payment_object')->nullable()->default(null);
            $table->longText('unit_package')->nullable()->default(null);

			$table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_order');
    }
}
