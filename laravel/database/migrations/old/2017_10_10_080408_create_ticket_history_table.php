<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_history', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ticket_id')->unsigned();
            $table->integer('parent_id')->nullable()->default(0);

            

            $table->enum('history_type', [
                'comment',
                'file',
                'action',
                'assign'
            ]);
            $table->longText('desc')->nullable()->default(null);
            $table->longText('note')->nullable()->default(null);

			$table->string('longitude', 150)->nullable();
			$table->string('latitude', 150)->nullable();
			
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0);

            $table->softDeletes();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_history');
    }
}
