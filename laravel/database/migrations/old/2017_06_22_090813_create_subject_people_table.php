<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('subject_people', function (Blueprint $table) {
            //
            $table->integer('subject_id')->unsigned();
            $table->integer('people_id')->unsigned();

            $table->integer('priority')->default(0);
            
            $table->timestamps();

            $table->foreign('subject_id')
                ->references('id')
                ->on('subjects')
                ->onDelete('cascade');

            $table->foreign('people_id')
                ->references('id')
                ->on('peoples')
                ->onDelete('cascade');

            $table->primary(['subject_id', 'people_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subject_people');
    }
}
