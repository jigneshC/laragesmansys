<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_availability', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('people_id');
            $table->string('day', 10)->nullable();
            $table->string('time');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0);//static 1

            $table->softDeletes();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people_availability');

    }
}
