<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDropdownValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

//ID -> Integer Auto Numbered
//Name -> Varchar(50)
//Type -> Integer (link to DropdownsTypes)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)

    public function up()
    {
        Schema::create('dropdown_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);

            $table->string('name', 50);
            $table->integer('type_id');
            $table->boolean('active')->default(false);

            $table->integer('created_by');
            $table->integer('updated_by');

            $table->integer('parent_id')->default(0);

            $table->string('lang_code')->default('en');

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dropdown_values');
    }
}
