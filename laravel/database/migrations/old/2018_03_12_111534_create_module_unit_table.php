<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_module', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action')->nullable()->comment("Same name as permission");
            $table->float('unit')->nullable()->default(0)->comment("Unit cahrge");
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('_website_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_module');
    }
}
