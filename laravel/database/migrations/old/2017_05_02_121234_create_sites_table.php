<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
//
//TABLE Sites
//ID -> int(10) Auto Numbered,
//SiteName -> varchar(40),
//Address -> varchar(100) DEFAULT NULL,
//City -> varchar(40) NOT NULL,
//Postcode -> varchar(10) DEFAULT NULL,
//Country -> varchar(3) DEFAULT NULL (ISO Country Code)
//PhoneNumber1 -> varchar(15) DEFAULT NULL,
//PhoneNumber2 -> varchar(15) DEFAULT NULL,
//PhoneType1 -> int(10) DEFAULT NULL (Link to DropDown ID)
//PhoneType2 -> int(10) DEFAULT NULL (Link to DropDown ID)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//
//TABLE SitesExtended
//ID -> int(10) Auto Numbered,
//SiteID -> int(10) (Link to Sites),
//SiteType -> int(10) DEFAULT NULL (Link to DropDown ID)
//LogoFilename -> varchar(50) DEFAULT NULL,
//AccessConditions -> text (Unlimited)
//DefaultEmail -> varchar(50) DEFAULT NULL,
//GuardPresence -> tinyint(1) DEFAULT NULL,
//DigiCode -> varchar(10) DEFAULT NULL,
//KeyboxPresence -> tinyint(1) DEFAULT NULL,
//KeyboxCode -> varchar(10) DEFAULT NULL,
//KeyboxPlace -> text (Unlimited),
//KnownIssues -> text (Unlimited),
//CompanyID -> int(10) (Link to Company),
//InstructionsTasksFile -> varchar(100) DEFAULT NULL (Link to file)
//OrdersFile -> varchar(100) DEFAULT NULL (Link to file)
//ProceduresFile -> varchar(100) DEFAULT NULL (Link to file)
//SitemapsFile -> varchar(100) DEFAULT NULL (Link to file)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//
    public function up()
    {
        Schema::create('sites', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);

            $table->string('name',40);
            $table->string('address',100)->nullable();
            $table->string('city',40)->nullable();
            $table->string('postcode',10)->nullable();
            $table->string('country',3)->nullable();
            $table->string('phone_number_1',15)->nullable();
            $table->string('phone_number_2',15)->nullable();
            $table->string('code_phone_number_1', 10)->nullable();
            $table->string('code_phone_number_2', 10)->nullable();
            $table->integer('phone_type_1')->nullable();
            $table->integer('phone_type_2')->nullable();
            $table->integer('site_type')->nullable();
            $table->string('logo')->nullable();
            $table->text('access_conditions')->nullable();
            $table->string('default_email')->nullable();
            $table->tinyInteger('guard_presence')->nullable();
            $table->string('digicode')->nullable();
            $table->tinyInteger('keybox_presence')->nullable();
            $table->string('keybox_code')->nullable();
            $table->text('keybox_place')->nullable();
            $table->text('keybox_issue')->nullable();
            $table->text('known_issue')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('instructions_tasks_file',200)->nullable();
            $table->string('order_file',200)->nullable();
            $table->string('procedures_file',200)->nullable();
            $table->string('sitemaps_file',200)->nullable();
            $table->boolean('active')->default(false);
            $table->integer('created_by');
            $table->integer('updated_by');
			
			
			$table->text('geolocation')->nullable()->default(null);

            $table->string('longitude')->nullable()->default(0);
            $table->string('latitude')->nullable()->default(0);
			
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sites');
    }
}
