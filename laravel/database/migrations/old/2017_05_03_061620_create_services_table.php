<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


//TABLE Services
//ID -> int(10) Auto Numbered,
//SiteID -> int(10) (Link to Sites),
//ServiceName -> varchar(40),
//Decription -> varchar(100) DEFAULT NULL,
//ManagerID -> Integer (Linked to people)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//

    public function up()
    {
        Schema::create('services', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);

            $table->integer('site_id');
            $table->string('name',40);
            $table->text('description',100)->nullable();
            $table->integer('manager_id')->nullable();
            $table->boolean('active')->default(false);

            $table->integer('created_by');
            $table->integer('updated_by');
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('services');
    }
}
