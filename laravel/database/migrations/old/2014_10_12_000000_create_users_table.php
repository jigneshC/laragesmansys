<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);
            $table->string('name');
            $table->string('first_name', 40)->nullable();
            $table->string('last_name', 40)->nullable();
            $table->string('email')->unique();
            $table->string('password');

            $table->string('language', 10)->default('fr')->nullable();
            $table->string('device_token', 191)->nullable();
            $table->enum('device_type', ['android','ios','web'])->default('android');


            //added on another migration
            //            $table->string('api_token')->unique();

            $table->softDeletes();

            $table->integer('created_by');
            $table->integer('updated_by');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
