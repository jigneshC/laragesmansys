<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

//The reason of having 2 tables is to manage Role Permission: for some information to be updated you need admin role (for example)
//
//TABLE Companies
//ID -> int(10) Auto Numbered,
//CompanyName -> varchar(40),
//Address -> varchar(100) DEFAULT NULL,
//City -> varchar(40) NOT NULL,
//Postcode -> varchar(10) DEFAULT NULL,
//Country -> varchar(3) DEFAULT NULL (ISO Country Code)
//PhoneNumber1 -> varchar(15) DEFAULT NULL,
//PhoneNumber2 -> varchar(15) DEFAULT NULL,
//PhoneType1 -> int(10) DEFAULT NULL (Link to DropDown ID)
//PhoneType2 -> int(10) DEFAULT NULL (Link to DropDown ID)
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)
//
//TABLE CompaniesExtended
//ID -> int(10) Auto Numbered,
//CompanyID -> int(10) (Link to Company),
//BusinessType -> int(10) DEFAULT NULL (Link to DropDown ID)
//LogoFilename -> varchar(50) DEFAULT NULL,
//Reference -> varchar(20) DEFAULT NULL,
//Active -> Boolean
//DateCreated ->Timestamp
//DateModifed ->Timestamp
//WhoCreated -> Integer (userID)
//WhoModified -> Integer (userID)

    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('_website_id')->default(0);

            $table->string('name', 40);
            $table->integer('business_type')->nullable();
            $table->string('logo')->nullable();
            $table->string('reference')->nullable();
            $table->string('address', 100)->nullable();
            $table->string('city', 40)->nullable();
            $table->string('postcode', 10)->nullable();
            $table->string('country', 3)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone_number_1', 15)->nullable();
            $table->string('phone_number_2', 15)->nullable();
            $table->string('code_phone_number_1', 10)->nullable();
            $table->string('code_phone_number_2', 10)->nullable();
            $table->integer('phone_type_1')->nullable();
            $table->integer('phone_type_2')->nullable();
            $table->boolean('active')->default(false);
            $table->integer('created_by');
            $table->integer('updated_by');
			
			$table->text('geolocation')->nullable()->default(null);

            $table->string('longitude')->nullable()->default(0);
            $table->string('latitude')->nullable()->default(0);
			
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
