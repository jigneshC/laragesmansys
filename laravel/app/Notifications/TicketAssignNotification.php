<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Auth;
use App\Jobs\SendEmailJob;

class TicketAssignNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $ticket;
    public $actioner;
	public $receiver;
    public $type;
    public function __construct($actioner,$receiver,$ticket,$type)
    {
        $this->ticket = $ticket;
        $this->actioner = $actioner;
        $this->receiver = $receiver;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
		if($this->type == "mail"){
			return ['mail'];
		}else if($this->type == "database"){
			
			 $mdata = ['actioner'=>$this->actioner,'subject'=>'','receiver'=>$this->receiver,'view'=>'','to'=>'','ticket'=>$this->ticket,'action'=>'ticket_assign'];
             SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
			return ['database'];
		}else{
			return ['database','mail'];
		}
        
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		
			
        $line1 = \Lang::get('ticket.notification.actioner_has_assing_you_ticket',['actioner_name'=>$this->actioner->name],$notifiable->language);
        $url =  "//"._WEBSITE."/admin/tickets/".$this->ticket->id;
        if($this->ticket->_website){
            $url =  "//".$this->ticket->_website->domain."/admin/tickets/".$this->ticket->id;
        }
        $url_text = \Lang::get('mail.label.view_assign_task',[],$notifiable->language);
        $line2 = \Lang::get('mail.label.thank_you_for_using_our_application',[],$notifiable->language);
		
		
		$url_ac =  "//".$this->ticket->_website->domain."/tickets-acknowledged/".$this->ticket->id."?email=".$this->receiver->email;
		$url_ac_text =  \Lang::get('mail.label.tickets_acknowledged_text',[],$notifiable->language);
		$alink = "<a href='".$url_ac."'> ".$url_ac_text." </a>";

        return (new MailMessage)
                    ->line($line1)
                    ->action($url_text,$url)
					->line($alink)
                    ->line($line2);
					
				
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    // in your notification
    public function toDatabase($notifiable)
    {
		
			
			return [
				'ticket_id' => $this->ticket->id,
				'actioner_id' => $this->actioner->id,
				'ticket' => $this->ticket,
				'actioner' => $this->actioner,
			];
		
    }
}
