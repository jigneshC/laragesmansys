<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Auth;

class TicketActionNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $ticket;
    public $ticket_history;
    public $ticket_history2;
    public $actioner;
    public $isStatusChanged;
	public $receiver;
    public $type;
	
    public function __construct($actioner,$ticket_history,$ticket,$isStatusChanged,$ticket_history2,$receiver,$type)
    {
        $this->ticket_history = $ticket_history;
        $this->ticket_history2 = $ticket_history2;
        $this->actioner = $actioner;
        $this->ticket = $ticket;
        $this->isStatusChanged = $isStatusChanged;
		$this->receiver = $receiver;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $line1 = "";
        $line2 = "";
        $line22 = "";
        $line3 = "";
        $status_arr = \Lang::get('dashboard.status_dropdown',[],$notifiable->language);
		
		$read=1;
        
        if($this->isStatusChanged==1){
            $line3 = \Lang::get('mail.label.ticket_has_been_marked_as',['status'=>$status_arr[$this->ticket->status]],$notifiable->language);;
        }
        if($this->ticket_history->history_type =="comment"){
            $line2 = "'".$this->ticket_history->desc."'";
            $line1 = \Lang::get('ticket.notification.actioner_has_comment_on_ticket',['actioner_name'=>$this->actioner->name],$notifiable->language);
        }else if($this->ticket_history->history_type =="file"){
            if($this->ticket_history->note !=""){
                $line2 = "'".$this->ticket_history->note."'";
            }
            $line1 = \Lang::get('ticket.notification.actioner_has_add_evidence_on_ticket',['actioner_name'=>$this->actioner->name],$notifiable->language);
        }else if($this->ticket_history->history_type =="action"){
            $line1 = \Lang::get('ticket.notification.actioner_has_change_ticket_status_to',['actioner_name'=>$this->actioner->name,'new_status'=>$status_arr[$this->ticket_history->desc]],$notifiable->language);
        }
        
        if($this->ticket_history2 && isset($this->ticket_history2->desc)){
            $line22 = "'".$this->ticket_history2->desc."'";
        }
        if($line1!=""){
            $url =  "//"._WEBSITE."/admin/tickets/".$this->ticket->id;
            if($this->ticket->_website){
                $url =  "//".$this->ticket->_website->domain."/admin/tickets/".$this->ticket->id;
            }
            $url_text = \Lang::get('mail.label.view_detail',[],$notifiable->language);
            $line4 = \Lang::get('mail.label.thank_you_for_using_our_application',[],$notifiable->language);
			
		$url_ac =  "//".$this->ticket->_website->domain."/tickets-acknowledged/".$this->ticket->id."?email=".$this->receiver->email;
		$url_ac_text =  \Lang::get('mail.label.tickets_acknowledged_text',[],$notifiable->language);
		$alink = "<a href='".$url_ac."'> ".$url_ac_text." </a>";	

            return (new MailMessage)
                        ->line($line1)
                        ->line($line2)
                        ->line($line22)
                        ->line($line3)
                        ->action($url_text,$url)
						->line($alink)
                        ->line($line4);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
