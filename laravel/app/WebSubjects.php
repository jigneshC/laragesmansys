<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebSubjects extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_subjects';

    protected $primaryKey = 'ID';
    
}
