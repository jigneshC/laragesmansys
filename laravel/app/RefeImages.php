<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class RefeImages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'refe_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'refe_table_field_name', 'ref_field_id', 'image', 'image_ref_code'];
	
	protected $appends = ['file_url','file_thumb_url'];
	
	public function getFileUrlAttribute()
    {
		$path = "";
		if($this->refe_table_field_name == "website_id"){
			$path = "uploads/website";
		}
		
		if($this->image && \File::exists(public_path($path)."/".$this->image)){
			return url($path)."/".$this->image;
		}else{
			return "";
		}
	}
	public function getFileThumbUrlAttribute()
    {
		$path = "";
		if($this->refe_table_field_name == "_websites_id"){
			$path = "uploads/website/thumb";
		}
		
		if($this->image && \File::exists(public_path($path)."/".$this->image)){
			return url($path)."/".$this->image;
		}else{
			return "";
		}
	}


   


}
