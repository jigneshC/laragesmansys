<?php

namespace App;

use App\BaseModel;
use App\Traits\BaseModelTrait;

class Subject extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    use BaseModelTrait;


    protected $table = 'subjects';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','subject_type' ,'file', 'service_id', 'time', 'action_date', 'action_id','_website_id','geolocation_id'];


    protected $hidden = ['created_at','updated_at','created_by','updated_by'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('App\Site');
    }
    public function services()
    {
        return $this->belongsTo('App\Service','service_id','id');
    }

    

    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function people()
    {
        return $this->belongsToMany('App\People');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
	 
    public function caller_list()
    {
        return $this->belongsToMany('App\People', 'subject_people')
            ->withPivot('priority')
            ->orderBy('priority', 'asc')->select("first_name","last_name","phone_number_1","code_phone_number_1","id","user_id");
    }

//    public function makeCallerList($permission, $by_id)
//    {
//        return $this->permissions()->attach($permission, ['by_id' => $by_id]);
//    }

    protected $appends = ['file_path','instruction_file_count','caller_list_count'];

	
	public function getCallerListCountAttribute()
    {
		$res = 0;
		
        if ($this->caller_list) {
            $res = $this->caller_list->count();
        }
        return $res;
    }
	
    public function getFilePathAttribute()
    {
		$path = "uploads/files";
		
        if ($this->file != '' && \File::exists(public_path($path)."/".$this->file)) {
            return asset($path."/" . $this->file);
        }else{
			return null;
		}
        
    }
	public function getInstructionFileCountAttribute()
    {
		$path = "uploads/files";
		
        if($this->file && \File::exists(public_path($path)."/".$this->file)){
			return 1;
		}else{
			return 0;
		}
       
    }

    public function next(){
    // get next user
        return Subject::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  user
        return Subject::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }

    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'subjects_id');
    }
	
	 public function geoid()
    {
        return $this->hasOne('App\DropdownValue','id', 'geolocation_id');
    }
	
    
}
