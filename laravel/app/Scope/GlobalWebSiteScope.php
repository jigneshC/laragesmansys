<?php
namespace App\Scope;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;
use Session;

class GlobalWebSiteScope implements Scope
{

    protected $user = false;

    public function __construct($user = false)
    {
        $this->user = $user;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (!\App::runningInConsole()) {

            $website_id = _WEBSITE_ID;
            $table = $model->getTable();

            if($table =="dropdowns_types" || $table =="dropdown_values" ||  $table=="users" || $table=="ticket_history" || $table=="unit_transaction" || $table=="unit_module" || $table=="unit_packages" ){

            }else{
                if (defined('_MASTER') && _MASTER) {

                    /*if($table =="users" && strtolower(\Request::method())=="post" && strpos(\Request::fullUrl(), 'login') != false){
                        $builder->where($table.'._website_id', $website_id);
                    }*/
                }else{
                    if($table !=""){
                        $builder->where($table.'._website_id', $website_id);
                    }else{
                        $builder->where('_website_id', $website_id);
                    }
                }
            }

//            $model->where('_website_id', $website_id);
//        $builder->whereHas('families', function ($query) use ($familyId) {
//            $query->where('families.id', $familyId);
//        });
//
//        if (Session::has('family_id')) {
//            $familyId = session('family_id');
//
//            $builder->whereHas('families', function ($query) use ($familyId) {
//                $query->where('families.id', $familyId);
//            });
//        }
        }
    }

}