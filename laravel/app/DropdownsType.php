<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\BaseModel;

class DropdownsType extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dropdowns_types';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', '_website_id', 'active', 'created_by', 'updated_by'];


    /**
     * Each DropDown Type Have Many Values
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany('App\DropdownValue', 'type_id', 'id')->language();
    }

    public function user(){
        return $this->belongsTo('App\User','created_by','id');
    }

    public static function getPluckValue($type){

        $pluck= [];
        $typevalue = DropdownsType::whereName($type)->with('values')->first();

        if($typevalue){
            $pluck = $typevalue->values()->pluck('name', 'parent_id')->prepend('Select', '');
        }
        return $pluck;

    }

}
