<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\HasRoles;
use App\Traits\BaseMasterModelTrait;

class WebSite extends Model
{

    use SoftDeletes ,HasRoles , BaseMasterModelTrait;
    protected $connection = 'mysql' ;
    protected $table = "_websites";

    /**
     * @var array
     */
    protected $fillable = [
        'domain', 'name', 'status', 'description','master','enable_charge','enable_qrcode','api_token'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'master' => 'boolean',
    ];

	protected $appends = ['file_url','file_thumb_url'];
	
	
	public function getFileUrlAttribute()
    {
		
		if($this->refimages){
			return $this->refimages->file_url;
		}else{
			return "";
		}
	}
	public function getFileThumbUrlAttribute()
    {
		if($this->refimages){
			return $this->refimages->file_thumb_url;
		}else{
			return "";
		}
	}
	
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
   /* public function users()
    {
        return $this->belongsToOne('App\User', 'user_website', 'user_id', 'id');
    }*/

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function peoples()
    {
        return $this->hasMany('App\People');
    }
	
	public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function refimages()
    {
        return $this->hasOne('App\RefeImages', 'ref_field_id', 'id')->where('refe_table_field_name', 'website_id');
    }
    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * @param $query
     * @param $bool
     * @return mixed
     */
    public function scopeMaster($query, $bool)
    {
        return $query->where('master', $bool);
    }

    public function scopeWebsite($q)
    {
        if (defined('_MASTER') && _MASTER) {
            return $q->where('id','>',0);
        }else{
            return $q->where('id',_WEBSITE_ID);
        }
    }

     public function next(){
        // get next website
         return Website::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  website
        return Website::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }
	
	public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', '_websites_id');
    }


}
