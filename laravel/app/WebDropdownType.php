<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebDropdownType extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_dropdowntype';

    protected $primaryKey = 'ID';
}
