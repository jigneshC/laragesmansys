<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use App\WebClients;
use App\WebDropdownType;
use App\WebDropdown;
use App\ccirdpWebSites;
use App\WebSubjects;
use App\WebSP;
use App\WebPeoples;
use App\WebCascade;
use App\WebTask;
use App\WebLogbook;
use App\WebLogbookdetail;

use App\Company;
use App\DropdownsType;
use App\DropdownValue;
use App\Site;
use App\User;
use App\People;
use App\Service;
use App\ServicesSites;
use App\Subject;
use App\SubjectPeople;
use App\Ticket;
use App\TicketHistory;


class Insertdatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Insertdatabase:insert_db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserting database from ccridp to gesmansys';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected  $website_id = 39;
    protected  $site_id = 62;
    protected  $company_id = 29;
    protected  $services_id = 73;
    protected  $subject_id = 253;
    protected  $active = 1;
    protected  $landline =  3;
    protected  $mobile = 4 ;
    protected  $fax =  2281;
    protected  $action_id =  1;
    protected  $manager_id =  204;
    protected  $created_by =  204;
    protected  $lang = 'fr';
    protected  $availability = 1;
    protected  $priority = 0;
	
	
    protected  $web_dropdown_bkid = 216;
    protected  $web_dropdown_type_bkid = 15;
    protected  $web_clients_bkid = 3;
    protected  $web_sites_bkid = 18;
    protected  $web_subjects_bkid = 221;
    protected  $web_people_bkid = 108;
    protected  $web_cascade_bkid = 13;
    protected  $web_tasks_bkid = 13;
    protected  $web_logbook_bkid = 13392;
	
	
    

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
       /* $this->dropdown_type_value();
        $this->company_site();
        $this->user_people();
        $this->people_subject();
        $this->tasks();*/
		//$this->subjectfile();
        $this->tickets();
	   // $this->subjectupdate();
		// $this->servicesUser();
		
		
      
    }

    public function dropdown_type_value(){

        $w_dropdown_type =  WebDropdownType::where("ID",">",$this->web_dropdown_type_bkid)->get(); 
        
        foreach ($w_dropdown_type as $web_dd_type){

            $g_dropdown_type =  DropdownsType::where('name',$web_dd_type->name_en)->first();
           

            if($g_dropdown_type){ 

                //get all the values of type which are done with new entry
                $w_dropdown =  WebDropdown::where('type',$web_dd_type->ID)->get();
                
                foreach($w_dropdown as $w_dd){
                    //check for en lang
                    $g_dropdown_value =  DropdownValue::where('name',$w_dd->name_en)->first();
                    if($g_dropdown_value){

                        
                    }else{
                        //make new entry of dropdownvalues
                        $g_dd_value = new DropdownValue;
                        $g_dd_value->_website_id = $this->website_id;
                        $g_dd_value->name = $w_dd->name_en ;
                        $g_dd_value->type_id = $g_dropdown_type->id;
                        $g_dd_value->active = $this->active;
                        $g_dd_value->lang_code = 'en';
                        $g_dd_value->save();

                        if($g_dd_value){
                            $g_dd_value->parent_id = $g_dd_value->id ;
                            $g_dd_value->save();
                        }

                        //check for en lang
                        $g_dropdown_value_fr =  DropdownValue::where('name',$w_dd->name_fr)->first();
                        
                            //make new entry of dropdownvalues
                            $g_dd_value_fr = new DropdownValue;
                            $g_dd_value_fr->_website_id = $this->website_id;
                            $g_dd_value_fr->name = $w_dd->name_fr ;
                            $g_dd_value_fr->type_id =  $g_dropdown_type->id;
                            $g_dd_value_fr->active = $this->active;
                            $g_dd_value_fr->lang_code = 'fr';
                            $g_dd_value_fr->save();
    
                            if($g_dd_value_fr){
                                $g_dd_value_fr->parent_id = $g_dd_value->id ;
                                $g_dd_value_fr->save();
                            }
    
                    }
                }
                
            }else{
                //make new entry
                $g_dd_type = new DropdownsType;
                $g_dd_type->_website_id = $this->website_id;
                $g_dd_type->name = $web_dd_type->name_en ;
                $g_dd_type->active = $this->active;
                $g_dd_type->save();

                //get all the values of type which are done with new entry
                $w_dropdown =  WebDropdown::where('type',$web_dd_type->ID)->get();
                
                foreach($w_dropdown as $w_dd){
                    
                    $g_dropdown_value =  DropdownValue::where('name',$w_dd->name_en)->first();
                    if($g_dropdown_value){
                        
                    }else{
                        //make new entry of dropdownvalues
                        $g_dd_value = new DropdownValue;
                        $g_dd_value->_website_id = $this->website_id;
                        $g_dd_value->name = $w_dd->name_en ;
                        $g_dd_value->type_id =  $g_dd_type->id;
                        $g_dd_value->active = $this->active;
                        $g_dd_value->lang_code = 'en';
                        $g_dd_value->save();

                        if($g_dd_value){
                            $g_dd_value->parent_id = $g_dd_value->id ;
                            $g_dd_value->save();
                        }

                        //check for en lang
                        $g_dropdown_value_fr =  DropdownValue::where('name',$w_dd->name_fr)->first();
                        
                            //make new entry of dropdownvalues
                            $g_dd_value_fr = new DropdownValue;
                            $g_dd_value_fr->_website_id = $this->website_id;
                            $g_dd_value_fr->name = $w_dd->name_fr ;
                            $g_dd_value_fr->type_id =  $g_dd_type->id;
                            $g_dd_value_fr->active = $this->active;
                            $g_dd_value_fr->lang_code = 'fr';
                            $g_dd_value_fr->save();
    
                            if($g_dd_value_fr){
                                $g_dd_value_fr->parent_id = $g_dd_value->id ;
                                $g_dd_value_fr->save();
                            }
                    }
                   
                }
                
            }
            
        }

     //   echo "Dropdown Type & Values are inserted"; echo "<br>" ;
    }


    public function company_site(){

        $client = WebClients::first();

        $companies = Company::where('name',$client->client)->where("_website_id",$this->website_id)->first();

        if(!$companies){

            //Insert database in Companies table
            $company = new Company;
            $company->_website_id =  $this->website_id ;
            $company->name = $client->client;
            $company->logo = $client->logo;
            $company->reference = $client->reference;
            $company->address = $client->adresse;
            $company->city = $client->city;
            $company->postcode = $client->postnumber;
            $company->phone_number_1 = $client->phonenumber;
            $company->phone_number_2 = $client->faxnumber;
            $company->phone_type_1 = $this->mobile ;
            $company->phone_type_2 = $this->fax ;
            $company->active = $this->active ;
            $company->save();
			
			echo "<br/> Companey : ".$company->id;
        }else{
            $company = $companies ;
        }
        
        

        

        //make entry for sites
        $web_sites = ccirdpWebSites::where("ID",">",$this->web_sites_bkid)->get(); 
        foreach ($web_sites as $website){
			
			
			$services = Service::where('name','=',$website->site)->where("_website_id",$this->website_id)->first();

			if(!$services){
				 //make a default Service
				$service = new Service;
				$service->_website_id =  $this->website_id ;
	   //         $service->site_id = 0 ;
				$service->name = $website->site;
				$service->description = "";
				$service->manager_id = $this->manager_id;
				$service->active = $this->active ;
				$service->created_by = $this->created_by ;
				$service->company_id = $this->company_id ;
				$service->save();

                echo "Services is Created. " ; echo "<br>" ;
                
                $ss = new ServicesSites;
                $ss->services_id = $service->id ;
                $ss->sites_id = $this->site_id ;
                $ss->save();
				
				echo "<br/> Service : ".$ss->id;
			}else{
				$service = $services;
			}
//------------------------------------------------------------------------------------------
            
            
           
        }

        $service_id = $service->id ;
        
        $this->subject($service_id);

      //  echo "Company and Services-Sites data are inserted. " ; echo "<br>" ;

    }

    public function subject($service_id){

        $service_id = $service_id ;
		
		$services_ids= [];
		
        $web_sites = ccirdpWebSites::all();
		foreach ($web_sites as $website){
			$services = Service::where('name','=',$website->site)->where("_website_id",$this->website_id)->first();
			if($services){
				$services_ids[$website->ID] = $services->id;
			}else{
				$services_ids[$website->ID] = $service_id;
			}
		}

        //make entry for subject

        $web_subjects = WebSubjects::where("ID",">",$this->web_subjects_bkid)->get(); 
        foreach ($web_subjects as $websub){

            $subject = Subject::where('name',$websub->subject)->where("_website_id",$this->website_id)->first();
            $value = "";
            if($websub->type){
                $s_type =  WebDropdown::where('ID',$websub->type)->first();
                if($s_type){
                    $type = DropdownValue::where('name',$s_type->name_en)->first();
                    if( $type->name == "Event" ){
                        $value = "event" ;
                    }elseif( $type->name == "Ticket" ){
                        $value = "ticket" ;
                    }elseif( $type->name == "Alarm" ){
                        $value = "alarm" ;
                    }elseif( $type->name == "Planned Task" ){
                        $value = "planned_activity" ;
                    }else{
                        $value = " ";
                    }

                }
            }
			
			if(isset($services_ids[$websub->siteID])){
				$service_id =$services_ids[$websub->siteID];
			}
            
            if(!$subject){
                
              $subjects = new Subject;
              $subjects->_website_id = $this->website_id ;
              $subjects->name =  $websub->subject ;
              if($websub->type && $s_type && $type && $value){
                $subjects->subject_type = $value;
              }else{
                $subjects->subject_type = 0 ;
              } 
              $subjects->service_id = $service_id ;
              $subjects->created_by =  $this->created_by ;
              $subjects->save();

			  echo "<br/> subject : ".$subjects->id;
			  
            }else{
				$subject->service_id = $service_id ;
				$subject->_website_id = $this->website_id ;
				$subject->save();
			}
        }

      //  echo "Subjects data are inserted. " ; echo "<br>" ;

    }

    public function user_people(){
		
        $web_users = WebPeoples::where("ID",">",$this->web_people_bkid)->get(); 
        
        foreach($web_users as $webuser){

            if($webuser->quality){
                $s_quality =  WebDropdown::where('ID',$webuser->quality)->first();
                if($s_quality){
                    $quality = DropdownValue::where('name',$s_quality->name_en)->first();
                }
            }

            if($webuser->Title){
                $s_title =  WebDropdown::where('ID',$webuser->Title)->first();
                if($s_title){
                    $title = DropdownValue::where('name',$s_title->name_en)->first();
                }
            }

            if($webuser->phonetype1){
                $s_phonetype1 =  WebDropdown::where('ID',$webuser->phonetype1)->first();
                if($s_phonetype1){
                    $phonetype1 = DropdownValue::where('name',$s_phonetype1->name_en)->first();
                }
            }

            if($webuser->phonetype2){
                $s_phonetype2 =  WebDropdown::where('ID',$webuser->phonetype2)->first();
                if($s_phonetype2){
                    $phonetype2 = DropdownValue::where('name',$s_phonetype2->name_en)->first();
                }
            }

            $users = User::where('email',$webuser->LoginName )->first();
            if(!$users && $webuser->LastName){
                // make new entry of user
                $user = new User;
                $user->_website_id = $this->website_id ;
                $user->name = $webuser->FirstName.' '.$webuser->LastName ;
                $user->first_name = $webuser->FirstName ;
                $user->last_name = $webuser->LastName ;
                $user->email = $webuser->LoginName ;
                $user->password = $webuser->Password ;
                $user->language = $this->lang ;
                $user->created_by = $this->created_by ;
                $user->save();
				
				echo "<br/> user : ".$user->id;

                // make entry in people table for other details
                $people = new People;
                $people->_website_id = $this->website_id ;
                $people->user_id = $user->id ;

                if($webuser->Title && $s_title && $title){
                    $people->title =  $title->name ;
                }else{
                    $people->title = '';
                }
               
                $people->first_name = $user->first_name ;
                $people->last_name = $user->last_name ;

                if($webuser->quality && $s_quality && $quality){
                    $people->quality = $quality->id ;
                }else{
                    $people->quality = 0;
                }
                
                $people->phone_number_1 = $webuser->phone1 ;
                $people->phone_number_2 = $webuser->phone2 ;

                if($webuser->phonetype1 && $s_phonetype1 && $phonetype1){
                    $people->phone_type_1 = $phonetype1->id ;
                }else{
                    $people->phone_type_1 = 0;
                }

                if($webuser->phonetype2 && $s_phonetype2 && $phonetype2){
                    $people->phone_type_2 = $phonetype2->id ;
                }else{
                    $people->phone_type_2 = 0;
                }
               
                $people->emergency_password = $webuser->emergencypw ;
                $people->availability =  $this->availability ;
                $people->hold_key = $webuser->holdskey ;
                $people->photo = $webuser->photo ;
                $people->active =  $this->active ;
                $people->created_by = $this->created_by ;
                $people->save();
            }
        }

     //   echo "Users & Peoples data are inserted" ;  echo "<br>" ;

    }

    public function people_subject(){
        
        $web_cascade = WebCascade::all();
         foreach($web_cascade as $ps){
             $web_people = WebPeoples::where('ID',$ps->peopleID)->first();
             $web_subject = WebSubjects::where('ID',$ps->subjectID)->first();
             if($web_people && $web_subject){
                 $user =  User::with('people')->where('email',$web_people->LoginName)->first();
                 $subject =  Subject::where('name',$web_subject->subject)->where("_website_id",$this->website_id)->first();
                 if($user && $user->people && $subject){
                     //make Entry for People & Subject Relation
                    $p_s = SubjectPeople::where('subject_id',$subject->id )->where('people_id',$user->people->id)->first();
                    if(!$p_s){
                        $people_sub = new SubjectPeople;
                        $people_sub->subject_id =  $subject->id ;
                        $people_sub->people_id =  $user->people->id ;
                        $people_sub->priority = $this->priority;
                        $people_sub->save();
						
						echo "<br/> People subject ";
                    }
                 }
             }
         }
        // echo "Subject & Peoples data are inserted" ;  echo "<br>" ;

    }
	public function subjectfile(){
		
		$web_subjects = WebSubjects::join('web_instructions','web_instructions.subjectID','=','web_subjects.ID')->select(["web_subjects.*",'web_instructions.filename'])->get();
		
        foreach ($web_subjects as $websub){
			
			$subject = Subject::where('name',$websub->subject)->where("_website_id",$this->website_id)->first();
			if($subject && $subject->file == "" || !$subject->file){
				Subject::whereId($subject->id)->update(['file'=>$websub->filename]);
				echo "<br>".$websub->filename;
			}
		}
	}

    public function tasks(){

        $service = Service::where('name','=','New Service')->where("_website_id",$this->website_id)->first();
        $web_task = WebTask::all();
        foreach ($web_task as $webtask){
            $subject = Subject::where('name',$webtask->task)->where("_website_id",$this->website_id)->first();
            if(!$subject){
                
            $subjects = new Subject;
            $subjects->_website_id = $this->website_id ;
            $subjects->name =  $webtask->task;
            $subjects->subject_type = "planned_activity";
            if($service){
                $subjects->service_id = $this->services_id;
            }else{
                $subjects->service_id = $this->services_id ;  
            }
            $subjects->created_by =  $this->created_by ;
            $subjects->save();
            }
        }
		echo "<br>" ;
        echo "Tasks data are inserted in Subjects Table with type Planned Activity. " ; 
    }
	
	public function tickets(){

		$subjectsids= [];
		$siteids= [];
		$actioner= [];
		
        $web_subjects = WebSubjects::all();
		foreach ($web_subjects as $websub){
			$subject = Subject::where('name',$websub->subject)->where("_website_id",$this->website_id)->first();
			if($subject){
				$subjectsids[$websub->ID] = $subject->id;
			}
		}
		
		/*$web_sites = ccirdpWebSites::all();
        foreach ($web_sites as $website){
			
			$sites = Site::where('name',$website->site)->first();

            if($sites){
               $siteids[$website->ID] = $sites->id;
            }
		}*/
		
		$web_users = WebPeoples::all();
        
        foreach($web_users as $webuser){
			$users = User::where('email',$webuser->LoginName )->first();
            if($users){
				$actioner[$webuser->ID] = $users->id;
			}
		}
		$counter = 0;
		//echo "<pre>"; print_r($siteids); exit;
		
		$defualt_sub = $this->subject_id;
		$defualt_site = $this->site_id;
		
		
		//$web_tickets = WebLogbook::where("ID",">",$this->web_logbook_bkid)->offset(6999)->limit(1000)->get();
		$web_tickets = WebLogbook::where("ID",">",$this->web_logbook_bkid)->offset(6999)->limit(1000)->get();
		
		
		foreach($web_tickets as $k=>$wticket){
			$_subjectid = $defualt_sub;
			if(isset($subjectsids[$wticket->subjectID])){
				$_subjectid =$subjectsids[$wticket->subjectID];
			}
			$_siteid = $defualt_site;
			if(isset($siteids[$wticket->siteID])){
				$_siteid =$siteids[$wticket->siteID];
			}
			$_userid = $this->created_by;
			if(isset($actioner[$wticket->actionID])){
				$_userid =$actioner[$wticket->actionID];
			}
			
			if($_siteid && $_subjectid){
				
				$ticket = Ticket::where('site_id',$_siteid)->where('subject_id',$_subjectid)->where('created_by',$_userid)->where('created_at',$wticket->actiondate)->first();
				if(!$ticket){
					
					if($wticket->closed == 1){
						$status = "solved";
					}else if($wticket->ack == 1){
						$status = "open";
					}else{
						$status = "new";
					}
					
					$cr = [
						"site_id"=>$_siteid,
						"title"=>"Migrate from old",
						"subject_id"=>$_subjectid,
						"user_id"=>$_userid,
						"content"=>$wticket->comment,
						"status"=>$status,
						"_website_id"=>$this->website_id,
						"created_by"=>$_userid,
						"updated_by"=>$_userid,
						"created_at"=>$wticket->actiondate,
						"updated_at"=>$wticket->actiondate,
					];
					
					$createId = Ticket::insertGetId($cr);
					
					$whistory = WebLogbookdetail::where("parentID",$wticket->ID)->get();
					
					foreach($whistory as $whis){
						$comment = ""; $note = "";
						if($whis->evidence && $whis->evidence != ""){
							$histype = "file";
							$comment = $whis->evidence;
							$note = $whis->comment;
						}else{
							$histype = "comment";
							$comment = $whis->comment;
						}
						
						$huserid = $_userid;
						if(isset($actioner[$whis->actionID])){
							$huserid =$actioner[$whis->actionID];
						}						
						$cra = [
							"ticket_id"=>$createId,
							"parent_id"=>0,
							"history_type"=>$histype,
							"desc"=>$comment,
							"note"=>$note,
							"_website_id"=>$this->website_id,
							"created_by"=>$huserid,
							"updated_by"=>$huserid,
							"created_at"=>$wticket->actiondate,
							"updated_at"=>$wticket->actiondate,
						];
					
						echo "|".$createIda = TicketHistory::insertGetId($cra);
					
					}
					echo "<br/> Ticket ".$wticket->ID;
				}else{
					if($ticket->logs->count() <=0){
						$whistory = WebLogbookdetail::where("parentID",$wticket->ID)->get();
					
						echo "<br/>".$wticket->ID." -- ";
						foreach($whistory as $whis){
							$comment = ""; $note = "";
							if($whis->evidence && $whis->evidence != ""){
								$histype = "file";
								$comment = $whis->evidence;
								$note = $whis->comment;
							}else{
								$histype = "comment";
								$comment = $whis->comment;
							}
							
							$huserid = $_userid;
							if(isset($actioner[$whis->actionID])){
								$huserid =$actioner[$whis->actionID];
							}						
							$cra = [
								"ticket_id"=>$ticket->id,
								"parent_id"=>0,
								"history_type"=>$histype,
								"desc"=>$comment,
								"note"=>$note,
								"_website_id"=>$this->website_id,
								"created_by"=>$huserid,
								"updated_by"=>$huserid,
								"created_at"=>$wticket->actiondate,
								"updated_at"=>$wticket->actiondate,
							];
						
							echo "|".$createIda = TicketHistory::insertGetId($cra);
						
						}
						
					}else{
						echo "<br/> done";
					}
					
				}
				
			}else{
				echo "<br/> Ticket data not found ".$wticket->ID;
			}
			
		}
	//	echo "Tickets and ticket history migrate complete";
		//echo "<pre>";print_r($subjectsids); print_r($siteids); print_r($actioner);
    }
	
	public function subjectupdate(){

        $service_id = 0 ;$services_ids= [];
		$web_sites = ccirdpWebSites::all();
		foreach ($web_sites as $website){
			
			$services = Service::where('name','=',$website->site)->where("_website_id",$this->website_id)->first();
			if($services){
				$services_ids[$website->ID] = $services->id;
			}else{
				$services_ids[$website->ID] = $service_id;
			}
		}

        //make entry for subject

        $web_subjects = WebSubjects::all();
        foreach ($web_subjects as $websub){

            $subject = Subject::where('name',$websub->subject)->where("_website_id",$this->website_id)->first();
            $value = "";
            if($websub->type){
                $s_type =  WebDropdown::where('ID',$websub->type)->first();
                if($s_type){
                    $type = DropdownValue::where('name',$s_type->name_en)->first();
                    if( $type->name == "Event" ){
                        $value = "event" ;
                    }elseif( $type->name == "Ticket" ){
                        $value = "ticket" ;
                    }elseif( $type->name == "Alarm" ){
                        $value = "alarm" ;
                    }elseif( $type->name == "Planned Task" ){
                        $value = "planned_activity" ;
                    }else{
                        $value = " ";
                    }

                }
            }
			
			if(isset($services_ids[$websub->siteID])){
				$service_id =$services_ids[$websub->siteID];
			}
            
            if($subject){
                echo "<br/> subjects->subject_type = ".$subject->subject_type." : ".$value;
				
              if($websub->type && $s_type  && $value){
                 $subject->subject_type = $value;
				 $subject->save();
              }else{
               // $subjects->subject_type = 0 ;
              } 
			  
			  
              //$subjects->save();

            }else{
				echo "<br/> Subject not found";
			}
        }

      

    }
	
	public function servicesUser(){

		$asign_role = "CO";
		$people_ids= [];
		$services_ids= [];
		
        $peoples = WebPeoples::all();
		foreach ($peoples as $people){
			$users = User::where('email',$people->LoginName)->first();
			if($users && $users->people){
				$user_roles = [];
				foreach ($users->roles as $role) {
					$user_roles[] = $role->name;
				}
				if (in_array($asign_role, $user_roles)){
					
				}else{
					$users->assignRole([$asign_role]);
				}
				
				$people_ids[$people->ID] = $users->people->id;
			}
		}
		
		$web_sites = ccirdpWebSites::all();
		foreach ($web_sites as $website){
			$services = Service::where('name','=',$website->site)->where("_website_id",$this->website_id)->first();
			if($services){
				$services_ids[$website->ID] = $services->id;
			}
		}
		
		$service_users = WebSP::all();
		
		foreach($service_users as $k=>$sp){
			if(isset($people_ids[$sp->peopleID]) && isset($services_ids[$sp->siteID])){
				$row = DB::table('people_service')->where('people_id', '=', $people_ids[$sp->peopleID])->where('service_id', '=', $services_ids[$sp->siteID])->first();
				if($row){
					echo "<br> exist";
				}else{
					DB::table('people_service')->insert(['people_id'=>$people_ids[$sp->peopleID],'service_id'=>$services_ids[$sp->siteID],'created_at'=>$sp->actiondate,'updated_at'=>$sp->actiondate]);
					echo "<br> created";
				}
			}else{
					echo "<br> Not found";
			}
			
		}
	}
}