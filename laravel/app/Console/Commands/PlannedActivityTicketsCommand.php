<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Ticketviewcron;

class PlannedActivityTicketsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:plannedtickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Planned Activity ticket generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$next_send_time = date('H:i:00');
		
		if($next_send_time == "00:00:00" || $next_send_time == "01:00:00" || $next_send_time == "03:00:00" || $next_send_time == "06:00:00"){
			Ticketviewcron::setTodayVisibilityOfTickets();
		}
    }
}
