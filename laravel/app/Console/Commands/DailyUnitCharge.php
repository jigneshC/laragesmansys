<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\WebSite;
use App\UnitsModule;
use App\User;
use App\UnitTransaction;

class DailyUnitCharge extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dailyunit:charge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $min_balance_cut_up_to = \config('settings.min_balance_cut_up_to');
        
        $websites = Website::Active()->Master(0)->Where('enable_charge',1)->get();
        
        foreach ($websites as $k => $website) {
            
            $sitecharge = UnitsModule::getSitePerDayCharge($website);
            
            $charge_unit_total = UnitsModule::calculateTotalPrice($sitecharge);
            $is_charged = UnitTransaction::where('website_id',$website->id)->where('reference','daily_charge')->where("created_at",">=",\Carbon\Carbon::today())->count();
            
			   
    

            if($is_charged <= 0 && ($website->units - $charge_unit_total) > $min_balance_cut_up_to){
				
	
                $charge = [
                    'unit_type' => 'debit',
                    'reference' => 'daily_charge',
                    'website_id' => $website->id,
                    'unit' => $charge_unit_total,
                    'comment' => "daily_charge",
					'created_by' => 0,
                    'updated_by' => 0,
                    'itemsobj' => json_encode($sitecharge)
                ];
				
	
				try{
					UnitTransaction::create($charge);
					Website::where('id',$website->id)->update(['units' => $website->units - $charge_unit_total]);
				}  catch (\Exception $e){
						\Mail::raw($e->getMessage(), function ($message) use ($k){
						  $message->to("jignesh.citrusbug@gmail.com")
							->subject("Call test ".$k);
						}); 
				}
                

            }
        }
    }

    

}
