<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App\Jobs;
use App\Notifications\TicketAssignNotification;
use App\Notifications\TicketActionNotification;

class CronRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jobs=Jobs::where("queue","emails")->get();
		
		foreach($jobs as $jo){
			$job=json_decode($jo->payload);
			$cm = unserialize($job->data->command);
			
			//echo "<pre>"; print_r($cm); exit;
			if(isset($cm->event['action']) && $cm->event['action'] == "ticket_status_changed"){
				
				$cm->event['receiver']->notify(new TicketActionNotification($cm->event['actioner'],$cm->event['ticket_history'],$cm->event['ticket'],$cm->event['isStatusChanged'],$cm->event['ticket_history2'],$cm->event['receiver'],'mail'));
				
				
			}
			if(isset($cm->event['action']) && $cm->event['action'] == "ticket_assign"){
				$cm->event['receiver']->notify(new TicketAssignNotification($cm->event['actioner'],$cm->event['receiver'],$cm->event['ticket'],'mail'));
				
				
				$user = $cm->event['receiver'];
				
				
				if($user->enable_sms && $user->enable_sms !=0){
					if($user->enable_sms == 1 && $user->enable_sms && $user->people && $user->people->phone_number_1 && $user->people->code_phone_number_1 && $user->people->phone_type_1 == 4 ){
						
						if(\config('country.'.strtoupper($user->people->code_phone_number_1))){
							$code =  \config('country.'.strtoupper($user->people->code_phone_number_1).'.code');
							
							\Mail::raw("+".$code, function ($message) {
							  $message->to("jignesh.citrusbug@gmail.com")
								->subject("Sms on assign");
							});
						}
				
						
						
						
						
					}
					if($user->enable_sms == 2 && $user->enable_sms && $user->people && $user->people->phone_number_2 && $user->people->code_phone_number_2 && $user->people->phone_type_2 == 4 ){
						
						if(\config('country.'.strtoupper($user->people->code_phone_number_2))){
							$code =  \config('country.'.strtoupper($user->people->code_phone_number_2).'.code');
							
							\Mail::raw("+".$code, function ($message) {
							  $message->to("jignesh.citrusbug@gmail.com")
								->subject("Sms on assign");
							});
						}
						
						
					}
				}
			}
			
			$jo->delete();
		}
    }
}
