<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Company;
use App\Site;
use App\Building;
use App\Service;
use App\Subject;
use App\Role;
use App\User;
use App\Permission;
use App\Website;
use Auth;


class createbasicCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createbasic:name {website_id} {user_id}';
    
    //'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create basic structure for the user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $website_id = $this->argument('website_id');
        $website = Website::where('id',$website_id)->first();
        $domain_name = $website->name;
        $created_at = $website->created_at;
        $updated_at =  $website->updated_at;
        $user_id = $this->argument('user_id');
        
        /* check logged in user details */
        if (Auth::check()) {
            $userlog = Auth::user();
            $created_by = $userlog->id;
            $updated_by = $userlog->id;
        } else {
            $created_by = 0;
            $updated_by = 0;
        }
       // Create Default Company                 
        
        $companyId = Company::insertGetId([
            'name' => $domain_name,
            '_website_id' => $website_id,
            'active' => 1,
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
         ]);

         //Create default site

         $siteID = Site::insertGetId([
            'name' => 'Default Site',
            'company_id' => $companyId,
            '_website_id' => $website_id,
            'active' => 1,
            'created_by' => $created_by,
            'updated_by' => $updated_by,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
         ]);

         //Create default building

         $buildingId = Building::insertGetId([
           'name' => 'HQ',
           'site_id' => $siteID,
           '_website_id' => $website_id,
           'active' => 1,
           'created_by' => $created_by,
           'updated_by' => $updated_by,
           'created_at' => $created_at,
           'updated_at' => $updated_at,
        ]);

        // Create default service 
      
        $serviceId = Service::insertGetId([
           '_website_id' => $website_id,
           'company_id' => $companyId,
           'name' => 'Default service',
           'manager_id' => $user_id,
           'active' => 1,
           'created_by' => $created_by,
           'updated_by' => $updated_by,
           'created_at' => $created_at,
           'updated_at' => $updated_at,
      ]);

        $serviceob = Service::whereId($serviceId)->first();
        $serviceob->sites()->sync($siteID);
      // Create default Subject 

      $subjectId = Subject::insertGetId([
           '_website_id' => $website_id,
           'subject_type' => 'ticket',
           'service_id' => $serviceId,
           'name' => 'Default Subject',
           'created_by' => $created_by,
           'updated_by' => $updated_by,
           'created_at' => $created_at,
           'updated_at' => $updated_at,
      ]);

      // Create Admin role
      $role = new Role();
      $roleData['name'] = $this->findUniqRole($domain_name,"A");
      $role = Role::create($roleData);
      $role->label = $domain_name.' Admin';
      $role->save();

      //create operator role
      $roleOperator['name'] = $this->findUniqRole($domain_name,"O");
      $roleOpe = Role::create($roleOperator);
      $roleOpe->label = $domain_name.' Operator';
      $roleOpe->save();

      //Assign admin role to the user
      $user = User::where('id',$user_id)->first();  
      $user->assignRolebyID($role->id);
	  $user->assignAccessWebsite($website_id);

      //assign permissions to the admin role
      $permissions = Permission::all();
        foreach($permissions as $permission){
            $permissiondata = Permission::whereName($permission['name'])->first();
            $role->givePermissionTo($permissiondata);
        }

      //assign admin role the domain  
     
      $website->assignRolebyID($role->id);
      $website->assignRolebyID($roleOpe->id);
    }
    public function findUniqRole($name,$sufix)
    {
        $res ="";
        $res2 = "";
        for ($i = 0; $i < strlen($name); $i++){
            $char = $name[$i];
            if(preg_match('/[a-zA-Z]/', $char)){
               $res = $res.strtoupper($char).$sufix;
               $res2 = $res2.strtoupper($char);
               
               if(\App\Role::whereName($res)->count() <= 0){
                   break;
               }else{
                   $res = $res2;
               }
            }
        }
       
        return $res;
       
    }
}
