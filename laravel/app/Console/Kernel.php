<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\LaraCache',
        'App\Console\Commands\LaraClear',
        'App\Console\Commands\Insertdatabase',
		Commands\PlannedActivityTicketsCommand::class,
        Commands\createbasicCommand::class,
        Commands\DailyUnitCharge::class,
		Commands\CronRun::class,
		
		
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('generate:plannedtickets')->everyMinute();
        $schedule->command('dailyunit:charge')->everyMinute();
		$schedule->command('cron:run')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
