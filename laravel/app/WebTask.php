<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebTask extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_tasks';
    protected $primaryKey = 'ID';
}
