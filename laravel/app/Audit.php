<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Audit extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'audits';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['table_name', 'table_row_id', 'old_values', 'audit_by'];


    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'audits_id');
    }
}
