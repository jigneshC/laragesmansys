<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebSP extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_sp';

    protected $primaryKey = 'ID';
    
}
