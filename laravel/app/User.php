<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;
use App\Traits\BaseModelTrait;

class User extends Authenticatable
{

	//only for user model
    public $is_user = true;

    use Notifiable, HasRoles ,SoftDeletes;


    use  BaseModelTrait;


   
    protected $fillable = [
        'name','first_name','last_name' ,'email', 'password', '_website_id', 'api_token','language','timezone','enable_sms','login_from_date','login_to_date'
    ];

    
    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'created_by', 'updated_by'
    ];

	protected $appends = ['full_name','created_at_tz'];

	
	public function getCreatedAtTzAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
        }
		return $this->created_at;
    }
   
    public function websites()
    {
        return $this->belongsTo('App\WebSite','_website_id', 'id');
    }

    public function accessiblewebsites()
    {
        return $this->belongsToMany(WebSite::class);
    }
    public function assignAccessWebsite($website_id)
    {
        return $this->accessiblewebsites()->save(
            WebSite::whereId($website_id)->firstOrFail()
        );
    }
	public function hasWeb($web_id)
    {
        return $this->accessiblewebsites->contains('id', $web_id);
    }
    /**
     * One User is one person
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function people()
    {
        return $this->hasOne('App\People');
    }


    /**
     * @param User $user
     * @return string
     */
    public static function genApiKey($unique_string = null)
    {
        if (is_null($unique_string)) {
            $unique_string = time();
        }

        $unique_string . str_random(5) . time();

        return md5(uniqid($unique_string, true));
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang()
    {
        return $this->belongsTo('App\Language', 'language', 'lang_code');
    }
    public function next(){
    // get next user
    return User::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  user
        return User::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }
    
    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'users_id');
    }
	public function otpuser()
    {
        return $this->hasOne('App\OtpUser');
    }
}
