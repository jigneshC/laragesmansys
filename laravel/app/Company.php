<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends BaseModel
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','_website_id', 'address', 'city', 'postcode', 'country', 'phone_number_1', 'phone_number_2','code_phone_number_1','code_phone_number_2' ,'phone_type_1', 'phone_type_2', 'active', 'created_by', 'updated_by'
        , 'business_type', 'logo', 'reference','email','geolocation','longitude','latitude'
    ];


    /**
     *
     * A Company have many sites
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sites()
    {
        return $this->hasMany('App\Site');
    }
    
    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'companies_id');
    }

    public function modulecharge()
    {
        return $this->hasMany('App\Companychargablemodule','company_id', 'id');
    }

}
