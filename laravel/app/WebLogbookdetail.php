<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebLogbookdetail extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_logbookdetails';
    protected $primaryKey = 'ID';
}
