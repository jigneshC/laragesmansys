<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectPeople extends Model
{
    //
    
    protected $connection = 'mysql' ;
    protected $table = 'subject_people';

    protected $fillable = ['subject_id','people_id','created_at','updated_at'];
}
