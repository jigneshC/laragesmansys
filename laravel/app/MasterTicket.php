<?php

namespace App;


use App\BaseModel;

class MasterTicket extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'master_tickets';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'site_id', 'subject_id', 'content', '_website_id','user_id' ,'equipment_id','view_on_date','view_on_date_val','view_before_date','activity_time_opt','view_start_date','view_end_date'];

    protected $hidden = [
        'created_by', 'updated_by'
    ];




    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    

}
