<?php
namespace App\Traits;
use Session;

use App\UnitTransaction;
use App\UnitsModule;
use App\WebSite;

use Auth;
use App\Curl\ApiQrcode;

trait BaseMasterModelTrait
{

    public static function boot()
    {
        parent::boot();

        $u = false;

        if (property_exists(self::class, 'is_user')) {
            $u = true;
        }



        static::created(function ($model) {
            BaseMasterModelTrait::resetSessionNotification();
            UnitsModule::generateItemCreateNotification($model->getTable(),"created");
            
            BaseMasterModelTrait::setQrcode($model);
        });
        static::updated(function ($model) {
            BaseMasterModelTrait::resetSessionNotification();
            BaseMasterModelTrait::setQrcode($model);
        });
        static::deleted(function ($model) {
            BaseMasterModelTrait::resetSessionNotification();
        });



        
    }
    public static function resetSessionNotification()
    {
        Session::forget('session_notification_header1');
        Session::forget('session_notification_header2');
        Session::forget('session_notification_set_time');
    }
    public static function setQrcode($model)
    {
        
         
       if((!$model->qrcode_apitoken || $model->qrcode_apitoken=="") && $model->getTable() == "_websites"){
           
            
           $arr = explode(".", $model->domain);
           $sub_dom_name = $arr[0];
           //Session::flash('session_notification_header2',$model->enable_charge);
           $user = [
               'email'=>$sub_dom_name."@gesmansys.com",
               'password'=>$model->id."_".$model->domain
           ];
           
           $api_token = ApiQrcode::register($user);
           $model->qrcode_apitoken = $api_token;
           $model->save();
           
       }
       
       //echo "<pre>";       print_r($model); die;
       $qrcode_module = \config('settings.qrcode.module');
       if($model->id && in_array($model->getTable(),$qrcode_module) && !$model->qrcode){
           $website = WebSite::where("id",$model->id)->first();
           if($website && $website->enable_qrcode && $website->qrcode_apitoken){
               ApiQrcode::getAndStoreQrcode($model, $website->qrcode_apitoken);
           }
       }
    }
/*
    public static function unitCharge($table,$method)
    {
        
        $action = $table."_".$method;
        if($table!="unit_transaction"){
            $unitsModule = UnitsModule::where("action",$action)->first();
            $website = Website::where('id',_WEBSITE_ID)->first();
            if($unitsModule && $website){
                $charge= ['unit_type'=>'debit','reference'=>'self_debit','website_id'=>_WEBSITE_ID,'unit'=>$unitsModule->unit,'comment'=>$action];
                UnitTransaction::create($charge);
                Website::where('id',_WEBSITE_ID)->update(['units'=>$website->units - $unitsModule->unit]);
            }
        }
        
    }*/


    

}