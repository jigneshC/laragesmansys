<?php
namespace App\Traits;


use App\Scope\GlobalWebSiteScope;
use App\UnitTransaction;
use App\UnitsModule;
use App\WebSite;

use Auth;
use App\Curl\ApiQrcode;
trait BaseModelTrait
{

	
	
    public static function boot()
    {
        parent::boot();

        $u = false;

        if (property_exists(self::class, 'is_user')) {
            $u = true;
        }

		
		//self::retrieved(function($model) {$model->appends = array_merge($model->appends, ['created_tz']);});		
		 
        self::addGlobalScope(new GlobalWebSiteScope($u));

        static::creating(function ($model) {
			
			if (Auth::check()) {
                $user = Auth::user();
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            }else if(auth('api')->user()) {
                $user = auth('api')->user();
                $model->created_by = $user->id;
                $model->updated_by = $user->id;
            } else if($model->created_by) {
                $model->created_by = $model->created_by;
                $model->updated_by = $model->created_by;
            }

            if (!\App::runningInConsole()) {

                if (!request('_website_id')) {
                    $model->_website_id = _WEBSITE_ID;
				}
            }

        });

        static::updating(function ($model) {
           //change to Auth::user() if you are using the default auth provider
            if (Auth::check()) {
                $user = Auth::user();
                $model->updated_by = $user->id;
            } else {
                $model->updated_by = 0;
            }
        });

        static::created(function ($model) {
           UnitsModule::generateItemCreateNotification($model->getTable(),"created");
           BaseModelTrait::setQrcode($model);
        });
        static::updated(function ($model) {
            BaseModelTrait::setQrcode($model);
        });
        static::deleted(function ($model) {
            
        });
        
        self::addGlobalScope(new GlobalWebSiteScope($u));

        
    }
    public static function setQrcode($model)
    {
        
       
       $qrcode_module = \config('settings.qrcode.module');
       if($model->_website_id && in_array($model->getTable(),$qrcode_module) && !$model->qrcode){
           $website = WebSite::where("id",$model->_website_id)->first();
           if($website && $website->enable_qrcode && $website->qrcode_apitoken){
               ApiQrcode::getAndStoreQrcode($model, $website->qrcode_apitoken);
           }
       }
    }

    

   /* public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }*/

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updated_by()
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }


    public function _website()
    {
        return $this->belongsTo('App\WebSite', '_website_id', 'id');
    }

    public function scopeWebsite($q,$tbl_name ="")
    {
        if (defined('_MASTER') && _MASTER) {
          return $q->where('_website_id','>',0);
        }else{
			if($tbl_name =="users"){
				$website = Website::findOrFail(_WEBSITE_ID);
				$uids = [];
				if($website){ $uids = $website->users->pluck('id')->toArray(); }
			
				$q->whereIn('id',$uids);
			}else if($tbl_name!=""){
				return $q->where($tbl_name.'._website_id',_WEBSITE_ID);
			}else{
				return $q->where('_website_id',_WEBSITE_ID);
			}
        }
    }

    public function scopeAccessible($q,$permision ="access.ticket.changes.all",$tbl_name ="")
    {
        if(!Auth::user()->can($permision)){

            if($tbl_name =="peoples" ){
                return $q->where($tbl_name.'.user_id',Auth::user()->id);
            }else{
                if($tbl_name!="") return $q->where($tbl_name.'.created_by',Auth::user()->id);
                else return $q->where('created_by',Auth::user()->id);
            }
        }
    }
    public function scopeAccessibleapi($q,$permision ="access.ticket.changes.all",$tbl_name ="",$user)
    {
         if(!$user->can($permision)){

            if($tbl_name =="peoples" || $tbl_name =="tickets"){
                return $q->where($tbl_name.'.user_id',$user->id);
            }else{
                if($tbl_name!="") return $q->where($tbl_name.'.created_by',$user->id);
                else return $q->where('created_by',$user->id);
            }
        }
    }
    /*public function scopeChangeable($q,$permision="access.ticket.changes.all",$tbl_name ="")
    {
        if(!Auth::user()->can($permision)){
            if($tbl_name!="") return $q->where($tbl_name.'.created_by',Auth::user()->id);
            else return $q->where('created_by',Auth::user()->id);
        }
    }*/
	
	public function getCreatedTzAttribute()
    {
		
		if(\Auth::user() && \Auth::user()->timezone)
        {
			return \Carbon\Carbon::parse($this->created_at)->timezone(\Auth::user()->timezone)->format('Y-m-d H:i:s');
			
        }else{
			return \Carbon\Carbon::parse($this->created_at)->timezone('CET')->format('Y-m-d H:i:s');
		}
        
    }

}