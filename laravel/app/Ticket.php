<?php

namespace App;


use App\BaseModel;

class Ticket extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['created_by', 'updated_by','title','site_id', 'subject_id', 'content', 'status', 'user_id', '_website_id', 'equipment_id','view_on_date','view_on_date_val','view_before_date','activity_time_opt','view_start_date','view_end_date'];

    protected $hidden = [
         'created_by', 'updated_by','title'
    ];


	protected $appends = ['is_editable','total_files','created_tz','created_at_tz','last_action','last_action_time'];
	
	public function getLastActionTimeAttribute()
    {
	   
       $evidence = $this->actions;
	   $res = "";
        if($evidence && $evidence->count()){
            $dd = $evidence[0];
			$res =  \Carbon\Carbon::parse($dd->created_at)->diffForHumans();
		}
		
		if($res == "" && $this->created_at != ""){
			$res =  \Carbon\Carbon::parse($this->created_at)->diffForHumans();
		}
		return $res;
    }
	public function getLastActionAttribute()
    {
	   $lang = "fr";
       $evidence = $this->actions;
	   $status_arr = \Lang::get('dashboard.status_dropdown',[],$lang);
       
        $res = "";
        if($evidence && $evidence->count()){
            $dd = $evidence[0];
			$res = $dd->action_text;
        }
		
		if($res == "" && $this->creator){
			$res = \Lang::get('ticket.notification.actioner_has_created_tikcet',['actioner_name'=>$this->creator->full_name],$lang);
		}
		return $res;
    }
	
	
	public function getCreatedAtTzAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
        }
		return $this->created_at;
    }
	
    public static function getStatus()
    {
        return [
            'new' => 'New',
            'open' => 'Open',
            'pending' => 'Pending',
            'on-hold' => 'On-hold',
            'solved' => 'Solved',
            'closed' => 'Closed'
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function site()
    {
        return $this->belongsTo('App\Site');
    }

	
    public function comments()
    {
        return $this->hasMany('App\TicketHistory','ticket_id', 'id')->with('user')->where('history_type','comment');
    }
	public function actions()
    {
        return $this->hasMany('App\TicketHistory','ticket_id', 'id')->with('user')->where('history_type','!=','file')->orderBy("id","desc");
    }
    public function logs()
    {
        return $this->hasMany('App\TicketHistory','ticket_id', 'id')->accessible("access.ticket.comment.all","ticket_history")->with('user');
    }
	public function evidence()
    {
        return $this->hasMany('App\TicketHistory','ticket_id', 'id')->where('history_type','file');
    }

    

    public function getIsEditableAttribute()
    {
       $limit = \config('settings.ticket_editable_time_limit');
       $created_at =  \Carbon\Carbon::parse($this->created_at);
       $now= \Carbon\Carbon::now()->subMinute($limit);
       //echo "-".$now= \Carbon\Carbon::now();
        
        if($created_at > $now){
            return 1;
        }else{
            return 0;
        }
    }
	public function getTotalFilesAttribute()
    {
       $evidence = $this->evidence;
       
        
        if($evidence && $evidence->count()){
            return $evidence->count();
        }else{
            return 0;
        }
    }

    public function next(){
    // get next user
        return Ticket::where('id', '>', $this->id)->orderBy('id','asc')->first();

    }
    public  function previous(){
        // get previous  user
        return Ticket::where('id', '<', $this->id)->orderBy('id','desc')->first();

    }
    
    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'tickets_id');
    }

	public static function getFullDetail($id)
    {
       $ticket = Ticket::where("id",$id)->with(['actions','evidence','user'])->first();
	   if($ticket && $ticket->subject){
		   $ticket->subject_name = $ticket->subject->name;
		   $ticket->file_path = $ticket->subject->file_path;
		   $ticket->instruction_file_count = $ticket->subject->instruction_file_count;
		   $ticket->caller_list_count = $ticket->subject->caller_list_count;
		   $ticket->caller_list = $ticket->subject->caller_list;
		   if($ticket->creator){
			$ticket->creator_name = $ticket->creator->full_name;
		   }else{
			$ticket->creator_name = "";
		   }
		   if($ticket->user){
			$ticket->assign_to = $ticket->user->full_name;
		   }else{
			$ticket->assign_to = "";
		   }
		   
		   $ticket = make_null($ticket);
		   
		   if(isset($ticket['actions'])){
			  
			   $_two = ["desc","history_type","creator_name","action_text","created_at_tz"];
			   $actions =  array_map(function($v) use($_two) { 
			    if(isset($v['user']) && isset($v['user']['full_name'])){
					$v['creator_name'] = $v['user']['full_name'];
				}else{
					$v['creator_name'] = "";
				}
				return array_intersect_key( $v , array_flip( $_two ) );
			   }, $ticket['actions']);
			   $ticket['actions'] = $actions;	
			  
		   }else{
				$ticket['actions'] = [];
		   }
		   
		   if(isset($ticket['evidence'])){
			  
			   $_two = ["file_url","history_type","creator_name","action_text","created_at_tz"];
			   $evidence =  array_map(function($v) use($_two) { 
			    if(isset($v['user']) && isset($v['user']['full_name'])){
					$v['creator_name'] = $v['user']['full_name'];
				}else{
					$v['creator_name'] = "";
				}
				return array_intersect_key( $v , array_flip( $_two ) );
			   }, $ticket['evidence']);
			   $ticket['evidence'] = $evidence;	
			  
		   }else{
				$ticket['evidence'] = [];
		   }
		   
		   unset($ticket['is_editable'],$ticket['_website_id'],$ticket['created_at'],$ticket['updated_at'],$ticket['subject'],$ticket['creator'],$ticket['user'],$ticket['deleted_at'],$ticket['view_on_date'],$ticket['view_on_date_val'],$ticket['view_before_date'],$ticket['activity_time_opt'],$ticket['view_start_date'],$ticket['view_end_date'],$ticket['display_from_time'],$ticket['display_end_time'],$ticket['master_ticket_id']);
		  // unset($ticket->_website_id,$ticket->updated_at,$ticket->subject,$ticket->creator,$ticket->user);
			
				return $ticket;
	   }else{
		   return null;
	   }
    }
	
}
