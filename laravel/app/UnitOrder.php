<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitOrder extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_order';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['website_id', 'unit_package_id','unit','buyer_name','buyer_contact','buyer_email','item_price','price_currency','item_qty','discount','order_total','payment_request_id','payment_type','payment_status','payment_object','unit_package'];


    public function website()
    {
        return $this->belongsTo('App\WebSite', 'website_id', 'id');
    }
    
    
	
	

    
}
