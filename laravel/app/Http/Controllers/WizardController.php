<?php

namespace App\Http\Controllers;

use App\Building;
use App\Company;
use App\Site;
use Illuminate\Http\Request;
use App\Country;
use App\DropdownsType;
use App\Companychargablemodule;
use App\UnitsModule;
use Session;
class WizardController extends Controller
{

    public function index(Request $request)
    {

        $countries = Country::pluck('name', 'code')->prepend('Select', '');


        $business = DropdownsType::whereName('business_type')->with('values')->first();

        $businessTypes = $business->values()->pluck('name', 'id')->prepend('Select', '');


        $phone = DropdownsType::whereName('phone_type')->with('values')->first();

        $phoneTypes = $phone->values()->pluck('name', 'id')->prepend('Select', '');


        if ($request->session()->has('wiz_company')) {
            $_id = $request->session()->get('wiz_company');
            $company = Company::find($_id);
        } else {

            $company = new Company();
        }


        $m = new \stdClass();
        $m->c_ac = 'active';
        $m->s_ac = 'disabled';
        $m->b_ac = 'disabled';

        $chargableModule = UnitsModule::where('action', 'LIKE', "%_module%")->get();

        return view('wizard.company', compact('countries', 'businessTypes', 'phoneTypes', 'company', 'm','chargableModule'));

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeCompany(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
//            'city' => 'required',
//            'logo' => 'required'
        ]);

        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }
        $company = Company::create($requestData);

        if ($company) {
            $request->session()->put('wiz_company', $company->id);
            return redirect('admin/create/site');
        }

        Session::flash('flash_error', 'Something wrong!');
       // flash('Something Wrong', 'error');

        return redirect('admin/create')->withInput();

    }


    /**
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function site(Request $request)
    {

        if (!$request->session()->has('wiz_company')) {

            Session::flash('flash_error', 'Please create comapany first!');

            return redirect('admin/create');
        }


        $countries = Country::pluck('name', 'code')->prepend('Select', '');

        $phone = DropdownsType::whereName('phone_type')->with('values')->first();

        $phoneTypes = $phone->values()->pluck('name', 'id')->prepend('Select', '');


        $companies = Company::pluck('name', 'id')->prepend('Select', '');

        $site_ = DropdownsType::whereName('site_type')->with('values')->first();

        $siteTypes = $site_->values()->pluck('name', 'parent_id')->prepend('Select', '');


        if ($request->session()->has('wiz_site')) {
            $_id = $request->session()->get('wiz_site');
            $site = Site::find($_id);
        } else {

            $site = new Site();
            $site->company_id = $request->session()->get('wiz_company');
        }

        $company = Company::find($request->session()->get('wiz_company'));


        $m = new \stdClass();
        $m->c_ac = 'disabled';
        $m->s_ac = 'active';
        $m->b_ac = 'disabled';

        $website_id  = $company->_website_id;

        return view('wizard.site', compact('countries', 'phoneTypes', 'companies', 'site', 'm', 'siteTypes', 'company','website_id'));
    }


    /**
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeSite(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required',
            'name' => 'required',
            "instructions_tasks_file" => 'mimes:pdf',
            "order_file" => 'mimes:pdf',
            "procedures_file" => 'mimes:pdf',
            "sitemaps_file" => 'mimes:pdf',
        ]);
//        mimes:jpeg,bmp,png
        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }

        $instruction_tasks_file = $this->_uploadFile('instructions_tasks_file', $request);
        if (!is_null($instruction_tasks_file)) {
            $requestData['instructions_tasks_file'] = $instruction_tasks_file;
        }

        $order_file = $this->_uploadFile('order_file', $request);
        if (!is_null($order_file)) {
            $requestData['order_file'] = $order_file;
        }

        $procedures_file = $this->_uploadFile('procedures_file', $request);
        if (!is_null($procedures_file)) {
            $requestData['procedures_file'] = $procedures_file;
        }

        $sitemaps_file = $this->_uploadFile('sitemaps_file', $request);
        if (!is_null($sitemaps_file)) {
            $requestData['sitemaps_file'] = $sitemaps_file;
        }

        $site = Site::create($requestData);

        if ($site) {

            $request->session()->put('wiz_site', $site->id);
            return redirect('admin/create/building');
        }

        Session::flash('flash_error', 'Something Wrong!');

        return redirect('admin/create/site')->withInput();
    }


    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     *
     */
    public function building(Request $request)
    {
        if (!$request->session()->has('wiz_site')) {

            Session::flash('flash_error', 'Please create Site first.');
            return redirect('admin/create/site');
        }


        $sites = Site::pluck('name', 'id')->prepend('Select', '');

        if ($request->session()->has('wiz_building')) {
            $_id = $request->session()->get('wiz_building');
            $building = Site::find($_id);
        } else {

            $building = new Building();
            $building->site_id = $request->session()->get('wiz_site');
        }

        $site = Site::find($request->session()->get('wiz_site'));



        $m = new \stdClass();
        $m->c_ac = 'disabled';
        $m->s_ac = 'disabled';
        $m->b_ac = 'active';

        $website_id = $site->_website_id;

        return view('wizard.building', compact('sites', 'building', 'm', 'site','website_id'));
    }


    public function storeBuilding(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//            'address' => 'required',
            'site_id' => 'required'
        ]);
        $requestData = $request->all();

        $building = Building::create($requestData);

        if ($building) {

            $request->session()->put('wiz_building', $building->id);
            return redirect('/admin/create/success');
        }

        flash('Something wrong!', 'error');
        return redirect('admin/create/building');
    }


    public function success(Request $request)
    {
        if (!$request->session()->has('wiz_building')) {

            flash('Please create Building first', 'info');

            return redirect('admin/create/building');
        }

        $request->session()->forget('wiz_company');
        $request->session()->forget('wiz_site');
        $request->session()->forget('wiz_building');


        return view('wizard.success');
    }



    /**
     *
     * ==============
     */


    //
    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadLogo(Request $request)
    {
        if ($request->hasFile('logo')) {

//            dd($request->file('image'));
            $file = $request->file('logo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }


    public function _uploadFile($filename, Request $request)
    {
        if ($request->hasFile($filename)) {
            $file = $request->file($filename);
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());
            $name = $timestamp . '-' . $file->getClientOriginalName();
            $file->move(public_path() . '/uploads/files/', $name);
            return $name;
        } else {
            return null;
        }
    }

}
