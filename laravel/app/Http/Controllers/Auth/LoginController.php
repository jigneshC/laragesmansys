<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;
use App\User;
use App\OtpUser;
use App\Qrcode;

use Carbon\Carbon;

use Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	protected function sendOtp(Request $request)
    {
		$message = "";
		$otp_exp_minuts = 30;
		$sms = 0;
		$email = 0;
		$rules = [];
		
		if($request->has('send_to_email') || $request->has('send_to_mobile')){
			
		}else{
			$rules['email_phone'] = "required";
		}

		$val_msg = [
			'email_phone.required'=> \Lang::get('auth.email_or_phone_selection_required')
		];
		
        $this->validate($request, $rules,$val_msg);
		
		$s = explode("/",$request->qrcode);
		$code = end($s);
		$qrcode = Qrcode::where("qrcode", $code)->where("refe_table_field_name","users_id")->first();
		
		if ($qrcode)
        {	
			$people = null;
			$user = $qrcode->user;
			
			if($user){
				$people = $user->people;
			}
			try{
				$otp = substr(number_format(time() * rand(),0,'',''),0,5);
				$odata = ["user_id"=>$user->id,"otp"=>$otp,"exp_time"=>Carbon::now()->addMinutes($otp_exp_minuts)];
				$otpuser = OtpUser::updateOrCreate(["user_id"=>$user->id],$odata);
							
				$msg= \Lang::get('auth.here_is_otp_to_login',["otp"=>$otp]);
				
				
				if($request->has('send_to_mobile') &&  $people && $people->phone_number_1 && $people->phone_number_1 != "" && $people->code_phone_number_1 && $people->code_phone_number_1 !=""){
					$send_sms = send_sms($people->code_phone_number_1,$people->phone_number_1, $msg);
					$sms = 1;
					$message =\Lang::get('constant.responce_msg.success_otp_to_login_sent_on_phone');
				}
				
				if($request->has('send_to_email') ){
					
					$lang = $user->language;
					$subject = \Lang::get('mail.subject.login_otp');
					
					\Mail::send('emails.login-otp', compact('user', 'otpuser','lang'), function ($message) use ($user, $subject) {
						$message->to($user->email)->subject($subject);
					});
					
					$email = 1;
					$message = \Lang::get('constant.responce_msg.success_otp_to_login_sent_on_email');
				}
				
				if($sms && $email){
					$message = \Lang::get('constant.responce_msg.success_otp_to_login_sent_on_phone_email');
				}
				Session::flash('flash_success',$message);
				
			}catch(\Exception $exception){
                $message = $exception->getMessage();
				Session::flash('flash_error',\Lang::get('comman.responce_msg.something_went_wrong'));
            }
		}else{
			Session::flash('flash_error',\Lang::get('constant.responce_msg.warning_invalid_qrcode'));
		}
		
		return redirect('login/'.$request->qrcode);
			
		
	}
	
	protected function loginQrcodeCheck($qrcode , Request $request)
    {
		$s = explode("/",$qrcode);
		$code = end($s);
		$qrcode = Qrcode::where("qrcode", $code)->where("refe_table_field_name","users_id")->first();
		
		$error = 1;
		$otpuser = null;
		$message = "";
		if ($qrcode)
        {	
			$people = null;
			$user = $qrcode->user;
				
			if($user){
				$people = $user->people;
				$error = 0;
				
				if(!$request->has('resend')){
					$otpuser = $user->otpuser;
				}
				
				$user_accessible_website = [$user->_website_id];
                foreach ($user->accessiblewebsites as $w){
                    $user_accessible_website[]=$w->id;
                }

                $masterEmail = \config('settings.MASTER_ADMIN');
			
                if (!in_array(_WEBSITE_ID,$user_accessible_website) && $user->email != $masterEmail ) {
                    $error = 1;
					$message = \Lang::get('constant.responce_msg.warning_you_have_no_permission_to_access_this_site');
                }
				
				if($user->login_from_date && $user->login_from_date !="" && Carbon::now() < $user->login_from_date){
					$message =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
					$error = 1;
					
				}else if($user->login_to_date && $user->login_to_date !="" && Carbon::now() > $user->login_to_date){
					$message =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
					$error = 1;
					
				}
			}
		}
		if($error){
			if($message != ""){
				Session::flash('flash_error',$message);
			}else{
				Session::flash('flash_error',\Lang::get('constant.responce_msg.warning_invalid_qrcode'));	
			}
			
			return view('auth.login');
		}else{
			return view('auth.qrcode-login', compact('qrcode','user','people','otpuser'));
		}
		
		
	}
	public function doLoginOtp(Request $request)
    {
		$message = "";
		
    	$rules = array(
            'qrcode' => 'required',
            'otp'=>'required'
        );

		$this->validate($request, $rules);

        
		$user = null;
		$s = explode("/",$request->qrcode);
		$code = end($s);
		$qrcode = Qrcode::where("qrcode", $code)->where("refe_table_field_name","users_id")->first();
		
		
		if ($qrcode)
        {	
			$people = null;
			$user = $qrcode->user;
		}
		
		if ($user)
        {
			$otpuser = $user->otpuser;
			
			if(!$otpuser || ($otpuser && $otpuser->otp != $request->otp)){
				$message  = \Lang::get('constant.responce_msg.warning_otp_token_is_not_valid');
			}else if(Carbon::now() > $otpuser->exp_time){
				$message = \Lang::get('constant.responce_msg.warning_otp_token_expired');
			}else{
				
				$user_accessible_website = [$user->_website_id];
                foreach ($user->accessiblewebsites as $w){
                    $user_accessible_website[]=$w->id;
                }

				$masterEmail = \config('settings.MASTER_ADMIN');
                if (!in_array(_WEBSITE_ID,$user_accessible_website) && $user->email != $masterEmail ) {
                    $error = 1;
					$message = \Lang::get('constant.responce_msg.warning_you_have_no_permission_to_access_this_site');
                }else{
					Auth::login($user);
					$message = \Lang::get('constant.responce_msg.success_login');	
					$otpuser->delete();
					
					if($request->has('login_to_option') && $request->login_to_option == "ticket"){
						return redirect('admin');
					}
					if($request->has('login_to_option') && $request->login_to_option == "duty"){
						return redirect('admin/duty');
					}
					if($request->has('login_to_option') && $request->login_to_option == "planned_ticket"){
						return redirect('admin/planned-activity');
					}else{
						return redirect('/');
					}
					
				}
				
			}
		}else{
			$message  =\Lang::get('constant.responce_msg.warning_invalid_qrcode');
		}
		
		Session::flash('flash_error',$message);
		return redirect()->back();
    }
    protected function loginA(Request $request)
    {
        $message = "";

        $masterEmail = \config('settings.MASTER_ADMIN');

        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );
        $credentials_site = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );

        $user = User::where("email", $request->email)->first();

        if ($user) {
			
			if($user->login_from_date && $user->login_from_date !="" && Carbon::now() < $user->login_from_date){
				$message = \Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
					
			}else if($user->login_to_date && $user->login_to_date !="" && Carbon::now() > $user->login_to_date){
				$message = \Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
				
			}else if (Hash::check($request->password, $user->password)) {

                // master user can login in any site
                if ($masterEmail == $request->email && Auth::attempt($credentials)) {
					return redirect()->intended($this->redirectPath());
                }

                // in master website only site user can login
                if (defined('_MASTER') && _MASTER && Auth::attempt($credentials_site)) {
                    return redirect()->intended($this->redirectPath());
                }


                $user_accessible_website = [$user->_website_id];
                foreach ($user->accessiblewebsites as $w){
                    $user_accessible_website[]=$w->id;
                }

                // in non-master website  site user can login and also user who have access to site can login
                if (!_MASTER && in_array(_WEBSITE_ID,$user_accessible_website) && Auth::attempt($credentials)) {
                    return redirect()->intended($this->redirectPath());
                }

                $message = __('Invalid username or password');

            } else {
                $message = __('Invalid username or password');
            }
        } else {
            $message = __('Invalid username or password');
        }

        Session::flash('flash_error', $message);

        return redirect()->back()->withInput();
    }
    protected function credentials(Request $request)
    {
        $masterEmail = env('MASTER_ADMIN', null);

        if($masterEmail == $request->email){
            return array_merge($request->only($this->username(), 'password'), []);
        }else{
            if (defined('_MASTER') && _MASTER) {
                return array_merge($request->only($this->username(), 'password'), []);
            }else{
                return array_merge($request->only($this->username(), 'password'), []);
               // return array_merge($request->only($this->username(), 'password'), []);
            }
        }


    }
}
