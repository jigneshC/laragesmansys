<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Subject;
use Illuminate\Http\Request;
use Session;
use App\Site;
use App\People;
use App\Service;
use App\Company;
use App\Companychargablemodule;
use Yajra\Datatables\Datatables;

use App\DropdownsType;

use Illuminate\Support\Facades\File;
class SubjectsController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:access.subjects')->except('search','getCaller','getSubjectCaller','getFileView');
        $this->middleware('permission:access.subject.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.subject.create')->only(['create', 'store']);
        $this->middleware('permission:access.subject.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.subjects.index');
    }

    public function datatable(Request $request) {

        $record = Subject::accessible("access.subject.all","subjects")->with('site','services','_website');
         if($request->has('filter_website')){
            $record->where('subjects._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
		
        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');

        $peoples = People::accessible("access.people.all","peoples")->pluck('first_name', 'id');
		

		$geolocation_ids = DropdownsType::getPluckValue('geolocation_id')->prepend('Select Geolocation Id', '');

        return view('admin.subjects.create', compact('sites', 'peoples','services','geolocation_ids'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

       // return $request->all();

        $this->validate($request, [
            'name' => 'required',
            "file" => 'mimes:pdf',
            "subject_type"=>"required",
            "company_id"=>"required"
        ]);

        $requestData = $request->all();

        if($request->subject_type == "planned_activity" || $request->subject_type == "duty"){
            if(_MASTER){
                $webid = $request->_website_id;
            }else{
                $webid = _WEBSITE_ID;
            }
            $subject = Subject::where('subjects._website_id',$webid)->where('subjects.subject_type',$request->subject_type)
            ->join('services','services.id','=','.service_id')
            ->where('services.company_id',$request->company_id)
            ->count();
            if($subject >0){
                Session::flash('flash_message', 'Already Subject exist with selected compony and type '.$request->subject_type);

                return redirect()->back()->withInput();
            }
        }
		
		if ($request->has('is_geolocation') &&  $request->get('is_geolocation') != '' &&  $request->get('is_geolocation') != 0 && $request->has('geolocation_id') && $request->get('geolocation_id') != '') {
            $requestData['geolocation_id']= $request->get('geolocation_id');
        }else{
			$requestData['geolocation_id']= 0;
		}
        
       

        $pdf = $this->_uploadFile('file', $request);
        if (!is_null($pdf)) {
            $requestData['file'] = $pdf;
        }

        $subject = Subject::create($requestData);

        $this->assignCallerList($subject, $request);

        Session::flash('flash_message', 'Subject added!');

        return redirect('admin/subjects');
    }


    /**
     * @param Subject $subject
     * @param Request $request
     */
    public function assignCallerList(Subject $subject, Request $request)
    {
        $caller_list = $request->input('caller_list');

        $list = explode(',', $caller_list);

        if (!empty($list)) {
            \DB::table('subject_people')->where('subject_id', $subject->id)->delete();
            foreach ($list as $string) {
                //10:9
                $a = explode(':', $string);
                $people_id = (int)$a[0];
                $priority = (int)isset($a[1]) ? $a[1] : 0;
//                $subject->caller_list()->detach($people_id);
//                $subject->caller_list()->attach($people_id, ['priority' => $priority]);
                \DB::table('subject_people')->insert([
                    'subject_id' => $subject->id,
                    'people_id' => $people_id,
                    'priority' => $priority,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subject = Subject::accessible("access.subject.all","subjects")->with('caller_list.availability_time')->where("id",$id)->first();
			
        if(!$subject){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

      //  return $subject;
        return view('admin.subjects.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');

        $peoples = People::accessible("access.people.all","peoples")->pluck('first_name', 'id');

        $subject = Subject::accessible("access.subject.changes.all","subjects")->where("id",$id)->first();

		$geolocation_ids = DropdownsType::getPluckValue('geolocation_id');
		
        if(!$subject){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        //todo users assigned and plucked
        $subject->xx = $subject->caller_list()->pluck('id');

        $users = $subject->caller_list;

        $website_id = $subject->_website_id;
        
        $comp = $this->getComponeyFromService($subject->service_id);
        if($comp && $comp->id){
            $subject->company_id = $comp->id;
        }

        return view('admin.subjects.edit', compact('subject', 'sites', 'peoples', 'users','services','website_id','geolocation_ids'));
    }
    public function getComponeyFromService($service_id){
        
        $s_data = Service::select('companies.*')->where("services.id",$service_id);
        $s_data->join('companies','companies.id','=','services.company_id');
        
        $res = $s_data->first();
        
        return $res;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            "file" => 'mimes:pdf',
        ]);
        $requestData = $request->all();

        $subject = Subject::accessible("access.subject.changes.all","subjects")->where("id",$id)->first();
        if(!$subject){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
        if($request->subject_type == "planned_activity" || $request->subject_type == "duty"){
            if(_MASTER){
                $webid = $request->_website_id;
            }else{
                $webid = _WEBSITE_ID;
            }
            $subjectcnt = Subject::select("subjects.*")->where('subjects._website_id',$webid)->where('subjects.subject_type',$request->subject_type)
            ->join('services','services.id','=','subjects.service_id')
            ->where('services.company_id',$request->company_id)
            ->where('subjects.id',"!=",$id)
            ->count();
            
            if($subjectcnt >0){
                Session::flash('flash_message', 'Already Subject exist with type '.$request->subject_type);

                return redirect()->back()->withInput();
            }
        }

        $pdf = $this->_uploadFile('file', $request);
        if (!is_null($pdf)) {
            $requestData['file'] = $pdf;
            $this->removeFile($subject->file);
        }


        $subject->update($requestData);

        $this->assignCallerList($subject, $request);

        Session::flash('flash_message', 'Subject updated!');
        return redirect('admin/subjects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $subject = Subject::accessible("access.subject.changes.all","subjects")->where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$subject && _MASTER){
			$subject = Subject::withTrashed()->accessible("access.subject.changes.all","subjects")->where("id",$id)->first();
			$is_hard_delete = 1;
		}
		
        if($subject){
            $record = \App\Setting::deleteParentLevel($id,'subject',$is_hard_delete);
			 
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/subjects');
        }
    }


    /**
     * @param $subject_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFileView($subject_id)
    {

        $subject = Subject::find($subject_id);

        if (!$subject->file) {
            return '';
        }
        return view('admin.subjects.fileview', compact('subject'));
    }


    public function _uploadFile($filename, Request $request)
    {
        if ($request->hasFile($filename)) {

//            dd($request->file('image'));
            $file = $request->file($filename);
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());


//            $_ext = $file->getClientOriginalExtension();
//
//            if (strtolower($_ext) != 'pdf') {
//                return null;
//            }


            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/files/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeFile($fileName)
    {
        $image_path1 = public_path()."/uploads/files/".$fileName;

        if ($fileName && $fileName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }


    public function getCaller(Request $request)
    {
        $ids = $request->input('ids');

        $users = People::whereIn('id', $ids)->get();

        return view('admin.subjects.callerAjax', compact('users'));
    }

    public function getSubjectCaller($id)
    {
        $subject = Subject::with('caller_list.availability_time')->where("id",$id)->first();

        $result = array();
        if($subject){
            $result['subject'] = $subject;
            $result['caller_list'] = $subject->caller_list;
        }

        return response()->json($result,200);

    }

    public function search(Request $request)
    {
        $result = array();

        $data =Subject::select('id','name','service_id')->with('services');

        if ($request->has('filter_subject') &&  $request->get('filter_subject') != '') {
            $data->where('name', 'LIKE', "%$request->filter_subject%");
        }
        if ($request->has('service_id') &&  $request->get('service_id') != '') {
			$enable_m_ids = explode(",",$request->service_id);
            $data->whereIn('service_id',$enable_m_ids);
        }
        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $data->where('_website_id',$request->_website_id);
        }
        if ($request->has('site_id') &&  $request->get('site_id') != '') {
            $s_ob =  Site::whereId($request->get('site_id'))->with('services')->first();
            $s_ids = [];
            if($s_ob && $s_ob->services){
                $s_ids = $s_ob->services->pluck("id");
            }
            // echo "<pre>"; print_r($s_ids);
            $data->whereIn('service_id',$s_ids);
            
        }
        if ($request->has('company_id') &&  $request->get('company_id') != '') {
            $s_ids = Service::where("company_id",$request->company_id)->pluck("id");
            $data->whereIn('service_id',$s_ids);
        }
        if ($request->has('tickettype') &&  $request->get('tickettype') == 'ticket') {
            $data->where('subject_type',"!=","planned_activity");
            $data->where('subject_type',"!=","duty");
        }
        if ($request->has('tickettype') &&  $request->get('tickettype') == 'planned_activity') {
            $data->where('subject_type',"=","planned_activity");
        }

        $result['data'] = $data->orderby("name","ASC")->get();

        return response()->json($result,200);
    }
    public function getSubjectType(Request $request)
    {
        $result = array();
        $chargable_module =Companychargablemodule::where('company_id',$request->company_id)->get();
        
        
        $subject_type =  trans('subject.subject_type');
        
        if(!$chargable_module->where("action","duty_module")->first()){
            unset($subject_type['duty']);
        }
        if(!$chargable_module->where("action","plannedactivity_module")->first()){
            unset($subject_type['planned_activity']);
        }
        
        $result['data'] = $subject_type;
        
       
        
        return response()->json($result,200);
    }
    public function filter_by_website(Request $request)
    {
        $result = array();
        $enable_m_ids =Companychargablemodule::where('action', "subjects_module")->pluck("company_id");

        $s_data = Service::select('services.*','services.company_id');
        $s_data->join('companies','companies.id','=','services.company_id');
        $s_data->whereIn('companies.id',$enable_m_ids);

        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $p_data = People::select('id','first_name','last_name');
            $p_data->where('_website_id',$request->_website_id);
            $result['data']['people'] = $p_data->get();

            $s_data->where('services._website_id',$request->_website_id);
            $result['data']['service'] = $s_data->get();
            
            $co_data = Company::select('id','name')->whereIn("id",$enable_m_ids);
            $co_data->where('companies._website_id',$request->_website_id);
            $result['data']['compoanies'] = $co_data->get();
        }

        return response()->json($result,200);
    }


}
