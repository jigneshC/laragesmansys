<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Site;
use App\Building;
use App\Company;
use Illuminate\Http\Request;
use Session;
use App\DropdownsType;
use Illuminate\Support\Facades\File;
use App\UnitsModule;
use App\Companychargablemodule;

use Yajra\Datatables\Datatables;

class CompaniesController extends Controller
{

    public function __construct()
    {
//                dd(\App::getLocale());
        $this->middleware('permission:access.companies');
        $this->middleware('permission:access.company.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.company.create')->only(['create', 'store']);
       // $this->middleware('permission:access.company.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.companies.index');
    }

    public function datatable(Request $request) {

        $record = Company::accessible("access.company.all","companies")->with('_website');
        if($request->has('filter_website')){
            $record->where('companies._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $countries = Country::pluck('name', 'code')->prepend('Select', '');

        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $businessTypes = DropdownsType::getPluckValue('business_type');
        
        $chargableModule = UnitsModule::where('action', 'LIKE', "%_module%")->get();
        
        return view('admin.companies.create', compact('countries', 'businessTypes', 'phoneTypes','chargableModule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
            'name' => 'required',
//            'city' => 'required',
//            'logo' => 'required'
        ]);
        $requestData = $request->all();


        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }

        $company = Company::create($requestData);
        
        if($company){
            $chargableModule = UnitsModule::where('action', 'LIKE', "%_module%")->get();
           // echo "<pre>";            print_r($chargableModule->toArray()); exit;
            foreach ($chargableModule as $string) {
                if($string->action != "duty_module" && $string->action != "plannedactivity_module"){
                    $inputData = ["action"=>$string->action,"company_id"=>$company->id];
                    Companychargablemodule::create($inputData);
                }
            }
        }
        
        Session::flash('flash_message', __('Company added!'));

        return redirect('admin/company-modules/'.$company->id);
    }
    
    public function chargableModule($id)
    {
        
        $company = Company::accessible("access.company.changes.all","companies")->where("id",$id)->first();

        if(!$company){
            Session::flash('flash_message', 'No Access !');
            return redirect('admin/companies');
        }
        $chargableModule = UnitsModule::where('action', 'LIKE', "%_module%")->get();
        $selected_chargable_module = Companychargablemodule::where("company_id",$company->id)->pluck("action")->toArray();
     
        return view('admin.companies.company-module', compact('company','chargableModule','selected_chargable_module'));
    }

    public function assignChargableModule($company_id, Request $request)
    {
        
        
        $company = Company::accessible("access.company.changes.all","companies")->where("id",$company_id)->first();

        if(!$company){
            Session::flash('flash_message', 'No Access !');
            return redirect('admin/companies');
        }
        
        $actions_list = $request->input('actions');

        Companychargablemodule::where('company_id', $company->id)->delete();
        
        if (!empty($actions_list)) {
            foreach ($actions_list as $string) {
                $inputData = ["action"=>$string,"company_id"=>$company->id];
                Companychargablemodule::create($inputData);
            }
        }
        Session::flash('flash_message', __('Company updated!'));

        return redirect('admin/companies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $company = Company::accessible("access.company.all","companies")->with('sites')->where("id",$id)->first();

        if(!$company){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $chargableModule = UnitsModule::where('action', 'LIKE', "%_module%")->get();
        $company = Company::accessible("access.company.changes.all","companies")->where("id",$id)->first();

        if(!$company){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
        $countries = Country::pluck('name', 'code')->prepend('Select', '');


        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $businessTypes = DropdownsType::getPluckValue('business_type');

        $website_id = $company->_website_id;
        $selected_chargable_module = Companychargablemodule::where("company_id",$company->id)->pluck("action");

        return view('admin.companies.edit', compact('company', 'countries', 'businessTypes', 'phoneTypes','website_id','chargableModule','selected_chargable_module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//            'city' => 'required'
        ]);
        $requestData = $request->all();

  //      echo "<pre>";        print_r($requestData); exit;
        $company = Company::accessible("access.company.changes.all","companies")->where("id",$id)->first();

		
        if(!$company){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
            $this->removeImage($company->logo);
        }
		$ischange_webid = 0;
		
		$oldweb_id = $company->_website_id;
	//	$this->changeCompanyDomain($id,$oldweb_id,0);exit;
		$res = $company->update($requestData);
		
		if($res && $request->has('_website_id') && $request->_website_id != $oldweb_id){
			$newweb_id = $request->_website_id;
			$this->changeCompanyDomain($id,$oldweb_id,$newweb_id,$request);
			
			$request->session()->put('dom_change_company_id',$id);
			$request->session()->put('dom_change_oldweb_id',$oldweb_id);
			$request->session()->put('dom_change_newweb_id',$newweb_id);
			
			Session::flash('flash_message', __('Company updated!'));
			return redirect('admin/company-users/'.$company->id);
		}
        
        Session::flash('flash_message', __('Company updated!'));

        return redirect('admin/company-modules/'.$company->id);
    }

	public function changeCompanyUserDomain($id, Request $request)
    {
		if ($request->session()->has('dom_change_company_id') && $request->session()->has('dom_change_oldweb_id') && $request->session()->has('dom_change_newweb_id')) {
            $id = $request->session()->get('dom_change_company_id');
			$oldweb_id = $request->session()->get('dom_change_oldweb_id');
			$newweb_id = $request->session()->get('dom_change_newweb_id');
			$sessionOb = $request->session()->get('dom_change_module_ids');
			$sessionOb = json_decode($sessionOb,true);
			
			$company = Company::where("id",$id)->first();
			$all_user = [];
				
			if($sessionOb && isset($sessionOb['site_ids']))
			{
				$site_ids = $sessionOb['site_ids'];
				$buildings_id = $sessionOb['buildings_id'];
				$service_id = $sessionOb['service_id'];
				$subject_id = $sessionOb['subject_id'];
				$master_ticket_id = $sessionOb['master_ticket_id'];
				$ticket_id = $sessionOb['ticket_id'];
				
				$manager_userd = \App\Service::whereIn('id',$service_id)->pluck("manager_id")->toArray();
				
				$service_user_id = [];
				$service_obj = \App\Service::whereIn('id',$service_id)->get();
				foreach ($service_obj as $so){
					foreach ($so->peoples as $po){
						$service_user_id[] = $po->user_id;
					}
				}
				
				$caller_list_user = [];
				$subjects_obj = \App\Subject::whereIn('id',$service_id)->get();
				foreach ($subjects_obj as $so2){
					foreach ($so2->caller_list as $po2){
						$caller_list_user[] = $po2->user_id;
					}
				}
				
				
				$all_user = array_merge($manager_userd,$service_user_id,$caller_list_user);
				
				
				
			}
			
			//dd($all_user);
			
			$webuser = \App\User::whereNotIn("id",$all_user)->where('_website_id',$oldweb_id)->get();
			
			$webuser_used = \App\User::whereIn("id",$all_user)->where('_website_id',$oldweb_id)->get();
			
			$oldweb = \App\Website::where('id',$oldweb_id)->withTrashed()->first();
			$newweb = \App\Website::where('id',$newweb_id)->first();
			
			return view('admin.companies.company-users', compact('webuser','webuser_used','oldweb','newweb','company'));
			
        } else {
			return redirect('admin/company-modules/'.$id);
		}
		
	}
	public function storeCompanyUserDomain($id, Request $request)
    {
		$company = Company::where("id",$id)->first(); 
		if ( $company && $request->session()->has('dom_change_company_id') && $request->session()->has('dom_change_oldweb_id') && $request->session()->has('dom_change_newweb_id')) {
            $id = $request->session()->get('dom_change_company_id');
			$oldweb_id = $request->session()->get('dom_change_oldweb_id');
			$newweb_id = $request->session()->get('dom_change_newweb_id');
			$sessionOb = $request->session()->get('dom_change_module_ids');
			$sessionOb = json_decode($sessionOb,true);
			
			$oldweb = \App\Website::where('id',$oldweb_id)->first();
			$newweb = \App\Website::where('id',$newweb_id)->first();
			
			
			if(count($request->users)){
				
				foreach ($request->users as $uid) {
					\App\User::where('id',$uid)->update(['_website_id'=>$newweb_id]);
					\App\People::where('user_id',$uid)->update(['_website_id'=>$newweb_id]);
					
					$user = \App\User::where('id',$uid)->first();
					foreach ($newweb->roles as $role) {
						if(!$user->hasRole($role->name)){
							$user->assignRole($role->name);
						}
					}
				
					$user->accessiblewebsites()->detach($newweb_id);
					$user->assignAccessWebsite($newweb_id);
					
					
				}
			
				Session::flash('flash_message', __('Users updated!'));
			}
			
		}	
		
		return redirect('admin/company-modules/'.$id);
	}
    public function changeCompanyDomain($id,$oldweb_id,$newweb_id,$request){
		
		$site_ids = \App\Site::where('company_id',$id)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		$buildings_id = \App\Building::whereIn('site_id',$site_ids)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		$service_id = \App\Service::where('company_id',$id)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		$subject_id = \App\Subject::whereIn('service_id',$service_id)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		$master_ticket_id = \App\MasterTicket::whereIn('subject_id',$subject_id)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		$ticket_id = \App\Ticket::whereIn('subject_id',$subject_id)->where('_website_id',$oldweb_id)->pluck("id")->toArray();
		
		
		\App\Site::whereIn('id',$site_ids)->update(['_website_id'=>$newweb_id]);
		\App\Building::whereIn('id',$buildings_id)->update(['_website_id'=>$newweb_id]);
		\App\Service::whereIn('id',$service_id)->update(['_website_id'=>$newweb_id]);
		\App\Subject::whereIn('id',$subject_id)->update(['_website_id'=>$newweb_id]);
		\App\MasterTicket::whereIn('id',$master_ticket_id)->update(['_website_id'=>$newweb_id]);
		\App\Ticket::whereIn('id',$ticket_id)->update(['_website_id'=>$newweb_id]);
		
		$sessionOb = [
			'site_ids'=>$site_ids,
			'buildings_id'=>$buildings_id,
			'service_id'=>$service_id,
			'subject_id'=>$subject_id,
			'master_ticket_id'=>$master_ticket_id,
			'ticket_id'=>$ticket_id,
		];
		
		$request->session()->put('dom_change_module_ids',json_encode($sessionOb));
		
		$subject = "Update company (".$id.") domain from ".$oldweb_id." to ".$newweb_id;
		\Mail::send('emails.update-company', with(['ids'=>$sessionOb]), function ($message) use ($subject) {
				$message->to("jignesh.citrusbug@gmail.com")->subject($subject);
			});
		
		
	}
    public function destroy($id,Request $request)
    {
	
        $company = Company::where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$company && _MASTER){
			$company = Company::withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}

        if($company){
            
			$record = \App\Setting::deleteParentLevel($id,'company',$is_hard_delete);
			
			if($record){
				$result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
				$result['code'] = 200;
			}else{
				$result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
				$result['code'] = 400;
			}
		}else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/companies');
        }

    }

    //
    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadLogo(Request $request)
    {
        if ($request->hasFile('logo')) {

//            dd($request->file('image'));
            $file = $request->file('logo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }
    
    public function search(Request $request)
    {
        $result = array();

        $data =Company::select('id','name');

        if ($request->has('filter_company') &&  $request->get('filter_company') != '') {
            $data->where('name', 'LIKE', "%$request->filter_company%");
        }
        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $data->where('_website_id',$request->_website_id);
        }

        $result['data'] = $data->get();

        return response()->json($result,200);
    }
}
