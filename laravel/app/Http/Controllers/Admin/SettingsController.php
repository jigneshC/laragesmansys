<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Setting;
use Illuminate\Http\Request;
use Session;
use Auth;

use Yajra\Datatables\Datatables;

class SettingsController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:access.settings');
        $this->middleware('permission:access.setting.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.setting.create')->only(['create', 'store']);
        $this->middleware('permission:access.setting.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //echo Auth()->user()->email; exit;
        return view('admin.settings.index');
    }

    public function datatable(Request $request) {

        $record = Setting::with('website','subjects')->get();
        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $rules = ["name"=>"required"];
        if($request->has("type") && $request->type=="subject_for_duty"){
            $rules["_website_id"]="required";
            $rules["subject_id"]="required";
        }else{
            $rules["value"]="required";
            $rules["key"]="required|unique:settings";
        }
        $this->validate($request,$rules);
        
        
        $requestData = $request->all();
        
        if($request->has("type") && $request->type=="subject_for_duty"){
            $requestData["key"]=$request->_website_id;
            $requestData["value"]=$request->subject_id;
            Setting::where("key",$request->_website_id)->delete();
        }

        Setting::create($requestData);
        
        

        Session::flash('flash_message', __('Setting added!'));

        return redirect('admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $setting = Setting::findOrFail($id);
        return view('admin.settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        $website_id = $setting->_website_id;
        
        return view('admin.settings.edit', compact('setting','website_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $rules = ["name"=>"required"];
        if($request->has("type") && $request->type=="subject_for_duty"){
            $rules["_website_id"]="required";
            $rules["subject_id"]="required";
        }else{
            $rules["value"]="required";
            $rules["key"]='required|unique:settings,key,' . $id;
        }
        $this->validate($request,$rules);
        $requestData = $request->all();

        if($request->has("type") && $request->type=="subject_for_duty"){
            $requestData["key"]=$request->_website_id;
            $requestData["value"]=$request->subject_id;
            Setting::where("key",$request->_website_id)->where("id","!=",$id)->delete();
        }
        
        $setting = Setting::where("id",$id)->first();
        $setting->update($requestData);

        Session::flash('flash_message', __('Setting updated!'));

        return redirect('admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = Setting::findOrFail($id);

        if($ob){
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/settings');
        }

    }
}
