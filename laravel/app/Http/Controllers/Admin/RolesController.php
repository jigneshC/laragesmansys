<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\RoleWebsite;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Session;
use App\Permission;
use App\WebSite;
use Yajra\Datatables\Datatables;

class RolesController extends Controller
{


    public function __construct()
    {
        $this->middleware('permission:access.roles');
        $this->middleware('permission:access.role.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.role.create')->only(['create', 'store']);
        $this->middleware('permission:access.role.delete')->only('destroy');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.roles.index');
    }
    public  function getWebsiteRole($website_id=0){
        $roles= [];

        $_websites = WebSite::with('roles')->website();
        if($website_id !=0){
            $_websites->where('id',$website_id);
        }
        $websites = $_websites->get();

        foreach ($websites as $website){
            foreach ($website->roles as $role){
                $roles[]=$role->id;
            }
        }
        return $roles;
    }
    public function datatable(Request $request) {

       
        $roles= $this->getWebsiteRole(0);

        $record = Role::with('websites')->whereIn('id',$roles)->lower();
        // For domain filter
        if($request->has('filter_website')){
           $roles= $this->getWebsiteRole($request->filter_website);
           $record->whereIn('id',$roles);
        }

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $permissions = Permission::with('child')->parent()->get();

        $isChecked = function ($name) {
            return '';
        };

        return view('admin.roles.create', compact('permissions', 'isChecked'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'permissions' => 'required']);


        $role = Role::create($request->all());

        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }

        if ($request->has('_website_ids')){
            foreach ($request->_website_ids as $website) {
                $web = WebSite::where('id',$website)->first();
                if($web && !$web->hasRole($role->name)){
                    $web->assignRole($role->name);
                }
            }
        }else{
            $web = WebSite::where('id',_WEBSITE_ID)->first();
            $web->assignRole($role->name);
        }

        Session::flash('flash_message', __('Role added!'));

        return redirect('admin/roles/' . $role->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $roles= $this->getWebsiteRole(0);

        $role = Role::with('main_permission.child')->whereIn('id',$roles)->lower()->findOrFail($id);

        $permissions = Permission::with('child')->parent()->get();

        $isChecked = function ($name) use ($role) {

            if ($role->permissions->contains('name', $name)) {
                return 'checked';
            }
            return '';
        };


        return view('admin.roles.show', compact('role', 'permissions', 'isChecked'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $roles= $this->getWebsiteRole(0);

        $role = Role::with('permissions')->whereIn('id',$roles)->lower()->findOrFail($id);

        $website_ids= RoleWebsite::where('role_id',$id)->get()->pluck('web_site_id')->toArray();

        $permissions = Permission::with('child')->parent()->get();

        $isChecked = function ($name) use ($role) {

            if ($role->permissions->contains('name', $name)) {
                return 'checked';
            }
            return '';
        };

        return view('admin.roles.edit', compact('role', 'permissions', 'isChecked','website_ids'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'permissions' => 'required']);

        $role = Role::findOrFail($id);

        $role->update($request->all());

        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }



        RoleWebsite::where('role_id',$id)->delete();
        if ($request->has('_website_ids')){
            foreach ($request->_website_ids as $website) {
                $web = WebSite::where('id',$website)->first();
                if($web && !$web->hasRole($role->name)){
                    $web->assignRole($role->name);
                }
            }
        }else{
            $web = WebSite::where('id',_WEBSITE_ID)->first();
            $web->assignRole($role->name);
        }
        Session::flash('flash_message', __('Role updated!'));

        return redirect('admin/roles/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $ob = Role::whereId($id)->first();

        if($ob){
            $ob->permissions()->detach();
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/roles');
        }

    }
}
