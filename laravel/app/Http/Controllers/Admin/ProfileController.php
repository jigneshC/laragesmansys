<?php

namespace App\Http\Controllers\Admin;

use App\Language;
use App\People;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Illuminate\Support\Facades\Lang;

class ProfileController extends Controller
{


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

//        return $user->lang;
        return view('admin.profile.index', compact('user'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $user = Auth::user();

        $languages = Language::active()->select(
            \DB::raw("CONCAT(name,' ( ',lang_code,' ) ') AS full_name"), 'lang_code')
            ->pluck('full_name', 'lang_code')->prepend('Select..', '');


        $phone = \App\DropdownsType::whereName('phone_type')->with('values')->first();

        $phoneTypes = $phone->values()->pluck('name', 'parent_id')->prepend('Select', '');

//        $services = Service::pluck('name', 'id');//->prepend('Select..', '');
        
        $id = $user->id;
        $peopleo = People::select("title","first_name","last_name","quality","phone_number_1","phone_number_2","code_phone_number_1","code_phone_number_2","phone_type_1","phone_type_2","emergency_password","availability","status_text","photo","active")
                ->where("user_id",$id)->first();
        
        if($peopleo){
            $peopleob = $peopleo->toArray();
            foreach ($peopleob as $key => $value) {
                $user->{$key} = $value;
            }
        }


        return view('admin.profile.edit', compact('user', 'languages', 'phoneTypes'));
    }


    /**
     * @param Request $request
     */
    public function update(Request $request)
    {


        $normal = [
            'language' => 'required',
        ];

        $people = [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number_1' => 'required',
            'phone_number_2' => 'required',
            'phone_type_1' => 'required',
            'phone_type_2' => 'required',
            'photo' =>'image',
            'timezone' =>'required',

        ];
        $user = Auth::user();


        if ($user->people) {
            $this->validate($request, array_merge($normal, $people));
        } else {
            $this->validate($request, $normal);
        }


        $user->name = $request->first_name." ".$request->last_name;
        $user->language = $request->language;
        $user->timezone = $request->timezone;
        $user->enable_sms = $request->enable_sms;

        if ($user->save()) {
            if ($user->people) {
                //update people table
                $people = $user->people()->first();

                $people->first_name = $request->first_name;
                $people->last_name = $request->last_name;
                $people->phone_number_1 = $request->phone_number_1;
                $people->phone_number_2 = $request->phone_number_2;
                $people->phone_type_1 = $request->phone_type_1;
                $people->phone_type_2 = $request->phone_type_2;
                $people->code_phone_number_1 = $request->code_phone_number_1;
                $people->code_phone_number_2 = $request->code_phone_number_2;


                $name = $this->uploadPhoto($request);

                if (!is_null($name)) {
                    $people->photo = $name;
                }

                $people->save();


            }
        }


        flash(__('Your profile updated successfully.'), 'success');

        return redirect()->back();

    }


    //
    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword()
    {
        return view('admin.profile.changePassword');
    }

    /**
     * @param Request $request
     */
    public function updatePassword(Request $request)
    {

        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);


        $cur_password = $request->input('current_password');


        $user = Auth::user();

        if (Hash::check($cur_password, $user->password)) {

            $user->password = Hash::make($request->input('password'));
            $user->save();

            flash('Password changed successfully.', 'success');

            return redirect()->route('profile.index');

        } else {
            $error = array('current-password' => __('Please enter correct current password'));
//            return response()->json(array('error' => $error), 400);
            return redirect()->back()->withErrors($error);
        }

        flash('Something wrong. Please try again latter.', 'error');

        return redirect()->back();


    }

}
