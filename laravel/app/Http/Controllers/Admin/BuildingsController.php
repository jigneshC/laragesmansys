<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Building;
use Illuminate\Http\Request;
use Session;
use App\Site;

use App\Company;
use App\Country;
use App\Companychargablemodule;
use App\DropdownsType;
use Yajra\Datatables\Datatables;

class BuildingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.buildings');
        $this->middleware('permission:access.building.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.building.create')->only(['create', 'store']);
        $this->middleware('permission:access.building.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.buildings.index');
    }

    public function datatable(Request $request) {

        $record = Building::accessible("access.building.all","buildings")->with('site','_website');
        if($request->has('filter_website')){
            $record->where('buildings._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
		
        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');
		
		$phoneTypes = DropdownsType::getPluckValue('phone_type');
		$countries = Country::pluck('name', 'code')->prepend('Select', '');

        return view('admin.buildings.create', compact('sites','phoneTypes','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//            'address' => 'required',
            'site_id' => 'required'
        ]);
        $requestData = $request->all();

        Building::create($requestData);

        Session::flash('flash_message', 'Building added!');

        return redirect('admin/buildings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $building = Building::accessible("access.building.all","buildings")->where("id",$id)->first();
       
       // return $building->sites;exit;
        if(!$building){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.buildings.show', compact('building'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $building = Building::accessible("access.building.changes.all","buildings")->where("id",$id)->first();

        if(!$building){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');


        $comp = $this->getComponeyFromSite($building->site_id);
        if($comp && $comp->id){
            $building->company_id = $comp->id;
        }
        
        $website_id = $building->_website_id;
		
		$phoneTypes = DropdownsType::getPluckValue('phone_type');
		$countries = Country::pluck('name', 'code')->prepend('Select', '');
		
        return view('admin.buildings.edit', compact('building', 'sites','website_id','phoneTypes','countries'));
    }
    
    public function getComponeyFromSite($site_id){
        
        $s_data = Site::select('companies.*')->where("sites.id",$site_id);
        $s_data->join('companies','companies.id','=','sites.company_id');
        
        $res = $s_data->first();
        
        return $res;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
//            'address' => 'required',
            'site_id' => 'required'
        ]);
        $requestData = $request->all();

        $building = Building::accessible("access.building.changes.all","buildings")->where("id",$id)->first();

        if(!$building){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
        $building->update($requestData);

        Session::flash('flash_message', 'Building updated!');

        return redirect('admin/buildings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = Building::accessible("access.building.changes.all","buildings")->where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$ob && _MASTER){
			$ob = Building::accessible("access.building.changes.all","buildings")->withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}

        if($ob){
           
			$record = \App\Setting::deleteParentLevel($id,'building',$is_hard_delete);
			
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/buildings');
        }
    }
    public function filter_by_website(Request $request)
    {
        $result = array();
        $enable_m_ids =Companychargablemodule::where('action', "buildings_module")->pluck("company_id");

        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $s_data = Site::select('sites.id','sites.name','sites.company_id')->join('companies','companies.id','=','sites.company_id');
            $s_data->whereIn('companies.id',$enable_m_ids);
            $s_data->where('sites._website_id',$request->_website_id);
            $result['data']['site'] = $s_data->get();
            
            $co_data = Company::select('id','name')->whereIn("id",$enable_m_ids);
            $co_data->where('companies._website_id',$request->_website_id);
            $result['data']['compoanies'] = $co_data->get();

        }

        return response()->json($result,200);
    }
}
