<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\ActivationMail;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\WebSite;
use App\Language;
use Illuminate\Http\Request;
use Session;
use App\Service;
use App\DropdownsType;
use App\People;
use App\Availability;
use App\UserUnitTransaction;

use Illuminate\Support\Facades\File;

use Yajra\Datatables\Datatables;

class UsersController extends Controller

{

    public function __construct()
    {
        $this->middleware('permission:access.users');
        $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.user.create')->only(['create', 'store']);
        $this->middleware('permission:access.user.delete')->only('destroy');
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function userDatatable(Request $request) {
        $record = User::accessible("access.user.all","users")->with('websites','roles')->website('users');

        if($request->has('filter_website')){
			$website = Website::findOrFail($request->filter_website);
			$uids = [];
			if($website){ $uids = $website->users->pluck('id')->toArray(); }
			
			$record->whereIn('id',$uids);
            //$record->where('users._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }

        return Datatables::of($record)->make(true);
    }

    public function index(Request $request)
    {
        
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {

        $websites = WebSite::with('roles')->website()->get();

        $languages = Language::active()->select(
            \DB::raw("CONCAT(name,' ( ',lang_code,' ) ') AS full_name"), 'lang_code')
            ->pluck('full_name', 'lang_code')->prepend('Select..', '');

        $roles= [];
        foreach ($websites as $website){
            foreach ($website->roles as $role){
                $role->website_id = $website->id;
                $roles[]=$role;
            }
        }

        $accessible_websites = WebSite::master(0)->get();
        $user_accessible_website = [];

        
        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $people_quality = DropdownsType::getPluckValue('people_quality');
        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');//->prepend('Select..', '');

        return view('admin.users.create', compact('roles','languages','accessible_websites','user_accessible_website','phoneTypes', 'services','people_quality'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users', 
                'password' => 'required',
                'roles' => 'required'
            ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $data['name'] = $data['first_name']." ".$data['last_name'];


//        $data['_website_id'] = _WEBSITE_ID;

        $user = User::create($data);

        if ($user) {
            $user->api_token = User::genApiKey($user->id);
            $user->save();
        }

        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }
		
		$access_web_count = 0;
        if($request->has('accessible_websites')){
            foreach ($request->accessible_websites as $web) {
                $user->assignAccessWebsite($web);
				$access_web_count++;
            }
        }else if(!$user->hasWeb(_WEBSITE_ID) && defined('_MASTER') && !_MASTER){
			$user->assignAccessWebsite(_WEBSITE_ID);
			$access_web_count++;
		}
		
		if($access_web_count >1 && !$user->hasRole("GSA")){
			 $user->assignRole("GSA");
		}
        
        $requestData = $data;
        $requestData['user_id'] = $user->id;        
        $name = $this->uploadPhoto($request);

        if (!is_null($name)) {
            $requestData['photo'] = $name;
        }

        $people = People::create($requestData);

        $this->insertAvailabilityData($request, $people->id);

        $people->services()->sync($request->services);
        
        
        if($request->has('sendpassword') && $request->sendpassword==1 && $request->has('password') && $request->password!=""){
            $user->notify(new ActivationMail($user->email,$request->password));
        }

        Session::flash('flash_message', __('User added!'));

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {

        $user = User::accessible("access.user.all","users")->website('users')->where("id",$id)->first();

        if(!$user){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
        $people = People::where("user_id",$id)->first();

        
        return view('admin.users.show', compact('user','people'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $websites = WebSite::with('roles')->website()->get();



		$rid = [];
        $roles= [];
        foreach ($websites as $website){
            foreach ($website->roles as $role){
                $role->website_id = $website->id;
				if(!in_array($role->id,$rid)){
					$roles[]=$role;
					$rid[] = $role->id;
				}
            }
        }
		
		

        $user = User::accessible("access.user.changes.all","users")->website('users')->with('roles')->where("id",$id)->first();

        
        
        if(!$user){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $website_id = $user->_website_id;

        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }
        $languages = Language::active()->select(
            \DB::raw("CONCAT(name,' ( ',lang_code,' ) ') AS full_name"), 'lang_code')
            ->pluck('full_name', 'lang_code')->prepend('Select..', '');

        $accessible_websites = WebSite::master(0)->get();
        $user_accessible_website = [];

        foreach ($user->accessiblewebsites as $w){
            $user_accessible_website[]=$w->id;
        }
        
        
        
        

        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $people_quality = DropdownsType::getPluckValue('people_quality');
        
        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');//->prepend('Select..', '');

        $people = People::where("user_id",$id)->first();
        $av = []; $user_services=[];
        if($people){
            $av = $this->getAvailability($people);
            $user_services = $people->services()->pluck('id');
        }
        
        $peopleo = People::select("title","first_name","last_name","quality","phone_number_1","phone_number_2","code_phone_number_1","code_phone_number_2","phone_type_1","phone_type_2","emergency_password","availability","status_text","photo","active")
                ->where("user_id",$id)->first();
        
        if($peopleo){
            $peopleob = $peopleo->toArray();
            foreach ($peopleob as $key => $value) {
                $user->{$key} = $value;
            }
        }

       
        
        return view('admin.users.edit', compact('user', 'roles', 'user_roles','website_id','languages','accessible_websites','user_accessible_website','people', 'phoneTypes','services', 'user_services', 'av','people_quality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['first_name' => 'required',
                'last_name' => 'required','email' => 'required|unique:users,email,' . $id, 'roles' => 'required']);



        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        $data['name'] = $data['first_name']." ".$data['last_name'];

        $user = User::accessible("access.user.changes.all","users")->website('users')->where("id",$id)->first();

        if(!$user){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

       $access_web_count = 0;
        if($request->has('accessible_websites')){
			$user->accessiblewebsites()->detach();
            foreach ($request->accessible_websites as $web) {
                $user->assignAccessWebsite($web);
				$access_web_count++;
            }
        }else if(!$user->hasWeb(_WEBSITE_ID) && defined('_MASTER') && !_MASTER){
			$user->assignAccessWebsite(_WEBSITE_ID);
			$access_web_count++;
		}
		
		


        if ($user) {
            $user->update($data);
           // $user->api_token = User::genApiKey($user->id);
            $user->save();
        }

        $result = array_unique($request->roles);
		$old_role = $user->roles()->pluck("name","id")->toArray();
		$gsacnt = 0;
		foreach($old_role as $k => $role) {
			$key = array_search($role,$result,true);
			if($key === 0 || ( $key && $key > 0)){
				if($role == "GSA"){
					$gsacnt = 1;
				}
				unset($result[$key]);
			}else{
				
				$user->roles()->detach($k);
				
				if($role == "GSA"){
					$gsacnt = 1;
				}
			}
        }
		
		foreach ($result as $role) {
			if($role == "GSA"){ $gsacnt = 1; }
			$user->assignRole($role);
		}
	
		if($access_web_count >1 && !$gsacnt){
			$user->assignRole("GSA");
		}
		
		
		
        $people = People::where("user_id",$id)->first();
        
        $requestData = $data;
        $name = $this->uploadPhoto($request);

        if (!is_null($name)) {
            $requestData['photo'] = $name;
            $this->removeImage($people->photo);
        }
        
        if($people){
            $people->update($requestData);
        }else{
            $requestData['user_id'] = $user->id;
            $people = People::create($requestData);
        }

        
        
        $this->insertAvailabilityData($request, $people->id);

        $people->services()->sync($request->services);
        
        if($request->has('sendpassword') && $request->sendpassword==1 && $request->has('password') && $request->password!=""){
            $user->notify(new ActivationMail($user->email,$request->password));
        }
        
        Session::flash('flash_message', __('User updated!'));

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id,Request $request)
    {
        $result = array();
        $ob = User::accessible("access.user.changes.all","users")->website('users')->where("id",$id)->first();
		$is_hard_delete = 0;
		
		if(!$ob && _MASTER){
			$ob = User::withTrashed()->accessible("access.user.changes.all","users")->website('users')->where("id",$id)->first();
			$is_hard_delete = 1;
		}

        if($ob){
            $record = \App\Setting::deleteParentLevel($id,'user',$is_hard_delete);
            
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/users');
        }
    }

    public function search(Request $request)
    {
        $result = array();

        $data =User::accessible("access.user.all","users")->select('id','name','_website_id');

        if ($request->has('filter_user') &&  $request->get('filter_user') != '') {
            $data->where('name', 'LIKE', "%$request->filter_user%");
        }
		
		if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
			
			$website = Website::findOrFail($request->_website_id);
			$uids = $website->users->pluck('id')->toArray();
			
            $data->whereIn('id',$uids);
			
			
			//$data->where('_website_id',$request->get('_website_id'));
        }
		$res = $data->get();
		
		foreach ($res as $key => $item) {
			  if($item->_website_id !=$request->get('_website_id')  && !$item->hasRole("GSA")){
				   unset($res[$key]);
			  }
		}
        $result['data'] = $res;
        $result['code'] = 200;

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/websites');
        }
    }
    public function filter_by_website(Request $request)
    {
        $result = array();


        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $s_data = Service::select('id','name');
            $s_data->where('_website_id',$request->_website_id);
            $result['data']['service'] = $s_data->get();
        }

        return response()->json($result,200);
    }
    public function getAvailability(People $people)
    {
//        $arr = $people->availability()->get();
        $rr = [];
        $ava = Availability::where('people_id', $people->id)->get();

        foreach ($ava as $av) {

            $time = str_replace('to', '-', $av->time);

            $a = explode('-', $time);

            $from = trim($a[0]);

            $to = trim($a[1]);

            $rr[$av->day] = [
                'from' => $from,
                'to' => $to
            ];

        }

        return $rr;
    }
    /**
     * @param Request $request
     * @param $people_id
     */
    public function insertAvailabilityData(Request $request, $people_id)
    {
        $av = $request->input('av', []);

        if (empty($av)) {
            return;
        }

        Availability::where('people_id', $people_id)->forceDelete();

        foreach ($av as $key => $value) {
            //done  remove data where pople_id = $request->id
            Availability::create([
                'day' => strtolower($key),
                'time' => $value['from'] . ' - ' . $value['to'],
                'people_id' => $people_id,
            ]);
        }
    }
    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }

}
