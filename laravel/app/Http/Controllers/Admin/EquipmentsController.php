<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Equipment;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;

class EquipmentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.equipments');
        $this->middleware('permission:access.equipment.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.equipment.create')->only(['create', 'store']);
        $this->middleware('permission:access.equipment.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.equipments.index');
    }

    public function datatable(Request $request) {

        $record = Equipment::get();
        if($request->has('filter_website')){
            $record->where('equipments._website_id',$request->filter_website);
        }

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.equipments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'equipment_id' => 'required'
        ]);
        $requestData = $request->all();

        Equipment::create($requestData);

        Session::flash('flash_message', 'Equipment added!');

        return redirect('admin/equipments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $equipment = Equipment::findOrFail($id);

        return view('admin.equipments.show', compact('equipment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $equipment = Equipment::findOrFail($id);

        return view('admin.equipments.edit', compact('equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'equipment_id' => 'required'
        ]);
        $requestData = $request->all();

        $equipment = Equipment::findOrFail($id);
        $equipment->update($requestData);

        Session::flash('flash_message', 'Equipment updated!');

        return redirect('admin/equipments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = Equipment::findOrFail($id);

        if($ob){
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/equipments');
        }
    }
}
