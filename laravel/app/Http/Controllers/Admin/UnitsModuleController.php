<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\UnitsModule;
use App\Permission;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class UnitsModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.moduleunits');
        $this->middleware('permission:access.moduleunits.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.moduleunits.create')->only(['create', 'store']);
        $this->middleware('permission:access.moduleunits.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        

        $actions = Permission::orderby("name")->pluck('name', 'name');

        return view('admin.moduleunit.index',compact('actions'));

    }
    public function datatable(Request $request) {
        $record = UnitsModule::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'action' => 'required',
            'unit' => 'required',
        ]);


        $requestData = $request->all();
       
        $module = UnitsModule::where("action",$request->action)->first();
        if($module){
            $requestData['unit'] = round($requestData['unit']/30,2);  // per month
            $module->update($requestData);
            $result['message'] = \Lang::get('moduleunit.responce_msg.moduel_unit_updated_success');
        }else{
            $ticket = UnitsModule::create($requestData);
            $result['message'] = \Lang::get('moduleunit.responce_msg.moduel_unit_add_success');
        }
        

        
        $result['code'] = 200;
        return response()->json($result, $result['code']);

    }
    

    public function destroy($id,Request $request)
    {
        $item = UnitsModule::where("id",$id)->first();

        if($item){
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/module-units');
        }
    }


}
