<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Building;
use App\Site;
use Illuminate\Http\Request;
use Session;
use App\Country;
use App\Company;
use App\Companychargablemodule;
use App\DropdownsType;
use Illuminate\Support\Facades\File;

use Yajra\Datatables\Datatables;

class SitesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.sites')->except('search');
        $this->middleware('permission:access.site.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.site.create')->only(['create', 'store']);
        $this->middleware('permission:access.site.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.sites.index');
    }

    public function datatable(Request $request) {

        $record = Site::accessible("access.site.all","sites")->with('_website');
         if($request->has('filter_website')){
            $record->where('sites._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        $countries = Country::pluck('name', 'code')->prepend('Select', '');


        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $siteTypes = DropdownsType::getPluckValue('site_type');
        $companies = Company::accessible("access.company.changes.all","companies")->pluck('name', 'id')->prepend('Select', '');

        return view('admin.sites.create', compact('countries', 'phoneTypes', 'companies', 'siteTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

//        return $request->file();

        $this->validate($request, [

            'company_id' => 'required',
            'name' => 'required',
            "instructions_tasks_file" => 'mimes:pdf',
            "order_file" => 'mimes:pdf',
            "procedures_file" => 'mimes:pdf',
            "sitemaps_file" => 'mimes:pdf',
        ]);

//        mimes:jpeg,bmp,png


        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }


        $instruction_tasks_file = $this->_uploadFile('instructions_tasks_file', $request);
        if (!is_null($instruction_tasks_file)) {
            $requestData['instructions_tasks_file'] = $instruction_tasks_file;
        }

        $order_file = $this->_uploadFile('order_file', $request);
        if (!is_null($order_file)) {
            $requestData['order_file'] = $order_file;
        }

        $procedures_file = $this->_uploadFile('procedures_file', $request);
        if (!is_null($procedures_file)) {
            $requestData['procedures_file'] = $procedures_file;
        }

        $sitemaps_file = $this->_uploadFile('sitemaps_file', $request);
        if (!is_null($sitemaps_file)) {
            $requestData['sitemaps_file'] = $sitemaps_file;
        }

        Site::create($requestData);

        Session::flash('flash_message', 'Site added!');

        return redirect('admin/sites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $site = Site::accessible("access.site.all","sites")->where("id",$id)->first();

        if(!$site){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.sites.show', compact('site'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $site = Site::accessible("access.site.changes.all","sites")->where("id",$id)->first();

        if(!$site){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $siteTypes = DropdownsType::getPluckValue('site_type');

        $companies = Company::pluck('name', 'id')->prepend('Select', '');
        $countries = Country::pluck('name', 'code')->prepend('Select', '');

        $website_id = $site->_website_id;

        return view('admin.sites.edit', compact('site', 'countries', 'phoneTypes', 'companies', 'siteTypes','website_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [

            'company_id' => 'required',
            'name' => 'required',
            "instructions_tasks_file" => 'mimes:pdf',
            "order_file" => 'mimes:pdf',
            "procedures_file" => 'mimes:pdf',
            "sitemaps_file" => 'mimes:pdf',
        ]);
        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        $site = Site::accessible("access.site.changes.all","sites")->where("id",$id)->first();

        if(!$site){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if (!is_null($name)) {
            $requestData['logo'] = $name;
            $this->removeLogoImage($site->logo);
        }

        $instruction_tasks_file = $this->_uploadFile('instructions_tasks_file', $request);
        if (!is_null($instruction_tasks_file)) {
            $requestData['instructions_tasks_file'] = $instruction_tasks_file;
            $this->removeFile($site->instructions_tasks_file);
        }

        $order_file = $this->_uploadFile('order_file', $request);
        if (!is_null($order_file)) {
            $requestData['order_file'] = $order_file;
            $this->removeFile($site->order_file);
        }

        $procedures_file = $this->_uploadFile('procedures_file', $request);
        if (!is_null($procedures_file)) {
            $requestData['procedures_file'] = $procedures_file;
            $this->removeFile($site->procedures_file);
        }

        $sitemaps_file = $this->_uploadFile('sitemaps_file', $request);
        if (!is_null($sitemaps_file)) {
            $requestData['sitemaps_file'] = $sitemaps_file;
            $this->removeFile($site->sitemaps_file);
        }



        $site->update($requestData);

        Session::flash('flash_message', 'Site updated!');

        return redirect('admin/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $site = Site::accessible("access.site.changes.all","sites")->where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$site && _MASTER){
			$site = Site::accessible("access.site.changes.all","sites")->withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}

        if($site){
            /* 
			$this->removeLogoImage($site->logo);
            $this->removeFile($site->instructions_tasks_file);
            $this->removeFile($site->order_file);
            $this->removeFile($site->procedures_file);
            $this->removeFile($site->sitemaps_file);
			Site::destroy($id); */
			
			$record = \App\Setting::deleteParentLevel($id,'site',$is_hard_delete);

            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/sites');
        }
    }


    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadLogo(Request $request)
    {
        if ($request->hasFile('logo')) {

//            dd($request->file('image'));
            $file = $request->file('logo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeLogoImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }

    public function removeFile($fileName)
    {
        $image_path1 = public_path()."/uploads/files/".$fileName;

        if ($fileName && $fileName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }


    public function _uploadFile($filename, Request $request)
    {
        if ($request->hasFile($filename)) {

//            dd($request->file('image'));
            $file = $request->file($filename);
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString());


//            $_ext = $file->getClientOriginalExtension();
//
//            if (strtolower($_ext) != 'pdf') {
//                return null;
//            }


            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/files/', $name);

            return $name;
        } else {

            return null;
        }

    }

    public function search(Request $request)
    {
        $result = array();

        $data =Site::select('id','name');

        if ($request->has('filter_site') &&  $request->get('filter_site') != '') {
            $data->where('name', 'LIKE', "%$request->filter_site%");
        }
        if ($request->has('filter_company_id') &&  $request->get('filter_company_id') != '') {
            $data->where('company_id',$request->filter_company_id);
        }
		if ($request->has('company_id') &&  $request->get('company_id') != '') {
            $data->where('company_id',$request->company_id);
        }
        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $data->where('_website_id',$request->_website_id);
        }

        $result['data'] = $data->get();

        return response()->json($result,200);
    }

    public function filter_by_website(Request $request)
    {
        $result = array();
        $enable_m_ids =Companychargablemodule::where('action', "sites_module")->pluck("company_id");

        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            $c_data = Company::select('id','name');
            $c_data->where('_website_id',$request->_website_id);
            $c_data->whereIn("id",$enable_m_ids);
            $result['data']['company'] = $c_data->get();

        } 

        return response()->json($result,200);
    }
}
