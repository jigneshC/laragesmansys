<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

use App\WebSite;
use App\RefeImages;
use Illuminate\Http\Request;
use Session;
use App\Role;
use App\User;
use Auth;
use App\Language;
use App\UnitTransaction;
use App\Company;
use App\Site;
use App\Building;

use Yajra\Datatables\Datatables;
use Ixudra\Curl\Facades\Curl;
use App\Curl\ApiCall;

class WebsitesController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.websites');
        $this->middleware('permission:access.website.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.website.create')->only(['create', 'store']);
        $this->middleware('permission:access.website.delete')->only('destroy');
        $this->middleware('permission:access.canaddcredit')->only('creditUnits');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
		
		/*  $curl = new ApiCall();
        $curl->getAllDomain();*/
       /* require_once storage_path('DNSMadeEasy/Autoloader.php');
        \DNSMadeEasy\Autoloader::init();

        $client = new \DNSMadeEasy\Client('5562f5b0-78a6-4cb2-b781-4c61f1f3306b', 'a6f77dc5-c0de-4810-a403-0de261e7cbed', false);

        $result = $client->domains()->add('sub2.gesmansys.com', array('ttl' => 1800));
       // $result = $client->domains()->update(5902588, array('gtdEnabled' => true));

        $result = $client->domains()->getAll();

        if($result->success){
            //yay!
            var_dump($result->statusCode);

            echo "<pre>";print_r($result->body);
        }else{
            //:(
            var_dump($result->errors);
        }*/

       


        $isReload=0;
        return view('admin.websites.index',compact("isReload"));
    }

    public function datatable(Request $request) {

        $record = Website::with('roles');
		
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
         $languages = Language::active()->select(
            \DB::raw("CONCAT(name,' ( ',lang_code,' ) ') AS full_name"), 'lang_code')
            ->pluck('full_name', 'lang_code')->prepend('Select..', '');
        
       
        return view('admin.websites.create', compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        
        $this->validate($request, [
            'domain' => 'required',
            'name' => 'required',
            'status' => 'required',
            'logo_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'username' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'language' => 'required',
        ]);
        $requestData = $request->all();

        $arr = explode(".", $request->domain);
        $sub_dom_name = $arr[0];
		
		if(!isset($arr[1])){
			$val_msg = [
				'domains.required'=> \Lang::get('website.label.domain_name_not_valid')
			];
			$this->validate($request, ['domains' => 'required'],$val_msg);
		}
		

        if(\config('settings.dnsmadeeasy.ALLOW_DOMAIN_API')){
            $curl = new ApiCall();
            $curl->createSubDomain($sub_dom_name,str_slug($request->name));
        }
        
        $ob = Website::create($requestData);
        
        /*if ($request->has('roles')){
            foreach ($request->roles as $role) {
                $ob->assignRole($role);
            }
        }*/

        $logo = $this->uploadPhoto($request);

        if($logo){
            $refimg = new RefeImages();
            $refimg->image = $logo;
            $refimg->ref_field_id = $ob->id;
            $refimg->refe_table_field_name = 'website_id';
            $refimg->save();
        }
          
        
        if ($request->has('password')) {
            $user['password'] = bcrypt($request->password);
            $password = bcrypt($request->password);
        }
      

        if (Auth::check()) {
            $userlog = Auth::user();
            $created_by = $userlog->id;
            $updated_by = $userlog->id;
        } else {
            $created_by = 0;
            $updated_by = 0;
        }
        
       
        $userId = User::insertGetId(['password'=>$password,
                        'name' => $request->username,
                        'email' => $request->email,
                        '_website_id' => $ob->id,
                        'language' => $request->language,
                        'created_by' => $created_by,
                        'updated_by' => $updated_by,
                        'created_at' => $ob->created_at,
                        'updated_at' => $ob->updated_at,
          ]);
      
      //call dynamic artisan command for creating basic data related domain

         $exitCode = Artisan::call('createbasic:name', [
            'website_id' => $ob->id, 
            'user_id' => $userId,
          ]);
       

       Session::flash('flash_message', __('Website added!'));
       return redirect('admin/websites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $website = Website::findOrFail($id);

        $isReload=1;
        return view('admin.websites.show', compact('website','isReload'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id , Request $request)
    {
        $website = Website::findOrFail($id);
		
		if($request->has('delete_existing_domain')){
				$arr = explode(".", $website->domain);
                $curl = new ApiCall();
                $curl->deleteSubDomainByName($arr[0]);
		}
		if($request->has('create_new_domain')){
				$arr = explode(".", $website->domain);
                $curl = new ApiCall();
            $curl->createSubDomain($arr[0],str_slug($arr[0]));
		}
		

       // return $website->roles;

        /*$roles = Role::select('id', 'name', 'label')->lower()->get();
        $roles = $roles->pluck('label', 'name');

        $website_roles = [];
        foreach ($website->roles as $role) {
            $website_roles[] = $role->name;
        }*/
       
        return view('admin.websites.edit', compact('website','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'domain' => 'required',
            'name' => 'required',
            'status' => 'required',
        ]);
        $requestData = $request->all();

        //if($requestData['master']=="1"){ $requestData['master']=1; }else{ $requestData['master'] = 0;}

        $website = Website::findOrFail($id);
        if(!$website){
            return redirect()->back();
        }
        $website->update($requestData);
       

        if($request->has('roles') && $request->roles){
            $website->roles()->detach();
            foreach ($request->roles as $role) {
                $website->assignRole($role);
            }
        }

        $logo = $this->uploadPhoto($request);

        if($logo){
            $refimg = RefeImages::where('ref_field_id', $id)->where('refe_table_field_name', 'website_id')->first();
            if($refimg) {
                $this->removeImage($refimg->image);
            }else{
                $refimg = new RefeImages();
            }
            $refimg->image = $logo;
            $refimg->ref_field_id = $id;
            $refimg->refe_table_field_name = 'website_id';
            $refimg->save();
        }

        Session::flash('flash_message', __('Website updated!'));

        return redirect('admin/websites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
		$is_hard_delete = 0;
        $ob = Website::where('id',$id)->first();
		
		if(!$ob && _MASTER){
			$ob = Website::withTrashed()->where('id',$id)->first();
			$is_hard_delete = 1;
		}
		
        if($ob){

			
            if(\config('settings.dnsmadeeasy.ALLOW_DOMAIN_API') && $is_hard_delete) {
                $arr = explode(".", $ob->domain);
                $curl = new ApiCall();
                $curl->deleteSubDomainByName($arr[0]);
            }
			
			$record = \App\Setting::deleteParentLevel($id,'website',$is_hard_delete);
        }

        $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
        $result['code'] = 200;


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/websites');
        }

    }

    public function uploadPhoto(Request $request)
    {
        $destinationPath = public_path() . '/uploads/website';
        File::exists($destinationPath) or File::makeDirectory($destinationPath);

        if ($request->hasFile('logo_image')) {
            $file = $request->file('logo_image');
            $name = str_replace([' ', ':'], '-', uniqid()).$file->getClientOriginalName();


            $file->move($destinationPath,$name);

            return $name;
        } else {

            return null;
        }

    }
    public function removeImage($imageName)
    {
        $image_path1 = public_path() . '/uploads/website/'.$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }

    }

    public function creditUnits(Request $request)
    {
        $result = array();
        $result = array();

        $this->validate($request, [
            'website_id' => 'required',
            'unit' => 'required',
        ]);


        $requestData = $request->all();
        $requestData['unit_type'] = "credit";
        $requestData['reference'] = "admin_credit";
       
        $ob =null;
        $website = Website::where('id',$request->website_id)->first();

        if($website){
            $ob = UnitTransaction::create($requestData);
        }

        if($ob){
            $website->units = $website->units+ $request->unit;
            $website->save();
            $result['message'] = \Lang::get('moduleunit.responce_msg.unit_credited_to_website_success');
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/websites');
        }
    }
}
