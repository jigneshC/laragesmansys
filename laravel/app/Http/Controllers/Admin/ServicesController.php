<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use App\Subject;
use App\Site;
use Illuminate\Http\Request;
use Session;
use App\User;
use App\Company;
use App\Website;
use App\Companychargablemodule;

use Yajra\Datatables\Datatables;

class ServicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.services');
        $this->middleware('permission:access.service.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.service.create')->only(['create', 'store']);
        $this->middleware('permission:access.service.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        /*
        $s_data = Service::select('services.id','sites.company_id');
        $s_data = $s_data->join('sites','sites.id','=','services.site_id')->get();
        
        foreach ($s_data as $key => $value) {
            Service::whereId($value->id)->update(['company_id'=>$value->company_id]);
        }*/
       
        /*
        $record = Service::all();
        foreach ($record as $key => $value) {
            $value->sites()->sync($value->site_id);
        }*/
        
        return view('admin.services.index');
    }

    public function datatable(Request $request) {
        
        

        $record = Service::accessible("access.service.all","services")->with('sites','_website');
        if($request->has('filter_website')){
            $record->where('services._website_id',$request->filter_website);
        }
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');

        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id')->prepend('Select', '');

        return view('admin.services.create', compact('sites', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'site_id' => 'required',
            'company_id'=>'required'
        ]);
        $requestData = $request->all();

        $service = Service::create($requestData);

        $service->sites()->sync($request->site_id);
        
        Session::flash('flash_message', __('Service added!'));

        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::accessible("access.service.all","services")->where("id",$id)->first();
       // return $service->subjects;exit;
        if(!$service){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::accessible("access.service.changes.all","services")->where("id",$id)->first();

        if(!$service){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $sites = Site::accessible("access.site.all","sites")->pluck('name', 'id')->prepend('Select', '');
        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id')->prepend('Select', '');

        $website_id = $service->_website_id;

        $selected_sites = [];

        
        foreach ($service->sites as $w){
            $selected_sites[]=$w->id;
        }
      //  echo "<pre>";        print_r($selected_sites); exit;
        return view('admin.services.edit', compact('service', 'sites', 'users','website_id','selected_sites'));
    }
    
    public function getComponeyFromSite($site_id){
        
        $s_data = Site::select('companies.*')->where("sites.id",$site_id);
        $s_data->join('companies','companies.id','=','sites.company_id');
        
        $res = $s_data->first();
        
        return $res;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'site_id' => 'required',
            'company_id'=>'required'
        ]);
        $requestData = $request->all();

        $service = Service::accessible("access.service.changes.all","services")->where("id",$id)->first();
        if(!$service){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $service->update($requestData);
        
        $service->sites()->sync($request->site_id);

        Session::flash('flash_message', __('Service updated!'));

        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = Service::accessible("access.service.changes.all","services")->where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$ob && _MASTER){
			$ob = Service::withTrashed()->accessible("access.service.changes.all","services")->where('id',$id)->first();
			$is_hard_delete = 1;
		}
		
        if($ob){
            
            $record = \App\Setting::deleteParentLevel($id,'services',$is_hard_delete);
            
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/services');
        }
    }

    public function search(Request $request)
    {
        $result = array();

        $data =Service::select('id','name');

        if ($request->has('filter_services') &&  $request->get('filter_services') != '') {
            $data->where('name', 'LIKE', "%$request->filter_services%");
        }
        if ($request->has('site_id') &&  $request->get('site_id') != '') {
            $s_ob =  Site::whereId($request->get('site_id'))->with('services')->first();
            $s_ids = [];
            if($s_ob && $s_ob->services){
                $s_ids = $s_ob->services->pluck("id");
            }
            $data->whereIn('id',$s_ids);
        }
		if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
			$data->where('_website_id',$request->_website_id);
		}

        $result['data'] = $data->get();

        return response()->json($result,200);
    }

    public function filter_by_website(Request $request)
    {
        $result = array();
        $enable_m_ids =Companychargablemodule::where('action', "services_module")->pluck("company_id");

        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
			
			$website = Website::findOrFail($request->_website_id);
			$uids = $website->users->pluck('id')->toArray();
			
            $u_data = User::select('id','name');
            $u_data->whereIn('id',$uids);
            $result['data']['user'] = $u_data->get();

            $s_data = Site::select('sites.id','sites.name','sites.company_id')->join('companies','companies.id','=','sites.company_id');
            $s_data->whereIn('companies.id',$enable_m_ids);
            $s_data->where('sites._website_id',$request->_website_id);
            $result['data']['site'] = $s_data->get();

            $co_data = Company::select('id','name')->whereIn("id",$enable_m_ids);
            $co_data->where('companies._website_id',$request->_website_id);
            $result['data']['compoanies'] = $co_data->get();
        }

        return response()->json($result,200);
    }
}
