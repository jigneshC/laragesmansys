<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\Ticketviewcron;
use App\MasterTicket;
use App\Company;
use App\Companychargablemodule;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class MasterTicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.tickets');
        $this->middleware('permission:access.ticket.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.ticket.create')->only(['create', 'store']);
        $this->middleware('permission:access.ticket.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function ticketDatatable(Request $request) {

        $record = MasterTicket::select('master_tickets.*','subjects.name as subject_name','sites.name as site_name','services.name as services_name');

        $record->join('subjects','subjects.id','=','master_tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','master_tickets.site_id');
		$record->join('companies','companies.id','=','sites.company_id');
        

        $record->accessible("access.ticket.all","master_tickets");
		
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }
		
		if($request->has('filter_company')){
            $record->where('companies.id',$request->filter_company);
		}else

        if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('status',$request->get('status'));
        }else{
            //$where_filter = "((tickets.status !='closed')  OR (tickets.updated_at > '$last24hrs') )";
            //$record->whereRaw($where_filter);
        }

        if($request->has('filter_website')){
            $record->where('master_tickets._website_id',$request->filter_website);
        }else{
        }

        if ($request->has('filter_site')  && $request->get('filter_site') != '') {
            $record->where('sites.id',$request->get('filter_site'));
        }else{
        }


        if ($request->has('filter_services')  && $request->get('filter_services') != '') {
            $record->where('services.id',$request->get('filter_services'));
        }else{
        }

		if ($request->has('filter_subject_type')  && $request->get('filter_subject_type') != '') {
            $record->where('subjects.subject_type',$request->get('filter_subject_type'));
        }
		

        if ($request->has('filter_subject')  && $request->get('filter_subject') != '') {
            $record->where('subjects.id',$request->get('filter_subject'));
        }else{
        }


        if ($request->has('range_start') && $request->get('range_start') != '') {
            $sdate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_start." 00:00:00");
            $edate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_end." 23:59:59");
            $record->whereBetween('master_tickets.created_at', [$sdate, $edate]);
            
            if($request->range_start =="2015-01-01" && $request->range_end == Carbon\Carbon::now()->format("Y-m-d")){
            }else{
            }
			
        }else{
        }
        

        
        return Datatables::of($record)->make(true);
    }
    public function index(Request $request)
    {
       /* $record = MasterTicket::select('master_tickets.id','services.site_id');
        $record->join('subjects','subjects.id','=','master_tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record = $record->get();
        
        foreach ($record as $key => $value) {
            MasterTicket::whereId($value->id)->update(['site_id'=>$value->site_id]);
        }
        */
        
        return view('admin.master-ticket.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        /*$subjects = [];
        $subjectsObs = Subject::with('services')->where("subject_type","planned_activity")->orderby('name','ASC')->get();
        foreach($subjectsObs as $subjectsOb){
            $subjects[$subjectsOb->id] = $subjectsOb->name." (".$subjectsOb->services->name.") ";
        }*/

        $eqps = Equipment::pluck('equipment_id', 'id')->prepend('None', 0);

        return view('admin.master-ticket.create', compact( 'status', 'eqps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_id' => 'required',
            //'title' => 'required',
            'content' => 'required'

        ]);

        $subject = Subject::where("id",$request->subject_id)->first();


        if($subject && $subject->subject_type =="planned_activity"){
            $this->validate($request, [
                'activity_time_opt' => 'required',
                'view_start_date' => 'date_format:"Y-m-d"|required',
                'view_end_date' => 'date_format:"Y-m-d"|required'
            ]);
            $requestData = $request->all();
            $requestData['view_on_date'] = 1;
        }else{
            $requestData = $request->except("activity_time_opt","view_start_date","view_end_date","view_before_date","view_on_date_val");
        }


        //return $requestData;

        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] = 'new';

        $ticket = MasterTicket::create($requestData);


       
        
      //  Ticketviewcron::setTodayVisibilityOfTickets($ticket->id);

        
        
        

        Session::flash('flash_message', 'Ticket added!');

        return redirect('admin/planned-activity');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id');
        $ticket = MasterTicket::accessible("access.ticket.all","tickets")->with('logs')->where('id',$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.master-ticket.show', compact('ticket','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ticket = MasterTicket::where('id',$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if(!$ticket){
            Session::flash('flash_message', 'Not Editable');
            return redirect()->back();
        }

        $website_id = $ticket->_website_id;
        
        

        return view('admin.master-ticket.edit', compact('ticket','website_id'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $ticket = MasterTicket::where("id",$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $this->validate($request, [
            'subject_id' => 'required',
            'content' => 'required'
        ]);
        $subject = Subject::where("id",$request->subject_id)->first();


        if($subject && $subject->subject_type =="planned_activity"){
            $this->validate($request, [
                'activity_time_opt' => 'required',
                'view_start_date' => 'date_format:"Y-m-d"|required',
                'view_end_date' => 'date_format:"Y-m-d"|required'
            ]);
            $requestData = $request->all();
            $requestData['view_on_date'] = 1;
        }else{
            $requestData = $request->except("activity_time_opt","view_start_date","view_end_date");
            $requestData['view_on_date'] = 0;
        }

        $requestData['user_id'] = Auth::user()->id;


        $ticket->update($requestData);
     //   Ticketviewcron::setTodayVisibilityOfTickets($id);
        Session::flash('flash_message', 'Ticket updated!');

        return redirect('admin/planned-activity');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $res = MasterTicket::where("id",$id)->first();
		
		$is_hard_delete = 0;
        
		if(!$res && _MASTER){
			$res = MasterTicket::withTrashed()->where("id",$id)->first();
			$is_hard_delete = 1;
		}

        $result = array();
        if(!$res){
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }else{
            try {
                $record = \App\Setting::deleteParentLevel($id,'planned_activity',$is_hard_delete);
                if ($record) {
                    $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
                    $result['code'] = 200;
                } else {
                    $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
                    $result['code'] = 400;
                }
            } catch (\Exception $e) {
                $result['message'] = $e->getMessage();
                $result['code'] = 400;
            }
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/planned-activity');
        }
    }

    public function filter_by_website(Request $request)
    {
        $result = array();
        $enable_m_ids =Companychargablemodule::where('action', "plannedactivity_module")->pluck("company_id");

        $result = array();
        
        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            
            

            $e_data = Equipment::select('id','equipment_id');
            $e_data->where('_website_id',$request->_website_id);
            $result['data']['equipment'] = $e_data->get();
    
            $site_data = \App\Site::select('sites.id','sites.name','sites.company_id');
            $site_data->where('sites._website_id',$request->_website_id);
            $site_data->whereIn('sites.company_id',$enable_m_ids);
            $result['data']['sites'] = $site_data->get();
        }

        return response()->json($result,200);
        
    }
   
}
