<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Audit;
use Illuminate\Http\Request;
use Session;
use App\WebSite;
use App\Qrcode;

use App\Curl\ApiQrcode;
use Yajra\Datatables\Datatables;

class QrcodeController extends Controller
{

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $websites = Website::where("enable_qrcode",1)->pluck("domain","id");
        return view('admin.qrcode.index',  compact("websites"));
    }

  
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            '_website_id' => 'required',
            'module_name' => 'required',
        ]);
        $records = null ; 
        $website = WebSite::where("id",$request->_website_id)->first();
        
        if($request->module_name == "users"){
            $records = \App\User::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "companies"){
            $records = \App\Company::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "sites"){
            $records = \App\Site::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "services"){
            $records = \App\Service::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "buildings"){
            $records = \App\Building::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "subjects"){
            $records = \App\Subject::where("_website_id",$request->_website_id);
        }
        else if($request->module_name == "tickets"){
            $records = \App\Ticket::where("_website_id",$request->_website_id);
        }
        
        if($request->has("active") && $request->active==1 && \Illuminate\Support\Facades\Schema::hasColumn($request->module_name, 'active')){
            $records->where("active",1);
        }
        $records = $records->get();
        
        $chargable_module = \config('settings.unit_charge_route');
        
        $count = 0;
        if($records && $website){
            foreach ($records as $record){
                
                if($record->_website_id && isset($chargable_module[$record->getTable()]) && !$record->qrcode){
                    if($website->enable_qrcode && $website->qrcode_apitoken){
                        $check = ApiQrcode::getAndStoreQrcode($record, $website->qrcode_apitoken);
                        if($check){
                            $count++;
                            echo "<br> Generated ". $record->getTable() ." #".$record->id;
                        }else{
                            echo "<br> Issue  ". $record->getTable() ." #".$record->id;
                        }
                         
                    }
                }
            }
            echo  '<br><br><br>  Total generated qrcode '.$count;
        }else{
            echo  '<br><br><br> flash_message', 'No record found!';
        }
        
        
        echo "<br><br> <a href='".url('admin/qrcode')."' > Back </a>";
        
      // return redirect()->back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $qrcode = Qrcode::where("id",$id)->first();
        if(!$qrcode){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
		
		$module = null;
		$title = "";
		if($qrcode->refe_table_field_name == "users_id"){
            $module = \App\User::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->full_name; }
		}
        else if($qrcode->refe_table_field_name == "companies_id"){
            $module = \App\Company::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->name; }
        }
        else if($qrcode->refe_table_field_name == "sites_id"){
            $module = \App\Site::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->name; }
        }
        else if($qrcode->refe_table_field_name == "services_id"){
            $module = \App\Service::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->name; }
        }
        else if($qrcode->refe_table_field_name == "buildings_id"){
            $module = \App\Building::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->name; }
        }
        else if($qrcode->refe_table_field_name == "subjects_id"){
            $module = \App\Subject::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->name; }
        }
        else if($qrcode->refe_table_field_name == "tickets_id"){
            $module = \App\Ticket::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->content; }
        }else if($qrcode->refe_table_field_name == "_websites_id"){
            $module = \App\WebSite::where("id",$qrcode->ref_field_id)->first();
			if($module){ $title = $module->domain; }
        }
		
		
		

        return view('admin.qrcode.show', compact('qrcode','module','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    
}
