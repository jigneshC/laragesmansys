<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\UnitsModule;
use App\UnitTransaction;
use App\Permission;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class UnitsTransactionController extends Controller
{
    public function __construct()
    {
       
    }
    public function index(Request $request)
    {
        
        return view('admin.unittransaction.index');

    }
    public function datatable(Request $request) {
        $website_id = _WEBSITE_ID;
        $record = UnitTransaction::with('creator','website')->where("id",">",0);
        
        if(_MASTER && $request->has('filter_website')){
            $record->where('website_id',$request->filter_website);
        }else{
            $record->where('website_id',$website_id);
        }
        return Datatables::of($record)->make(true);
    }
    public function show($id)
    {
        $transaction = UnitTransaction::findOrFail($id);
        if(!$transaction){
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect('admin/unit-history');
        }
        $itemsobj = json_decode($transaction->itemsobj);
        return view('admin.unittransaction.show', compact('transaction','itemsobj'));
    }
}
