<?php

namespace App\Http\Controllers\Admin;

use App\DropdownsType;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Language;

use App\DropdownValue;
use Illuminate\Http\Request;
use Session;

use Yajra\Datatables\Datatables;

class DropdownValuesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.dropdownsvalues');
        $this->middleware('permission:access.dropdownsvalue.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.dropdownsvalue.create')->only(['create', 'store']);
        $this->middleware('permission:access.dropdownsvalue.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.dropdown-values.index');
    }


    public function datatable(Request $request) {

        $record = DropdownValue::with('type')->language();

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $types = DropdownsType::pluck('name', 'id')->prepend('Select', '');

        return view('admin.dropdown-values.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'type_id' => 'required'
        ]);

        $requestData = $request->all();


        $d = DropdownValue::create($requestData);

        $d->parent_id = $d->id;
        $d->save();


        //enter in all language with this parent id
        $languages = Language::active()->pluck('lang_code');

        $requestData['parent_id'] = $d->parent_id;
        $requestData['active'] = true;

        foreach ($languages as $code) {
            if ($code != 'en') {
                $requestData['lang_code'] = $code;
                DropdownValue::create($requestData);
            }

        }


        Session::flash('flash_message', 'DropdownValue added!');

        return redirect('admin/dropdown-values');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dropdownvalue = DropdownValue::findOrFail($id);
      
        return view('admin.dropdown-values.show', compact('dropdownvalue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dropdownvalue = DropdownValue::findOrFail($id);

        $types = DropdownsType::pluck('name', 'id')->prepend('Select', '');

        return view('admin.dropdown-values.edit', compact('dropdownvalue', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'type_id' => 'required'
        ]);

        $requestData = $request->all();

        $dropdownvalue = DropdownValue::findOrFail($id);
        $dropdownvalue->update($requestData);

        Session::flash('flash_message', 'DropdownValue updated!');

        return redirect('admin/dropdown-values');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = DropdownValue::findOrFail($id);

        if($ob){
            DropdownValue::where('parent_id',$id)->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/dropdown-values');
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function languageForm($id)
    {
//        $languages = Language::pluck('name', 'native_name', 'lang_code');
        $languages = Language::active()->select(
            \DB::raw("CONCAT(name,' ( ', native_name,' )' ) AS full_name"), 'name', 'native_name', 'lang_code')->pluck('full_name', 'lang_code')->toArray();

//        return $languages;
        $dropdownvalue = DropdownValue::find($id);

        $lines = DropdownValue::whereParentId($dropdownvalue->id)->pluck('name', 'lang_code')->toArray();

        $getLine = function ($code) use ($lines) {

            if (isset($lines[$code])) {
                if (isset($lines[$code])) {
                    return $lines[$code];
                }
                return '';
            }
        };

        $parent_id = $id;
        return view('admin.dropdown-values.langform', compact('languages', 'dropdownvalue', 'getLine', 'parent_id'));
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function languageStore($id, Request $request)
    {
        $parent_id = $request->input('parent_id');

        //get Parent
        $parent = DropdownValue::findOrFail($id);

        //get parent lang
        $parent_lang = $parent->lang_code;


        //delete all childs
        DropdownValue::where('parent_id', $parent_id)->whereRaw('id != parent_id')->forceDelete();


        foreach ($request->lang as $code => $string) {

            if (is_null($string)) {
                $string = $parent->name;
            }
            //if code is parent than update
            if ($parent_lang == $code) {
                //update
                $parent->name = $string;
                $parent->save();
            } else {

                //create new
                DropdownValue::create(
                    [
                        'name' => $string,
                        'lang_code' => $code,
                        'type_id' => $parent->type_id,
                        'parent_id' => $parent->parent_id,
                        'active' => $parent->active
                    ]
                );
            }

        }

        flash('Update successful', 'success');

        return redirect()->back();
    }

}
