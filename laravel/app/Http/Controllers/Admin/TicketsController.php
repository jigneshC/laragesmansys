<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;
use App\Notifications\TicketActionNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\Ticketviewcron;
use App\MasterTicket;
use App\Service;
use App\Company;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\File;

use Image;

//require storage_path('packages/vendor/autoload.php');

class TicketsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.tickets');
        $this->middleware('permission:access.ticket.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.ticket.create')->only(['create', 'store']);
        $this->middleware('permission:access.ticket.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
	 
	public function commentDatatable($id,Request $request) { 
		$record = TicketHistory::select('ticket_history.*','users.name','users.last_name');
		$record->join('users','users.id','=','ticket_history.created_by');
		$record->where('ticket_history.ticket_id',$id);
		
		if($request->has('history_type')){
            $record->where('ticket_history.history_type',$request->history_type);
		}
		
		return Datatables::of($record)->with(['types' => ""])->make(true);
	}
    public function ticketDatatable(Request $request) {

        $last24hrs = Carbon\Carbon::now()->subHours(24);
        
        $record = Ticket::select('tickets.*','creater.name as creater_name','subjects.subject_type','subjects.name as subject_name','users.name as user_name','sites.name as site_name','services.name as services_name','companies.name as company_name')->with('logs');

        $record->join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
		$record->join('companies','companies.id','=','sites.company_id');
        $record->leftjoin('users','users.id','=','tickets.user_id');
        $record->leftjoin('users AS creater','creater.id','=','tickets.created_by');
		
		if($request->has('enable_deleted') && $request->enable_deleted == 1){
            $record->onlyTrashed();
        }

        if($request->has('v_notification') && $request->v_notification=="all"){
            $ticket_id_arr = [];
            foreach (Auth::user()->notifications  as $notification) {
                if($notification->type == 'App\Notifications\TicketAssignNotification'){
                    $ticket_id_arr[] = $notification->data['ticket_id'];
                }
            }
            $record->whereIn('tickets.id',$ticket_id_arr);
        }else{

           /* $where_filter = "((view_on_date=0) OR ((NOW() BETWEEN view_start_date AND view_end_date) AND (NOW() BETWEEN display_from_time AND display_end_time) ) )";
            $record->whereRaw($where_filter);*/

            $record->accessible("access.ticket.all","tickets");
        }
		
		if($request->has('filter_company')){
            $record->where('companies.id',$request->filter_company);
			Session::put('filter_company',$request->filter_company);
			Session::put('filter_company_text',$request->filter_company_text);
        }else{
			 Session::forget('filter_company');
			 Session::forget('filter_company_text');
		}

        if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('status',$request->get('status'));
            Session::put('filter_status',$request->status);
        }else{
            $where_filter = "((tickets.status !='closed')  OR (tickets.updated_at > '$last24hrs') )";
            $record->whereRaw($where_filter);
            Session::forget('filter_status');
        }

        if($request->has('filter_website')){
            $record->where('tickets._website_id',$request->filter_website);
            Session::put('filter_website',$request->filter_website);
        }else{
            Session::forget('filter_website');
        }

        if ($request->has('filter_site')  && $request->get('filter_site') != '') {
            $record->where('sites.id',$request->get('filter_site'));
            Session::put('filter_site',$request->filter_site);
            Session::put('filter_site_text',$request->filter_site_text);
        }else{
            Session::forget('filter_site');
            Session::forget('filter_site_text');
        }


        if ($request->has('filter_services')  && $request->get('filter_services') != '') {
            $enable_m_ids = explode(",",$request->filter_services);
            $record->whereIn('services.id',$enable_m_ids);
            Session::put('filter_services',$request->filter_services);
            
        }else{
            Session::forget('filter_services');
        }

		if ($request->has('filter_subject_type')  && $request->get('filter_subject_type') != '') {
            $record->where('subjects.subject_type',$request->get('filter_subject_type'));
        }
		

        if ($request->has('filter_subject')  && $request->get('filter_subject') != '') {
            $enable_m_ids = explode(",",$request->filter_subject);
            $record->whereIn('subjects.id',$enable_m_ids);
            Session::put('filter_subject',$request->filter_subject);
           
        }else{
            Session::forget('filter_subject');
        }


        if ($request->has('range_start') && $request->get('range_start') != '') {
            $sdate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_start." 00:00:00");
            $edate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_end." 23:59:59");
            $record->whereBetween('tickets.created_at', [$sdate, $edate]);
            
            if($request->range_start =="2015-01-01" && $request->range_end == Carbon\Carbon::now()->format("Y-m-d")){
                Session::forget('range_start');
                Session::forget('range_end');
            }else{
                Session::put('range_start',$request->range_start);
                Session::put('range_end',$request->range_end);    
            }
			
        }else{
            Session::forget('range_start');
            Session::forget('range_end');
        }

        if($request->has('view_assign')){
            if($request->view_assign==1){
                $record->where('tickets.user_id',Auth::user()->id);
            }else if($request->view_assign==0){
                $where_filter = "((tickets.user_id = 0)  OR (tickets.user_id is null) )";
                $record->whereRaw($where_filter);
            }else if($request->view_assign==2){
               $record->where('tickets.created_by',Auth::user()->id);
            }
        }

		//$record->get();
      //  $record->where('tickets.deleted_at',null);
		
        return Datatables::of($record)->with(['types' => ""])->make(true);
    }
	public function typesdata(Request $request)
    {
		
        
        $record = Ticket::select('tickets.*','subjects.subject_type','subjects.name as subject_name','sites.name as site_name','services.name as services_name');

        $record->join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
        $record->where('status',"!=",'closed');
        $record = $record->get();
	   
		$res = [
			'ticket'=>$record->where("subject_type",'ticket')->count(),
			'alarm'=>$record->where("subject_type",'alarm')->count(),
			'event'=>$record->where("subject_type",'event')->count(),
			'planned_activity'=>$record->where("subject_type",'planned_activity')->count(),
			'duty'=>$record->where("subject_type",'duty')->count(),
			'all'=>$record->count(),
			'my'=>$record->where('user_id',Auth::user()->id)->count(),
			'my_created'=>$record->where('created_by',Auth::user()->id)->count(),
			'unassign'=>$record->where('user_id',null)->count() + $record->where('user_id',0)->count(),
		];
		
		
		return $res;
	}
    public function index(Request $request)
    {
		$selected_service = null;
		if(Session::has('filter_services')){
			$enable_m_ids = explode(",",Session::get('filter_services'));
            $selected_service =Service::select('id','name as text')->whereIn('id',$enable_m_ids)->get();
		}
		
		$selected_subject = null;
		if(Session::has('filter_subject')){
			$enable_m_ids = explode(",",Session::get('filter_subject'));
            $selected_subject =Subject::select('id','name as text')->whereIn('id',$enable_m_ids)->get();
		}
        /*
         * site id store in ticket table
        $record = Ticket::select('tickets.id','services.site_id');
        $record->join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record = $record->get();
        
        foreach ($record as $key => $value) {
            Ticket::whereId($value->id)->update(['site_id'=>$value->site_id]);
        }
        
        echo "<pre>";        print_r($record->toArray()); exit;*/
        
       /* $isTodayCron = Ticketviewcron::where('updated_at','>=',Carbon\Carbon::now()->startOfDay())->where("key","ticket_today_visibility")->first();
        if(!$isTodayCron){ Ticketviewcron::setTodayVisibilityOfTickets(); }*/
     

        $status = Ticket::getStatus();
		$type_count = $this->typesdata($request);
        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id');

       //  return view('admin.theme-test');
         
        return view('admin.dashboard',compact('status','users','type_count','selected_service','selected_subject'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $status = Ticket::getStatus();

        $eqps = Equipment::pluck('equipment_id', 'id')->prepend('None', 0);

        return view('admin.tickets.create', compact('status', 'eqps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_id' => 'required',
            //'title' => 'required',
            'content' => 'required'

        ]);

        $subject = Subject::where("id",$request->subject_id)->first();


        if($subject && $subject->subject_type =="planned_activity"){
            $this->validate($request, [
                'activity_time_opt' => 'required',
                'view_start_date' => 'date_format:"Y-m-d"|required',
                'view_end_date' => 'date_format:"Y-m-d"|required'
            ]);
            $requestData = $request->all();
            $requestData['view_on_date'] = 1;
        }else{
            $requestData = $request->except("activity_time_opt","view_start_date","view_end_date","view_before_date","view_on_date_val");
        }


        //return $requestData;

        $requestData['user_id'] = 0;
        $requestData['status'] = 'new';
        $requestData['master_ticket_id'] = 0;


        if($subject && $subject->subject_type =="planned_activity"){
           $mt= MasterTicket::create($requestData);
            Ticketviewcron::setTodayVisibilityOfTickets($mt->id);
        }else{
            $ticket = Ticket::create($requestData);
            if ($request->hasFile('file')) {


                $name = $this->uploadFile($request,$ticket);
                if($name){
                    $his = new TicketHistory();
                    $his->ticket_id=$ticket->id;
                    $his->history_type="file";
                    $his->desc=$name;
                    $his->save();
                }
            }
        }
        
        
        
        

        Session::flash('flash_message', 'Ticket added!');

        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id');
        $ticket = Ticket::accessible("access.ticket.all","tickets")->with('logs')->where('id',$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

      //   echo "<pre>";        print_r($ticket->qrcode); 
        return view('admin.tickets.show', compact('ticket','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ticket = Ticket::accessible("access.ticket.changes.all","tickets")->where('id',$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        if(!$ticket || !$ticket->isEditable){
            Session::flash('flash_message', 'Not Editable');
            return redirect()->back();
        }

        

        $status = Ticket::getStatus();
        

        $website_id = $ticket->_website_id;

        return view('admin.tickets.edit', compact('ticket', 'status','website_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $ticket = Ticket::accessible("access.ticket.changes.all","tickets")->where("id",$id)->first();

        if(!$ticket){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $this->validate($request, [
            'subject_id' => 'required',
           // 'title' => 'required',
            'content' => 'required'
        ]);
        $subject = Subject::where("id",$request->subject_id)->first();


        if($subject && $subject->subject_type =="planned_activity"){
            $this->validate($request, [
                'activity_time_opt' => 'required',
                'view_start_date' => 'date_format:"Y-m-d"|required',
                'view_end_date' => 'date_format:"Y-m-d"|required'
            ]);
            $requestData = $request->all();
            $requestData['view_on_date'] = 1;
        }else{
            $requestData = $request->except("activity_time_opt","view_start_date","view_end_date");
            $requestData['view_on_date'] = 0;
        }

        //$requestData['user_id'] = Auth::user()->id;


        $ticket->update($requestData);
        if ($request->hasFile('file')) {

            $name = $this->uploadFile($request,$ticket);
            if($name){
                $his = new TicketHistory();
                $his->ticket_id=$id;
                $his->history_type="file";
                $his->desc=$name;
                $his->save();
            }
        }

        Session::flash('flash_message', 'Ticket updated!');

        return redirect('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector 
     */
    public function destroy($id,Request $request)
    {
        $res = Ticket::accessible("access.ticket.changes.all","tickets")->where("id",$id)->first();
		$is_hard_delete = 0;
        
		if(!$res && _MASTER){
			$res = Ticket::withTrashed()->accessible("access.ticket.changes.all","tickets")->where("id",$id)->first();
			$is_hard_delete = 1;
		}
		

        $result = array();
        if(!$res || !$res->isEditable){
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }else{
            try {
				
				$record = \App\Setting::deleteParentLevel($id,'ticket',$is_hard_delete);
				
                if ($record) {
                    $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
                    $result['code'] = 200;
                } else {
                    $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
                    $result['code'] = 400;
                }
            } catch (\Exception $e) {
                $result['message'] = $e->getMessage();
                $result['code'] = 400;
            }
        }
        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin');
        }
    }


    public function postComment(Request $request)
    {
        $ticket_history2 = null;
        $isStatusChanged = 0;
        $this->validate($request, ['desc' => 'required|min:5']);
        $requestData = $request->all();

        $ticket = Ticket::find($request->ticket_id);

        if(!$ticket){
            Session::flash('flash_message', 'Ticket Not found!');
            return redirect()->back();
        }

        if(!Auth::user()->can('access.ticket.action') && Auth::user()->id != $ticket->user_id){
            Session::flash('flash_message', 'Not Permision!');
            return redirect()->back();
        }

        if($ticket->status == "solved" || $ticket->status =="closed" || $ticket->status =="on-hold" || $ticket->status =="pending"){ 
            $ticket->status = "open"; 
            $ticket->save();
            $isStatusChanged = 1;
        }

        
            $ticket_history = TicketHistory::create($requestData);

            $user = User::where("id",$ticket->user_id)->first();

            if($user && Auth::user()->id != $user->id){
                $mdata = ['actioner'=>Auth::user(),'subject'=>'','receiver'=>$user,'view'=>'','to'=>'','isStatusChanged'=>$isStatusChanged,'ticket'=>$ticket,'action'=>'ticket_status_changed','ticket_history'=>$ticket_history,'ticket_history2'=>$ticket_history2];
                SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
                //$user->notify(new TicketActionNotification(Auth::user(),$ticket_history,$ticket,$isStatusChanged,null,$user,'database'));
            }
            Session::flash('flash_message', 'Posted !');
			return redirect()->back();
           // return redirect('admin/tickets/'.$request->ticket_id);

    }

    public function assignUser(Request $request)
    {
       // return $request->all();

        $ticket = Ticket::where("id",$request->ticket_id)->first();
        $user = User::accessible("access.user.changes.all","users")->website('users')->where("id",$request->user_id)->first();

       // return $user;

        $result = array();
        if(!$ticket || !$user){
            $result['message'] = "no data";
            $result['code'] = 400;
        }else{
            $ticket->user_id = $request->user_id;
            if($ticket->status == "new"){ $ticket->status = "open"; }

            $ticket->save();

            $his = new TicketHistory();
            $his->ticket_id=$request->ticket_id;
            $his->history_type="assign";
            $his->desc=$user->full_name;
            $his->save();

            if($request->has('is_reload')){
                Session::flash('flash_message',\Lang::get('ticket.notification.assign_user_success',['user_name'=>$user->name]));
            }

            if(Auth::user()->id != $user->id){
                $user->notify(new TicketAssignNotification(Auth::user(),$user,$ticket,'database'));

				if($user && $user->enable_sms){
					$msg= \Lang::get('ticket.notification.actioner_has_assing_you_ticket',["actioner_name"=>Auth::user()->full_name]). " Gesmansys App";
					$people = $user->people;
					
					if($people && $people->phone_number_1 && $people->phone_number_1 != "" && $people->code_phone_number_1 && $people->code_phone_number_1 !=""){
						$send_sms = send_sms($people->code_phone_number_1,$people->phone_number_1, $msg);
					}
				}
            }
			
			

            
        //    \Artisan::queue('cron:run');
         //   \Artisan::call('cron:run');

            $result['message'] = \Lang::get('ticket.notification.assign_user_success',['user_name'=>$user->name]);
            $result['code'] = 200;
        }

        return response()->json($result, $result['code']);

    }

    public function postAction(Request $request)
    {
        $isStatusChanged = 0;
        $ticket = Ticket::find($request->ticket_id);
        $ticket_history = null;
        $ticket_history2 = null;

		$result = array();
        if(!$ticket){
			$result['message'] = "Ticket Not found!";
            $result['code'] = 400;
           
			if($request->ajax()){
				return response()->json($result, $result['code']);
			}else{
				Session::flash('flash_error',$result['message']);
				return redirect()->back();
			}
           
        }

        if ($request->has('status')){
            $his = new TicketHistory();
            $his->ticket_id=$request->ticket_id;
            $his->history_type="action";
            $his->desc=$request->status;
            if (!$request->hasFile('file')){ 
                $his->note=$request->note;
            }
            $his->save();

            $old_status = $ticket->status;
            $ticket->status=$request->status;
            $ticket->save();

            if($old_status !=$request->status){
                $ticket_history = $his;
            }
        }
        
        if ($request->hasFile('file')) {

            $name = $this->uploadFile($request,$ticket);
            if($name){
                $his = new TicketHistory();
                $his->ticket_id=$request->ticket_id;
                $his->history_type="file";
                $his->note=$request->note;
                $his->desc=$name;
                $his->save();

                $ticket_history = $his;

                
            }
        }
        
        if ($request->has('comment')){
            $his = new TicketHistory();
            $his->ticket_id=$request->ticket_id;
            $his->history_type="comment";
            $his->desc=$request->comment;
            $his->save();
            if($ticket_history){
                $ticket_history2 = $his;
            }else{
                $ticket_history = $his;
            }
            
           
        }
        if(($ticket->status == "solved" || $ticket->status =="closed" || $ticket->status =="on-hold" || $ticket->status =="pending" ) && !$request->has('status') && ($request->hasFile('file') || $request->has('comment'))){ 
                $ticket->status = "open"; 
                $ticket->save();
                $isStatusChanged = 1;
				
				
        }

        $user = User::where("id",$ticket->user_id)->first();
        if($ticket_history && $user && Auth::user()->id != $user->id){
            $mdata = ['actioner'=>Auth::user(),'subject'=>'','receiver'=>$user,'view'=>'','to'=>'','ticket'=>$ticket,'isStatusChanged'=>$isStatusChanged,'action'=>'ticket_status_changed','ticket_history'=>$ticket_history,'ticket_history2'=>$ticket_history2];
            SendEmailJob::dispatch($mdata)->onConnection('database')->onQueue('emails');
            //$user->notify(new TicketActionNotification(Auth::user(),$ticket_history,$ticket,$isStatusChanged,$ticket_history2,$user,'database'));
        }

		if($request->ajax()){
			
			$result['message'] = "Posted !";
            $result['code'] = 200;
			
			return response()->json($result, $result['code']);
		}else{
			Session::flash('flash_message','Posted !');
			return redirect()->back();
		}
        //('admin/tickets/'.$request->ticket_id);
    }

    public function generatePdf(Request $request){

		ini_set('memory_limit', '2256M');
		
        $filter = array();
        
        $record = Ticket::select('tickets.*','subjects.name as subject_name','users.name as user_name','sites.name as site_name')->with('comments','_website');

        $record->leftjoin('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
        $record->leftjoin('users','users.id','=','tickets.user_id');


        if ($request->has('status') && $request->get('status') != 'all' && $request->get('status') != '') {
            $record->where('status',$request->get('status'));
        }
        if($request->has('_website_id')){
            $record->where('tickets._website_id',$request->_website_id);
        }
        if ($request->has('filter_site')  && $request->get('filter_site') != '') {
            $record->where('sites.id',$request->get('filter_site'));
        }
        if ($request->has('filter_services')  && $request->get('filter_services') != '') {
			$enable_m_ids = explode(",",$request->filter_services);
            $record->whereIn('services.id',$enable_m_ids);
        }
        if ($request->has('filter_subject')  && $request->get('filter_subject') != '') {
            $enable_m_ids = explode(",",$request->filter_subject);
            $record->whereIn('subjects.id',$enable_m_ids);
        }

        if ($request->has('range_start') && $request->get('range_start') != '') {
            $sdate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_start." 00:00:00");
            $edate =  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_end." 23:59:59");
            $record->whereBetween('tickets.created_at', [$sdate, $edate]);
            $filter['range_start'] = $sdate;
            $filter['range_end'] = $edate;
        }

        
        $record->accessible("access.ticket.all","tickets");

        $tickets = $record->orderby('_website_id','asc')->orderby('tickets.id','DESC')->offset(0)->limit(1000)->get();
//        return $tickets;

    //    return view('admin.tickets.pdf',compact('tickets','filter'));

         $view = view('admin.tickets.pdf',compact('tickets','filter'))->render();

     //   echo $view; exit;
		
		

        $pdf = \PDF::loadHTML($view);
        return $pdf->download("ticket.pdf");
	  
	  
	    //$pdf = \PDF::loadView('admin.tickets.pdf',compact('tickets','filter'));
		//return $pdf->download('invoice.pdf');

    }
    public function uploadFile(Request $request,$ticket)
    {

        $ticket_path = "d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket";
        $dir = public_path() . '/uploads/'."d".$ticket->_website_id; File::exists($dir) or File::makeDirectory($dir);
		$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id; File::exists($dir) or File::makeDirectory($dir);
		$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id; File::exists($dir) or File::makeDirectory($dir);
        $dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket"; File::exists($dir) or File::makeDirectory($dir);
		$dir_thumb = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/thumbs"; 
		File::exists($dir_thumb) or File::makeDirectory($dir_thumb);
                        
        $name = null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
			$timestamp = uniqid();
			$extension = $file->getClientOriginalExtension();
            $name = $timestamp.'.'.$extension;
			
			if(in_array($extension,['jpg','jpeg','png','PNG','JPEG','JPG'])){
				
				$img = Image::make($file->getRealPath(),array(

					'width' => 100,

					'height' => 100,

					'grayscale' => false

				));

				$img->save($dir_thumb.'/'.$name);
				
			}
			
            
            $file->move($dir,$name);
			
			

        }

        return $ticket_path."/".$name;

    }
    public function filter_by_website(Request $request)
    {
        $result = array();
        
        if ($request->has('_website_id') &&  $request->get('_website_id') != '') {
            
            

            $e_data = Equipment::select('id','equipment_id');
            $e_data->where('_website_id',$request->_website_id);
            $result['data']['equipment'] = $e_data->get();

         /*   $se_data = Service::select('id','name');
            $se_data->where('_website_id',$request->_website_id);
            $result['data']['service'] = $se_data->get();
            
            $co_data = Company::select('id','name');
            $co_data->where('companies._website_id',$request->_website_id);
            $result['data']['compoanies'] = $co_data->get();*/
            
            $site_data = \App\Site::select('sites.id','sites.name','sites.company_id');
            $site_data->where('sites._website_id',$request->_website_id);
            $result['data']['sites'] = $site_data->get();
        }

        return response()->json($result,200);
    }
}
