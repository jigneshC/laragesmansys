<?php

namespace App\Http\Controllers\Admin;

use App\Availability;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\People;
use App\Service;
use Illuminate\Http\Request;
use Session;
use App\User;
use App\DropdownsType;

use Illuminate\Support\Facades\File;

use Yajra\Datatables\Datatables;

class PeoplesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.peoples');
        $this->middleware('permission:access.people.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.people.create')->only(['create', 'store']);
        $this->middleware('permission:access.people.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.peoples.index');
    }

   /* public function datatable(Request $request) {

        $record = People::accessible("access.people.all","peoples")->with('user', 'services','_website');
        if($request->has('filter_website')){
            $record->where('peoples._website_id',$request->filter_website);
        }
        return Datatables::of($record)->make(true);
    }*/
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
   /* public function create()
    {
       // print file_get_contents('curl -X GET --header "Accept: application/json" --header "Authorization: Bearer U0pDMTFQMDFQQVMwMHxBQUF3bnZES214OHNXUi1kQzhNN1NfV2MwVmlwVnVJenZpd1FVbks3UEdXUkg1OWJOQlQ3YTJhcXFaQXJXZllRVnQ1NF9QWWRPMEVYNENYQjd4dmJsWHJodlNMR1hOeERrQ1hUbnJ2enFlcXBkdTh4UW8zTVV0UDJSX0FBY1pfVlBwTl8xZUpHVjJUYVZoSXZvMVFWcllIZ0ZabjNsRTdpbjBBVjJ0ZzFTbWw1NVdHeDhncmx6YWIxNnBJdC04Ty1pelV8YXY3VWVBfHNHaUVqSEJhd1drdFFyWGsxMkVLT2c" "https://platform.devtest.ringcentral.com/restapi/v1.0/account/229973004/extension/229973004/call-log"');

        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $people_quality = DropdownsType::getPluckValue('people_quality');

        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id')->prepend('No Default',0);
        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');//->prepend('Select..', '');

        return view('admin.peoples.create', compact('phoneTypes', 'users', 'services','people_quality'));
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
   /* {

        $this->validate($request, [
            'user_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:peoples'
        ]);

        $requestData = $request->all();

        $name = $this->uploadPhoto($request);

        if (!is_null($name)) {
            $requestData['photo'] = $name;
        }

        $people = People::create($requestData);

        $this->insertAvailabilityData($request, $people->id);

        $people->services()->sync($request->services);

        Session::flash('flash_message', __('People added!'));

        return redirect('admin/peoples');
    }*/


    /**
     * @param Request $request
     * @param $people_id
     */
 /*   public function insertAvailabilityData(Request $request, $people_id)
    {
        $av = $request->input('av', []);

        if (empty($av)) {
            return;
        }

        Availability::where('people_id', $people_id)->forceDelete();

        foreach ($av as $key => $value) {
            //done  remove data where pople_id = $request->id
            Availability::create([
                'day' => strtolower($key),
                'time' => $value['from'] . ' - ' . $value['to'],
                'people_id' => $people_id,
            ]);
        }
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
  /*  public function show($id)
    {
       $people = People::accessible("access.people.all","peoples")->with('user', 'availability_time')->where("id",$id)->first();

       if(!$people){
           Session::flash('flash_message', 'No Access !');
           return redirect()->back();
       }
       return view('admin.peoples.show', compact('people'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
  /*  public function edit($id)
    {
        $people = People::accessible("access.people.changes.all","peoples")->with('user', 'availability_time')->where("id",$id)->first();

        if(!$people){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $av = $this->getAvailability($people);

        $phoneTypes = DropdownsType::getPluckValue('phone_type');
        $people_quality = DropdownsType::getPluckValue('people_quality');

        $users = User::accessible("access.user.all","users")->website('users')->pluck('name', 'id')->prepend('No Default',0);

        $services = Service::accessible("access.service.all","services")->pluck('name', 'id');//->prepend('Select..', '');

        $user_services = $people->services()->pluck('id');
		
		//echo  $user_services;exit;
		$website_id = $people->_website_id;

        return view('admin.peoples.edit', compact('people', 'phoneTypes', 'users', 'services', 'user_services', 'av','people_quality','website_id'));
    }*/


   /* public function getAvailability(People $people)
    {
//        $arr = $people->availability()->get();
        $rr = [];
        $ava = Availability::where('people_id', $people->id)->get();

        foreach ($ava as $av) {

            $time = str_replace('to', '-', $av->time);

            $a = explode('-', $time);

            $from = trim($a[0]);

            $to = trim($a[1]);

            $rr[$av->day] = [
                'from' => $from,
                'to' => $to
            ];

        }

        return $rr;
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
   /* public function update($id, Request $request)
    {


        $this->validate($request, [
            'user_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:peoples,email,' . $id
        ]);


        $requestData = $request->all();

        $people = People::accessible("access.people.changes.all","peoples")->where("id",$id)->first();

        if(!$people){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        $name = $this->uploadPhoto($request);

        if (!is_null($name)) {
            $requestData['photo'] = $name;
            $this->removeImage($people->photo);
        }



        $people->update($requestData);

        $this->insertAvailabilityData($request, $people->id);

        $people->services()->sync($request->services);

        Session::flash('flash_message', __('People updated!'));

        return redirect('admin/peoples');
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $people = People::accessible("access.people.changes.all","peoples")->where("id",$id)->first();

        if($people){
            $this->removeImage($people->photo);

            $people->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/peoples');
        }
    }


    //

    /**
     * @param Request $request
     * @return null|string
     */
   /* public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('photo')) {

//            dd($request->file('image'));
            $file = $request->file('photo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }*/
  /*  public function removeImage($imageName)
    {
        $image_path1 = public_path()."/uploads/".$imageName;

        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
    }*/

    
}
