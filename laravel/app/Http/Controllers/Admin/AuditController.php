<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Audit;
use Illuminate\Http\Request;
use Session;

use Yajra\Datatables\Datatables;

class AuditController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.audits');
        $this->middleware('permission:access.audit.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.audit.create')->only(['create', 'store']);
        $this->middleware('permission:access.audit.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.audit.index');
    }

    public function datatable(Request $request) {

        $record = Audit::accessible("access.audit.all","audits");

        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.audit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'table_name' => 'required',
            'table_row_id' => 'required',
            'old_values' => 'required',
            'audit_by' => 'required'
        ]);



        $requestData = $request->all();

        Audit::create($requestData);

        Session::flash('flash_message', 'Audit added!');

        return redirect('admin/audits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $audit = Audit::accessible("access.audit.all","audits")->where("id",$id)->first();

        if(!$audit){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.audit.show', compact('audit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $audit = Audit::accessible("access.audit.changes.all","audits")->where("id",$id)->first();

        if(!$audit){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }

        return view('admin.audit.edit', compact('audit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'table_name' => 'required',
            'table_row_id' => 'required',
            'old_values' => 'required',
            'audit_by' => 'required'
        ]);

        $requestData = $request->all();

        $audit = Audit::accessible("access.audit.changes.all","audits")->where("id",$id)->first();

        if(!$audit){
            Session::flash('flash_message', 'No Access !');
            return redirect()->back();
        }
        $audit->update($requestData);

        Session::flash('flash_message', 'Audit updated!');

        return redirect('admin/audits');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob= Audit::accessible("access.audit.changes.all","audits")->where("id",$id)->first();
        if($ob){
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/audits');
        }

    }
}
