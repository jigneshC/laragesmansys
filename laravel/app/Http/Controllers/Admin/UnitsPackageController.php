<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\ModuleUnits;
use App\Permission;
use App\UnitPackages;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class UnitsPackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.unitpackages');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.unitpackages.index');
    }
    public function datatable(Request $request) {
        $record = UnitPackages::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

    
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
            'price_currency' => 'required',
            'unit' => 'required',
        ]);


        $requestData = $request->all();
       
        $module = UnitPackages::create($requestData);
        if($module){
            $result['message'] = \Lang::get('comman.responce_msg.item_created_success',['item'=>"Unit Package"]);
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }
    public function edit($id)
    {
        $result = array();
        $item = UnitPackages::findOrFail($id);

        if($item){
            $result['data'] = $item;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');
            $result['code'] = 400;
        }
        return response()->json($result, $result['code']);

    }

    public function update($id, Request $request)
    {
        $result = array();

        $this->validate($request, [
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
            'price_currency' => 'required',
            'unit' => 'required',
        ]);

        $item = UnitPackages::where("id",$id)->first();
        $requestData = $request->all();
        
        if($item){
            $item->update($requestData);
            $result['message'] = \Lang::get('comman.responce_msg.record_updated_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.something_went_wrong');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/units-packages');
        }
        
    }
    

    public function destroy($id,Request $request)
    {
        $item = UnitPackages::where("id",$id)->first();

        $result = array();

        if($item){
            $item->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;

        }else{
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/units-packages');
        }
    }


}
