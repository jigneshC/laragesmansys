<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\TicketHistory;
use App\User;
use App\ModuleUnits;
use App\Permission;
use App\UnitPackages;
use App\People;
use App\UnitOrder;
use App\UnitTransaction;
use App\WebSite;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;


use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;

use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

use App\Http\Controllers\EmailController;
class UnitsOrderController extends Controller
{
    private $_api_context;
    private $mail_function;

    public function __construct()
    {
        $mode = config('paypal.mode');
        $settings =  config('paypal.settings');
        ($mode == "live") ? $paypal_conf = config('paypal.live') : $paypal_conf = config('paypal.sandbox');

        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($settings);
        $this->mail_function = new EmailController();

        $this->middleware('permission:access.domainunits');
       
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    

    public function index(Request $request)
    {
        $website_id = _WEBSITE_ID;
        $orders = UnitOrder::where("id",">",0);
        
        if(_MASTER && $request->has('website_id')){
            $orders->where('website_id',$request->website_id);
        }else{
            $orders->where('website_id',$website_id);
        }

        $orders =  $orders->latest()->paginate(10);

        return view('admin.unitorder.index',compact('orders'));
    }
    public function show($id)
    {
       
        $order = UnitOrder::whereId($id)->first();
        
        //$this->mail_function->sendMailOnOrderCreate($id,$order->payment_status);
        if(!$order){
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect('admin/order');
        }
        $package = json_decode($order->unit_package);
        return view('admin.unitorder.show', compact('order','package'));
    }
    public function datatable(Request $request) {
        $record = UnitPackages::where("id",">",0);
        return Datatables::of($record)->make(true);
    }

    
    public function confirmOrder($package_id)
    {
        $package = UnitPackages::where("id",$package_id)->first();

        if(!$package){
            Session::flash('flash_message',\Lang::get('unitpackage.responce_msg.no_packages_available_for_purchase_an_units'));
            return redirect()->back();
        }

        $user = Auth::user();
        $id = $user->id;
        $peopleo = People::select("title","first_name","last_name","quality","phone_number_1","phone_number_2","code_phone_number_1","code_phone_number_2","phone_type_1","phone_type_2","emergency_password","availability","status_text","photo","active")
                ->where("user_id",$id)->first();
        
        if($peopleo){
            $peopleob = $peopleo->toArray();
            foreach ($peopleob as $key => $value) {
                $user->{$key} = $value;
            }
        }
        return view('admin.unitorder.confirm-order',compact('package','user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'buyer_name' => 'required',
            'buyer_email' => 'required',
            'unit_package_id' => 'required',
        ]);

        $package = UnitPackages::where("id",$request->unit_package_id)->first();

        if(!$package){
            Session::flash('flash_message',\Lang::get('unitpackage.responce_msg.no_packages_available_for_purchase_an_units'));
            return redirect()->back();
        }

        $requestData = $request->all();
        $requestData['website_id'] = _WEBSITE_ID;
        $requestData['unit'] = $package->unit;
        $requestData['item_price'] = $package->price;
        $requestData['price_currency'] = $package->price_currency;
        $requestData['item_qty'] = 1;
        $requestData['discount'] = 0;
        $requestData['order_total'] = $package->price;
        $requestData['unit_package'] = json_encode($package);


        $module = UnitOrder::create($requestData);
        if($module){
            return redirect('admin/order-payment/'.$module->id);
        }else{
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect()->back();
        }
       
    }
    public function pricing()
    {
        $packages = UnitPackages::where("id",">",0)->orderBy("price","ASC")->get();

        if(!$packages){
            Session::flash('flash_message',\Lang::get('unitpackage.responce_msg.no_packages_available_for_purchase_an_units'));
            return redirect()->back();
        }

        return view('admin.unitpackages.packages',compact('packages'));
        
    }


    //****************Paypal Payment************ */

    public function payment($order_id)
    {
        $order = UnitOrder::where("id",$order_id)->where("created_by", Auth::user()->id)->where("payment_status","!=","completed")->first();

        if(!$order){
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect('admin/order');
        }

        $package = UnitPackages::where("id",$order->unit_package_id)->first();

        if(!$package){
            Session::flash('flash_message',\Lang::get('unitpackage.responce_msg.no_packages_available_for_purchase_an_units'));
            return redirect('admin/order');
        }


        $des = $package->name . " " . $package->desc;

            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_1 = new Item();
            $item_1->setName($package->name)// item name
            ->setCurrency($order->price_currency)
                ->setQuantity(1)
                ->setPrice($order->order_total); // unit price

            // add item to list
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));

            $amount = new Amount();
            $amount->setCurrency($order->price_currency)
                ->setTotal($order->order_total);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription($des);

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL('admin/order-payment-success'))// Specify return URL
            ->setCancelUrl(URL('admin/order-payment-cancel'));

            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));


            try {
                $payment->create($this->_api_context);

                Session::put('paypal_payment_id', $payment->getId());
                
                $order->payment_type = 'paypal';
                $order->payment_status = "pending";
                $order->payment_request_id = $payment->getId();
                $order->save();

            } catch (\PayPal\Exception\PPConnectionException $ex) {

                Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
                $this->mail_function->sendMailOnOrderCreate($order->id,"failed");
                return redirect('admin/order');

            }


            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            if (isset($redirect_url)) {
                return redirect($redirect_url);
            } else {
                $this->mail_function->sendMailOnOrderCreate($order->id,"failed");
                Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
                return redirect('admin/order');
            }
        
        
    }
    public function paymentSuccess(Request $request)
    {
        $payment_id = Session::get('paypal_payment_id');
        $order = null;
        if (Session::has('paypal_payment_id')) {
            Session::forget('paypal_payment_id');
            $order = UnitOrder::where("payment_request_id", $payment_id)->first();
        } else {
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect("admin/order");
        }

        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            $order->payment_status = "failed";
            $order->save();

            $this->mail_function->sendMailOnOrderCreate($order->id,"failed");
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect("admin/order/" . $order->id);
        }

        try {
            $payment = Payment::get($payment_id, $this->_api_context);

            $execution = new PaymentExecution();
            $execution->setPayerId($request->input('PayerID'));
           
            //Execute the payment
            $payment->execute($execution, $this->_api_context);

            $transactions = $payment->getTransactions();
            $related_resources = $transactions[0]->getRelatedResources();
            $sale = $related_resources[0]->getSale();
            $sale_id = $sale->getId();
            if($sale_id!=""){
                 $status = "completed";
            }else{
                $status = "completed"; //$sale->getState();
            }
            $order->payment_status = $status;
            $order->payment_object = $transactions[0]->toJSON();
            $order->save();

            UnitTransaction::creditUnits(_WEBSITE_ID,$order->unit,'paypal_credit');

            $this->mail_function->sendMailOnOrderCreate($order->id,"completed");
            Session::flash('flash_message', \Lang::get('unitorder.responce_msg.order_place_successfully'));
        
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            $order->order_status = "failed";
            $order->save();

            $this->mail_function->sendMailOnOrderCreate($order->id,"failed");
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect("admin/order");
        }
        return redirect("admin/order/" . $order->id);

    }
    public function paymentCancel(Request $request)
    {
        if (Session::has('paypal_payment_id')) {
            $order = UnitOrder::where("payment_request_id", Session::get('paypal_payment_id'))->first();
            Session::forget('paypal_payment_id');
            
            $order->order_status = "canceled";
            $order->save();
            $this->mail_function->sendMailOnOrderCreate($order->id,"cancelled");
            Session::flash('flash_message',\Lang::get('unitorder.responce_msg.order_canceled'));
            return redirect("admin/order/". $order->id);
        } else {
           
            Session::flash('flash_message',\Lang::get('comman.responce_msg.something_went_wrong'));
            return redirect("admin/order");
        }
        
        
    }

    

}
