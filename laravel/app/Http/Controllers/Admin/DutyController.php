<?php

namespace App\Http\Controllers\Admin;

use App\Equipment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Setting;
use Yajra\Datatables\Datatables;
use App\Notifications\TicketAssignNotification;

use App\Ticket;
use App\Subject;
use App\Company;
use App\TicketHistory;
use App\User;
use App\Companychargablemodule;

use Illuminate\Http\Request;
use Session;
use Auth;
use Carbon;

class DutyController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:access.tickets');
        $this->middleware('permission:access.ticket.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.ticket.create')->only(['create', 'store']);
        $this->middleware('permission:access.ticket.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
     
        $enable_m_ids =Companychargablemodule::where('action', "duty_module")->pluck("company_id");
        
        $companies = Company::where('companies._website_id',_WEBSITE_ID)->whereIn('companies.id',$enable_m_ids)->pluck("name","id");
        
      /*  $subject = Subject::where('subjects._website_id',_WEBSITE_ID)->where('subjects.subject_type','duty')
            ->join('services','services.id','=','subjects.service_id')
            ->join('sites','sites.id','=','services.site_id')
            ->join('companies','companies.id','=','sites.company_id')
            ->whereIn('companies.id',$enable_m_ids);
        
        $s_data = Service::select('services.*','sites.company_id');
        $s_data->join('sites','sites.id','=','services.site_id');
        $s_data->join('companies','companies.id','=','sites.company_id');
        $s_data->whereIn('companies.id',$enable_m_ids);*/
        
        $subject = Subject::where('_website_id',_WEBSITE_ID)->where('subject_type',"duty")->first();
        return view('admin.duty.index',compact('companies'));

    }
    public function dutyDatatable(Request $request) {
        $record = Ticket::select('tickets.*','subjects.name as subject_name','users.name as user_name','sites.name as site_name','services.name as services_name')->with('logs');

        $record->join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
        $record->join('users','users.id','=','tickets.user_id');

        $record->where('subjects.subject_type',"duty");
        

        $record->where('tickets.status',"new");

        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'company_id' => 'required',
            'user_id' => 'required',
        ]);

        $subject = Subject::select("subjects.*")->where('subjects._website_id',_WEBSITE_ID)->where('subjects.subject_type',"duty")
            ->join('services','services.id','=','subjects.service_id')
            ->where('services.company_id',$request->company_id)
            ->first();
        
       

        $requestData = $request->all();
        $requestData['title'] = \Lang::get('duty.data.default_title');;
        $requestData['content'] = \Lang::get('duty.data.default_content');;

        if($subject){
            $service = \App\Service::where("id",$subject->service_id)->first();
            if($service && $service->sites){
                $requestData['site_id'] = $service->sites->first()->id;
            }
            
            $requestData['subject_id'] = $subject->id;
            $ticket = Ticket::create($requestData);
            
            $result['message'] = $service->sites;
            $result['code'] = 200;
        }else{
            $result['message'] = \Lang::get('duty.responce_msg.no_duty_subject_for_given_company');
            $result['code'] = 400;
        }
        

        
        return response()->json($result, $result['code']);

    }
    /*public function storeSubject(Request $request)
    {
        $result = array();

        $this->validate($request, [
            'subject_id' => 'required',
        ]);

      //  $subject = Subject::where("id",$request->subject_id)->first();

        $subject_for_duty_key = "subject_for_duty"._WEBSITE_ID;
        $setting = Setting::where("key",$subject_for_duty_key)->first();

        $result['message'] = \Lang::get('duty.responce_msg.subject_add_success');

        if(!$setting){
            $setting = new Setting();
            $setting->key = $subject_for_duty_key;
            $setting->name = "Subject For Duty";
            $result['message'] = \Lang::get('duty.responce_msg.subject_update_success');
        }

        $setting->value = $request->subject_id;
        $setting->save();

        $result['code'] = 200;
        return response()->json($result, $result['code']);

    }*/

    public function closeticket(Request $request)
    {
       // return $request->all();

        $ticket = Ticket::accessible("access.ticket.changes.all","tickets")->where("id",$request->ticket_id)->first();

        $result = array();

        if(!$ticket){
            $result['message'] = \Lang::get('ticket.notification.no_permission_to_access');
            $result['code'] = 400;
        }else{
            $ticket->status = 'closed';
            $ticket->save();

            $his = new TicketHistory();
            $his->ticket_id=$request->ticket_id;
            $his->history_type="action";
            $his->desc='closed';
            $his->save();

            $result['message'] = \Lang::get('ticket.notification.ticket_closed');
            $result['code'] = 200;
        }

        return response()->json($result, $result['code']);

    }


}
