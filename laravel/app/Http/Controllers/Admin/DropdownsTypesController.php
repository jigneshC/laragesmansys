<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DropdownsType;
use Illuminate\Http\Request;
use Session;

use Yajra\Datatables\Datatables;

class DropdownsTypesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.dropdowns');
        $this->middleware('permission:access.dropdown.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.dropdown.create')->only(['create', 'store']);
        $this->middleware('permission:access.dropdown.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.dropdowns-types.index');
    }

    public function datatable(Request $request) {

        $record = DropdownsType::with('creator');

        return Datatables::of($record)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.dropdowns-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:dropdowns_types',
        ]);

        $requestData = $request->all();

        DropdownsType::create($requestData);

        Session::flash('flash_message', 'DropdownsType added!');

        return redirect('admin/dropdowns-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dropdownstype = DropdownsType::findOrFail($id);
        

        return view('admin.dropdowns-types.show', compact('dropdownstype','username'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dropdownstype = DropdownsType::findOrFail($id);

        return view('admin.dropdowns-types.edit', compact('dropdownstype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $this->validate($request,
            [
                'name' => 'required|unique:dropdowns_types,name,' . $id,
            ]);

        $requestData = $request->all();

        $dropdownstype = DropdownsType::findOrFail($id);
        $dropdownstype->update($requestData);

        Session::flash('flash_message', 'DropdownsType updated!');

        return redirect('admin/dropdowns-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = DropdownsType::findOrFail($id);

        if($ob){
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/dropdowns-types');
        }
    }
}
