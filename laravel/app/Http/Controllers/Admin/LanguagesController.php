<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Language;
use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;

class LanguagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.languages');
        $this->middleware('permission:access.language.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.language.create')->only(['create', 'store']);
        $this->middleware('permission:access.language.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('admin.languages.index');
    }

    public function datatable(Request $request) {

        $record = Language::activeOrder();

        return Datatables::of($record)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'lang_code' => 'required|unique:languages',

        ]);
        $requestData = $request->all();

        Language::create($requestData);

        Session::flash('flash_message', 'Language added!');

        return redirect('admin/languages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $language = Language::findOrFail($id);

        return view('admin.languages.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $language = Language::findOrFail($id);

        return view('admin.languages.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lang_code' => 'required|unique:languages,lang_code,' . $id
        ]);
        $requestData = $request->all();

        $language = Language::findOrFail($id);
        $language->update($requestData);

        Session::flash('flash_message', 'Language updated!');

        return redirect('admin/languages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id,Request $request)
    {
        $ob = Language::find($id);

        if($ob){
            $ob->delete();
            $result['message'] = \Lang::get('comman.responce_msg.record_deleted_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            Session::flash('flash_message',$result['message']);
            return redirect('admin/languages');
        }
    }
}
