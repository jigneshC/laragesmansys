<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Session;
use App\Subject;
use App\Ticket;
use App\Company;
use App\People;
use App\Service;
use App\Site;
use App\MasterTicket;
use App\Building;
use App\Audit;
use App\Equipment;
use App\Setting;
use App\User;
use App\WebSite;

use Dedicated\GoogleTranslate\Translator;
use Illuminate\Support\Facades\File;

use Image;
use App\TicketHistory;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index() {
        return view('admin.dashboard');
    }

    public function exportData(Request $request) {
		$file_name = $request->module;
		
		if($request->module == "audits"){
			$SELECT = ['audits.*'];
			$result= Audit::select($SELECT);
		}else if($request->module == "buildings"){
			$SELECT = ['buildings.*','sites.name as site_name'];
			$result= Building::select($SELECT);
			$result->join('sites','sites.id','=','buildings.site_id');
		}else if($request->module == "master_tickets"){
			$SELECT = ['master_tickets.*','subjects.name as subject_name','sites.name as site_name','services.name as services_name'];
			$result= MasterTicket::select($SELECT);
			$result->join('subjects','subjects.id','=','master_tickets.subject_id');
			$result->join('services','services.id','=','subjects.service_id');
			$result->join('sites','sites.id','=','master_tickets.site_id');
			$result->join('companies','companies.id','=','sites.company_id');
			
		}else if($request->module == "duty"){
			$SELECT = ['tickets.*','subjects.name as subject_name','users.name as user_name','sites.name as site_name','services.name as services_name'];
			$result = Ticket::select($SELECT);
            
			$result->join('subjects','subjects.id','=','tickets.subject_id');
			$result->join('services','services.id','=','subjects.service_id');
			$result->join('sites','sites.id','=','tickets.site_id');
			$result->join('users','users.id','=','tickets.user_id');
            
			$result->where('subjects.subject_type',"duty");
			$result->where('tickets.status',"new");
			
		}else if($request->module == "companies"){
			$SELECT = ['companies.*'];
			$result= Company::select($SELECT)->with('_website');
			
		}else if($request->module == "equipments"){
			$SELECT = ['equipments.*'];
			$result= Equipment::select($SELECT);
		}else if($request->module == "roles"){
			$SELECT = ['roles.*'];
			$result= Role::select($SELECT);
		}else if($request->module == "services"){
			$SELECT = ['services.*'];
			$result= Service::select($SELECT);
		}else if($request->module == "settings"){
			$SELECT = ['settings.*'];
			$result= Setting::select($SELECT);
		}else if($request->module == "sites"){
			$SELECT = ['sites.*'];
			$result= Site::select($SELECT)->with('_website');
		}else if($request->module == "subjects"){
			$SELECT = ['subjects.*','services.name as services_name'];
			$result= Subject::select($SELECT);
			$result->join('services','services.id','=','subjects.service_id');
		}else if($request->module == "users"){
			$SELECT = ['users.*'];
			$result= User::select($SELECT);
		}else if($request->module == "_websites"){
			$SELECT = ['_websites.*'];
			$result= WebSite::select($SELECT);
		}
		
		
		
		if($request->has('is_deleted_record') && $request->is_deleted_record == 1){
            $record->onlyTrashed();
			$file_name = $file_name ."_deleted_record";
        }
		
		if($request->has('filter_website')){
            $result->where($request->module.'._website_id',$request->filter_website);
			$file_name = $file_name ."_subdomain(".$request->filter_website.")";
        }
		if ($request->has('filter_range_start') && $request->get('filter_range_start') != '') {
            $sdate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->filter_range_start." 00:00:00");
            $edate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->filter_range_end." 23:59:59");
            $result->whereBetween($request->module.'.created_at', [$sdate, $edate]);
			
			$file_name = $file_name ."_range_".$sdate."_to_".$edate;
        }
        
		if($request->module == "master_tickets"){
			if($request->has('filter_company')){
				$result->where('companies.id',$request->filter_company);
				$file_name = $file_name ."_companies(".$request->filter_company.")";
			}
			
			if($request->has('filter_site')){
				$result->where('sites.id',$request->filter_site);
				$file_name = $file_name ."_site(".$request->filter_site.")";
			}
			
			if($request->has('filter_services')){
				$result->where('services.id',$request->filter_services);
				$file_name = $file_name ."_services(".$request->filter_services.")";
			}
		} 
		
		$res = $result->get()->toArray();
		
		$file_name = "uploads/".str_slug($file_name).".csv";
		
		$fp = fopen($file_name, 'w');

		if(isset($res['0'])){
			//echo "<pre>"; print_r($res['0']); exit;
			
			$finalArr = array_map(function($v) use($request){ 
				$_two = [];
				if($request->module == "master_tickets"){
					$_two = ["id","subject_name","site_name","services_name","active_time","activity_time_opt","view_on_date_val","view_before_date","view_start_date","view_end_date","content","created_at"];
					return array_intersect_key( $v , array_flip( $_two ) );
				}
				if($request->module == "duty"){
					$_two = ["id","subject_name","site_name","services_name","user_name","content","created_at"];
					return array_intersect_key( $v , array_flip( $_two ) );
				}
				if($request->module == "subjects"){
					$_two = ["id","name","subject_type","services_name","service_id","time","created_at"];
					return array_intersect_key( $v , array_flip( $_two ) );
				}
				if($request->module == "buildings" || $request->module == "services" ){
					return $v;
				}
				if($request->module == "companies" || $request->module == "sites" ){
					
					if(isset($v['_website']) && $v['_website']){
						$v['domain'] = $v['_website']['domain'];
					}
					// echo "<pre>"; print_r($v); exit;
					unset($v['_website']);
					
					return $v;
				}
				if($request->module == "users"){
					unset($v['created_at_tz']);
					return $v;
				}
				
				return $v;
				
				
				
				
			}, $res);
			fputcsv($fp, array_keys($finalArr['0']));
			foreach ($finalArr as $fields) {
				fputcsv($fp, $fields);
			}
		}
		
		return response()->download(public_path($file_name));
	}
    public function statistics(Request $request) {
        
        
        $record = Ticket::join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
        $record->join('companies','companies.id','=','services.company_id');
        
        $select = ['tickets.*','subjects.subject_type'];
        
        if ($request->has('range_start') && $request->get('range_start') != '') {
            $sdate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_start." 00:00:00");
            $edate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_end." 23:59:59");
            $record->whereBetween('tickets.created_at', [$sdate, $edate]);
        }
        if ($request->has('filter_services')  && $request->get('filter_services') != '') {
             $record->where('services.id',$request->get('filter_services'));
        }
        if ($request->has('filter_site')  && $request->get('filter_site') != '') {
            $record->where('sites.id',$request->get('filter_site'));
        }
        if($request->has('filter_companies')){
            $record->where('companies.id',$request->filter_companies);
        }
        if($request->has('filter_website')){
            $record->where('tickets._website_id',$request->filter_website);
        }else{
			$record->where('tickets._website_id',_WEBSITE_ID); // too many data show error timeout (need yo filter)
		}
       
        $record->select($select);
        
        $results = $record->orderBy('subject_type','DESC')->get();
        
        $graph_pie = $this->getPieGaph($results,$request);
        $graph_xy = $this->getXYGaph($results,$request);
        
        $top_twenty = $this->getToptwentyGraph($results,$request);
       
        if($request->ajax()){
            $result = ['graph_pie'=>$graph_pie,'graph_xy'=>$graph_xy,'top_twenty'=>$top_twenty];
            return response()->json($result,200);
        }else{
            return view('admin.statistics', compact('graph_pie','graph_xy','top_twenty'));
        }
        
    }
    public function getPieGaph($results,$request) {
        $sub_ticket = $results->where("subject_type",'ticket')->pluck('id');
        $sub_alarm = $results->where("subject_type",'alarm')->pluck('id');
        $sub_planned_activity = $results->where("subject_type",'planned_activity')->pluck('id');
        $sub_event = $results->where("subject_type",'event')->pluck('id');
        
        
        $stat1 = [
            ['name' => \Lang::get('statastic.label.Tickets'), 'value' => $sub_ticket->count()],
            ['name' => \Lang::get('statastic.label.planned_activity'), 'value' => $sub_planned_activity->count()],
            ['name' => \Lang::get('statastic.label.alarm'), 'value' => $sub_alarm->count()],
            ['name' => \Lang::get('statastic.label.event'), 'value' => $sub_event->count()],
        ];
        
        return $stat1;
    }
    public function getXYGaph($results,$request) {
        
        $duration_gap = $request->duration ? $request->duration : "monthly";
        $graph_x_limit = $request->x_limit ? $request->x_limit:5;
        
        $sub_ticket = $results->where("subject_type",'ticket')->pluck('id');
        $sub_alarm = $results->where("subject_type",'alarm')->pluck('id');
        $sub_planned_activity = $results->where("subject_type",'planned_activity')->pluck('id');
        $sub_event = $results->where("subject_type",'event')->pluck('id');
        
        $graph_x = [];
        $graph_y = [
                [
                    'name' => \Lang::get('statastic.label.Tickets'),
                    'type' => 'bar',
                    'stack' => 'Total',
                    'data' => []
                ],
                [
                    'name' => \Lang::get('statastic.label.planned_activity'),
                    'type' => 'bar',
                    'stack' => 'Total',
                    'data' => []
                ],
                [
                    'name' => \Lang::get('statastic.label.alarm'),
                    'type' => 'bar',
                    'stack' => 'Total',
                    'data' => []
                ],
                [
                    'name' => \Lang::get('statastic.label.event'),
                    'type' => 'bar',
                    'stack' => 'Total',
                    'data' => []
                ]
            ];
        
            for ($i = 0; $i < $graph_x_limit; $i++) {
                if($duration_gap=="weekly"){
                    $date1 = \Carbon\Carbon::now()->subWeek($i)->startOfWeek();
                    $date2 = \Carbon\Carbon::now()->subWeek($i)->endOfWeek();

                    $graph_x[] =$date1->format('d-M');
                    $graph_y[0]['data'][] = Ticket::whereIn("id", $sub_ticket)->where("created_at","<=",$date2)->where("created_at",">=",$date1)->count();
                    $graph_y[1]['data'][] = Ticket::whereIn("id", $sub_planned_activity)->where("created_at","<=",$date2)->where("created_at",">=",$date1)->count();
                    $graph_y[2]['data'][] = Ticket::whereIn("id", $sub_alarm)->where("created_at","<=",$date2)->where("created_at",">=",$date1)->count();
                    $graph_y[3]['data'][] = Ticket::whereIn("id", $sub_event)->where("created_at","<=",$date2)->where("created_at",">=",$date1)->count();
                }else if($duration_gap=="yearly"){
                    $date = \Carbon\Carbon::now()->subYear($i);
                    $graph_x[] =$date->format('Y');
                    $graph_y[0]['data'][] = Ticket::whereIn("id", $sub_ticket)->whereYear("created_at",$date->format('Y'))->count();
                    $graph_y[1]['data'][] = Ticket::whereIn("id", $sub_planned_activity)->whereYear("created_at",$date->format('Y'))->count();
                    $graph_y[2]['data'][] = Ticket::whereIn("id", $sub_alarm)->whereYear("created_at",$date->format('Y'))->count();
                    $graph_y[3]['data'][] = Ticket::whereIn("id", $sub_event)->whereYear("created_at",$date->format('Y'))->count();
                }else{
                    $date = \Carbon\Carbon::now()->subMonth($i);
                    $graph_x[] =$date->format('Y-M');
                    $graph_y[0]['data'][] = Ticket::whereIn("id", $sub_ticket)->whereYear("created_at",$date->format('Y'))->whereMonth("created_at",$date->format('m'))->count();
                    $graph_y[1]['data'][] = Ticket::whereIn("id", $sub_planned_activity)->whereYear("created_at",$date->format('Y'))->whereMonth("created_at",$date->format('m'))->count();
                    $graph_y[2]['data'][] = Ticket::whereIn("id", $sub_alarm)->whereYear("created_at",$date->format('Y'))->whereMonth("created_at",$date->format('m'))->count();
                    $graph_y[3]['data'][] = Ticket::whereIn("id", $sub_event)->whereYear("created_at",$date->format('Y'))->whereMonth("created_at",$date->format('m'))->count();
                }
            }
            
            return [
                'graph_x'=>$graph_x,
                'graph_y'=>$graph_y
            ];
    }
    public function getToptwentyGraph($results,$request) {
        
        $total_limit = $request->total_limit ? $request->total_limit:20;
        $module_name = "";
        
        $record = Ticket::join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record->join('sites','sites.id','=','tickets.site_id');
        $record->join('companies','companies.id','=','services.company_id');
        
        if ($request->has('range_start') && $request->get('range_start') != '') {
            $sdate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_start." 00:00:00");
            $edate =  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$request->range_end." 23:59:59");
            $record->whereBetween('tickets.created_at', [$sdate, $edate]);
        }
        
        $select = [];
        $select[] = \DB::raw('count(tickets.id) as value');
        if ($request->has('filter_services')  && $request->get('filter_services') != '') {
             $record->where('services.id',$request->get('filter_services'));
             $record->groupBy('subjects.id');
             $select[] = "subjects.name as name";
             $module_name = \Lang::get('statastic.label.subjects');
        }else if ($request->has('filter_site')  && $request->get('filter_site') != '') {
            $record->where('sites.id',$request->get('filter_site'));
            $record->groupBy('services.id');
            $select[] = "services.name as name";
            $module_name = \Lang::get('statastic.label.services');
        }else if($request->has('filter_companies')){
            $record->where('companies.id',$request->filter_companies);
            $record->groupBy('sites.id');
            $select[] = "sites.name as name";
            $module_name = \Lang::get('statastic.label.sites');
        }else if($request->has('filter_website')){
            $record->where('companies._website_id',$request->filter_website);
            $record->groupBy('companies.id');
            $select[] = "companies.name as name";
            $module_name = \Lang::get('statastic.label.companies');
        }else{
            $record->groupBy('companies.id');
            $select[] = "companies.name as name";
            $module_name = \Lang::get('statastic.label.companies');
        }
        $record->select($select);
        
        $results = $record->orderBy('value','DESC')->limit($total_limit)->get();
        
        $resultOb = ['module_name'=>$module_name,'items'=>[],'item_value'=>[]];
        foreach ($results as $key => $value) {
            $resultOb['items'][] = $value->name;
            $resultOb['item_value'][] = ['name'=>$value->name,'value'=>$value->value];
        }
        
        return $resultOb;
        
    }
    /**
     * Display given permissions to role.
     *
     * @return void
     */
    public function getGiveRolePermissions() {
        $roles = Role::with('permissions')->get();
//        return $roles;
        $permissions = Permission::with('child')->parent()->get();

        return view('admin.permissions.role-give-permissions', compact('roles', 'permissions'));
    }
	
	public function recoverItem(Request $request)
    {
		
		$result = array();

        $rules = array(
            'item' => 'required',
            'id' => 'required',
        );
		$validator = \Validator::make($request->all(), $rules,[]);

        if ($validator->fails())
        {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json(['message' =>$messages,'success' => false,'status' => 400],400);
        }
		
		
        $ob = 0;
		if(_MASTER){
			$ob = \App\Setting::recoverParentLevel($request->id,$request->item);
		}
		
        if($ob){
            $result['message'] = \Lang::get('comman.responce_msg.record_restored_succes');;
            $result['code'] = 200;
        }else{
            Session::flash('flash_message', 'No Access !');
            $result['message'] = \Lang::get('comman.responce_msg.you_have_no_permision_to_delete_record');;
            $result['code'] = 400;
        }


        return response()->json($result, $result['code']);
    }
	

    /**
     * Store given permissions to role.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function postGiveRolePermissions(Request $request) {
        $this->validate($request, ['role' => 'required', 'permissions' => 'required']);

        $role = Role::with('permissions')->whereName($request->role)->first();
        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }

        Session::flash('flash_message', _('Permission granted!'));

        return redirect('admin/roles');
    }

    public function getLangTranslation($to, Request $request) {

        ini_set('max_execution_time', 60000); 

        if (!$request->has('filename')) {
            echo "not valid file ";
            exit;
        }

        \App::setLocale('en');

        $langs = \Lang::get($request->filename);
        

        print "--------------------------------" . $request->filename . "-------------------------------------------<br><br><br>";
        foreach ($langs as $key => $val) {
          //  $langs[$key] = ["key"=>$val['key'],"value"=>$this->doTranslator('en', $to, $val['value'])];
            if (is_array($val)) {
                foreach ($val as $k => $s) {
                    if (is_array($s)) {
                        foreach ($s as $d => $j) {
                            if (!is_array($j))
                                $langs[$key][$k][$d] = $this->doTranslator('en', $to, $j);
                        }
                    }else {
                        $langs[$key][$k] = $this->doTranslator('en', $to, $s);
                    }
                }
            } else {
                $langs[$key] = $this->doTranslator('en', $to, $val);
            }
        }
        print "<pre>";
        var_export($langs);
        
        
        print "<br><br><br>---------------------------------------------------------------------------";
        echo json_encode($langs);
        exit;
    }

    public function doTranslator($from, $to, $data) {
        //$from = "fr";
        //$to = "en";
        $translator = new Translator;
        try {
            $data = $translator->setSourceLang($from)
                    ->setTargetLang($to)
                    ->translate($data);
        } catch (\Exception $e) {
           // $desc =  $e->getMessage();
        }
        return $data;
    }
	
	public function servecetoManysite(Request $request) {
        
		if(!\Schema::hasColumn("services", "company_id")){
			$q = "ALTER TABLE `services` ADD `company_id` INT NULL DEFAULT '0' AFTER `id`;" ;  
			\DB::statement($q);
		}
		if(!\Schema::hasColumn("tickets", "site_id")){
			$q = "ALTER TABLE `tickets` ADD `site_id` INT NULL DEFAULT NULL AFTER `id`;" ;  
			\DB::statement($q);
		}
		if(!\Schema::hasColumn("master_tickets", "site_id")){
			$q = "ALTER TABLE `master_tickets` ADD `site_id` INT NULL AFTER `id`;" ;   
			\DB::statement($q);
		}
		// ticket  (first need run script coppy current site id to ticket relation than remove : by script in index (ticket table))
		$record = Ticket::select('tickets.id','services.site_id');
        $record->join('subjects','subjects.id','=','tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record = $record->get();
        
        foreach ($record as $key => $value) {
            Ticket::whereId($value->id)->update(['site_id'=>$value->site_id]);
        }
		
		// services (set company id value in services : by script in index)
		
		$s_data = Service::select('services.id','sites.company_id');
        $s_data = $s_data->join('sites','sites.id','=','services.site_id')->get();
        
        foreach ($s_data as $key => $value) {
            Service::whereId($value->id)->update(['company_id'=>$value->company_id]);
        }
       
        $record = Service::all();
        foreach ($record as $key => $value) {
            $value->sites()->sync($value->site_id);
        }
		
		// Masteer ticket (set site id value in masterticket : by script in index)
		
		
		$record = MasterTicket::select('master_tickets.id','services.site_id');
        $record->join('subjects','subjects.id','=','master_tickets.subject_id');
        $record->join('services','services.id','=','subjects.service_id');
        $record = $record->get();
        
        foreach ($record as $key => $value) {
            MasterTicket::whereId($value->id)->update(['site_id'=>$value->site_id]);
        }
		
		if(\Schema::hasColumn("services", "site_id")){
			$q = "ALTER TABLE `services` DROP `site_id`;" ;   
			\DB::statement($q);
		}
    }
	
	public function resetFolder(Request $request) {
		ini_set('max_execution_time', 90000); 
		
		if($request->has('model') && $request->model == "ticket_crop"){

			$all_ticket = TicketHistory::orderBy("id","DESC")->offset(25000)->limit(5000)->get();
			foreach ($all_ticket as $log){
					$path_info = pathinfo($log->desc);
						if($log->history_type == "file" && $log->image_thumb_path != "" && in_array($path_info['extension'],['jpg','jpeg','png','PNG','JPEG','JPG'])){
								if(\File::exists(public_path()."/uploads/".$log->image_thumb_path)){
									
								}else{
									$result = explode('/',$log->image_thumb_path);
			
									if(isset($result[0])){
										$filename = end($result);
										$newpath = public_path()."/uploads/".str_replace($filename, '',$log->image_thumb_path);
										\File::exists($newpath) or File::makeDirectory($newpath);
										
										$img = Image::make(public_path()."/uploads/".$log->desc,array(

											'width' => 100,

											'height' => 100,

											'grayscale' => false

										));
											
										$img->save(public_path()."/uploads/".$log->image_thumb_path);
										
										echo "<br/>".public_path()."/uploads/".$log->image_thumb_path;
									}
									
									
									
									
								}
								
							
						}
			}
		}
		if($request->has('model') && $request->model == "ticket"){
			$all_ticket = Ticket::orderBy("id","DESC")->offset(7999)->limit(1000)->get();
			foreach ($all_ticket as $ticket){
				if($ticket->site){
						// create directory
						$dir = public_path() . '/uploads/'."d".$ticket->_website_id; File::exists($dir) or File::makeDirectory($dir);
						$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id; File::exists($dir) or File::makeDirectory($dir);
						$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id; File::exists($dir) or File::makeDirectory($dir);
						$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket"; File::exists($dir) or File::makeDirectory($dir);
						
						
						$ticket_path = "d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket";
						
						foreach ($ticket->logs as $log){
							if($log->history_type == "file"){
								$oldpath = "";
								if(File::exists(public_path() . '/uploads/ticket/'.$log->desc)){
									$oldpath = public_path() . '/uploads/ticket/'.$log->desc;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$log->desc)){
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$log->desc;
								}
								if($oldpath != ""){
                                    
                                    
									$destinationPath = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket";
									
									
									echo "<br/>".$oldpath;
									File::move($oldpath, $destinationPath."/".$log->desc);
									
									$log->desc = $ticket_path."/".$log->desc;
									$log->save();
									
								}else{
                                    echo "<br/> No-";
                                }
								
							}
						}
				}
				
			}
		}
		
		if($request->has('model') && $request->model == "people"){
			$all_ticket = People::all();
			foreach ($all_ticket as $ob){
				
				if($ob){
						// create directory
						$dir = public_path() . '/uploads/people'; File::exists($dir) or File::makeDirectory($dir);
						$f_path = "people";
						
						if($ob->photo != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/'.$ob->photo)){
									$oldpath = public_path() . '/uploads/'.$ob->photo;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->photo)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->photo;
								}
								if($oldpath != ""){
                                  //  $destinationPath = public_path() . '/uploads/people';
                                    $destinationPath = public_path() . '/uploads';
									
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->photo);
									
								//	$ob->photo = $f_path."/".$ob->photo;
								//	$ob->save();
									
								}
								
							}
				}
				
			}
		}
		if($request->has('model') && $request->model == "company"){
			$all_ticket = Company::all();
			foreach ($all_ticket as $ob){
				
				if($ob){
						// create directory
						$dir = public_path() . '/uploads/company'; File::exists($dir) or File::makeDirectory($dir);
						$f_path = "company";
						
						if($ob->logo != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/'.$ob->logo)){
									$oldpath = public_path() . '/uploads/'.$ob->logo;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->logo)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->logo;
								}
								if($oldpath != ""){
                                 //   $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads/';
									
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->logo);
									
								//	$ob->logo = $f_path."/".$ob->logo;
								//	$ob->save();
									
								}
								
							}
				}
				
			}
		}
		
		if($request->has('model') && $request->model == "subject"){
			$all_ticket = Subject::all();
			foreach ($all_ticket as $ob){
				
				if($ob){
						// create directory
						$dir = public_path() . '/uploads/subject'; File::exists($dir) or File::makeDirectory($dir);
						$f_path = "subject";
						
						if($ob->file != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/files/'.$ob->file)){
									$oldpath = public_path() . '/uploads/files/'.$ob->file;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->file)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->file;
								}
								if($oldpath != ""){
                                  //  $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads/files';
									
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->file);
									
									//$ob->file = $f_path."/".$ob->file;
									//$ob->save();
									
								}
								
							}
				}
				
			}
		}
		
		if($request->has('model') && $request->model == "site"){
			$all_ticket = Site::all();
			foreach ($all_ticket as $ob){
				
				if($ob){
						// create directory
						$dir = public_path() . '/uploads/site'; File::exists($dir) or File::makeDirectory($dir);
						$f_path = "site";
						
						if($ob->logo != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/'.$ob->logo)){
									$oldpath = public_path() . '/uploads/'.$ob->logo;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->logo)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->logo;
								}
								if($oldpath != ""){
                                 //   $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads';
									
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->logo);
									
								/*	$ob->logo = $f_path."/".$ob->logo;
									$ob->save();*/
									
								}
								
						}
						
						
						
						if($ob->instructions_tasks_file != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/files/'.$ob->instructions_tasks_file)){
									$oldpath = public_path() . '/uploads/files/'.$ob->instructions_tasks_file;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->instructions_tasks_file)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->instructions_tasks_file;
								}
								if($oldpath != ""){
                                    $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads';

									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->instructions_tasks_file);
									/*$ob->instructions_tasks_file = $f_path."/".$ob->instructions_tasks_file;
									$ob->save();*/
									
								}
								
						}
						if($ob->order_file != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/files/'.$ob->order_file)){
									$oldpath = public_path() . '/uploads/files/'.$ob->order_file;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->order_file)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->order_file;
								}
								if($oldpath != ""){
                                    $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads';
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->order_file);
									/*$ob->order_file = $f_path."/".$ob->order_file;
									$ob->save();*/
									
								}
								
						}
						if($ob->procedures_file != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/files/'.$ob->procedures_file)){
									$oldpath = public_path() . '/uploads/files/'.$ob->procedures_file;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->procedures_file)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->procedures_file;
								}
								if($oldpath != ""){
                                    $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads';
									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->procedures_file);
								/*	$ob->procedures_file = $f_path."/".$ob->procedures_file;
									$ob->save();*/
									
								}
								
						}
						
						if($ob->sitemaps_file != ""){
								$oldpath = "";
								if(0 && File::exists(public_path() . '/uploads/files/'.$ob->sitemaps_file)){
									$oldpath = public_path() . '/uploads/files/'.$ob->sitemaps_file;
									
								}else if(File::exists(public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->sitemaps_file)){
									
									$oldpath = public_path() . '/uploads/d39/c29/s62/ticket/'.$ob->sitemaps_file;
								}
								if($oldpath != ""){
                                    $destinationPath = public_path() . '/uploads/'.$f_path;
                                    $destinationPath = public_path() . '/uploads';

									echo "<br/>".File::move($oldpath, $destinationPath."/".$ob->sitemaps_file);
									/* $ob->sitemaps_file = $f_path."/".$ob->sitemaps_file;
									$ob->save();*/
									
								}
								
						}
				}
				
			}
		}
    }

}
