<?php

namespace App\Http\Controllers\Api;

use App\Equipment;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Subject;
use App\DropdownValue;
use App\DropdownsType;


use Auth;

class TicketController extends Controller
{


    protected $user;
	protected $lang;

    public function __construct(Request $request)
    {
//        \Event::listen('illuminate.query', function($query, $params, $time, $conn)
//        {
//            dd(array($query, $params, $time, $conn));
//        });

		
		
        $this->user = Auth::guard('api')->user();
		
		$this->lang = "gu";
		if($this->user && ($this->user->language=="en" || $this->user->language=="fr" )){
			$this->lang = $this->user->language;
		}
		
		if($request->has('lang') && $request->lang=="en" || $request->lang=="fr"){
			$this->lang = $request->lang;
		}

		\App::setLocale($this->lang);
//        $this->user = User::find(1);
    }


    protected function toJson($status = false, $data = [], $errors = [], $code = 200)
    {
        return response()->json([
            'messages' => $errors,
            'data' => $data,
            'status' => $status
        ], $code);
    }

    /**
     * //did getSubjects
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubjects(Request $request)
    {
		$lang = \App::getLocale();
		
        $subjects = Subject::select('subjects.*','dropdown_values.name as geolocation_name')
					->join('dropdown_values','subjects.geolocation_id','=','dropdown_values.parent_id')
					->where("dropdown_values.lang_code",$lang);
		
		if ($request->has('site_id') &&  $request->get('site_id') != '') {
            $s_ob =  Site::whereId($request->get('site_id'))->with('services')->first();
            $s_ids = [];
            if($s_ob && $s_ob->services){
                $s_ids = $s_ob->services->pluck("id");
            }
            $subjects->whereIn('service_id',$s_ids);
            
        }
		 

        $subjects = $subjects->get()->toArray();
		
        return $this->toJson(true, $subjects);
    }
	public function getGeolocation(Request $request)
    {
		$lang = \App::getLocale();
		
        $subjects = DropdownValue::where("dropdown_values.lang_code",$lang)
					->where("type_id",25);
		
		
		 

        $subjects = $subjects->get()->toArray();
		
        return $this->toJson(true, $subjects);
    }
	
	
	
	public function getSites()
    {
        $sites = Site::get()->toArray();

        return $this->toJson(true, $sites);
    }

    /**
     * //did getSubject(subjectId)
     * @param $subject_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubject($subject_id)
    {

        $subject = Subject::accessibleapi("access.subject.all","subjects",$this->user)->find($subject_id);
        if ($subject) {

            $data = $subject->toArray();
            return $this->toJson(true, $data);
        }
        //
        return $this->toJson(false, [], ['error' => 'No Subject Found!'], 404);
    }
	
	


    /**
     * did getTickets
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTickets()
    {
        $tickets = Ticket::accessibleapi("access.ticket.all","tickets",$this->user)->get()->toArray();

        return $this->toJson(true,$tickets);
    }

    /**
     * did getTicket(ticketId)
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTicket($ticket_id)
    {
        $ticket = Ticket::accessibleapi("access.ticket.all","tickets",$this->user)->with('subject', 'user')->find($ticket_id);
        if ($ticket) {
            return $this->toJson(true, $ticket->toArray());
        }
        //
        return $this->toJson(false, [], ['error' => 'No Ticket Found!'], 404);
    }

    /**
     * Create Ticket
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postTicket(Request $request)
    {
		$rules = array(
			'subject_id' => 'required',
            'content' => 'required',
			'site_id' => 'required',
        );
		$subject_id = 0;
		if ($request->has('geolocation_id') && $request->geolocation_id != "") {
			
			$subject = Subject::select('subjects.*')
					->join('dropdown_values','subjects.geolocation_id','=','dropdown_values.parent_id')
					->where("subjects.geolocation_id",$request->geolocation_id)->first();
			if($subject){
				unset($rules['subject_id']);
				$subject_id = $subject->id;
			}		
            
        }
		$val_msg = [
			'subject_id.required'=> \Lang::get('validation.subject_or_geolocation_require')
		];
		
		$validator = \Validator::make($request->all(), $rules, $val_msg);
		
        if ($validator->fails()) {
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'status' => 400],200);
        }

        $requestData = $request->all();

        $eq = $request->input('equipment_id', null);
        if (!is_null($eq)) {
            $requestData['equipment_id'] = $this->getEqId($eq);
        }
		
		if($subject_id){
			$requestData['subject_id'] = $subject_id;
		}

        if ($request->input('status', null) == null) {
            $requestData['status'] = 'new';
        }

        $requestData['created_by'] = $this->user->id;
		$requestData['updated_by'] = $this->user->id;

        $ticket = Ticket::create($requestData);
        if ($ticket) {
            return $this->toJson(true, $ticket, ['message' => 'Ticket Created!']);
        }
        return $this->toJson(false, [], ['error' => 'Something wrong!']);
    }


    public function getEqId($string = null)
    {
        if (is_null($string)) {
            return $string;
        }
        $eq = Equipment::firstOrCreate(array('equipment_id' => $string));

        return $eq->id;
    }


    /**
     *Edit ticket using ticket_id
     * @param $ticket_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function putTicket($ticket_id, Request $request)
    {
        $ticket = Ticket::accessibleapi("access.ticket.changes.all","tickets",$this->user)->find($ticket_id);
        if (!$ticket) {
            return $this->toJson(false, [], ['error' => 'No Ticket Found!'], 404);
        }
        $validator = \Validator::make($request->all(), [
//            'subject_id' => 'required',
//            'title' => 'required',
//            'content' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return $this->toJson(false, [], $errors);
        }

        $requestData = $request->all();

        $requestData['user_id'] = $this->user->id;

        $eq = $request->input('equipment_id', null);
        if (!is_null($eq)) {
            $requestData['equipment_id'] = $this->getEqId($eq);
        }

        $update = $ticket->update($requestData);
        if ($update) {
            return $this->toJson(true, $ticket, ['message' => 'Ticket Updated!']);
        }
        return $this->toJson(false, [], ['error' => 'Something wrong!']);
    }

    /**
     *
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTicket($ticket_id)
    {
        $ticket = Ticket::accessibleapi("access.ticket.changes.all","tickets",$this->user)->delete($ticket_id);
        if ($ticket) {
            return $this->toJson(true, [], ['message' => 'Ticket deleted!']);
        }
        return $this->toJson(false, [], ['error' => 'Something wrong!']);
    }

}
