<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\WebSite;
use App\Qrcode;
use App\User;
use App\Ticket;
use App\Subject;


class QrcodeController extends Controller
{

    public function scanQrcode(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
        $rules = array(
            'qrcode' => 'required',
            'api_token'=>'required'
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$user = auth('api')->user();
			$user_accessible_website = [$user->_website_id];
            foreach ($user->accessiblewebsites as $w){
                $user_accessible_website[]=$w->id;
            }
				
			$s = explode("/",$request->qrcode);
			$code = end($s);
			$qrcode = Qrcode::where("qrcode", $code)->first();
			
			if($qrcode){
				
				if($qrcode->website && $qrcode->refe_table_field_name == "_websites_id"){
					$website = $qrcode->website;
					if ($website && in_array($website->id,$user_accessible_website)) {
						unset($website->refimages,$website->deleted_at,$website->master,$website->enable_charge,$website->enable_qrcode,$website->qrcode_apitoken,$website->units,$website->created_at,$website->updated_at);
						
						$res = ["qrcode_type"=>"domain","domain"=>make_null($website),"site"=>[]];
						$result["data"]  = $res;
						$result["code"] = 200;
					}else{
						$result["messages"]  = \Lang::get('comman.responce_msg.you_have_no_permision_to_access');;
					}
				}else if($qrcode->site && $qrcode->refe_table_field_name == "sites_id"){
					$website = null;
					$site = $qrcode->site;
					if($site) {
						$website = $site->_website;
						
						if($site->company){
						 $subject = Subject::select("subjects.*")->where('subjects.subject_type',"duty")
							->join('services','services.id','=','subjects.service_id')
							->where('services.company_id',$site->company->id)
							->first();
						
							if($subject && $request->has('longitude') && $request->has('latitude')){
								
								
						$requestData['title'] = \Lang::get('duty.data.default_title');;
						$link  = "https://maps.google.com/?q=".$request->latitude.",".$request->longitude."";
						$requestData['content'] = "Site - ".$site->name." <a href='".$link."' target='_blank'> ".$link." </a>";	
						$requestData['site_id'] = $site->id;
						$requestData['subject_id'] = $subject->id;
						$requestData['_website_id'] = $site->_website->id;
						$requestData['user_id'] = $user->id;
						$requestData['company_id'] = $site->company->id;
						$ticket = Ticket::create($requestData);
						
									
						
							}
						}
					}
					if ($website && in_array($website->id,$user_accessible_website)) {
						
						$website = make_null($website);
						unset($website['refimages'],$website['deleted_at'],$website['master'],$website['enable_charge'],$website['qrcode_apitoken'],$website['units'],$website['created_at'],$website['updated_at']);
						
						unset($site->_website);
						$res = ["qrcode_type"=>"site","domain"=>$website,"site"=>make_null($site)];
						$result["data"]  = $res;
						$result["code"] = 200;
					}else{
						$result["messages"]  = \Lang::get('comman.responce_msg.you_have_no_permision_to_access');;
					}
				}else{
					$result["messages"]  = \Lang::get('comman.responce_msg.you_have_no_permision_to_access');;
				}
				
			}else{
				$result["messages"]  = \Lang::get('constant.responce_msg.warning_invalid_qrcode');
			}
			
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
		}

	/*	\Mail::raw(json_encode($request->all()), function ($message) {
  $message->to("jignesh.citrusbug@gmail.com")
    ->subject("Call test");
});*/
		
		return $this->JsonResponse($result);
    }
}
