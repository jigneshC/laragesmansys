<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\WebSite;
use App\Qrcode;
use App\User;
use App\People;
use App\Subject;
use App\Site;
use App\Ticket;
use App\TicketHistory;

use Illuminate\Support\Facades\File;
use Image;

class TicketController extends Controller
{
	protected $user;
	protected $lang;
	
	public function __construct(Request $request)
    {
		$this->user = auth('api')->user();
		if($request->has('lang') && $request->lang=="en" || $request->lang=="fr"){
			$this->lang = $request->lang;
		}else if($this->user && ($this->user->language=="en" || $this->user->language=="fr" )){
			$this->lang = $this->user->language;
		}else{
			$this->lang = "fr";
		}

		\App::setLocale($this->lang);
	}

	public function getTicket(Request $request)
    {
		$result = ["data"=>["ticket"=>[]],"code"=>400,"messages"=>""];
		
		$rules = array(
            'ticket_id' => 'required',
            'api_token'=>'required'
        );
		

		$validator = \Validator::make($request->all(), $rules, [],trans('ticket.label'));

        if (!$validator->fails())
        {
			$user = auth('api')->user();
			$ticket = Ticket::getFullDetail($request->ticket_id);
			if ($ticket) {
				$result["data"]["ticket"] = $ticket; 
				$result["messages"]  = "";
				$result["code"] = 200;		
			}else{
				$result["messages"]  = \Lang::get('ticket.notification.error_tikcet_not_found');
				$result["code"] = 400;
			}		
				
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}		
			
		return $this->JsonResponse($result);
    }
    public function getTickets(Request $request)
    {
		($request->has('per_page')) ? $perPage = $request->per_page : $perPage = \config('settings.PER_PAGE_DATA');
		$result = ["data"=>["tickets"=>[]],"code"=>400,"messages"=>""];
		
		$rules = array(
            'site_id' => 'required',
            'api_token'=>'required'
        );
		

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$user = auth('api')->user();
			$last24hrs = \Carbon\Carbon::now()->subHours(24);
			
			$select = ["tickets.created_by","tickets.id","tickets.site_id","tickets.subject_id","tickets.user_id","tickets.content","tickets.status","tickets.created_at","subjects.name as subject_name","subjects.subject_type"];
			$record = Ticket::select($select);
			$record->join('subjects','subjects.id','=','tickets.subject_id');
			$record->where('site_id',$request->site_id);
			
			$where_filter = "((tickets.status !='closed')  OR (tickets.updated_at > '$last24hrs') )";
            $record->whereRaw($where_filter);
			
			
			if($request->has('search') && $request->search !="" ){
				$record->where('content', 'LIKE', "%$request->search%")->orWhere('subjects.name', 'LIKE', "%$request->search%");
			}
			$record->orderBy("id","DESC");
			$record = $record->paginate($perPage);
			//echo "<pre>"; print_r($record); exit;
			$record = make_null($record);
			$record['data'] = array_map(function($v) { unset($v['created_at'],$v['creator'],$v['evidence'],$v['actions'],$v['total_files'],$v['is_editable']); return $v; }, $record['data']);
			
			$result["data"]['tickets']  = $record;
			$result["code"] = 200;		
				
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}		
			
		return $this->JsonResponse($result);
    }
	
	public function createTicket(Request $request)
    {
		$result = ["data"=>["ticket"=>[]],"code"=>400,"messages"=>""];
		$user = auth('api')->user();
		
		$rules = array(
			'subject_id' => 'required',
            'content' => 'required',
			'site_id' => 'required',
        );
		
		$site = Site::whereId($request->site_id)->first();
		
		$subject_id = 0;
		if ($request->has('geolocation_id') && $request->geolocation_id != "") {
			
			$subject = Subject::select('subjects.*')
					->join('dropdown_values','subjects.geolocation_id','=','dropdown_values.parent_id')
					->where("subjects.geolocation_id",$request->geolocation_id)->first();
			if($subject){
				unset($rules['subject_id']);
				$subject_id = $subject->id;
			}		
            
        }
		
		$validator = \Validator::make($request->all(), $rules, [],trans('ticket.label'));
		if(!$site){
			$result["messages"]  = "Invalid site ID or subject ID";
        }else if (!$validator->fails()) {
			$requestData = $request->all();

			$eq = $request->input('equipment_id', null);
			if (!is_null($eq)) {
				$requestData['equipment_id'] = $this->getEqId($eq);
			}
			
			if($subject_id){
				$requestData['subject_id'] = $subject_id;
			}

			if ($request->input('status', null) == null) {
				$requestData['status'] = 'new';
			}

			$requestData['created_by'] = $user->id;
			$requestData['updated_by'] = $user->id;

			$obj = Ticket::create($requestData);
			if ($obj) {
				
				
				if($request->has('longitude') && $request->has('latitude') && $request->longitude != '' ){
					$his = new TicketHistory();
					$his->ticket_id=$obj->id;
					$his->longitude=$request->longitude;
					$his->latitude=$request->latitude;
					$his->history_type="created";
					$his->save();
				}
				$ticket = Ticket::getFullDetail($obj->id);
				$result["data"]['ticket'] = $ticket; 
				$result["messages"]  = \Lang::get('ticket.notification.ticket_created_success');
				$result["code"] = 200;		
			}
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}

        
        return $this->JsonResponse($result);
    }
	
	public function createTicketAction(Request $request)
    {
		$result = ["data"=>["ticket"=>[]],"code"=>400,"messages"=>""];
		$user = auth('api')->user();
		
		$rules = array(
			'ticket_id' => 'required',
            'action_type' => 'required',
		);
		
		if($request->has('action_type') && $request->action_type == 'document'){
			$rules['file'] = 'required|mimes:jpeg,png,jpg,gif,svg|max:5000';
		}
		if($request->has('action_type') && $request->action_type == 'comment'){
			$rules['comment'] = 'required';
		}
		if($request->has('action_type') && $request->action_type =='call_log'){
			$rules['caller_id'] = 'required';
		}
		
		$validator = \Validator::make($request->all(), $rules,[],trans('ticket.label'));
		
		if (!$validator->fails()) {
			$ticket = Ticket::where("id",$request->ticket_id)->first();
			if ($ticket) {
				$his = new TicketHistory();
				$his->ticket_id=$request->ticket_id;
				
				if($request->has('longitude') && $request->has('latitude') && $request->longitude != '' ){
					$his->longitude=$request->longitude;
					$his->latitude=$request->latitude;
				}
				
				if ($request->action_type =='document' && $request->hasFile('file') ) {

					$name = $this->uploadFile($request,$ticket);
					if($name){
						
						$his->history_type="file";
						$his->note=$request->get('comment','');
						$his->desc=$name;
						$his->save();
					}
					$result["messages"]  = \Lang::get('ticket.notification.sucess_file_added');
				}else if ($request->action_type =='comment') {
					
					$his->history_type="comment";
					$his->desc=$request->comment;
					$his->save();
					
					$result["messages"]  = \Lang::get('ticket.notification.sucess_comment_added');
				}else if ($request->action_type =='call_log') {
					$user = People::where("id",$request->caller_id)->first();
					
					$his->history_type="call_log";
					$his->desc= ($user)? $user->full_name : $request->caller_id;
					$his->save();
					$result["messages"]  = \Lang::get('ticket.notification.sucess_calllog_added');
				}
				
				$ticket = Ticket::getFullDetail($request->ticket_id);
				$result["data"]["ticket"] = $ticket;
				$result["code"] = 200;		
				
			}else{
				$result["messages"]  = \Lang::get('ticket.notification.error_tikcet_not_found');
				$result["code"] = 400;
			}
			
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}

        
        return $this->JsonResponse($result);
    }
	
	public function uploadFile(Request $request,$ticket)
    {

        $ticket_path = "d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket";
        $dir = public_path() . '/uploads/'."d".$ticket->_website_id; File::exists($dir) or File::makeDirectory($dir);
		$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id; File::exists($dir) or File::makeDirectory($dir);
		$dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id; File::exists($dir) or File::makeDirectory($dir);
        $dir = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/ticket"; File::exists($dir) or File::makeDirectory($dir);
		$dir_thumb = public_path() . '/uploads/'."d".$ticket->_website_id."/c".$ticket->site->company_id."/s".$ticket->site->id."/thumbs"; 
		File::exists($dir_thumb) or File::makeDirectory($dir_thumb);
                        
        $name = null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
			$timestamp = uniqid();
			$extension = $file->getClientOriginalExtension();
            $name = $timestamp.'.'.$extension;
			
			if(in_array($extension,['jpg','jpeg','png','PNG','JPEG','JPG'])){
				
				$img = Image::make($file->getRealPath(),array(

					'width' => 100,

					'height' => 100,

					'grayscale' => false

				));

				$img->save($dir_thumb.'/'.$name);
				
			}
			
            
            $file->move($dir,$name);
			
			

        }

        return $ticket_path."/".$name;

    }
	
}
