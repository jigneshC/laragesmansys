<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\WebSite;
use App\Qrcode;
use App\User;
use App\Site;


class SiteController extends Controller
{

    public function getSites(Request $request)
    {
		$result = ["data"=>["sites"=>[]],"code"=>400,"messages"=>""];
		
		$rules = array(
            '_website_id' => 'required',
            'api_token'=>'required'
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
        
			$user = auth('api')->user();
			if($user->hasWeb($request->_website_id) || $user->_website_id == $request->_website_id){
				
			}else{
				$result["messages"]  = \Lang::get('comman.responce_msg.you_have_no_permision_to_access');;
			}
			
			$select = ["id",'name','address','city','postcode','country','email'];
			
			$sites = Site::select($select);	
			if($request->has('search') && $request->search !="" ){
				$sites->where('name', 'LIKE', "%$request->search%");
			}
			$sites->where('_website_id',$request->_website_id);
			
			$sites = make_null($sites->get());
			//$sites = array_map(function($v) { unset($v['refimages']); return $v; }, $sites);
			
			$result["data"]['sites']['data']  = $sites;
			$result["code"] = 200;		
				
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}		
			
		return $this->JsonResponse($result);
    }
}
