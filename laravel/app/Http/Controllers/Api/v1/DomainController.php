<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\WebSite;
use App\Qrcode;
use App\User;


class DomainController extends Controller
{

    public function getDomain(Request $request)
    {
		$result = ["data"=>["domains"=>[]],"code"=>400,"messages"=>""];
		
        
		$user = auth('api')->user();
		$user_accessible_website = [$user->_website_id];
        foreach ($user->accessiblewebsites as $w){
            $user_accessible_website[]=$w->id;
        }
		$select = ["id","domain","description","name","status"];
		
		$domains = WebSite::select($select)->with('refimages')->active()->whereIn("id",$user_accessible_website);	
		if($request->has('search') && $request->search !="" ){
			$domains->where('domain', 'LIKE', "%$request->search%");
		}
		$website = make_null($domains->get());
		$website = array_map(function($v) { unset($v['refimages']); return $v; }, $website);
		
		$result["data"]['domains']['data']  = $website;
		$result["code"] = 200;		
				
			
		return $this->JsonResponse($result);
    }
}
