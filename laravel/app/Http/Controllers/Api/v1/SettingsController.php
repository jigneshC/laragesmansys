<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Carbon\Carbon;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Response;

use App\User;
use App\Categories;
use App\Branches;
use App\Setting;
use App\Survey;
use App\Issue;

use App\Responcetext as RT;

class SettingsController extends Controller
{
	
	public function getSettings(Request $request)
    {   
		$result = ["data"=>[],"code"=>400,"messages"=>""];
       
	   
		$categories = Categories::select(['id','name','slug','parent_id'])->with('child')->Parentonly()->orderby('display_order','asc')->get();
		$branches = Branches::select(['id','name'])->get();
		$constants = RT::select(['id','slug','desc'])->constantonly()->get();
		
		$data =[
			'branches'=>getBranchwithImages($branches),
			'categories'=>make_null($categories),
			'constants'=>make_null($constants)
		];

		$result['data'] = $data;

		$result["code"] = 200;
		return $this->JsonResponse($result);
    }
	
	public function syncData(Request $request)
    {
		$lang ="en";
		try{
			
		$user = JWTAuth::touser($request->header('authorization'));
		$result = ["data"=>[],"code"=>400,"messages"=>trans('common.responce_msg.something_went_wrong',[],$lang)];
        $requestData = $request->all();
		$date_format = "d/m/Y";
		$survey_imgs = [];
		$issue_imgs = [];
		$survey = null;
		$steps = "";
		
		$sync_data = json_decode($request->sync_data,true);
		
		if(!is_array($sync_data)){
			$steps = $steps." need-second-time-json-encode ,";
			$sync_data = json_decode($sync_data,true);
		}
		
		
		
		
		
		
		
		if(isset($sync_data['survey'])){
			$steps = $steps." survey-exist-request ,";
			$survey_date = Carbon::createFromFormat($date_format,$sync_data['survey']['survey_date'])->format('Y-m-d');
			$created = Carbon::createFromTimestamp($sync_data['survey']['created'])->format('Y-m-d');
			
			$input =  [];
			$input['owner_name'] =  $sync_data['survey']['owner_name'];
			$input['owner_email'] =  $sync_data['survey']['owner_email'];
			$input['property_desc'] =  $sync_data['survey']['property_desc'];
			$input['address'] =  $sync_data['survey']['address'];
			$input['legal_thing_a'] =  $sync_data['survey']['legal_thing_a'];
			$input['legal_thing_isa'] =  $sync_data['survey']['legal_thing_isa'];
			$input['is_the_sale_law_1973'] =  $sync_data['survey']['is_the_sale_law_1973'];
			$input['is_consultants'] =  $sync_data['survey']['is_consultants'];
			$input['not_tested'] =  $sync_data['survey']['not_tested'];
			$input['general_info'] =  $sync_data['survey']['general_info'];
			$input['other_info'] =  $sync_data['survey']['other_info'];
			$input['surveyor_note'] =  $sync_data['survey']['surveyor_note'];
			$input['reviews_and_summary'] =  $sync_data['survey']['reviews_and_summary'];
			$input['unique_id'] =  $sync_data['survey']['unique_id'];
			$input['surway_logo'] =  $sync_data['survey']['surway_logo'];
			$input['surveyor_id'] =  $user->id;
			$input['created'] =  $created;
			$input['survey_date'] =  $survey_date;
			
			if(isset($sync_data['survey']['id'])){
				$input['local_id'] =  $sync_data['survey']['id'];
			}
			
			
			
			$exist = Survey::where('unique_id',$sync_data['survey']['unique_id'])->first();
			if(!$exist){
				$input['status'] =  "new";
				$steps = $steps." survey-exist-db ,";
			}
			
			$survey =Survey::updateOrCreate(['unique_id'=>$sync_data['survey']['unique_id']],$input);
			
			if($survey && isset($sync_data['survey']['images']) && $sync_data['survey']['images'] !="")
			{
				
				$files =explode(",",$sync_data['survey']['images']);
				foreach($files as $k=> $file){
					$pathinfo = pathinfo($file);
					if(isset($pathinfo['extension'])){
					 $survey_imgs[$pathinfo['filename'].'.'.$pathinfo['extension']] = $survey->id;
					}
				}
			}
			
			if($survey){
				$result['messages'] = trans('common.responce_msg.record_created_succes',[],$lang);
				$result['code'] = 200;
				$steps = $steps." survey-created-or-updated-done ,";
			}else{
				$steps = $steps." survey-created-or-updated-failed ,";
			}
		
			
			$result['data'] = $survey;
		}else{
			$steps = $steps." survey-ob-not-exitst-in request ,";
		}
		
		if(isset($sync_data['issues'])){
			
			foreach($sync_data['issues'] as $i => $val){
			
				$input =  [];
				$created = Carbon::createFromTimestamp($sync_data['issues'][$i]['created'])->format('Y-m-d');
				$input['survey_id'] =  $survey->id;
				$input['surveyor_id'] =  $user->id;
				$input['child_category_id'] =  $sync_data['issues'][$i]['child_category_id'];
				
				if(isset($sync_data['survey']['category_id'])){
					$input['category_id'] =  $sync_data['issues'][$i]['category_id'];
				}
				
				$input['location'] =  $sync_data['issues'][$i]['location'];
				$input['issue_detail'] =  $sync_data['issues'][$i]['issue_detail'];
				$input['recommendation'] =  $sync_data['issues'][$i]['recommendation'];
				$input['quote'] =  $sync_data['issues'][$i]['quote'];
				$input['note'] =  $sync_data['issues'][$i]['note'];
				$input['unit_cost'] =  $sync_data['issues'][$i]['unit_cost'];
				$input['number_of_unit'] =  $sync_data['issues'][$i]['number_of_unit'];
				$input['created'] =  $created;
				
				if(isset($sync_data['survey']['id'])){
					$input['local_id'] =  $sync_data['issues'][$i]['id'];
				}
			
			
				$exist = Issue::where('unique_id',$sync_data['issues'][$i]['unique_id'])->first();
				if(!$exist){
					$input['status'] =  "new";
				}
			
			
				$issue =Issue::updateOrCreate(['unique_id'=>$sync_data['issues'][$i]['unique_id']],$input);
				
				if($issue && isset($sync_data['issues'][$i]['images']) && $sync_data['issues'][$i]['images'] != "")
				{
					
						$files =explode(",",$sync_data['issues'][$i]['images']);
						foreach($files as $k=> $file){
							$pathinfo = pathinfo($file);
							if(isset($pathinfo['extension'])){
							$issue_imgs[$pathinfo['filename'].'.'.$pathinfo['extension']] = $issue->id;
							}
						}
				}
			
			}
			
			if($survey){
				$survey->issue_count = $survey->issue->count();
				$survey->save();
				
				$result['messages'] = trans('common.responce_msg.record_created_succes',[],$lang);
				$result['code'] = 200;
				
				
				
			}else{
				$result['messages'] = trans('common.responce_msg.something_went_wrong',[],$lang);
				$result['code'] = 400;
				$steps = $steps." survey-ob-not-found-in-issue loop ,";
			}
		}else{
			$steps = $steps." no-issue-found ,";
		}
		
		if($request->hasFile('images'))
		{
			$survey_imgs_ob = [];
			$issue_imgs_ob = [];
			
			$files = $request->file('images');
			foreach ($files as $i => $file) {
				$real_name = $file->getClientOriginalName();
				if(isset($survey_imgs[$real_name])){
					$dfiles = [$file];
					uploadModalReferenceFile($dfiles,'uploads/survey/'.$survey_imgs[$real_name],'survey_id',$survey_imgs[$real_name],'survey_image');
				}else if(isset($issue_imgs[$real_name])){
					$dfiles = [$file];
					uploadModalReferenceFile($dfiles,'uploads/issues/'.$issue_imgs[$real_name],'issue_id',$issue_imgs[$real_name],'issue_image');
				}
			}
			
		}
			
		
		}catch(\Exception $e){
			$result['messages'] = $e->getMessage();
			$result['code'] = 400;
			$steps = $steps." try-catch : ".$e->getMessage();
		}
		
		$now = Carbon::now();
		if($request->has('sync_data')){
			\Mail::send('email.test', ['data'=>$sync_data,'requestData'=>$requestData,'steps'=>$steps], function ($message) use($now) {
			  $message->to("jitendra.citrusbug@gmail.com")->cc(['jignesh.citrusbug@gmail.com'])
				->subject("Sync api test ".$now);
			});
		} 
		
		
		return $this->JsonResponse($result);

        
    }
	
	public function getUpdatetime(Request $request)
    {   
		$result = ["data"=>[],"code"=>400,"messages"=>""];
       
	   
		$settings = Setting::get();
		$data =[];
		
		foreach($settings as $setting){
			$data[$setting->key] = $setting->value;
		}

		$result['data'] = $data;

		$result["code"] = 200;
		return $this->JsonResponse($result);
    }
	
	
}
