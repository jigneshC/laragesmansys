<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

use App\WebSite;
use App\Qrcode;
use App\User;
use App\Site;
use App\Ticket;
use App\Subject;


class SubjectsController extends Controller
{

    public function getSubjects(Request $request)
    {
		($request->has('per_page')) ? $perPage = $request->per_page : $perPage = \config('settings.PER_PAGE_DATA');
		$result = ["data"=>["subjects"=>[]],"code"=>400,"messages"=>""];
		
		$rules = array(
            'site_id' => 'required',
            'api_token'=>'required'
        );
		

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$s_ob =  Site::whereId($request->get('site_id'))->with('services')->first();
            $s_ids = [];
            if($s_ob && $s_ob->services){
                $s_ids = $s_ob->services->pluck("id");
            }
           
			
			$user = auth('api')->user();
			$record = Subject::select('id','name','service_id','subject_type','file','geolocation_id')->with('caller_list');
			$record->whereIn('service_id',$s_ids);
			
			if($request->has('search') && $request->search !="" ){
				$record->where('name', 'LIKE', "%$request->search%");
			}
			$record = $record->paginate($perPage);
			$record = make_null($record);
			
			/*$record['data'] = array_map(function($v) { unset($v['created_at'],$v['creator'],$v['evidence'],$v['actions'],$v['total_files'],$v['is_editable']); return $v; }, $record['data']);*/
			
			$result["data"]["subjects"]  = $record;
			$result["code"] = 200;		
				
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}		
			
		return $this->JsonResponse($result);
    }
}
