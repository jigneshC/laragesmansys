<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Password;
use Hash;

use Illuminate\Support\Facades\Validator;


use App\Response;
use App\User;
use App\OtpUser;
use App\Qrcode;
use Carbon\Carbon;


class UserController extends Controller
{
	public function UserLogin(Request $request)
    {
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'email' => 'required|email',
            'password'=>'required'
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$input = $request->only('email','password');
            
			$user = User::where("email", $request->email)->first();
			
			if ($user)
            {
				if($user->login_from_date && $user->login_from_date !="" && Carbon::now() < $user->login_from_date){
					$result["messages"] =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
					
				}else if($user->login_to_date && $user->login_to_date !="" && Carbon::now() > $user->login_to_date){
					$result["messages"] =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
					
				}else if (Hash::check($request->password, $user->password))
				{
					if($request->has('longitude')){
						$user->longitude = $request->longitude;
					}
					if($request->has('latitude')){
						$user->latitude = $request->latitude;
					}
					if($request->has('device_token')){
						$user->device_token = $request->device_token;
					}
					if($request->has('device_type')){
						$user->device_type = $request->device_type;
					}
					$user->save();
					
					
					$people = $user->people;
					if($people && $people->phone_number_1 && $people->phone_number_1 != ""){
						$code = "";
						if($people->code_phone_number_1 && $people->code_phone_number_1 != ""){
							$code = "+".\config('country.'.strtoupper($people->code_phone_number_1).'.code');
						}
						$user->phone_number = $code." ".$people->phone_number_1;
					}
					unset($user->people);
                    $res=make_null($user);
					
					$result["data"]["user"] = $res;
					$result["code"] = 200;
					$result["messages"] = \Lang::get('constant.responce_msg.success_login');
						
					
					
				}else{
					$result["messages"] = \Lang::get('constant.responce_msg.warning_incorrect_email_or_password');
				}
			   
		    }else{
				$result["messages"] =\Lang::get('constant.responce_msg.warning_incorrect_email_or_password');
			}
            
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
             
    }
	
	public function qrcodeLoginSubmit(Request $request)
    {
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'qrcode' => 'required',
            'otp'=>'required'
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$user = null;
			$s = explode("/",$request->qrcode);
			$code = end($s);
			$qrcode = Qrcode::where("qrcode", $code)->where("refe_table_field_name","users_id")->first();
			
			
			if ($qrcode)
            {	
				$people = null;
				$user = $qrcode->user;
			}
			
			if ($user)
            {
				$otpuser = $user->otpuser;
				
				if(!$otpuser || ($otpuser && $otpuser->otp != $request->otp)){
					$result["messages"] = \Lang::get('constant.responce_msg.warning_otp_token_is_not_valid');
				}else if(Carbon::now() > $otpuser->exp_time){
					$result["messages"] = \Lang::get('constant.responce_msg.warning_otp_token_expired');
				}else{
					if($request->has('longitude')){
						$user->longitude = $request->longitude;
					}
					if($request->has('latitude')){
						$user->latitude = $request->latitude;
					}
					if($request->has('device_token')){
						$user->device_token = $request->device_token;
					}
					if($request->has('device_type')){
						$user->device_type = $request->device_type;
					}
					$user->save();
					
					$people = $user->people;
					if($people && $people->phone_number_1 && $people->phone_number_1 != ""){
						$code = "";
						if($people->code_phone_number_1 && $people->code_phone_number_1 != ""){
							$code = "+".\config('country.'.strtoupper($people->code_phone_number_1).'.code');
						}
						$user->phone_number = $code." ".$people->phone_number_1;
					}
					unset($user->people);
                    $res=make_null($user);
                    
					$result["data"]["user"] = $res;
					$result["code"] = 200;
					$result["messages"] = \Lang::get('constant.responce_msg.success_login');
				}
			}else{
				$result["messages"] =\Lang::get('constant.responce_msg.warning_invalid_qrcode');
			}
            
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
             
    }
	
	
	public function qrcodeLogin(Request $request)
    {
		$otp_exp_minuts = 30;
		
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'qrcode'=>'required',
        );

		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$s = explode("/",$request->qrcode);
			$code = end($s);
			$qrcode = Qrcode::where("qrcode", $code)->where("refe_table_field_name","users_id")->first();
			
			if ($qrcode)
            {	
				$people = null;
				$user = $qrcode->user;
				
				if($user){
					$people = $user->people;
				}
				 
				if(!$user){
					$result["messages"] =\Lang::get('constant.responce_msg.warning_invalid_qrcode');
				}else if($user->login_from_date && $user->login_from_date !="" && Carbon::now() < $user->login_from_date){
					$result["messages"] =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
				}else if($user->login_to_date && $user->login_to_date !="" && Carbon::now() > $user->login_to_date){
					$result["messages"] =\Lang::get('constant.responce_msg.warning_your_account_not_activated_yet_or_disabled');
				}else {
					$lang = $user->language;
					$send_sms = false;
					$otp = substr(number_format(time() * rand(),0,'',''),0,5);
					$odata = ["user_id"=>$user->id,"otp"=>$otp,"exp_time"=>Carbon::now()->addMinutes($otp_exp_minuts)];
					$otpuser = OtpUser::updateOrCreate(["user_id"=>$user->id],$odata);
					
					$msg= \Lang::get('auth.here_is_otp_to_login',["otp"=>$otp]);
					
					$udata = [];
					$udata['full_name'] = $user->full_name;
					$udata['email'] = substr($user->email, 0,3)."XXXXXXX". substr($user->email,-4);
					$udata['phone'] = "";
					
					
					
					if(($request->has('send_to') && $request->send_to == "email") || !$people || !$people->phone_number_1 || $people->phone_number_1 == "" || !$people->code_phone_number_1 || $people->code_phone_number_1 == ""){
						
					}else{
						$send_sms = send_sms($people->code_phone_number_1,$people->phone_number_1, $msg);
						$udata['phone'] = \config('country.'.strtoupper($people->code_phone_number_1).'.code')."XXXXXXX". substr($people->phone_number_1, -3);
						$udata['send_to'] = "phone";
						$result["messages"] =\Lang::get('constant.responce_msg.success_otp_to_login_sent_on_phone');
						
					}
					
					
					if(!$send_sms || $send_sms ==""){
						
						$lang = $user->language;
						$subject = \Lang::get('mail.subject.login_otp');
						
						\Mail::send('emails.login-otp', compact('user', 'otpuser','lang'), function ($message) use ($user, $subject) {
							$message->to($user->email)->subject($subject);
						});
						$udata['send_to'] = "email";
						$result["messages"] =\Lang::get('constant.responce_msg.success_otp_to_login_sent_on_email');
					}
					$result['data']["user"] = $udata;
					$result["code"] = 200;
					
				}  
				
			   
		    }else{
				$result["messages"] =\Lang::get('constant.responce_msg.warning_invalid_qrcode');
			}
            
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
             
    }
	
	public function logout(Request $request)
    {   
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		$user = auth('api')->user();
        if($user != null)
        {
            
                $token = User::genApiKey($user->id);
				$user->api_token = $token;
				$user->save();

				$result["code"] = 200;
				$result["messages"] = \Lang::get('constant.responce_msg.success_logout');
           
        }
        else
        {
            $result["code"] = 200;
			$result["messages"] = \Lang::get('constant.responce_msg.success_logouty');
        }
		
		return $this->JsonResponse($result);
    }
	
	public function forgotpassword(Request $request)
    {
		$otp_exp_minuts = 120;
		
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		
    	$rules = array(
            'email' => 'required|email'
        );
		
		
		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if (!$validator->fails())
        {
			$user=User::where('email',$request->email)->first();
            if($user)
            {
				$otp = substr(number_format(time() * rand(),0,'',''),0,6);
                $odata = ["user_id"=>$user->id,"otp"=>$otp,"exp_time"=>Carbon::now()->addMinutes($otp_exp_minuts)];
				$otpuser = OtpUser::updateOrCreate(["user_id"=>$user->id],$odata);
                
				$subject = \Lang::get('mail.subject.reset_password');
				\Mail::send('emails.forgot-password', compact('user','otp_exp_minuts','otpuser'), function ($message) use ($user, $subject) {
					$message->to($user->email)->subject($subject);
				});
                
				$result["messages"]  = \Lang::get('constant.responce_msg.success_forgot_password_send_to_email');
				$result["code"] = 200;
            }
            else
            {
				$result["messages"]  = \Lang::get('constant.responce_msg.warning_user_data_not_found');
				$result["code"] = 400;
            }
			
		}else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		
		return $this->JsonResponse($result);
		
        
    }
	public function forgotpasswordSubmit(Request $request)
    {
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		
        $rules = array(
            'email' => 'required',
            'otp'=>'required',
            'password'=>'required', 
        );

        $validator = \Validator::make($request->all(), $rules, [],trans('user.label'));

        if(!$validator->fails())
        {
			$userE=User::where('email',$request->email)->first();
			
			$otpuser = OtpUser::where("otp",$request->otp)->first();
			$user = null;	
			if($otpuser){
				$user = $otpuser->user;	
			}
            
			if($user && $otpuser)
            {
				if(Carbon::now() > $otpuser->exp_time){
					$result["messages"] = \Lang::get('constant.responce_msg.warning_otp_token_expired');
				}else{
					$user->password = bcrypt($request->password);
					$user->save();

					//$result["data"]= []; //make_null($user);
					
					$result["messages"]  = \Lang::get('constant.responce_msg.success_password_changed');
					$result["code"] = 200;
				}
            }
            else
            {
				if(!$userE){
					$result["messages"]  = \Lang::get('constant.responce_msg.warning_user_data_not_found');
				}else{
					$result["messages"]  = \Lang::get('constant.responce_msg.warning_otp_token_is_not_valid');
				}
				$result["code"] = 400;

			}
           
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}
       
	    return $this->JsonResponse($result);
		
    }
	
	public function changePassword(Request $request)
	{
		$result = ["data" => ["user"=>[]], "code" => 400, "messages" => ""];

		$rules = array(
			'password' => 'required|min:6|max:255',
			'current_password' => 'required',
		);
		
		$validator = \Validator::make($request->all(), $rules, [],trans('user.label'));
		
		if(!$validator->fails())
        {
			$user = auth('api')->user();
			
			if (!$user) {
				$result["messages"] = \Lang::get('constant.responce_msg.warning_user_data_not_found');
			}else {
				if (Hash::check($request->input('current_password'), $user->password)) {
					$user->password = Hash::make($request->input('password'));
					$user->save();
					
					$result["messages"] = \Lang::get('constant.responce_msg.success_password_changed');
					$result["code"] = 200;
					//$result["data"] = [];
				} else {
					$result["messages"] = \Lang::get('constant.responce_msg.warning_current_password_incorrect');
				}
			}
           
        }else{
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $result["messages"]  = reset($msgArr)[0];
		}
		
		return $this->JsonResponse($result);
	}
	
	public function getProfile(Request $request)
    {   
		$result = ["data"=>["user"=>[]],"code"=>400,"messages"=>""];
		$user = auth('api')->user();
        if($user != null)
        {
			$people = $user->people;
			if($people && $people->phone_number_1 && $people->phone_number_1 != ""){
				$code = "";
				if($people->code_phone_number_1 && $people->code_phone_number_1 != ""){
					$code = "+".\config('country.'.strtoupper($people->code_phone_number_1).'.code');
				}
				$user->phone_number = $code." ".$people->phone_number_1;
			}
			unset($user->people);
            $res=make_null($user);
            
			$result["data"]["user"] = $res;
			$result["code"] = 200;
			
           
        }
        else
        {
            $result["messages"] = \Lang::get('constant.responce_msg.warning_user_data_not_found');
        }
		
		return $this->JsonResponse($result);
    }
	
		
	/*
	public function register(Request $request)
    {
		$result = ["data"=>[],"code"=>400,"messages"=>""];
		
        $rules = array(
            'email' => 'required|email|unique:users',
            'password'=>'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/|unique:users,phone_number',
            'occupation' => 'required'
        );
		$val_msg = [
			'phone_number.unique'=> RT::rtext("warning_require_unique_phone_number")
		];
		
        $validator = \Validator::make($request->all(), $rules, $val_msg);

        if (!$validator->fails())
        {
			$input = $request->all();
			
			$input['password'] = Hash::make($request->password);
			$input['status'] = "active";
			$input['name'] = "";
            $user= User::create($input);
			
			if($user){
				$user->assignRole("LU");
			}
			
			$result["data"] = $user;
			$result["code"] = 200;
			$result["messages"] = RT::rtext("success_register");
		
		}else{
			
			$validation = $validator;
            $msgArr = $validator->messages()->toArray();
			
			$result["messages"]  = reset($msgArr)[0];
			$result["code"] = 400;
		}

		return $this->JsonResponse($result);

    }*/
}
