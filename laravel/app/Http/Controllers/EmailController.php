<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\User;
use App\Setting;
use App\UnitOrder;

use Session;
use Mail;
use Lang;
use View;

class EmailController extends Controller {





    
    public function sendMailOnOrderCreate($order_id,$status) {
        
        $admin_to_mail = env("MAIL_ADMIN_TO_ADDRESS","jignesh.citrusbug@gmail.com");
        $admin_cc_email = [];

        $setting = Setting::where('key','emails_as_cc')->first();
        if($setting && $setting->value != ''){
            $admin_cc_email = explode(',',$setting->value);
        }
        $admin_cc_email[] = $admin_to_mail;
       

        $order = $order = UnitOrder::where("id",$order_id)->first();


        if($order){
            $package = json_decode($order->unit_package);
           
            $buyer_email =$order->buyer_email;
            $creater = $order->creator;
            $website = $order->website;

          //  echo "<pre>"; print_r($creater); exit;
            if($creater){
                $buyer_email = $creater->email;
            }
           
           
            if($status=="completed"){
                $subject = Lang::get('unitorder.mail_user.order_place_success_subject');
                $subject_admin = Lang::get('unitorder.mail_admin.order_place_success_subject');
            }else if($status=="cancelled"){
                $subject = Lang::get('unitorder.mail_user.order_place_canceled_subject');
                $subject_admin = Lang::get('unitorder.mail_admin.order_place_canceled_subject');
            }else {
                $subject = Lang::get('unitorder.mail_user.order_place_failed_subject');
                $subject_admin = Lang::get('unitorder.mail_admin.order_place_failed_subject');
            }



            
            $lang = $creater->language;
            Mail::send('emails.order.buyer.place-order', compact('order', 'package', 'creater','website','status','lang'), function ($message) use ($buyer_email, $subject) {
                $message->to($buyer_email)->subject($subject);
            });
           
            
            $lang = \config('settings.DEFAULT_LANG');
            Mail::send('emails.order.admin.place-order', compact('order', 'package', 'creater','website','status','lang'), function ($message) use ($subject_admin, $admin_to_mail,$admin_cc_email) {
                $message->to($admin_cc_email)->subject($subject_admin);
               /* if(count($admin_cc_email)>0){
                    foreach($admin_cc_email as $email){
                       $message->bcc($email);
                    }
                   }*/
            });

        }
    }

    
}
