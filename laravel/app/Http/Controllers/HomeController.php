<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ticket;
use App\TicketHistory;
use App\User;
use Session;
use Auth;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	
	public function ticketack($id,Request $request)
    {
		
		if($request->has('email') && !Auth::user()){
			$user = User::where("email",$request->email)->first();
			$ticket = Ticket::where("id",$id)->first();
			
			if($user && $ticket && $ticket->user_id == $user->id){
				$url_ac_text =  \Lang::get('mail.label.tickets_acknowledged_comment',[],$user->language);
				
				$tickethis = TicketHistory::where("ticket_id",$id)->orderBy("id","DESC")->first();
				
				if($tickethis && $tickethis->desc == $url_ac_text && $tickethis->created_by == $user->id){
					
				}else{
					$his = new TicketHistory();
					$his->ticket_id=$id;
					$his->history_type="comment";
					$his->desc=$url_ac_text;
					$his->created_by=$user->id;
					$his->save();
					
					Session::flash('flash_success', 'Acknowledgement Success !!');
					return redirect('login');
				}
				
				
			}
		}
		
        return redirect('admin/tickets/'.$id);
      
    }

    public function subDomain($account)
    {
        return $account;
    }

    public function redirect()
    {
        return redirect('/admin');
    }
    public function phpinfo()
    {
		/*$sid = \config('settings.twilio.SID'); // Your Account SID from www.twilio.com/console
        $token = \config('settings.twilio.TOKEN'); // Your Auth Token from www.twilio.com/console

        $client = new \Twilio\Rest\Client($sid, $token);
        $message = $client->messages->create(
			"+919998543618", 
			array(
				'from' => \config('settings.twilio.FROM'), // From a valid Twilio number
				'body' => "this is testing"
			)
        );
		*/
		
		echo 'Testing Git...';
         echo phpinfo();
    }
}
