<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;

class LanguageController extends Controller
{

    //

    public function change($lang = 'en', Request $request)
    {

        $request->session()->put('_lang', $lang);

        flash(__('Your language changed successfully'), 'success');

        return redirect('/');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeByForm(Request $request)
    {
        $_lang = $request->input('_lang', 'en');

        $request->session()->put('_lang', $_lang);

        flash(__('Your language changed successfully'), 'success');

        return redirect('/');

    }
}

////
//    {{ Form::open(['action' => 'HomeController@postChangeLanguage']) }}
//    {{Form::select('lang',['en'=>'en','fr'=>'fr'],$lang,['onchange'=>'submit()'])}}
//    {{ Form::close()}}
