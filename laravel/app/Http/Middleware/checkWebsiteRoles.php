<?php

namespace App\Http\Middleware;

use Closure;
use App\WebSite;
use Auth;


class checkWebsiteRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $masterEmail =  \config('settings.MASTER_ADMIN');


        //allow Master Admin to All Process
        if (!is_null($masterEmail) && $request->user()->email == strtolower($masterEmail)) {
            return $next($request);
        }
       // return $next($request);
      //  echo "<pre>"; print_r(auth()->user()->roles);

        $data = $this->findSiteData(_WEBSITE_ID);

        foreach ($data as $role_name)
        {
            if(Auth::check() && auth()->user()->hasRole($role_name)){
                return $next($request);
            }
        }
		if(Auth::check() && auth()->user()->hasRole("GSA")){
                return $next($request);
        }
      //  return $next($request);

        return response(view('auth.accessDenied'), 401);
    }

    protected function findSiteData($host)
    {
        $website = Website::where('id', $host)->first();

        if($website && $website->roles){
            $roles = $website->roles;
            return $roles->pluck('name');
        }else{
            return [];
        }
    }
}
