<?php

namespace App\Http\Middleware;

use App\WebSite;
use Closure;

class WebSiteFilterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (\App::runningInConsole()) {
            return $next($request);
        }
        //

        $parse = (parse_url($request->url()));

        $host = strtolower($parse['host']);

        define('_WEBSITE', $host);

        $data = $this->findSiteData($host);

        if ($data) {
            if($data->refimages){
                define('_WEBSITE_LOGO', $data->refimages->image);
            }else{
                define('_WEBSITE_LOGO',null);
            }
            define('_WEBSITE_ID', $data->id);
            define('_WEBSITE_DOMAIN', $data->domain);
            define('_WEBSITE_NAME', $data->name);
            define('_WEBSITE_UNITS', $data->units);
            define('_WEBSITE_DESCRIPTION', $data->description);
            define('_MASTER', $data->master);
            define('_ENABLE_CHARGE', $data->enable_charge);
            define('_ENABLE_QRCODE', $data->enable_qrcode);
            define('_QRCODE_APITOKEN', $data->qrcode_apitoken);
			
			if($data->status != "active"){
				return response(view('errors.maintainance'));
			}

        } else {
            return response(view('errors.maintainance'));
        }

        return $next($request);
    }

    protected function findSiteData($host)
    {
        return WebSite::where('domain', $host)->first();
    }
}
