<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Session;
use Carbon;

use App\UnitTransaction;

class SessionNotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('session_notification_set_time') && Session::get('session_notification_set_time')!=""){
            $start_time = Session::get('session_notification_set_time');
            $created_at =  \Carbon\Carbon::parse($start_time)->addMinutes(100);
            $now= \Carbon\Carbon::now();
            
            if($created_at > $now){
                return $next($request);
            }
        }
        
        if(!_MASTER && Auth::user()->can('access.domainunits')){
            $this->setSessionNotification();
        }
        return $next($request);
    }
    public function setSessionNotification(){
        $h1 = "";
        $h2 = "";
        $notification_befor_day = \config('settings.notification_at_balance_about_to_expire_in_day');
        $low_min_balance = \config('settings.notification_at_low_min_balance');
        
        $charged = UnitTransaction::where('website_id',_WEBSITE_ID)->where('reference','daily_charge')->where("created_at",">=",\Carbon\Carbon::today())->first();
        if(!$charged) $charged = UnitTransaction::where('website_id',_WEBSITE_ID)->where('reference','daily_charge')->where("created_at",">=",\Carbon\Carbon::yesterday())->first();
        
        if($charged){
            $perday_charge = $charged->unit;
            if(_WEBSITE_UNITS > $perday_charge && $perday_charge >0){
                $day = (int) (_WEBSITE_UNITS / $perday_charge);
                if($day < $notification_befor_day){
                    $exp_in_day = ($day ==0)? \Lang::get('unittransaction.label.today'): \Lang::get('unittransaction.label.in_day',['day'=>$day]);
                    $h2 = \Lang::get('unittransaction.notification.unit_will_end_in_check_more_plan',['in_number_days'=>$exp_in_day]);
                }
            }
        }
        
        if(_WEBSITE_UNITS < $low_min_balance){
            $h1 = \Lang::get('unittransaction.notification.unit_is_low',['unit'=>_WEBSITE_UNITS]);
        }
        
        Session::flash('session_notification_header1',$h1);
        Session::flash('session_notification_header2',$h2);
        Session::put('session_notification_set_time', \Carbon\Carbon::now());
        
    }
    
}
