<?php

namespace App\Http\Middleware;

use Closure;
use App\WebSite;
use Auth;
use App\Notifications\TicketAssignNotification;


class Notification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('v_notification') && Auth::check()){

            if($request->v_notification =="all"){
                foreach (Auth::user()->unreadNotifications as $notification) {
                    $notification->markAsRead();
                }
              //  Auth::user()->unreadNotifications->markAsRead();
            }else{
                $notification= Auth::user()->notifications()->find($request->v_notification);
				if($notification){
					$notification->markAsRead();
				}
            }

           // echo "<pre>"; print_r($notification); exit;
        }
        return $next($request);

    }


}
