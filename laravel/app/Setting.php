<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\File;

use App\Ticket;
use App\TicketHistory;
use App\MasterTicket;
use App\Service;
use App\Subject;
use App\Company;
use App\Site;
use App\Building;
use App\WebSite;
use App\RefeImages;
use App\User;
use App\People;
use App\Availability;

class Setting extends Model

{

//    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'value', 'key', '_website_id','type'];

    public function dutysubject()
    {
        return $this->hasOne('App\Subject','id', 'value');
    }
    public static function getMasterMailId()
    {
        $masterEmail = env('MASTER_ADMIN', null);
        $res = [];
        
        if($masterEmail){
            $res[] = $masterEmail;
        }
        $master_emails = Setting::where("key","master_email")->first();
        if($master_emails){
            $emails = explode(",",$master_emails->value);
            
            foreach ($emails as $id){
                $res[] = trim($id);
            }
        }
        return $res;
    }
    public function website()
    {
        return $this->belongsTo('App\WebSite', 'key', 'id');
    }
    public function subjects()
    {
        return $this->belongsTo('App\Subject', 'value', 'id');
    }

	public static function recoverParentLevel($id,$modal){
		
		if($modal == 'website'){
			return Setting::recoverWebsite($id);
		}else if($modal == 'company'){
			return Setting::recoverCompany($id);
		}else if($modal == 'site'){
			return Setting::recoverSite($id);
		}else if($modal == 'building'){
			return Setting::recoverBuilding($id);
		}else if($modal == 'services'){
			return Setting::recoverServices($id);
		}else if($modal == 'subject'){
			return Setting::recoverSubject($id);
		}else if($modal == 'ticket'){
			return Setting::recoverTicket($id);
		}else if($modal == 'planned_activity'){
			return Setting::recoverPlanedTicket($id);
		}else if($modal == 'ticket_history'){
			return Setting::recoverTicketHistory($id);
		}else if($modal == 'user'){
			return Setting::recoverUser($id);
		}
		
	}
	public static function recoverWebsite($id){
		
		$companies = Company::onlyTrashed()->where("_website_id",$id)->get();
		
		foreach($companies as $comp){
			Setting::recoverCompany($comp->id);
		}
		
		$ob = Website::withTrashed()->where('id',$id)->first();
		
		if($ob){
			$ob->restore();
			return 1;
        }else{
			return 0;
		}
	}
	public static function recoverCompany($id){

		$sites = Site::onlyTrashed()->where("company_id",$id)->get();
		$services = Service::onlyTrashed()->where("company_id",$id)->get();
		
		foreach($sites as $site){
			Setting::recoverSite($site->id);
		}
		
		foreach($services as $service){
			Setting::recoverServices($service->id);
		}
		
		
		$company = Company::withTrashed()->where("id",$id)->first();
		if($company){
			$company->restore();
			return 1;
		}else{
			return 0;
		}
		
	}
	
	public static function recoverSite($id){
		
		$buildings = Building::onlyTrashed()->where("site_id",$id)->get();
		
		foreach($buildings as $building){
			Setting::recoverBuilding($building->id);
		}
		//delete site
		
		$site = Site::withTrashed()->where("id",$id)->first();
		
		if($site){
			$site->restore();
			return 1;
		}else{
			return 0;
		}
	}
	public static function recoverBuilding($id){
		$building = Building::withTrashed()->where("id",$id)->first();
		
		if($building){
			$building->restore();
			
			return 1;
		}else{
			return 0;
		}
	}
	
	public static function recoverServices($id){
		$subjects = Subject::onlyTrashed()->where("service_id",$id)->get();
		
		foreach($subjects as $subject){
			Setting::recoverSubject($subject->id);
		}
		//delete services
		
		$ob = Service::withTrashed()->where("id",$id)->first();
		
		if($ob){
			$ob->restore();
			return 1;
		}else{
			return 0;
		}
	}
	
	public static function recoverSubject($id){
		
		$tickets = Ticket::onlyTrashed()->where("subject_id",$id)->get();
		$mtickets = MasterTicket::onlyTrashed()->where("subject_id",$id)->get();
		
		foreach($tickets as $ticket){
			Setting::recoverTicket($ticket->id);
		}
		
		foreach($mtickets as $mticket){
			Setting::recoverPlanedTicket($mticket->id);
		}
		
		$ob = Subject::withTrashed()->where('id',$id)->first();
		
		if($ob){
			$ob->restore();
			return 1;
        }else{
			return 0;
		}
	}
	
	public static function recoverTicket($id){
		
		$tickeths = TicketHistory::onlyTrashed()->where("ticket_id",$id)->get();
		
		foreach($tickeths as $ticketh){
			Setting::recoverTicketHistory($ticketh->id);
		}
		$ob = Ticket::withTrashed()->where('id',$id)->first();
		
		if($ob){
			$ob->restore();
			
			return 1;
        }else{
			return 0;
		}
	}
	public static function recoverPlanedTicket($id){
		$ob = MasterTicket::withTrashed()->where('id',$id)->first();
		
		if($ob){
			$ob->restore();
			return 1;
        }else{
			return 0;
		}
	}
	public static function recoverTicketHistory($id){
		$ob = TicketHistory::withTrashed()->where('id',$id)->first();
		
		if($ob){
			$ob->restore();
			return 1;
        }else{
			return 0;
		}
	}
	public static function recoverUser($id){
		$ob = User::withTrashed()->where('id',$id)->first();
		$people = People::where("user_id",$id)->first();
		
		if($people){
            $people->restore();
        }
		
		if($ob){
			$ob->restore();
			return 1;
        }else{
			return 0;
		}
	}
	
	
	
	
	
	public static function deleteParentLevel($id,$modal,$is_hard_delete){
		
		if($modal == 'website'){
			return Setting::deleteWebsite($id,$is_hard_delete);
		}else if($modal == 'company'){
			return Setting::deleteCompany($id,$is_hard_delete);
		}else if($modal == 'site'){
			return Setting::deleteSite($id,$is_hard_delete);
		}else if($modal == 'building'){
			return Setting::deleteBuilding($id,$is_hard_delete);
		}else if($modal == 'services'){
			return Setting::deleteServices($id,$is_hard_delete);
		}else if($modal == 'subject'){
			return Setting::deleteSubject($id,$is_hard_delete);
		}else if($modal == 'ticket'){
			return Setting::deleteTicket($id,$is_hard_delete);
		}else if($modal == 'planned_activity'){
			return Setting::deletePlanedTicket($id,$is_hard_delete);
		}else if($modal == 'ticket_history'){
			return Setting::deleteTicketHistory($id,$is_hard_delete);
		}else if($modal == 'user'){
			return Setting::deleteUser($id,$is_hard_delete);
		}
		
	}
	public static function deleteWebsite($id,$is_hard_delete){
		
		
		if($is_hard_delete){
			$companies = Company::withTrashed()->where("_website_id",$id)->get();
		}else{
			$companies = Company::where("_website_id",$id)->get();
		}

		foreach($companies as $comp){
			Setting::deleteCompany($comp->id,$is_hard_delete);
		}
		//delete website
		
		$refimg = RefeImages::where('ref_field_id', $id)->where('refe_table_field_name', 'website_id')->first();
		
        if($refimg && $is_hard_delete) {
            $image_path1 = public_path() . '/uploads/website/'.$refimg->image;
			Setting::removeImage($refimg->image,$image_path1);
            $refimg->delete();
        }
		
		$ob = Website::withTrashed()->where('id',$id)->first();
		
		if($ob){
			if($is_hard_delete){
				$ob->forceDelete();
			}else{
				$ob->delete();
			}
			
			return 1;
        }else{
			return 0;
		}
	}
	public static function deleteCompany($id,$is_hard_delete){

		if($is_hard_delete){
			$sites = Site::withTrashed()->where("company_id",$id)->get();
			$services = Service::withTrashed()->where("company_id",$id)->get();
		}else{
			$sites = Site::where("company_id",$id)->get();
			$services = Service::where("company_id",$id)->get();
		}

		foreach($sites as $site){
			Setting::deleteSite($site->id,$is_hard_delete);
		}
		
		foreach($services as $service){
			Setting::deleteServices($service->id,$is_hard_delete);
		}
		//delete company
		
		$company = Company::withTrashed()->where("id",$id)->first();
		if($company){
			if($is_hard_delete) {
				$image_path1 = public_path()."/uploads/".$company->logo;	
				Setting::removeImage($company->logo,$image_path1);
				$company->forceDelete();
			}else{
				$company->delete();
			}
			return 1;
		}else{
			return 0;
		}
		
	}
	public static function deleteSite($id,$is_hard_delete){

		if($is_hard_delete){
			$buildings = Building::withTrashed()->where("site_id",$id)->get();
		}else{
			$buildings = Building::where("site_id",$id)->get();
		}
		foreach($buildings as $building){
			Setting::deleteBuilding($building->id,$is_hard_delete);
		}
		//delete site
		
		$site = Site::withTrashed()->where("id",$id)->first();
		
		if($site){
			if($is_hard_delete) {
				$image_path1 = public_path()."/uploads/".$site->logo;	
				Setting::removeImage($site->logo,$image_path1);
				
				$image_path1 = public_path()."/uploads/files/".$site->instructions_tasks_file;
				Setting::removeImage($site->instructions_tasks_file,$image_path1);
				
				$image_path1 = public_path()."/uploads/files/".$site->order_file;
				Setting::removeImage($site->order_file,$image_path1);
				
				$image_path1 = public_path()."/uploads/files/".$site->procedures_file;
				Setting::removeImage($site->procedures_file,$image_path1);
				
				$image_path1 = public_path()."/uploads/files/".$site->sitemaps_file;
				Setting::removeImage($site->sitemaps_file,$image_path1);
				
				$site->forceDelete();
			}else{
				$site->delete();
			}
			return 1;
		}else{
			return 0;
		}
	}
	public static function deleteBuilding($id,$is_hard_delete){
		$building = Building::withTrashed()->where("id",$id)->first();
		
		if($building){
			if($is_hard_delete){
				$building->forceDelete();
			}else{
				$building->delete();
			}
			
			return 1;
		}else{
			return 0;
		}
	}
	public static function deleteServices($id,$is_hard_delete){

		if($is_hard_delete){
			$subjects = Subject::withTrashed()->where("service_id",$id)->get();
		}else{
			$subjects = Subject::where("service_id",$id)->get();
		}

		foreach($subjects as $subject){
			Setting::deleteSubject($subject->id,$is_hard_delete);
		}
		//delete services
		
		$ob = Service::withTrashed()->where("id",$id)->first();
		
		if($ob){
			if($is_hard_delete){
				$ob->sites()->detach();
				$ob->forceDelete();
			}else{
				$ob->delete();
			}
			return 1;
		}else{
			return 0;
		}
	}
	public static function deleteSubject($id,$is_hard_delete){
		
		if($is_hard_delete){
			$tickets = Ticket::withTrashed()->where("subject_id",$id)->get();
			$mtickets = MasterTicket::withTrashed()->where("subject_id",$id)->get();
		}else{
			$tickets = Ticket::where("subject_id",$id)->get();
			$mtickets = MasterTicket::where("subject_id",$id)->get();
		}


		
		foreach($tickets as $ticket){
			Setting::deleteTicket($ticket->id,$is_hard_delete);
		}
		
		foreach($mtickets as $mticket){
			Setting::deletePlanedTicket($mticket->id,$is_hard_delete);
		}
		
		//delete subject
		
		$ob = Subject::withTrashed()->where('id',$id)->first();
		
		if($ob){
			if($is_hard_delete) {
				$image_path1 = public_path()."/uploads/files/".$ob->file;	
				Setting::removeImage($ob->file,$image_path1);

				$ob->forceDelete();
			}else{
				$ob->delete();
			}
			
			return 1;
        }else{
			return 0;
		}
	}
	public static function deleteTicket($id,$is_hard_delete){

		if($is_hard_delete){
			$tickeths = TicketHistory::withTrashed()->where("ticket_id",$id)->get();
		}else{
			$tickeths = TicketHistory::where("ticket_id",$id)->get();
		}

		
		foreach($tickeths as $ticketh){
			Setting::deleteTicketHistory($ticketh->id,$is_hard_delete);
		}
		//delete ticket
		
		$ob = Ticket::withTrashed()->where('id',$id)->first();
		
		if($ob){
			if($is_hard_delete) {
				$ob->forceDelete();
			}else{
				$ob->delete();
			}
			return 1;
        }else{
			return 0;
		}
	}
	
	public static function deletePlanedTicket($id,$is_hard_delete){
		$ob = MasterTicket::withTrashed()->where('id',$id)->first();
		
		if($ob){
			if($is_hard_delete) {
				$ob->forceDelete();
			}else{
				$ob->delete();
			}
			return 1;
        }else{
			return 0;
		}
	}
	
	public static function deleteTicketHistory($id,$is_hard_delete){
		$ob = TicketHistory::withTrashed()->where('id',$id)->first();
		
		if($ob){
			if($is_hard_delete && $ob->history_type == "file") {
				$ticket = Ticket::where('id',$ob->ticket_id)->first();
				
				$dir = public_path() . '/uploads/'.$ob->desc;
				$dir_thumb = str_replace('ticket', 'thumbs', $dir);
				
				Setting::removeImage($ob->desc,$dir);
				Setting::removeImage($ob->desc,$dir_thumb);

				$ob->forceDelete();
			}else{
				$ob->delete();
			};
			return 1;
        }else{
			return 0;
		}
	}

	public static function deleteUser($id,$is_hard_delete){
		$ob = User::withTrashed()->where('id',$id)->first();
		$people = People::where("user_id",$id)->first();
		
		if($ob){
			if($is_hard_delete) {
				$ob->roles()->detach();
				
				if($people){
					$image_path1 = public_path()."/uploads/".$people->photo;
					Setting::removeImage($people->photo,$image_path1);
					Availability::where('people_id', $people->id)->delete();

					$people->forceDelete();
				}
				$ob->forceDelete();
			}else{
				$ob->delete();
				if($people){
					$people->delete();
				}
			}
			return 1;
        }else{
			return 0;
		}
	}
	
	public static function removeImage($imageName,$image_path1)
    {
        if ($imageName && $imageName !="" && File::exists($image_path1)) {
            unlink($image_path1);
        }
	}
}
