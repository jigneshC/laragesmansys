<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;


class People extends BaseModel
{

	use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peoples';


    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', '_website_id', 'title', 'first_name', 'last_name', 'quality', 'phone_number_1', 'phone_number_2','code_phone_number_1','code_phone_number_2'  ,'phone_type_1', 'phone_type_2', 'emergency_password', 'availability', 'status_text', 'hold_key', 'photo', 'active', 'created_by', 'updated_by'];


	protected $appends = ['full_name'];
	
    /**
     * Each People Belongs to one service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function services()
    {
        return $this->belongsToMany('App\Service')->withTimestamps();
    }


    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function subject()
    {
        return $this->belongsToMany('App\Subject');
    }

    public function availability_time()
    {
        return $this->hasMany('App\Availability')->oldest();
    }

    /*landline name*/
    public function dropdownvalue()
    {
        return $this->HasOne('App\DropdownValue','parent_id','phone_type_1')->language();
    }

     /*landline name*/
    public function dropdownvalueM()
    {
        return $this->HasOne('App\DropdownValue','parent_id','phone_type_2')->language();
    }



}
