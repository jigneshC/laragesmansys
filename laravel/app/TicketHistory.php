<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class TicketHistory extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ticket_history';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

	protected $appends = ['created_tz','file_url','image_thumb','image_thumb_path','action_text','created_at_tz'];
	
	
	public function child()
    {
        return $this->hasMany('App\Comment', 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
	
	public function getCreatedAtTzAttribute()
    {
        if($this->created_at != ""){
            return \Carbon\Carbon::parse($this->created_at)->diffForHumans();
        }
		return $this->created_at;
    }
	
	public function getActionTextAttribute($value)
    {
		$lang = "fr";
        $status_arr = \Lang::get('dashboard.status_dropdown',[],$lang);
		$res = "";
        
        
		if($this->history_type == "action" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_change_ticket_status_to',['actioner_name'=>$this->user->full_name,'new_status'=>$status_arr[$this->desc]]);
		}else if($this->history_type == "file" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_add_evidence_on_ticket',['actioner_name'=>$this->user->full_name]);
		}else if($this->history_type == "comment" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_comment_on_ticket',['actioner_name'=>$this->user->full_name]);
		}else if($this->history_type == "assign" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_assing_ticket_to_user',['actioner_name'=>$this->user->full_name,"assinee"=>$this->desc]);
		}else if($this->history_type == "created" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_created_tikcet',['actioner_name'=>$this->user->full_name]);
		}else if($this->history_type == "call_log" && $this->user){
			$res = \Lang::get('ticket.notification.actioner_has_created_call',['actioner_name'=>$this->user->full_name,"assinee"=>$this->desc]);
		}
        
		
		if($res == "" && $this->creator){
			$res = \Lang::get('ticket.notification.actioner_has_created_tikcet',['actioner_name'=>$this->creator->full_name],$lang);
		}
		return $res;
	}
	
	public function getFileUrlAttribute($value)
    {
		if($this->history_type == "file" ){
			if(\File::exists(public_path()."/uploads/".$this->desc)){
				return url("/")."/"."uploads/".$this->desc;
			}else{
				return "";
			}
		}else{
			return "";
		}
	}
	public function getImageThumbPathAttribute($value)
    {
		if($this->history_type == "file" ){
			if(\File::exists(public_path()."/uploads/".$this->desc)){
				return str_replace('ticket', 'thumbs', $this->desc);
			}else{
				return "";
			}
		}else{
			return "";
		}
	}
	
	public function getImageThumbAttribute($value)
    {
		$ret = "";
		if($this->history_type == "file" ){
			$thumb_path = str_replace('ticket', 'thumbs', $this->desc);
			
			if(\File::exists(public_path()."/uploads/".$thumb_path)){
				$ret = url("/")."/uploads/".$thumb_path;
			}
			
		}
		
		return $ret;
	}
}
