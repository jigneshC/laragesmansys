<?php

namespace App\Curl;



class ApiCall
{



    private function getAuthenticationHeaders()
    {
        $APIKey = \config('settings.dnsmadeeasy.API_KEY');
        $SecretKey= \config('settings.dnsmadeeasy.SECREAT_KEY');

        $date = date_create("now", new \DateTimeZone("GMT"))->format('D, d M Y H:i:s e');
        $hash = hash_hmac("sha1", $date,$SecretKey);

        return array("x-dnsme-apiKey: {$APIKey}",
            "x-dnsme-requestDate: $date",
            "x-dnsme-hmac: $hash"
        );
    }
    public  function getAllDomain()
    {

        $url = "https://api.dnsmadeeasy.com/V2.0/dns/managed/";
        $this->send($url,'get',null);
    }
    public  function getSingleDomain()
    {

        $url = "https://api.dnsmadeeasy.com/V2.0/dns/managed/5885470/records?type=CNAME&recordName=kishan";
        return $this->send($url,'get',null);
    }




    public  function deleteSubDomainByName($name)
    {
        $domain_id = \config('settings.dnsmadeeasy.ROOT_DOMAIN.id');
        $url = "https://api.dnsmadeeasy.com/V2.0/dns/managed/$domain_id/records?type=CNAME&recordName=$name";
        $ob = $this->send($url,'get',null);
        if($ob && isset($ob['data']) && $ob['totalRecords']==1){
            $this->deleteSubDomainById($ob['data'][0]['id']);
        }

    }
    public  function deleteSubDomainById($id)
    {
        $domain_id = \config('settings.dnsmadeeasy.ROOT_DOMAIN.id');
        $url = "https://api.dnsmadeeasy.com/V2.0/dns/managed/$domain_id/records/$id";
        $ob = $this->send($url,'DELETE',null);
    }
    public  function createSubDomain($name,$value)
    {
        $domain_id = \config('settings.dnsmadeeasy.ROOT_DOMAIN.id');
        $ttl =\config('settings.dnsmadeeasy.ROOT_DOMAIN.ttl');
        $value = \config('settings.dnsmadeeasy.ROOT_DOMAIN.name');

        $url = "https://api.dnsmadeeasy.com/V2.0/dns/managed/$domain_id/records/";
        $this->send($url,'post',['name'=>$name,'value'=>$value,'type'=>'CNAME','gtdLocation'=>'DEFAULT','ttl'=>$ttl]);

    }


    private function send($url,$method, $content = NULL)
    {
        try{

            $ch = curl_init ($url); // your URL to send array data
            if ($content !== NULL) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));
            }
            switch (strtolower($method)) {

                case "get":
                    curl_setopt($ch, CURLOPT_HTTPGET, true);
                    break;

                case "post":
                    curl_setopt($ch, CURLOPT_POST, true);
                    break;

                case "put":
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); //CURL_OPTPUT requires INFILE and writing to ram/disk, which is annoying
                    break;

                case "delete":
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                    break;
            }

            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getAuthenticationHeaders());
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

            if(0){
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            }else{
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            }


            $result = curl_exec ($ch);

           // echo $result; exit;

            if(!curl_errno($ch))
            {
                return json_decode($result,true);

            }else{
                return [];
            }




        }catch (\Exception $e){
              
			  
			  \Mail::raw(json_encode($content)." - ".$e->getMessage(), function ($message) {
							  $message->to("jignesh.citrusbug@gmail.com")
								->subject("Error : Gesmansys - subdomain - api");
							});
							
            return [];
        }


    }


}