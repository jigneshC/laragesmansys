<?php

namespace App\Curl;



class ApiQrcode
{



    public static function register($user)
    {
        $resultOp = null;
        
        try{
            
        
        $url = config('settings.qrcode.WEB_URL')."/create-user";
        
        $ch = curl_init($url); // your URL to send array data
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$user); // Your array field
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        
      //  echo "<pre>";        print_r($result); 

        $result = json_decode($result,true);
     //    echo "<pre>";        print_r($result); 
        if($result && isset($result['status']) && isset($result['code']) && $result['status'] && $result['code']==200){
            $resultOp = $result['data']['api_token'];
        }
        
        }catch (\Exception $e) {
            $resultOp =null;
        }
        
        
        return $resultOp;
    }
    public static function getAndStoreQrcode($model,$api_token)
    {
        
        $url = config('settings.qrcode.WEB_URL')."/generate-qr";
        
        $params = [
            "redirect_link"=>url('')."/admin/".$model->getTable()."/".$model->id,
            "api_token"=>$api_token
        ];
        
     //   echo "<pre>";     print_r($params);  die;
        $resultOp = null;
        
        try{
            
        
        
        
        $ch = curl_init($url); // your URL to send array data
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params); // Your array field
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
       
        $result = json_decode($result,true);
       // echo "<pre>";     print_r($result);  die;
        if($result && isset($result['status']) && isset($result['code']) && $result['status'] && $result['code']==200){
            $qrcode = $result['data']['hash'];
            $isexist = \App\Qrcode::where("refe_table_field_name",$model->getTable()."_id")->where("ref_field_id",$model->id)->first();
            if($isexist){
                $isexist->qrcode = $qrcode;
                $isexist->save();
            }else{
                \App\Qrcode::create(['refe_table_field_name'=>$model->getTable()."_id","ref_field_id"=>$model->id,"qrcode"=>$qrcode]);
            }
            $resultOp = 1;
        }
        
        }catch (\Exception $e) {
            $resultOp =null;
        }
        
         
        
        return $resultOp;
    }


}