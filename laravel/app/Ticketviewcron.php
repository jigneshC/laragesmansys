<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\HasRoles;
use App\Ticket;
use App\MasterTicket;
use Carbon;
use Illuminate\Support\Facades\File;


class Ticketviewcron extends Model
{

    protected $table = "ticket_view_cron";

    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $fillable = [
        'key'
    ];

    public static function setTodayVisibilityOfTickets($id=0){

        
		try{
			
        $activity_count = 0;
         
        $tickets = MasterTicket::where("view_start_date","<=",Carbon\Carbon::now()->format("Y-m-d"))->where("view_end_date",">=",Carbon\Carbon::now()->format("Y-m-d"));
        if($id){
            $tickets->where('id',$id);
        }

        $tickets = $tickets->get();


        foreach ($tickets as $ticket){

            $vew_end_date = "";
            $vew_start_date = "";

            if($ticket->activity_time_opt == "daily"){
                $vew_end_date = Carbon\Carbon::now()->endOfDay();
                $vew_start_date = Carbon\Carbon::now()->startOfDay()->subHours($ticket->view_before_date);
            }
            if($ticket->activity_time_opt == "weekly"){
                $pl = $ticket->view_on_date_val;
                if($pl==0){ $pl =7; }
                $pl = $pl-1;

                $vew_end_date = Carbon\Carbon::now()->startOfWeek()->addDays($pl)->endOfDay();
                $vew_start_date = Carbon\Carbon::now()->startOfWeek()->addDays($pl)->startOfDay()->subHours($ticket->view_before_date);
            }
            if($ticket->activity_time_opt == "monthly"){
                $vew_end_date = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y-m")."-".$ticket->view_on_date_val)->endOfDay();
                $vew_start_date = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y-m")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date);
            }
            if($ticket->activity_time_opt == "yearly"){
                $vew_end_date = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay();
                $vew_start_date = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date);
            }
            if($ticket->activity_time_opt == "monthly_3"){

                $ved1 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay();
                $ved1 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved1->format("m-d H:i:s"));
                $vsd1 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date);
                $vsd1 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd1->format("m-d H:i:s"));

                $ved2 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay()->addMonths(3);
                $ved2 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved2->format("m-d H:i:s"));
                $vsd2 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date)->addMonths(3);
                $vsd2 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd2->format("m-d H:i:s"));

                $ved3 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay()->addMonths(6);
                $ved3 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved3->format("m-d H:i:s"));
                $vsd3 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date)->addMonths(6);
                $vsd3 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd3->format("m-d H:i:s"));

                $ved4 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay()->addMonths(9);
                $ved4 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved4->format("m-d H:i:s"));
                $vsd4 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date)->addMonths(9);
                $vsd4 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd4->format("m-d H:i:s"));

               // echo $ved4."--".$vsd4; exit;

                if(Carbon\Carbon::now() <= $ved4 && Carbon\Carbon::now() >= $vsd4){
                    $vew_end_date = $ved4;
                    $vew_start_date = $vsd4;
                }else if(Carbon\Carbon::now() <= $ved2 && Carbon\Carbon::now() >= $vsd2){
                    $vew_end_date = $ved2;
                    $vew_start_date = $vsd2;
                }else if(Carbon\Carbon::now() <= $ved3 && Carbon\Carbon::now() >= $vsd3){
                    $vew_end_date = $ved3;
                    $vew_start_date = $vsd3;
                }else{
                    $vew_end_date = $ved1;
                    $vew_start_date = $vsd1;
                }


            }
            if($ticket->activity_time_opt == "monthly_6"){

                $ved1 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay();
                $ved1 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved1->format("m-d H:i:s"));
                $vsd1 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date);
                $vsd1 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd1->format("m-d H:i:s"));

                $ved2 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->endOfDay()->addMonths(6);
                $ved2 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$ved2->format("m-d H:i:s"));
                $vsd2 = Carbon\Carbon::createFromFormat('Y-m-d',Carbon\Carbon::now()->format("Y")."-".$ticket->view_on_date_val)->startOfDay()->subHours($ticket->view_before_date)->addMonths(6);
                $vsd2 = Carbon\Carbon::createFromFormat('Y-m-d H:i:s',Carbon\Carbon::now()->format("Y")."-".$vsd2->format("m-d H:i:s"));

              //  echo $ved2."--".$vsd2; exit;
                if(Carbon\Carbon::now() <= $ved2 && Carbon\Carbon::now() >= $vsd2){
                    $vew_end_date = $ved2;
                    $vew_start_date = $vsd2;
                }else{
                    $vew_end_date = $ved1;
                    $vew_start_date = $vsd1;
                }
            }

            if($vew_start_date !=""){
                
                $ticket->display_from_time = $vew_start_date;
                $ticket->display_end_time = $vew_end_date;
                $ticket->save();
                
                $viewtick = Ticket::where("master_ticket_id",$ticket->id)->where("display_from_time",$vew_start_date)->where("display_end_time",$vew_end_date)->first();
                if(!$viewtick){ 
                    $viewtick = new Ticket();
                    $viewtick->master_ticket_id = $ticket->id;
                    $viewtick->subject_id = $ticket->subject_id;
                    $viewtick->site_id = $ticket->site_id;
                    $viewtick->content = $ticket->content;
                    $viewtick->_website_id = $ticket->_website_id;
                    $viewtick->equipment_id = $ticket->equipment_id;
                    $viewtick->view_on_date = $ticket->view_on_date;
                    $viewtick->view_on_date_val = $ticket->view_on_date_val;
                    $viewtick->view_before_date = $ticket->view_before_date;
                    $viewtick->activity_time_opt = $ticket->activity_time_opt;
                    $viewtick->view_start_date = $ticket->view_start_date;
                    $viewtick->view_end_date = $ticket->view_end_date;
                    $viewtick->user_id =0;
                    $viewtick->created_by =0;
                    $viewtick->updated_by =0;

                    $viewtick->display_from_time = $vew_start_date;
                    $viewtick->display_end_time = $vew_end_date;
                    

                    $viewtick->save();
                    
                    $activity_count ++;
                
                }
                
            }
        }

        $todayCron = Ticketviewcron::where("key","ticket_today_visibility")->first();
        if(!$todayCron){ $todayCron = new Ticketviewcron(); }

        $todayCron->key="ticket_today_visibility";
        $todayCron->updated_at = Carbon\Carbon::now();
        $todayCron->save();
        
        
        
            $filePath = public_path() . '/log';
            File::isDirectory($filePath) or File::makeDirectory($filePath, 0777, true, true);
            $filePath= $filePath."/cronlog.csv";
        
            $fp = fopen($filePath, 'a');    
            fputcsv($fp, array(Carbon\Carbon::now(),"Total Planned Ticket Added :".$activity_count));
            fclose($fp);
        }  catch (\Exception $e){
            \Mail::raw($e->getMessage(), function ($message) {
				$message->to("jignesh.citrusbug@gmail.com")
				->subject("Ticket view cron ");
			}); 
        }

    }

}
