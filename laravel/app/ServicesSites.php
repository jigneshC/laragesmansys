<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesSites extends Model
{
    //
      //
      public $timestamps = false;
      protected $connection = 'mysql' ;
      protected $table = 'services_sites';

      protected $fillable = ['services_id','sites_id'];


}
