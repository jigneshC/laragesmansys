<?php

namespace App;
use Session;
use App\Company;
use App\Companychargablemodule;
use Illuminate\Database\Eloquent\Model;

class UnitsModule extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_module';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['action', 'unit'];

    public static function getModuleChargeByComponey($website_id, $action) {
        $action_count = 0;
        $componeys = Company::where("_website_id", $website_id)->pluck("id");
        if ($componeys && $componeys->count() > 0) {
            $action_count = Companychargablemodule::whereIn("company_id",$componeys)->where("action",$action)->count();
        }
        return $action_count;
    }

    public static function getSitePerDayCharge($website)
    {
        $free_licence = \config('settings.free_license');
       
        $chargeOb = [];
        $unitcharges = UnitsModule::all();
        foreach ($unitcharges as $unitcharge) {
            if ($unitcharge->action == "license") {
                $chargeOb[$unitcharge->action] = ["type" => "license", "unit_charge_per_item" => $unitcharge->unit, 'net_unit_charge' => $unitcharge->unit, 'total_qty' => 1, 'free_qty' => 0, 'charge_qty' => 1];
            }else if (strpos($unitcharge->action, '_module') !== false) {
                $action_count = UnitsModule::getModuleChargeByComponey($website->id,$unitcharge->action);
                $chargeOb[$unitcharge->action] = ["type" => "module", "unit_charge_per_item" => $unitcharge->unit, 'net_unit_charge' => $action_count * $unitcharge->unit, 'total_qty' => $action_count, 'free_qty' => 0, 'charge_qty' => $action_count];
            } else {
                $chargeOb[$unitcharge->action] = ["type" => "item", "unit_charge_per_item" => $unitcharge->unit, 'net_unit_charge' => 0, 'total_qty' => 1, 'free_qty' => 0, 'charge_qty' => 0];
            }
        }
        
            $sitecharge = $chargeOb;
            if (isset($sitecharge['users'])) {
                $item_exist_list = \App\User::where('_website_id', $website->id)->get();
                $item_name = 'users';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }
            if (isset($sitecharge['companies'])) {
                $item_exist_list = \App\Company::where('_website_id', $website->id)->get();
                $item_name = 'companies';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }

            if (isset($sitecharge['sites'])) {
                $item_exist_list = \App\Site::where('_website_id', $website->id)->get();
                $item_name = 'sites';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }

            if (isset($sitecharge['services'])) {
                $item_exist_list = \App\Service::where('_website_id', $website->id)->get();
                $item_name = 'services';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }

            if (isset($sitecharge['buildings'])) {
                $item_exist_list = \App\Building::where('_website_id', $website->id)->get();
                $item_name = 'buildings';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }

            if (isset($sitecharge['subjects'])) {
                $item_exist_list = \App\Subject::where('_website_id', $website->id)->get();
                $item_name = 'subjects';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }

            if (isset($sitecharge['tickets'])) {
                $item_exist_list = \App\Ticket::where('_website_id', $website->id)->get();
                $item_name = 'tickets';
                $sitecharge[$item_name]['free_qty'] = (isset($free_licence[$item_name])) ? $free_licence[$item_name] : 0;
                $sitecharge[$item_name]['total_qty'] = $item_exist_list->count();
                $sitecharge[$item_name]['item_ids'] = $item_exist_list->pluck('id')->toArray();
                $sitecharge[$item_name]['charge_qty'] = max(0, $sitecharge[$item_name]['total_qty'] - $sitecharge[$item_name]['free_qty']);
                $sitecharge[$item_name]['net_unit_charge'] = $sitecharge[$item_name]['charge_qty'] * $sitecharge[$item_name]['unit_charge_per_item'];
            }
            
            return $sitecharge;
    }
    public static function calculateTotalPrice($sitecharges) {
        $total = 0;
        foreach ($sitecharges as $charge) {
            $total = $total + $charge['net_unit_charge'];
        }
        return $total;
    }
    public static function generateItemCreateNotification($model,$action) {
        if($action=="created" && !_MASTER && _ENABLE_CHARGE){
            $unitsModule = UnitsModule::where("action",$model)->first();
            if($unitsModule && _WEBSITE_ID){
                $website = WebSite::where('id',_WEBSITE_ID)->first();
                $sitecharge = UnitsModule::getSitePerDayCharge($website);
                $charge_unit_total = UnitsModule::calculateTotalPrice($sitecharge);
                
                $h2 = \Lang::get('unittransaction.notification.domin_charge_per_day',['units'=>$charge_unit_total]);
                Session::flash('session_notification_header2',$h2);
            }
        }
        
    }
    
}
