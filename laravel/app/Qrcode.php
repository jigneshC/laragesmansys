<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Qrcode extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qrcode';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'ref_field_id', 'id');
    }
	public function website()
    {
        return $this->belongsTo('App\WebSite', 'ref_field_id', 'id');
    }
	public function site()
    {
        return $this->belongsTo('App\Site', 'ref_field_id', 'id')->select("id","company_id","_website_id",'name','address','city','postcode','country','email');
    }
}
