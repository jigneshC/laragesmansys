<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpUser extends Model
{
	protected $table = 'otp_users';

    
    protected $primaryKey = 'user_id';
	
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
