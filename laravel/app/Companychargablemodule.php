<?php

namespace App;

use Faker\Provider\Base;
use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companychargablemodule extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_chargable_module';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['action','company_id'];


}
