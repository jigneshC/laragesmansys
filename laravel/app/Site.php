<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;


class Site extends BaseModel
{

    protected $connection = 'mysql' ;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sites';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', '_website_id', 'address', 'city', 'postcode', 'country', 'phone_number_1', 'phone_number_2','code_phone_number_1','code_phone_number_2'  ,'phone_type_1', 'phone_type_2', 'site_type', 'logo', 'access_conditions', 'default_email', 'guard_presence', 'digicode', 'keybox_presence', 'keybox_code', 'keybox_place', 'keybox_issue', 'known_issue', 'company_id', 'instructions_tasks_file', 'order_file', 'procedures_file', 'sitemaps_file', 'active', 'created_by', 'updated_by','email','geolocation','longitude','latitude'];


	protected $appends = ['file_url','file_thumb_url'];
	
    public function getFileUrlAttribute()
    {
		$path = "uploads";
		
		if($this->logo && \File::exists(public_path($path)."/".$this->logo)){
			return url($path)."/".$this->logo;
		}else{
			return "";
		}
	}
	public function getFileThumbUrlAttribute()
    {
		$path = "uploads/thumb";
		
		
		if($this->logo && \File::exists(public_path($path)."/".$this->logo)){
			return url($path)."/".$this->logo;
		}else{
			return "";
		}
	}
	
    public function company()
    {
        return $this->belongsTo('App\Company','company_id', 'id');
    }


    /**
     * Site  have Many Services
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services()
    {
       return $this->belongsToMany('App\Service', 'services_sites','sites_id','services_id');
       // return $this->hasMany('App\Service');
    }


    public function buildings()
    {
        return $this->hasMany('App\Building');
    }


    public function phone_type_one()
    {
        return $this->belongsTo('App\DropdownValue', 'parent_id', 'phone_type_1')->language();
    }

    public function phone_type_two()
    {
        return $this->belongsTo('App\DropdownValue', 'parent_id', 'phone_type_2')->language();
    }

    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'sites_id');
    }

}
