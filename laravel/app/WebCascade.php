<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebCascade extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_cascade';
    protected $primaryKey = 'ID';

    protected $fillable = ['ID','siteID','peopleID','subjectID'];

}
