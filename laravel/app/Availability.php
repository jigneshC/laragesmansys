<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\BaseModelTrait;
use App\BaseModel;

class Availability extends BaseModel
{

    protected $table = "people_availability";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'people_id', 'day', 'time'
    ];


    public function people()
    {
        return $this->belongsTo('App\People');
    }

}
