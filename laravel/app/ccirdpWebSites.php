<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ccirdpWebSites extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_sites';

    protected $primaryKey = 'Id';
}
