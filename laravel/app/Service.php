<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Service extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id' ,'name', '_website_id', 'description', 'manager_id', 'active', 'created_by', 'updated_by'];


    /**
     * Each Service belongs to one site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
   /* public function site()
    {
        return $this->belongsTo('App\Site');
    }*/


    /**
     * Each Service have many peoples
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function peoples()
    {
        return $this->belongsToMany('App\People')->withTimestamps();
    }

    /**
     * Each Service have many Subjects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    
    public function sites()
    {
        return $this->belongsToMany('App\Site', 'services_sites','services_id', 'sites_id');
    }
    
    public function subjects()
    {
        return $this->HasMany('App\Subject','service_id','id');
    }

    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id', 'id');
    }

    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'services_id');
    }
}
