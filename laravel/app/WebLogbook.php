<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebLogbook extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_logbook';
    protected $primaryKey = 'ID';
}
