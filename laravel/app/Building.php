<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Building extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'buildings';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','_website_id', 'address', 'site_id','geolocation','longitude','latitude','email','city','postcode','country','phone_number_1','code_phone_number_1','phone_type_1'];


    public function site()
    {
        return $this->belongsTo('App\Site');
    }


     public function sites()
    {
        return $this->HasMany('App\Site','id','site_id');
    }
    
    
    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'buildings_id');
    }
    

}
