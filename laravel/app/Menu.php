<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseModel;

class Menu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $hidden = [
        'parent_lang_id', 'parent_menu_id', 'lang_code', 'created_at', 'updated_at', 'master', 'display_order','status','id'
    ];

	public function child()
    {
        return $this->hasMany('App\Menu', 'parent_menu_id', 'parent_lang_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Menu', 'parent_menu_id', 'parent_lang_id');
    }

    public function scopeParent($query)
    {
        return $query->where('parent_menu_id', 0);
    }
}
