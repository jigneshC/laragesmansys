<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebPeoples extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_people';

    protected $primaryKey = 'ID';
    
}
