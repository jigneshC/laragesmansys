<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'equipments';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['equipment_id', '_website_id'];

    public function qrcode()
    {
        return $this->hasOne('App\Qrcode', 'ref_field_id', 'id')->where('refe_table_field_name', 'equipments_id');
    }
    
}
