<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = "languages";

    protected $fillable = [
        'lang_code', 'name', 'description', 'active', 'native_name'
    ];


    public function user()
    {
        return $this->hasMany('App\User', 'lang_code', 'language');
    }


    public function scopeActive($q)
    {
        return $q->where('active', true);
    }

    public function scopeActiveOrder($q)
    {
        return $q->orderBy('active', 'desc');
    }

}
