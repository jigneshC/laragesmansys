<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebDropdown extends Model
{
    //
    protected $connection = 'mysql2' ;
    protected $table = 'web_dropdown';

    protected $primaryKey = 'ID';
}
